# Tezos Domains Client App

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 9.1.8.

## Prerequisities

1. NodeJS (latest version 14) (https://nodejs.org/en/download/)
1. Yarn (`npm install -g yarn`)
1. Install packages (`yarn install`)
1. _(Optional)_ Install gulp (`npm i -g gulp-cli`)

## Development server

Make sure to generate `app-config.json` first by running `yarn gulp switch -e dev` (or `gulp switch -e dev` if you have `gulp` installed globally).

Generate a certificate for localhost ([follow this guide](https://medium.com/@richardr39/using-angular-cli-to-serve-over-https-locally-70dab07417c8)) and place `.crt` and `.key` files in the `/cert` folder.

Run `ng serve` for a dev server. Navigate to `https://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).
