const execSync = require('child_process').execSync;
const pkg = require('./package.json');
const fs = require('fs-extra');
const path = require('path');
const UglifyJS = require('uglify-js');
const readJson = require('read-package-json')
const { parse } = require('node-html-parser');

module.exports = async (targetOptions, indexHtml) => {
  const html = parse(indexHtml);

  const versionString = await getVersion();
  const head = html.querySelector('head');

  head.appendChild(versionString);
  head.appendChild(getGTM(targetOptions));

  return html.toString()
};

async function getVersion() {
  execSync('git config --global --add safe.directory \'*\'');

  const branch = execSync('git rev-parse --abbrev-ref HEAD').toString('utf-8').trim();
  const hash = execSync('git rev-parse HEAD').toString('utf-8').trim();
  const version = pkg.version;
  const deps = Object.keys(pkg.dependencies);
  const packages = {};

  for await (const packName of deps) {
    const file = `${packName}/package.json`;
    try {
      packages[packName] = await getPackageVersion(file);
    } catch (ex) {
      console.error(`Exception when generating index.html from index-html.transform.js - reading file: ${file}`, ex)
    }
  }

  const metadata = JSON.stringify({
    app: {
      branch,
      hash,
      version,
    },
    dependencies: packages,
  });

  return parse(`<script>window.VERSION = ${metadata};</script>`);
}

async function getPackageVersion(file) {
  return new Promise((resolve, reject) => {
    readJson('node_modules/' + file, () => { }, false, function (er, data) {
      if (er) {
        console.error("There was an error reading the file", er)
        reject();
        return
      }

      resolve(data.version)
    });
  })
}

function getGTM(targetOptions) {
  const file = fs.readFileSync(path.join(__dirname, 'src/gtm.js'), { encoding: 'utf-8' });
  const config = getGTMConfig(targetOptions);
  return parse(`<script async src="https://www.googletagmanager.com/gtag/js?id=${config.id}"></script><script>${UglifyJS.minify(file).code.replace(
    '__CONFIG__',
    JSON.stringify(config)
  )}</script>`);
}

function getGTMConfig(targetOptions) {

  if (targetOptions.target === 'build' && targetOptions.configuration === 'production') {
    return { id: 'G-NSXTMDWQNT', allowedHosts: ['app.tezos.domains'] };
  }

  return { id: 'G-JV39TB1V0M', allowedHosts: ['localhost'] };
}

