const webpack = require('webpack');

module.exports = (config, options, targetOptions) => {
    config.plugins.push(new webpack.ContextReplacementPlugin(/dayjs[\/]locale/, /(en|zh|pt-br|cs|de|it|es|ru|fr|ja|tr|id|ko).js/));
    config.plugins.push(new webpack.IgnorePlugin({ contextRegExp: /^\.\/wordlists\/.*?.json$/, resourceRegExp: /bip39/ }));
    config.plugins.push(new webpack.ProvidePlugin({
        Buffer: ['buffer', 'Buffer'],
        process: 'process/browser',
        url: 'url',
    }));

    return config;
};
