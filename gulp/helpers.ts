import { spawn as cpSpawn } from 'child_process';

export function spawn(command: string, ...params: string[]) {
    const p = cpSpawn(command, params, { shell: true });

    p.stderr.pipe(process.stderr);
    p.stdout.pipe(process.stdout);

    return p;
}

export function spawnNgCli(...params: string[]) {
    return spawn('yarn ng', ...params);
}
