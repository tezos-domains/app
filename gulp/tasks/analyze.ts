import * as gulp from 'gulp';

import { args } from '../config';
import { spawnNgCli, spawn } from '../helpers';

function buildProdWithStats() {
    return spawnNgCli('build', '--configuration', 'production', '--stats-json');
}

function analyze() {
    return spawn('.\\node_modules\\.bin\\webpack-bundle-analyzer', 'dist/stats.json');
}

if (args.build === false) {
    gulp.task('analyze', analyze);
} else {
    gulp.task('analyze', gulp.series(buildProdWithStats, analyze));
}
