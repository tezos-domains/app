import * as gulp from 'gulp';
import * as fs from 'fs-extra';
import * as PluginError from 'plugin-error';
import { merge, forOwn } from 'lodash';

import { config, args } from '../config';

async function readConfig(env: string) {
    return await fs.readJSON(`config/app-config.${env ? `${env}.` : ''}json`, {});
}

async function writeConfig(target: string, content: any) {
    await fs.writeJSON(target, content, { spaces: 2 });
}

async function makeConfig(env: string) {
    const defaultConfig = await readConfig('default');
    const envConfig = await readConfig(env);

    const config = merge({}, defaultConfig, envConfig);

    const variables: Record<string, string> = {
        env: env,
        network: config.network.name,
        'network-short': config.network.name.replace(/net$/, ''),
    };

    return replaceProperties(config, value => {
        if (typeof value === 'string') {
            return replacePlaceholders(value, variables);
        } else {
            return value;
        }
    });
}

async function switchConfig() {
    const env = args['e'];

    if (!config.envs.includes(env)) {
        throw new PluginError('switch', `Invalid environment. Following values are supported: ${config.envs.join()}`);
    }

    await writeConfig('src/app-config.json', await makeConfig(env));
}

async function generateConfigs() {
    await fs.ensureDir('dist/config');

    for (const env of config.envs) {
        await writeConfig(`dist/config/app-config.${env}.json`, await makeConfig(env));
    }
}

async function installConfig() {
    const env = args['e'];

    if (!config.envs.includes(env)) {
        throw new PluginError('install-config', `Invalid environment. Following values are supported: ${config.envs.join()}`);
    }

    await writeConfig('dist/app-config.json', await fs.readJSON(`dist/config/app-config.${env}.json`));
}

function replacePlaceholders(value: string, placeholders: Record<string, string>) {
    return value.replace(/{{\s*([\w-:;]+)\s*}}/g, (_, placeholder: string) => {
        const variable = placeholder
            .split(';')
            .map(command => {
                const [a, b, c] = command.split(':');
                if (!b) {
                    return { variable: a };
                } else {
                    return { variable: c, overrideProperty: a, overrideValue: b };
                }
            })
            .reduce((current, next) => {
                if (!current) {
                    return next.variable;
                } else if (next.overrideProperty && placeholders[next.overrideProperty] === next.overrideValue) {
                    return next.variable;
                } else {
                    return current;
                }
            }, '');

        return placeholders[variable] || variable;
    });
}

function replaceProperties(value: any, replacer: (value: any) => any): any {
    if (Array.isArray(value)) {
        const newArray = [];
        for (const item of value) {
            newArray.push(replaceProperties(item, replacer));
        }

        return newArray;
    } else if (typeof value === 'object') {
        const newObj: any = {};

        forOwn(value, (value, key) => {
            newObj[key] = replaceProperties(value, replacer);
        });

        return newObj;
    } else {
        return replacer(value);
    }
}

gulp.task('switch', switchConfig);
gulp.task('generate-configs', generateConfigs);
gulp.task('install-config', installConfig);
