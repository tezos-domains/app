import * as minimist from 'minimist';

export const args = minimist(process.argv.slice(2));

export const config = {
    envs: ['dev', 'ghost', 'staging', 'prod'],
};
