declare module '@ledgerhq/hw-transport-webhid' {
    /// <reference types="node" />
    import Transport from '@ledgerhq/hw-transport';

    export class TransportWebHID extends Transport {
        constructor();
        static open(_: any, _openTimeout?: number): Promise<TransportWebHID>;
    }

    export default TransportWebHID;
}
