declare module '@download/blockies' {
    export function createIcon(options?: {
        size?: number;
        scale?: number;
        color?: string;
        bgcolor?: string;
        spotcolor?: string;
        seed?: string;
    }): HTMLCanvasElement;
}
