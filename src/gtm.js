(function (config) {
    var generateGUID =
        typeof window.crypto != 'undefined' && typeof window.crypto.getRandomValues != 'undefined'
            ? function () {
                  // If we have a cryptographically secure PRNG, use that
                  // https://stackoverflow.com/questions/6906916/collisions-when-generating-uuids-in-javascript
                  var buf = new Uint16Array(8);
                  window.crypto.getRandomValues(buf);
                  var S4 = function (num) {
                      var ret = num.toString(16);
                      while (ret.length < 4) {
                          ret = '0' + ret;
                      }
                      return ret;
                  };
                  return S4(buf[0]) + S4(buf[1]) + '-' + S4(buf[2]) + '-' + S4(buf[3]) + '-' + S4(buf[4]) + '-' + S4(buf[5]) + S4(buf[6]) + S4(buf[7]);
              }
            : function () {
                  // Otherwise, just use Math.random
                  // https://stackoverflow.com/questions/105034/how-to-create-a-guid-uuid-in-javascript/2117523#2117523
                  return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
                      var r = (Math.random() * 16) | 0,
                          v = c == 'x' ? r : (r & 0x3) | 0x8;
                      return v.toString(16);
                  });
              };

    var host = window.location.hostname;

    if (config.id && config.allowedHosts && config.allowedHosts.indexOf(host) !== -1) {
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }

        var KEY = 'gtm-client-id';
        var clientId;
        if (window.localStorage) {
            clientId = window.localStorage.getItem(KEY);

            if (!clientId) {
                clientId = generateGUID();

                window.localStorage.setItem(KEY, clientId);
            }
        } else {
            clientId = generateGUID();
        }

        gtag('js', new Date());
        gtag('config', config.id, {
            client_storage: 'none',
            client_id: clientId,
            anonymize_ip: true,
        });
    }
})(__CONFIG__);
