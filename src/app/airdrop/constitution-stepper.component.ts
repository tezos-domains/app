import { CdkStepper } from '@angular/cdk/stepper';
import { Component, EventEmitter, inject, Output } from '@angular/core';
import { OverlayService } from '../overlay.service';

@Component({
    selector: 'td-constitution-stepper',
    templateUrl: './constitution-stepper.component.html',
    styleUrls: ['./constitution-stepper.component.scss'],
    providers: [{ provide: CdkStepper, useExisting: ConstitutionStepperComponent }],
})
export class ConstitutionStepperComponent extends CdkStepper {
    @Output() approvalChange = new EventEmitter<boolean>();
    @Output() exit = new EventEmitter<void>();

    private overlayService = inject(OverlayService);

    get progressValue(): number {
        if (this.selectedIndex < 1) {
            return 0;
        }

        return this.selectedIndex * 25;
    }

    reject() {
        this.overlayService.openRejectWarning(rejected => {
            if (rejected) {
                this.reset();
                this.approvalChange.next(false);
            }
        });
    }

    onPrevStep() {
        const oldSelectedIdx = this.selectedIndex;
        this.previous();

        if (oldSelectedIdx === this.selectedIndex && this.selectedIndex === 0) {
            this.exit.next();
        }
    }

    onNextStep() {
        const oldSelectedIdx = this.selectedIndex;
        this.next();

        if (oldSelectedIdx === this.selectedIndex && this.selectedIndex === this.steps.length - 1) {
            this.approvalChange.next(true);
        }
    }
}
