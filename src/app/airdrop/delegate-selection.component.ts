import { Component, EventEmitter, Input, Output } from '@angular/core';
import { AirdropClaim } from './models';
import { ReverseRecord } from '../delegation/models';

@Component({
    selector: 'td-delegate-selection',
    templateUrl: './delegate-selection.component.html',
    styleUrls: ['./delegate-selection.component.scss'],
})
export class DelegateSelectionComponent {
    @Input() claim: AirdropClaim;
    @Input() selected?: ReverseRecord | null;
    @Input() disabled = false;

    @Output() clear = new EventEmitter<void>();
}
