import { HttpClientTestingModule, HttpTestingController, TestRequest } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';

import BigNumber from 'bignumber.js';
import { TestConfig, TestWallet, TezosAddress } from '../../test/data';
import { Configuration } from '../configuration';
import { AirdropFileService } from './airdrop-file.service';
import { AirdropClaimFile } from './models';

describe('AirdropFileService', () => {
    let service: AirdropFileService;
    let httpMock: HttpTestingController;

    const response = [
        <AirdropClaimFile>{
            t: 25,
            o: TestWallet.address,
            m: { h: 12, f: 13, ms: new Date(2021, 3, 4).toISOString(), d: 'domain1.tez', td: new Date(2028, 1, 1) },
            v: [
                {
                    fr: new Date(2021, 1, 2).toISOString(),
                    proof: [{ left: 'left' }],
                    ad: true,
                    t: 25,
                },
            ],
        },
    ];

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [HttpClientTestingModule],
            providers: [AirdropFileService, { provide: Configuration, useValue: TestConfig }],
        });

        service = TestBed.inject(AirdropFileService);
        httpMock = TestBed.inject(HttpTestingController);
    });

    let spy: jasmine.Spy;
    let request: TestRequest;

    beforeEach(() => {
        spy = jasmine.createSpy('load');
        service.load(TezosAddress).subscribe(spy);

        request = httpMock.expectOne(`${TestConfig.storageUrl}/claims-folder/${TezosAddress}.json`);
    });

    xit('should return data from storage file', () => {
        expect(request.request.method).toBe('GET');

        request.flush(response);

        expect(spy).toHaveBeenCalledOnceWith({
            airdrop: true,
            amount: BigNumber(25),
            proof: [{ left: 'left' }],
            from: new Date(2021, 1, 2),
            owner: TezosAddress,
            meta: {
                historicalAmount: BigNumber(12),
                futureRegistrationAmount: BigNumber(13),
                memberSince: new Date(2021, 3, 4),
                domain: 'domain1.tez',
            },
        });
    });

    it('should return empty if file not found', () => {
        request.error(new ProgressEvent('oh no'));

        expect(spy).toHaveBeenCalledOnceWith({ owner: TestWallet.address, claims: [] });
    });

    afterEach(() => {
        httpMock.verify();
    });
});
