import { PoolStats } from '@/gv-pool/models';
import { PoolStatsService } from '@/gv-pool/pool-stats.service';
import { fakeAsync, tick } from '@angular/core/testing';
import { TestConfig, TestWallet, TezosAddress, mockNow, resetNow } from '@td/test';
import BigNumber from 'bignumber.js';
import dayjs from 'dayjs';
import { cloneDeep } from 'lodash-es';
import { of } from 'rxjs';
import { anyString, instance, mock, verify, when } from 'ts-mockito';
import { Configuration } from '../configuration';
import { AirdropFileService } from './airdrop-file.service';
import { ClaimInfoService } from './claim-info.service';
import { ClaimFileData, ClaimStatus, EmptyClaimMeta } from './models';

describe('ClaimInfoService', () => {
    let service: ClaimInfoService;
    let airdropFileMock: AirdropFileService;
    let poolStatsMock: PoolStatsService;
    let config: Configuration;

    beforeEach(() => {
        poolStatsMock = mock(PoolStatsService);
        airdropFileMock = mock(AirdropFileService);
        config = cloneDeep(TestConfig);

        service = new ClaimInfoService(instance(airdropFileMock), instance(poolStatsMock), config);
    });

    describe('canClaimTokens()', () => {
        describe('when airdrop not in progress', () => {
            let spy: jasmine.Spy;

            beforeEach(() => {
                spy = jasmine.createSpy('vm$.subscribe');
            });

            afterEach(() => {
                resetNow();
            });

            it('should return false if before start date', () => {
                mockNow(dayjs('2020-01-01T00:00:00Z'));
                config.airdrop.start = new Date(2022, 10, 1);
                config.airdrop.end = new Date(2023, 10, 1);

                service.canClaimTokens(TezosAddress).subscribe(spy);

                expect(spy).toHaveBeenCalledOnceWith(jasmine.objectContaining({ status: ClaimStatus.NoClaim }));
            });

            it('should return false if after end date', () => {
                mockNow(dayjs('2024-01-01T00:00:00Z'));
                config.airdrop.start = new Date(2022, 10, 1);
                config.airdrop.end = new Date(2023, 10, 1);

                service.canClaimTokens(TezosAddress).subscribe(spy);

                expect(spy).toHaveBeenCalledOnceWith(jasmine.objectContaining({ status: ClaimStatus.AirdropEnded }));
            });
        });

        describe('when file present', () => {
            beforeEach(() => {
                fileResolves({
                    owner: TestWallet.address,
                    claims: [
                        {
                            amount: new BigNumber(21),
                            from: dayjs(new Date(2021, 1, 1)),
                            proof: [],
                            owner: TestWallet.address,
                        },
                    ],
                    meta: {
                        ...EmptyClaimMeta,
                        domain: '',
                        memberSince: dayjs(),
                        historicalAmount: new BigNumber(2),
                        futureRegistrationAmount: new BigNumber(2),
                    },
                });
            });

            it('should return Unclaimed status', fakeAsync(() => {
                when(poolStatsMock.stats$).thenReturn(of(<PoolStats>{ owner: TestWallet.address, claims: <any>[] }));
                const spy = jasmine.createSpy('canClaimTokensCallback');

                service.canClaimTokens(TestWallet.address).subscribe(spy);
                tick();

                expect(spy).toHaveBeenCalledOnceWith(jasmine.objectContaining({ status: ClaimStatus.Unclaimed }));
            }));

            it('should return Claimed status', fakeAsync(() => {
                when(poolStatsMock.stats$).thenReturn(
                    of({
                        owner: TestWallet.address,
                        claims: [{ amount: new BigNumber(21), from: dayjs(new Date(2021, 1, 1)), proof: [], owner: TestWallet.address }],
                    } as unknown as PoolStats)
                );

                const spy = jasmine.createSpy('canClaimTokensCallback');

                service.canClaimTokens(TestWallet.address).subscribe(spy);
                tick();

                expect(spy).toHaveBeenCalledOnceWith(jasmine.objectContaining({ status: ClaimStatus.Claimed }));
            }));

            it('should return claim status from cache', fakeAsync(() => {
                when(poolStatsMock.stats$).thenReturn(of(<PoolStats>{ owner: TestWallet.address, claims: <any>[] }));

                const spy = jasmine.createSpy('canClaimTokensCallback');

                service.canClaimTokens(TestWallet.address).subscribe(spy);
                tick();

                spy.calls.reset();

                service.canClaimTokens(TestWallet.address).subscribe(spy);
                tick();

                expect(spy).toHaveBeenCalledOnceWith(jasmine.objectContaining({ status: ClaimStatus.Unclaimed }));
                verify(airdropFileMock.load(anyString())).atMost(1);
            }));
        });

        describe('when file not found', () => {
            it('should return claim status NoClaim', fakeAsync(() => {
                fileResolves({
                    owner: TestWallet.address,
                    claims: [],
                });
                when(poolStatsMock.stats$).thenReturn(of(<PoolStats>{ owner: TestWallet.address, claims: <any>[] }));

                const spy = jasmine.createSpy('canClaimTokensCallback');

                service.canClaimTokens(TestWallet.address).subscribe(spy);
                tick();

                expect(spy).toHaveBeenCalledOnceWith(jasmine.objectContaining({ status: ClaimStatus.NoClaim }));
                verify(airdropFileMock.load(anyString())).atMost(1);
            }));
        });
    });

    function fileResolves(data: ClaimFileData): void {
        when(airdropFileMock.load(anyString())).thenReturn(of(data));
    }
});
