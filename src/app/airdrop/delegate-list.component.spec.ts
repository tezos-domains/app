import { fakeAsync } from '@angular/core/testing';
import { TranslocoModule } from '@ngneat/transloco';
import { TestConfig, provider, transloco } from '@td/test';
import { getByText } from '@testing-library/angular';
import BigNumber from 'bignumber.js';
import { MockBuilder, MockRender, MockedComponentFixture, ngMocks } from 'ng-mocks';
import { of } from 'rxjs';
import { mock, when } from 'ts-mockito';
import { Configuration } from '../configuration';
import { PoolStatsService } from '../gv-pool/pool-stats.service';
import { LocaleService } from '../i18n/locale.service';
import { FormatTedPipe } from '../shared/format-ted.pipe';
import { FromMutezPipe } from '../shared/from-mutez.pipe';
import { SharedModule } from '../shared/shared.module';
import { AirdropModule } from './airdrop.module';
import { DelegateListComponent } from './delegate-list.component';

describe('DelegateListComponent', () => {
    let component: DelegateListComponent;
    let fixture: MockedComponentFixture<DelegateListComponent, Partial<DelegateListComponent>>;
    let poolStatisticsMock: PoolStatsService;
    let localeServiceMock: LocaleService;

    beforeEach(() => {
        poolStatisticsMock = mock(PoolStatsService);
        localeServiceMock = mock(LocaleService);

        when(localeServiceMock.getLocale()).thenReturn('en-US');
        when(poolStatisticsMock.stats$).thenReturn(of(null));

        return MockBuilder(DelegateListComponent, AirdropModule)
            .mock(SharedModule)
            .provide([provider(poolStatisticsMock), provider(localeServiceMock), { provide: Configuration, useValue: TestConfig }])
            .keep(FromMutezPipe)
            .keep(FormatTedPipe)
            .keep(TranslocoModule, { export: true })
            .keep(transloco(), { export: true });
    });

    it('should output on selection', fakeAsync(() => {
        init();
        spyOn(component.delegateSelected, 'next');

        component.selected = <any>{};
        ngMocks.click(getByText(fixture.nativeElement, 'Claim 10 TED'));

        expect(component.delegateSelected.next).toHaveBeenCalledOnceWith({ wantsToDepositVotes: true, wantsToDelegate: true, delegate: <any>{} });
    }));

    function init() {
        fixture = MockRender(DelegateListComponent, {
            tokenAmount: BigNumber(10 * 1e6),
        });
        component = fixture.point.componentInstance;
        fixture.detectChanges();
    }
});
