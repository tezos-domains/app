import { Component, EventEmitter, Output } from '@angular/core';

@Component({
    selector: 'td-claim-constitution',
    templateUrl: './claim-constitution.component.html',
    styleUrls: ['./claim-constitution.component.scss'],
})
export class ClaimConstitutionComponent {
    @Output() approvalChange = new EventEmitter<boolean>();
    @Output() back = new EventEmitter<void>();
}
