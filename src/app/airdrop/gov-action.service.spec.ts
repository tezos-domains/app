import { fakeAsync, tick } from '@angular/core/testing';
import { ContractAbstraction, ContractMethod, OpKind, TezosToolkit, Wallet, WalletOperationBatch } from '@taquito/taquito';
import BigNumber from 'bignumber.js';
import { Observable, Subject, of } from 'rxjs';
import { anything, capture, instance, mock, when } from 'ts-mockito';
import { TezosNetwork } from '../tezos/models';
import { TezosNetworkService } from '../tezos/tezos-network.service';
import { SmartContractOperationEvent, TezosService } from '../tezos/tezos.service';
import { GovActionImpl } from './gov-action.service';
import dayjs from 'dayjs';

describe('GovActionService', () => {
    let tezosServiceMock: TezosService;
    let tezosNetworkMock: TezosNetworkService;
    let operation: Observable<SmartContractOperationEvent>;
    let tezosToolkitMock: TezosToolkit;
    let walletMock: Wallet;
    let votesContractMock: ContractAbstraction<Wallet>;
    let vestingContractMock: ContractAbstraction<Wallet>;
    let tokenContractMock: ContractAbstraction<Wallet>;
    let poolContractMock: ContractAbstraction<Wallet>;
    let setDelegateMethodMock: ContractMethod<Wallet>;
    let updateOperatorsMethodMock: ContractMethod<Wallet>;
    let withdrawMethodMock: ContractMethod<Wallet>;
    let depositMethodMock: ContractMethod<Wallet>;
    let claimMethodMock: ContractMethod<Wallet>;
    let walletOperationMock: WalletOperationBatch;
    let service: GovActionImpl;
    let setDelegateSpy: jasmine.Spy;
    let updateOperatorsSpy: jasmine.Spy;
    let withdrawSpy: jasmine.Spy;
    let depositSpy: jasmine.Spy;
    let claimSpy: jasmine.Spy;

    const network: Readonly<TezosNetwork> = <TezosNetwork>{ tedVotesContract: 'KT1xxx', tedPoolContract: 'KT1yyy', tedTokenContract: 'KT1zzz' };
    const dummySetDelegateTransferParams = { to: 'set_delegate', amount: 0 };
    const dummyWitdrawTransferParams = { to: 'withdraw', amount: 0 };
    const dummyDepositTransferParams = { to: 'deposit', amount: 0 };
    const dummyUpdateOperatorsTransferParams = { to: 'update_operators', amount: 0 };
    const dummyClaimTransferParams = { to: 'claim', amount: 0 };

    beforeEach(() => {
        tezosServiceMock = mock(TezosService);
        tezosNetworkMock = mock(TezosNetworkService);

        when(tezosNetworkMock.activeNetwork).thenReturn(of(network));

        operation = new Subject();

        walletMock = mock(Wallet);
        votesContractMock = mock(ContractAbstraction);
        vestingContractMock = mock(ContractAbstraction);
        tokenContractMock = mock(ContractAbstraction);
        poolContractMock = mock(ContractAbstraction);
        setDelegateMethodMock = mock(ContractMethod);
        withdrawMethodMock = mock(ContractMethod);
        depositMethodMock = mock(ContractMethod);
        claimMethodMock = mock(ContractMethod);
        updateOperatorsMethodMock = mock(ContractMethod);
        walletOperationMock = mock(WalletOperationBatch);

        when(setDelegateMethodMock.toTransferParams(anything())).thenReturn(dummySetDelegateTransferParams);
        when(withdrawMethodMock.toTransferParams(anything())).thenReturn(dummyWitdrawTransferParams);
        when(depositMethodMock.toTransferParams(anything())).thenReturn(dummyDepositTransferParams);
        when(updateOperatorsMethodMock.toTransferParams(anything())).thenReturn(dummyUpdateOperatorsTransferParams);
        when(claimMethodMock.toTransferParams(anything())).thenReturn(dummyClaimTransferParams);

        setDelegateSpy = jasmine.createSpy('set_delegate_spy').and.returnValue(instance(setDelegateMethodMock));
        updateOperatorsSpy = jasmine.createSpy('update_operators_spy').and.returnValue(instance(updateOperatorsMethodMock));
        withdrawSpy = jasmine.createSpy('withdraw_spy').and.returnValue(instance(withdrawMethodMock));
        depositSpy = jasmine.createSpy('deposit_spy').and.returnValue(instance(depositMethodMock));
        claimSpy = jasmine.createSpy('claim_spy').and.returnValue(instance(claimMethodMock));

        when(walletMock.batch(anything())).thenReturn(instance(walletOperationMock));

        when(tokenContractMock.methods).thenReturn({ update_operators: updateOperatorsSpy });
        when(tokenContractMock.address).thenReturn(network.tedTokenContract);

        when(votesContractMock.methods).thenReturn({ set_delegate: setDelegateSpy, update_operators: updateOperatorsSpy });
        when(votesContractMock.address).thenReturn(network.tedVotesContract);

        when(poolContractMock.methods).thenReturn({ withdraw: withdrawSpy, deposit: depositSpy });
        when(poolContractMock.address).thenReturn(network.tedPoolContract);

        when(vestingContractMock.methods).thenReturn({ claim: claimSpy });
        when(vestingContractMock.address).thenReturn(network.vestingContract);

        tezosToolkitMock = mock(TezosToolkit);

        when(tezosServiceMock.execute(anything(), anything())).thenCall(fn => {
            fn(undefined, instance(tezosToolkitMock));
            return operation;
        });

        when(tezosToolkitMock.wallet).thenReturn(instance(walletMock));

        service = new GovActionImpl(instance(tezosNetworkMock), instance(tezosServiceMock));
    });

    it('should execute set_delegate call', fakeAsync(() => {
        when(walletMock.at(network.tedVotesContract)).thenResolve(instance(votesContractMock));

        service.delegate('tz1xxx').send().subscribe();
        tick();

        expect(setDelegateSpy).toHaveBeenCalledWith('tz1xxx');

        const [firstArg] = capture(walletMock.batch).first();
        expect(firstArg).toEqual([{ kind: OpKind.TRANSACTION, ...dummySetDelegateTransferParams }]);
    }));

    it('should execute witdraw_call', fakeAsync(() => {
        when(walletMock.at(network.tedVotesContract)).thenResolve(instance(votesContractMock));
        when(walletMock.at(network.tedPoolContract)).thenResolve(instance(poolContractMock));

        const amount = BigNumber(10);

        service.withdraw(amount, 'tz1xxx').send().subscribe();
        tick();

        expect(withdrawSpy).toHaveBeenCalledWith(amount);
        expect(updateOperatorsSpy).toHaveBeenCalledTimes(2);
        expect(updateOperatorsSpy).toHaveBeenCalledWith([{ add_operator: { owner: 'tz1xxx', operator: network.tedPoolContract, token_id: 0 } }]);
        expect(updateOperatorsSpy).toHaveBeenCalledWith([{ remove_operator: { owner: 'tz1xxx', operator: network.tedPoolContract, token_id: 0 } }]);

        const [firstArg] = capture(walletMock.batch).first();
        expect(firstArg).toEqual([
            { kind: OpKind.TRANSACTION, ...dummyUpdateOperatorsTransferParams },
            { kind: OpKind.TRANSACTION, ...dummyWitdrawTransferParams },
            { kind: OpKind.TRANSACTION, ...dummyUpdateOperatorsTransferParams },
        ]);
    }));

    it('should execute deposit_call', fakeAsync(() => {
        when(walletMock.at(network.tedVotesContract)).thenThrow(new Error('should not be called'));
        when(walletMock.at(network.tedTokenContract)).thenResolve(instance(tokenContractMock));
        when(walletMock.at(network.tedPoolContract)).thenResolve(instance(poolContractMock));

        const amount = BigNumber(10);

        service.deposit(amount, 'tz1xxx').send().subscribe();
        tick();

        expect(depositSpy).toHaveBeenCalledWith(amount);
        expect(updateOperatorsSpy).toHaveBeenCalledTimes(2);
        expect(updateOperatorsSpy).toHaveBeenCalledWith([{ add_operator: { owner: 'tz1xxx', operator: network.tedPoolContract, token_id: 0 } }]);
        expect(updateOperatorsSpy).toHaveBeenCalledWith([{ remove_operator: { owner: 'tz1xxx', operator: network.tedPoolContract, token_id: 0 } }]);

        const [firstArg] = capture(walletMock.batch).first();
        expect(firstArg).toEqual([
            { kind: OpKind.TRANSACTION, ...dummyUpdateOperatorsTransferParams },
            { kind: OpKind.TRANSACTION, ...dummyDepositTransferParams },
            { kind: OpKind.TRANSACTION, ...dummyUpdateOperatorsTransferParams },
        ]);
    }));

    it('should execute claim_call', fakeAsync(() => {
        when(walletMock.at(network.vestingContract)).thenResolve(instance(vestingContractMock));

        const amount = BigNumber(10);
        const from = dayjs();

        service
            .claim({ amount, from, proof: [{ left: 'hash' }], owner: 'tz1xxx' })
            .send()
            .subscribe();

        tick();

        expect(claimSpy).toHaveBeenCalledWith(amount, from, true, [{ left: 'hash' }]);

        const [firstArg] = capture(walletMock.batch).first();
        expect(firstArg).toEqual([{ kind: OpKind.TRANSACTION, ...dummyClaimTransferParams }]);
    }));
});
