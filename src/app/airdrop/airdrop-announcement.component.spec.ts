import { TranslocoModule } from '@ngneat/transloco';
import { providers, TestWallet, transloco } from '@td/test';
import BigNumber from 'bignumber.js';
import { MockBuilder, MockedComponentFixture, MockRender } from 'ng-mocks';
import { of } from 'rxjs';
import { mock, when } from 'ts-mockito';
import { TezosService } from '../tezos/tezos.service';
import { AirdropAnnouncementComponent } from './airdrop-announcement.component';
import { AirdropModule } from './airdrop.module';
import { ClaimInfoService } from './claim-info.service';
import { AirdropClaimState, ClaimStatus, EmptyClaimMeta } from './models';

describe('AirdropAnnouncementComponent', () => {
    let fixture: MockedComponentFixture<AirdropAnnouncementComponent, Partial<AirdropAnnouncementComponent>>;
    let component: AirdropAnnouncementComponent;

    let vestingMock: ClaimInfoService;
    let tezosMock: TezosService;
    let spy: jasmine.Spy;

    beforeEach(() => {
        vestingMock = mock(ClaimInfoService);
        tezosMock = mock(TezosService);

        when(tezosMock.activeWallet).thenReturn(of(TestWallet));

        spy = jasmine.createSpy('vm$.subscribe');

        return MockBuilder(AirdropAnnouncementComponent, AirdropModule)
            .keep(TranslocoModule, { export: true })
            .keep(transloco(), { export: true })
            .provide([...providers(vestingMock, tezosMock)]);
    });

    it('should return false if no wallet', () => {
        when(tezosMock.activeWallet).thenReturn(of(null));
        init();

        component.vm$.subscribe(spy);

        expect(spy).toHaveBeenCalledOnceWith(false);
    });

    it('should return false if cannot claim airdrop', () => {
        when(tezosMock.activeWallet).thenReturn(of(TestWallet));
        when(vestingMock.canClaimTokens(TestWallet.address)).thenReturn(of(mockClaimState(ClaimStatus.NoClaim)));
        init();

        component.vm$.subscribe(spy);

        expect(spy).toHaveBeenCalledOnceWith(false);
    });

    it('should return true if can claim airdrop', () => {
        when(tezosMock.activeWallet).thenReturn(of(TestWallet));
        when(vestingMock.canClaimTokens(TestWallet.address)).thenReturn(of(mockClaimState(ClaimStatus.Unclaimed)));
        init();

        component.vm$.subscribe(spy);

        expect(spy).toHaveBeenCalledOnceWith(true);
    });

    function init() {
        fixture = MockRender(AirdropAnnouncementComponent);
        component = fixture.point.componentInstance;
        fixture.detectChanges();
    }

    function mockClaimState(status: ClaimStatus): AirdropClaimState {
        return {
            status,
            meta: EmptyClaimMeta,
            owner: TestWallet.address,
            claimableClaims: [],
            futureClaims: [],
            amountClaimed: BigNumber(0),
            amountLocked: BigNumber(0),
            amountClaimable: BigNumber(0),
            totalAmount: BigNumber(0),
            totalClaims: 0,
            hasOwnershipClaimsBlocked: false,
        };
    }
});
