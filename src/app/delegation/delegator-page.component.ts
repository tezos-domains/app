import { Component } from '@angular/core';

@Component({
    selector: 'td-delegator-page',
    templateUrl: './delegator-page.component.html',
    styleUrls: ['./delegator-page.component.scss'],
})
export class DelegatorListComponent {}
