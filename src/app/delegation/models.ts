import { DelegateQuery } from '../governance-core/governance-graphql.generated';
import { ReverseRecordListQuery } from '../graphql/graphql.generated';
import { Unarray } from '../utils/types';

export type ReverseRecord = Unarray<ReverseRecordListQuery['reverseRecords']['edges']>['node'];
export type Delegate = Unarray<DelegateQuery['delegates']>;

export interface DelegationStatus {
    address: string;
    votingPower: number;
    reverseRecord: ReverseRecord | null;
}
