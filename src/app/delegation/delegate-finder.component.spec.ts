import { fakeAsync, tick } from '@angular/core/testing';
import { FormBuilder, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { StandardRecordMetadataKey } from '@tezos-domains/core';

import { MatProgressSpinner } from '@angular/material/progress-spinner';
import { TranslocoModule } from '@ngneat/transloco';
import { providers, transloco } from '@td/test';
import { MockBuilder, MockRender, MockedComponentFixture, ngMocks } from 'ng-mocks';
import { of } from 'rxjs';
import { delay } from 'rxjs/operators';
import { anything, mock, verify, when } from 'ts-mockito';
import { RecipientComponent } from '../shared/recipient.component';
import { SharedModule } from '../shared/shared.module';
import { EmptyProfile, TzprofilesService } from '../tzprofiles/tzprofiles.service';
import { DelegateFinderComponent } from './delegate-finder.component';
import { DelegatesFinderService } from './delegate-finder.service';
import { DelegateItemComponent } from './delegate-item.component';
import { DelegationModule } from './delegation.module';
import { Delegate } from './models';

describe('DelegateFinderComponent', () => {
    let component: DelegateFinderComponent;
    let fixture: MockedComponentFixture<DelegateFinderComponent, Partial<DelegateFinderComponent>>;
    let tzprofilesServiceMock: TzprofilesService;
    let delegatesFinderServiceMock: DelegatesFinderService;
    let delegateSelectedSpy: jasmine.Spy;

    beforeEach(() => {
        tzprofilesServiceMock = mock(TzprofilesService);
        delegatesFinderServiceMock = mock(DelegatesFinderService);
        delegateSelectedSpy = jasmine.createSpy('delegateSelectedSpy');

        when(delegatesFinderServiceMock.loadReverseRecords(anything())).thenReturn(
            of([
                {
                    id: '1',
                    address: 'tz1xxx',
                    owner: 'tz1xxx',
                    domain: { id: '', name: '', data: [{ id: '', key: StandardRecordMetadataKey.GOVERNANCE_PROFILE_URL, rawValue: '...' }] },
                },
                {
                    id: '2',
                    address: 'tz1yyy',
                    owner: 'tz1yyy',
                    domain: { id: '', name: '', data: [{ id: '', key: StandardRecordMetadataKey.GOVERNANCE_PROFILE_URL, rawValue: '...' }] },
                },
                { id: '3', address: 'tz1zzz', owner: 'tz1zzz' },
            ])
        );
        when(delegatesFinderServiceMock.loadTopPublicDelegates()).thenReturn(of(indexerDelegates()).pipe(delay(10)));
        when(tzprofilesServiceMock.getProfiles(anything())).thenReturn(
            of({
                tz1xxx: { account: 'tz1xxx', verified: [] },
                tz1yyy: { account: 'tz1yyy', verified: [] },
            })
        );

        return MockBuilder(DelegateFinderComponent, DelegationModule)
            .mock(SharedModule)
            .provide(providers(tzprofilesServiceMock, delegatesFinderServiceMock))
            .keep(RecipientComponent)
            .keep(ReactiveFormsModule)
            .keep(FormsModule)
            .keep(FormBuilder, { export: true })
            .keep(TranslocoModule, { export: true })
            .keep(transloco(), { export: true });
    });

    it('should not have a delegate selected', fakeAsync(() => {
        init();
        expect(component.selected).toBeNull();
    }));

    it('should show loading indicator', fakeAsync(() => {
        init(5);

        let loadingIndicator = ngMocks.findInstances(fixture.point, MatProgressSpinner);
        expect(loadingIndicator.length).toBe(1);

        tick(5);
        fixture.detectChanges();

        loadingIndicator = ngMocks.findInstances(fixture.point, MatProgressSpinner);
        expect(loadingIndicator.length).toBe(0);
    }));

    it('should display default delegates after load', fakeAsync(() => {
        init();

        const delegateCards = ngMocks.findInstances(fixture.point, DelegateItemComponent);
        const loadingIndicator = ngMocks.findInstances(fixture.point, MatProgressSpinner);

        expect(delegateCards.length).toBe(2);
        expect(loadingIndicator.length).toBe(0);

        verify(tzprofilesServiceMock.getProfiles(anything())).once();
        verify(delegatesFinderServiceMock.loadTopPublicDelegates()).once();
        verify(delegatesFinderServiceMock.loadReverseRecords(anything())).once();
    }));

    it('should pass data to DelegateItemComponent', fakeAsync(() => {
        init();

        const delegateCards = ngMocks.findInstances(fixture.point, DelegateItemComponent);

        expect(delegateCards[0].address).toBe('tz1xxx');
        expect(delegateCards[0].reverseRecord?.owner).toEqual('tz1xxx');
        expect(delegateCards[0].tzProfile).toEqual({ account: 'tz1xxx', verified: [] });
        expect(delegateCards[0].votingPower).toBe(1);

        expect(delegateCards[1].address).toBe('tz1yyy');
        expect(delegateCards[1].reverseRecord?.owner).toEqual('tz1yyy');
        expect(delegateCards[1].tzProfile).toEqual({ account: 'tz1yyy', verified: [] });
        expect(delegateCards[1].votingPower).toBe(2);
    }));

    it('should output selected delegate', fakeAsync(() => {
        init();

        ngMocks.click('td-delegate-item');

        expect(delegateSelectedSpy).toHaveBeenCalledOnceWith(jasmine.objectContaining({ id: '1', address: 'tz1xxx', owner: 'tz1xxx' }));
        expect(component.selected).not.toBeNull();

        fixture.detectChanges();
        const delegateCards = ngMocks.findInstances(fixture.point, DelegateItemComponent);

        expect(delegateCards[0].selected).toBe(true);
    }));

    it('should render the searched delegate', fakeAsync(() => {
        init();

        component.form.controls.delegateInput.setValue('tz1mememe');

        tick(250);
        fixture.detectChanges();

        const delegateCards = ngMocks.findInstances(fixture.point, DelegateItemComponent);

        expect(delegateCards.length).toBe(1);
        expect(delegateCards[0].address).toBe('tz1mememe');

        verify(tzprofilesServiceMock.getProfiles(anything())).twice();
        verify(delegatesFinderServiceMock.loadTopPublicDelegates()).once();
        verify(delegatesFinderServiceMock.loadReverseRecords(anything())).twice();
    }));

    function indexerDelegates(): Delegate[] {
        return [
            { address: 'tz1xxx', voting_power: 1 },
            { address: 'tz1yyy', voting_power: 2 },
            { address: 'tz1zzz', voting_power: 3 },
            { address: 'tz1zyz', voting_power: 4 },
        ];
    }

    function init(delay = 10) {
        fixture = MockRender(DelegateFinderComponent, { delegateSelected: <any>delegateSelectedSpy });
        component = fixture.point.componentInstance;

        tick(delay);
        fixture.detectChanges();
    }
});
