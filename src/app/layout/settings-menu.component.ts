import { Component } from '@angular/core';

@Component({
    selector: 'td-settings-menu',
    templateUrl: './settings-menu.component.html',
    styleUrls: ['./settings-menu.component.scss'],
})
export class SettingsMenuComponent {}
