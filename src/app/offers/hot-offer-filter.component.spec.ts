import { fakeAsync, tick } from '@angular/core/testing';
import { TranslocoModule } from '@ngneat/transloco';
import { TestConfig, provider, transloco } from '@td/test';
import BigNumber from 'bignumber.js';
import dayjs from 'dayjs';
import { MockBuilder, MockRender, MockedComponentFixture, ngMocks } from 'ng-mocks';
import { FilterHistoryStateService } from 'src/app/browser/filter-history-state.service';
import { OfferOrderField } from 'src/app/graphql/graphql.generated';
import { mock, when } from 'ts-mockito';
import { Configuration } from '../configuration';
import { DateFilterComponent } from '../filters/date-filter.component';
import { DomainFilterComponent } from '../filters/domain-filter.component';
import { PriceFilterComponent } from '../filters/price-filter.component';
import { SortOption, SorterComponent } from '../filters/sorter.component';
import { SharedModule } from '../shared/shared.module';
import { HotOfferFilter, HotOfferFilterComponent, HotOfferFilterState, OfferCategory } from './hot-offer-filter.component';
import { OffersModule } from './offers.module';

describe('HotOfferFilterComponent', () => {
    let component: HotOfferFilterComponent;
    let fixture: MockedComponentFixture<HotOfferFilterComponent, Partial<HotOfferFilterComponent>>;
    let historyStateMock: FilterHistoryStateService;

    beforeEach(() => {
        historyStateMock = mock(FilterHistoryStateService);

        return MockBuilder(HotOfferFilterComponent, OffersModule)
            .mock(SharedModule)
            .keep(TranslocoModule, { export: true })
            .keep(transloco(), { export: true })
            .provide([provider(historyStateMock), { provide: Configuration, useValue: TestConfig }]);
    });

    describe('onInit()', () => {
        it('should output default filter state', fakeAsync(() => {
            when(historyStateMock.get<HotOfferFilterState>()).thenReturn({});

            init();

            const onFilterChange = jasmine.createSpy('onFilterChange');
            component.filterChange.subscribe(onFilterChange);

            tick(350);

            expect(onFilterChange).toHaveBeenCalledWith(<HotOfferFilter>{
                domainName: '',
                categories: [],
                sorting: <any>jasmine.objectContaining({ direction: 'desc', value: OfferOrderField.CreatedAt }),
                date: { from: dayjs(TestConfig.minEventsFilterDate).startOf('day') },
                price: null,
                allCategoriesSelected: true,
            });
        }));

        it('should output saved state', fakeAsync(() => {
            when(historyStateMock.get<HotOfferFilterState>()).thenReturn({
                hotOfferDate: { from: dayjs().subtract(2, 'days'), to: dayjs().subtract(1, 'day') },
                hotOfferDomain: 'domain',
                hotOfferPrice: { min: 1, max: 12 },
                hotOfferCategories: [OfferCategory.MoreThanFiveLetters, OfferCategory.ThreeLetters],
                hotOfferSorting: { label: 'label', value: OfferOrderField.DomainName, direction: 'asc' },
            });

            init();

            const onFilterChange = jasmine.createSpy('onFilterChange');
            component.filterChange.subscribe(onFilterChange);

            tick(350);

            expect(onFilterChange).toHaveBeenCalledWith(<HotOfferFilter>{
                domainName: 'domain',
                categories: [OfferCategory.MoreThanFiveLetters, OfferCategory.ThreeLetters],
                date: { from: dayjs().subtract(2, 'days').startOf('day'), to: dayjs().subtract(1, 'day').endOf('day') },
                price: { from: new BigNumber(1_000_000), to: new BigNumber(12_000_000) },
                sorting: { label: 'label', value: OfferOrderField.DomainName, direction: 'asc' },
                allCategoriesSelected: false,
            });
        }));
    });

    describe('on child filter changed', () => {
        let onFilterChange: jasmine.Spy;
        beforeEach(() => {
            when(historyStateMock.get<HotOfferFilterState>()).thenReturn({});
            init();

            onFilterChange = jasmine.createSpy('onFilterChange');
            component.filterChange.subscribe(onFilterChange);

            fixture.detectChanges();
        });

        it('should emit updated domain', fakeAsync(() => {
            const domainFilter = ngMocks.findInstance(DomainFilterComponent);
            domainFilter.domainChange.next('blue-');

            tick(350);

            expect(onFilterChange).toHaveBeenCalledWith(jasmine.objectContaining({ domainName: 'blue-' }));
        }));

        it('should emit updated price', fakeAsync(() => {
            const priceFilter = ngMocks.findInstance(PriceFilterComponent);
            priceFilter.priceFilterChange.next({ min: 1, max: 2 });

            tick(350);

            expect(onFilterChange).toHaveBeenCalledWith(jasmine.objectContaining({ price: { from: new BigNumber(1_000_000), to: new BigNumber(2_000_000) } }));

            onFilterChange.calls.reset();
            priceFilter.priceFilterChange.next({ min: 1 });

            tick(350);

            expect(onFilterChange).toHaveBeenCalledWith(jasmine.objectContaining({ price: { from: new BigNumber(1_000_000) } }));
            onFilterChange.calls.reset();

            priceFilter.priceFilterChange.next(null);

            tick(350);

            expect(onFilterChange).toHaveBeenCalledWith(jasmine.objectContaining({ price: null }));
        }));

        it('should emit updated date', fakeAsync(() => {
            const dateFilter = ngMocks.findInstance(DateFilterComponent);

            dateFilter.dateRangeChange.next({ from: dayjs() });

            tick(350);

            expect(onFilterChange).toHaveBeenCalledWith(jasmine.objectContaining({ date: { from: dayjs().startOf('day'), to: undefined } }));
        }));

        it('should emit updated sort', fakeAsync(() => {
            const sorter = ngMocks.findInstance(SorterComponent);

            const sorting = <SortOption>{ direction: 'asc', label: 'd-name', value: OfferOrderField.DomainName };
            sorter.sortChange.next(sorting);

            tick(350);

            expect(onFilterChange).toHaveBeenCalledWith(jasmine.objectContaining({ sorting }));
        }));
    });

    function init(data?: Partial<HotOfferFilterComponent>) {
        fixture = MockRender(HotOfferFilterComponent, data ?? {});
        component = fixture.point.componentInstance;
    }
});
