query DomainExists($name: String!) {
    domain(name: $name) {
        name
        address
    }
}

query ClaimableTLDsList($where: DomainsFilter!) {
    domains(where: $where) {
        edges {
            node {
                name
            }
        }
    }
}

query DomainDetail($name: String!, $domainValidity: RecordValidity = ALL) {
    domain: domain(name: $name, validity: $domainValidity) {
        id
        name
        address
        owner
        parentOwner
        tokenId
        operators {
            id
            address
        }
        data {
            id
            key
            rawValue
        }
        expires: expiresAtUtc
        subdomains {
            totalCount
        }
        reverseRecord {
            id
            owner
            address
        }
        ownerReverseRecord {
            id
            domain {
                id
                name
                data {
                    id
                    key
                    rawValue
                }
            }
        }
        addressReverseRecord {
            id
            domain {
                id
                name
                data {
                    id
                    key
                    rawValue
                }
            }
        }
    }
    auction: currentAuction(domainName: $name) {
        id
        endsAt: endsAtUtc
        ownedUntil: ownedUntilUtc
        state
        highestBid {
            id
            amount
            bidder
        }
        bids {
            id
            amount
            bidder
            bidderReverseRecord {
                id
                domain {
                    id
                    name
                    data {
                        id
                        key
                        rawValue
                    }
                }
            }
            timestamp
        }
    }
    lastAuction: auctions(where: { domainName: { equalTo: $name } }, order: { field: ENDS_AT, direction: DESC }, first: 1) {
        edges {
            node {
                id
                endsAt: endsAtUtc
                ownedUntil: ownedUntilUtc
                state
                highestBid {
                    id
                    amount
                    bidder
                }
            }
        }
    }
    offer: currentOffer(domainName: $name) {
        id
        tokenContract
        tokenId
        domain {
            id
            name
            expires: expiresAtUtc
            owner
            data {
                id
                key
                rawValue
            }
        }
        seller: sellerAddress
        sellerReverseRecord: sellerAddressReverseRecord {
            id
            domain {
                id
                name
                data {
                    id
                    key
                    rawValue
                }
            }
        }
        expiration: expiresAtUtc
        price
        state
    }
}

query CurrentBuyOffer($name: String!, $address: Address!) {
    currentBuyOffer(domainName: $name, buyerAddress: $address) {
        id
        domain {
            id
            name
            owner
            expires: expiresAtUtc
            data {
                id
                key
                rawValue
            }
        }
        tokenContract
        tokenId
        seller: sellerAddress
        buyer: buyerAddress
        buyerReverseRecord: buyerAddressReverseRecord {
            id
            domain {
                id
                name
                data {
                    id
                    key
                    rawValue
                }
            }
        }
        expiration: expiresAtUtc
        price
        priceWithoutFee
        state
    }
}

query AuctionDetail($name: String!, $startedAtLevel: Int!) {
    auction: auction(domainName: $name, startedAtLevel: $startedAtLevel) {
        id
        endsAtUtc
        state
        highestBid {
            id
            amount
            bidder
            bidderReverseRecord {
                id
                domain {
                    id
                    name
                    data {
                        id
                        key
                        rawValue
                    }
                }
            }
        }
        bids {
            id
            amount
            bidder
            bidderReverseRecord {
                id
                domain {
                    id
                    name
                    data {
                        id
                        key
                        rawValue
                    }
                }
            }
            timestamp
        }
    }
}

query ReverseRecordDetail($address: String!) {
    reverseRecord: reverseRecord(address: $address) {
        id
        address
        domain {
            id
            name
            data {
                id
                key
                rawValue
            }
        }
        owner
        ownerReverseRecord {
            id
            domain {
                id
                name
                data {
                    id
                    key
                    rawValue
                }
            }
        }
    }
}

query DomainsList($where: DomainsFilter!, $order: DomainOrder, $first: Int, $after: String) {
    domains(where: $where, order: $order, first: $first, after: $after) {
        edges {
            node {
                id
                name
                level
                owner
                address
                tokenId
                data {
                    id
                    key
                    rawValue
                }
                operators {
                    id
                    address
                }
                parentOwner
                expires: expiresAtUtc
            }
            cursor
        }
        pageInfo {
            startCursor
            endCursor
            hasNextPage
            hasPreviousPage
        }
    }
}

query DomainsWithAuctionList($where: DomainsFilter!, $order: DomainOrder, $first: Int, $after: String) {
    domains(where: $where, order: $order, first: $first, after: $after) {
        edges {
            node {
                id
                name
                level
                owner
                operators {
                    id
                    address
                }
                parentOwner
                lastAuction {
                    id
                    endsAt: endsAtUtc
                    ownedUntil: ownedUntilUtc
                    state
                    highestBid {
                        id
                        amount
                        bidder
                    }
                }
                expires: expiresAtUtc
            }
            cursor
        }
        pageInfo {
            startCursor
            endCursor
            hasNextPage
            hasPreviousPage
        }
    }
}

query ReverseRecordList($where: ReverseRecordsFilter!, $order: ReverseRecordOrder, $first: Int, $after: String) {
    reverseRecords(where: $where, order: $order, first: $first, after: $after) {
        edges {
            node {
                id
                address
                owner
                domain {
                    id
                    name
                    data {
                        id
                        key
                        rawValue
                    }
                }
            }
            cursor
        }
        pageInfo {
            startCursor
            endCursor
            hasNextPage
            hasPreviousPage
        }
    }
}

query Stats($owner: Address, $expiringThreshold: DateTime!) @namedClient(name: "stats") {
    bought: domains(where: { owner: { equalTo: $owner }, level: { equalTo: 2 } }) {
        totalCount
    }
    expiring: domains(where: { owner: { equalTo: $owner }, expiresAtUtc: { lessThan: $expiringThreshold }, level: { equalTo: 2 } }) {
        totalCount
    }
    total: domains(where: {}) {
        totalCount
    }
}

query UserData($address: Address!) @namedClient(name: "stats") {
    domains: domains(where: { owner: { equalTo: $address }, level: { equalTo: 2 } }) {
        totalCount
    }
    auctions: auctions(where: { bidders: { include: $address } }) {
        totalCount
    }
    buyOffers: buyOffers(where: { or: [{ buyerAddress: { in: [$address] } }, { sellerAddress: { in: [$address] } }] }) {
        totalCount
    }
}

query Events($first: Int, $after: String, $last: Int, $before: String, $where: EventsFilter) {
    events(first: $first, after: $after, last: $last, before: $before, where: $where) {
        edges {
            node {
                type
                id
                sourceAddress
                sourceAddressReverseRecord {
                    id
                    domain {
                        id
                        name
                        data {
                            id
                            key
                            rawValue
                        }
                    }
                }
                block {
                    timestamp
                }
                ... on AuctionBidEvent {
                    operationGroupHash
                    amount: bidAmount
                    balance: transactionAmount
                    domainName
                    block {
                        timestamp
                    }
                }
                ... on AuctionWithdrawEvent {
                    amount: withdrawnAmount
                    balance: withdrawnAmount
                    tldName
                    operationGroupHash
                }
                ... on AuctionSettleEvent {
                    operationGroupHash
                    domainName
                    durationInDays: registrationDurationInDays
                    amount: winningBid
                }
                ... on AuctionEndEvent {
                    domainName
                    amount: winningBid
                }
                ... on DomainBuyEvent {
                    operationGroupHash
                    domainName
                    amount: price
                    balance: price
                    durationInDays
                }
                ... on DomainRenewEvent {
                    operationGroupHash
                    domainName
                    durationInDays
                    amount: price
                    balance: price
                }
                ... on DomainCommitEvent {
                    operationGroupHash
                }
                ... on DomainUpdateEvent {
                    domainName
                    operationGroupHash
                }
                ... on DomainSetChildRecordEvent {
                    domainName
                    operationGroupHash
                    isNew: isNewRecord
                }
                ... on ReverseRecordUpdateEvent {
                    name
                    operationGroupHash
                    reverseRecordAddress
                }
                ... on ReverseRecordClaimEvent {
                    name
                    operationGroupHash
                    reverseRecordAddress: sourceAddress
                }
                ... on DomainGrantEvent {
                    domainName
                    maybeDurationInDays: durationInDays
                    operationGroupHash
                }
                ... on DomainTransferEvent {
                    domainName
                    operationGroupHash
                    secondaryAddress: newOwner
                    secondaryReverseRecord: newOwnerReverseRecord {
                        id
                        domain {
                            id
                            name
                            data {
                                id
                                key
                                rawValue
                            }
                        }
                    }
                }
                ... on OfferPlacedEvent {
                    tokenId
                    maybeDomainName: domainName
                    amount: price
                    operationGroupHash
                }
                ... on OfferUpdatedEvent {
                    tokenId
                    maybeDomainName: domainName
                    operationGroupHash
                }
                ... on OfferExecutedEvent {
                    maybeDomainName: domainName
                    amount: price
                    balance: price
                    altBalance: priceWithoutFee
                    secondaryAddress: sellerAddress
                    secondaryReverseRecord: sellerAddressReverseRecord {
                        id
                        domain {
                            id
                            name
                            data {
                                id
                                key
                                rawValue
                            }
                        }
                    }
                    operationGroupHash
                }
                ... on OfferRemovedEvent {
                    tokenId
                    maybeDomainName: domainName
                    operationGroupHash
                }
                ... on BuyOfferPlacedEvent {
                    tokenId
                    maybeDomainName: domainName
                    amount: priceWithoutFee
                    operationGroupHash
                }
                ... on BuyOfferRemovedEvent {
                    tokenId
                    maybeDomainName: domainName
                    operationGroupHash
                }
                ... on BuyOfferExecutedEvent {
                    maybeDomainName: domainName
                    amount: priceWithoutFee
                    altBalance: priceWithoutFee
                    secondaryAddress: buyerAddress
                    secondaryReverseRecord: buyerAddressReverseRecord {
                        id
                        domain {
                            id
                            name
                            data {
                                id
                                key
                                rawValue
                            }
                        }
                    }
                    operationGroupHash
                }
                ... on DomainUpdateOperatorsEvent {
                    domainName
                    operationGroupHash
                }
                ... on DomainClaimEvent {
                    domainName
                    operationGroupHash
                }
            }
        }
        pageInfo {
            startCursor
            endCursor
            hasNextPage
            hasPreviousPage
        }
    }
}

query Block($hash: String, $level: Int) {
    block(hash: $hash, level: $level) {
        hash
        level
        timestamp
    }
}

query AuctionList($where: AuctionsFilter!, $order: AuctionOrder, $first: Int, $after: String) {
    auctions(where: $where, order: $order, first: $first, after: $after) {
        edges {
            node {
                id
                domainName
                startedAtLevel
                endsAtUtc
                state
                highestBid {
                    id
                    amount
                    bidder
                }
                bidCount
            }
        }
        pageInfo {
            startCursor
            endCursor
            hasNextPage
            hasPreviousPage
        }
    }
}

query OfferList($where: OffersFilter!, $order: OfferOrder, $first: Int, $after: String) {
    offers(where: $where, order: $order, first: $first, after: $after) {
        edges {
            node {
                id
                domain {
                    id
                    name
                    owner
                    expires: expiresAtUtc
                    data {
                        id
                        key
                        rawValue
                    }
                }
                tokenContract
                tokenId
                seller: sellerAddress
                sellerReverseRecord: sellerAddressReverseRecord {
                    id
                    domain {
                        id
                        name
                        data {
                            id
                            key
                            rawValue
                        }
                    }
                }
                expiration: expiresAtUtc
                price
                state
            }
        }
        pageInfo {
            startCursor
            endCursor
            hasNextPage
            hasPreviousPage
        }
    }
}

query BuyOfferList($where: BuyOffersFilter!, $order: OfferOrder, $first: Int, $after: String) {
    buyOffers(where: $where, order: $order, first: $first, after: $after) {
        edges {
            node {
                id
                domain {
                    id
                    name
                    owner
                    expires: expiresAtUtc
                    data {
                        id
                        key
                        rawValue
                    }
                }
                tokenContract
                tokenId
                seller: sellerAddress
                buyer: buyerAddress
                buyerReverseRecord: buyerAddressReverseRecord {
                    id
                    domain {
                        id
                        name
                        data {
                            id
                            key
                            rawValue
                        }
                    }
                }
                expiration: expiresAtUtc
                price
                priceWithoutFee
                state
            }
        }
        pageInfo {
            startCursor
            endCursor
            hasNextPage
            hasPreviousPage
        }
    }
}

query BidderBalance($address: String!) {
    bidderBalances(address: $address) {
        id
        balances {
            id
            balance
            tldName
        }
    }
}
