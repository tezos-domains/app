import dayjs from 'dayjs';
import BigNumber from 'bignumber.js';
import { gql } from 'apollo-angular';
import { Injectable } from '@angular/core';
import * as Apollo from 'apollo-angular';
export type Maybe<T> = T | null;
export type InputMaybe<T> = Maybe<T>;
export type Exact<T extends { [key: string]: unknown }> = { [K in keyof T]: T[K] };
export type MakeOptional<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]?: Maybe<T[SubKey]> };
export type MakeMaybe<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]: Maybe<T[SubKey]> };
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string;
  String: string;
  Boolean: boolean;
  Int: number;
  Float: number;
  /** Address identifier - Base58Check-encoded string of length 36 prefixed with tz1 (ed25519), tz2 (secp256k1), tz3 (p256), KT1 or txr1 (rollup). */
  Address: string;
  /** The `DateTime` scalar type represents a date and time. `DateTime` expects timestamps to be formatted in accordance with the [ISO-8601](https://en.wikipedia.org/wiki/ISO_8601) standard. */
  DateTime: dayjs.Dayjs;
  /** JSON value. */
  Json: any;
  /** Micro TEZ, the smallest monetary unit in Tezos. */
  Mutez: BigNumber;
};

/** Filter conditions for filtering associated property of the type `[Address!]`. */
export type AddressArrayFilter = {
  /** Specifies a value that should be included in the array of the associated property. */
  include?: InputMaybe<Scalars['Address']>;
};

/** Filter conditions for filtering associated property of the type `Address!`. */
export type AddressFilter = {
  /** Specifies the exact value to match. */
  equalTo?: InputMaybe<Scalars['Address']>;
  /** Specifies a list of exact values to match. */
  in?: InputMaybe<Array<Scalars['Address']>>;
  /** Specifies the exact value NOT to match. */
  notEqualTo?: InputMaybe<Scalars['Address']>;
  /** Specifies a list of exact values NOT to match. */
  notIn?: InputMaybe<Array<Scalars['Address']>>;
  /** Specifies a prefix of the `Address` to match. */
  startsWith?: InputMaybe<AddressPrefix>;
};

/** Address prefix which specifies its kind. */
export enum AddressPrefix {
  /** Prefix `KT1` used for smart contract addresses. */
  Kt1 = 'KT1',
  /** Prefix `txr1` used for originated tx rollup. */
  Txr1 = 'TXR1',
  /** Prefix `tz1` used for user addresses. */
  Tz1 = 'TZ1',
  /** Prefix `tz2` used for user addresses. */
  Tz2 = 'TZ2',
  /** Prefix `tz3` used for user addresses. */
  Tz3 = 'TZ3'
}

/** Filter conditions for filtering items by associated `Block` (of last change). */
export type AssociatedBlockFilter = {
  /** Filters items by `hash` of the associated `Block`. */
  hash?: InputMaybe<StringEqualityFilter>;
  /** Filters items by `level` of the associated `Block`. */
  level?: InputMaybe<IntFilter>;
  /** Filters items by `timestamp` of the associated `Block`. */
  timestamp?: InputMaybe<DateTimeFilter>;
};

/** Domain auction. */
export type Auction = Node & {
  __typename?: 'Auction';
  /** The sum of bids' amounts. */
  bidAmountSum: Scalars['Mutez'];
  /** Number of bids for this auction. */
  bidCount: Scalars['Int'];
  /** Auction bids. */
  bids: Array<Bid>;
  /** The count of unique bidders. */
  countOfUniqueBidders: Scalars['Int'];
  /** Domain name. */
  domainName: Scalars['String'];
  /** Auction end `DateTime`. */
  endsAtUtc: Scalars['DateTime'];
  /** The current highest bid. */
  highestBid: Bid;
  /** Unique global ID of the object. */
  id: Scalars['ID'];
  /** The hash of the operation group that contained this/last change. */
  operationGroupHash: Scalars['String'];
  /** `DateTime` until an auction winner can claim the domain. */
  ownedUntilUtc: Scalars['DateTime'];
  /** Block level of the auction's first bid. */
  startedAtLevel: Scalars['Int'];
  /** The auction state. */
  state: AuctionState;
};

/** An event triggered by a user's bid to an auction. */
export type AuctionBidEvent = Event & Node & TezosDocument & {
  __typename?: 'AuctionBidEvent';
  /** The bid amount. */
  bidAmount: Scalars['Mutez'];
  /** Block in which last change happened. */
  block: Block;
  /** The domain name. */
  domainName: Scalars['String'];
  /** Unique global ID of the object. */
  id: Scalars['ID'];
  /** The hash of the operation group that contained this/last change. */
  operationGroupHash: Scalars['String'];
  /** The last bid amount before this bid. */
  previousBidAmount?: Maybe<Scalars['Mutez']>;
  /** The last bidder address before this bid. */
  previousBidderAddress?: Maybe<Scalars['Address']>;
  /** `ReverseRecord` corresponding to `previousBidderAddress`. */
  previousBidderAddressReverseRecord?: Maybe<ReverseRecord>;
  /** An address that triggered the event. */
  sourceAddress: Scalars['Address'];
  /** `ReverseRecord` corresponding to `sourceAddress`. */
  sourceAddressReverseRecord?: Maybe<ReverseRecord>;
  /** The amount spent in this transaction. */
  transactionAmount: Scalars['Mutez'];
  /** Event type. */
  type: EventType;
};

/** A relay connection from an object to a list of objects of a particular type. For more details, see: `https://relay.dev/graphql/connections.htm` */
export type AuctionConnection = {
  __typename?: 'AuctionConnection';
  /** A list of all of the edges returned in the connection. */
  edges: Array<AuctionEdge>;
  /** A list of all of the objects returned in the connection. This is a convenience field provided for quickly exploring the API; rather than querying for `{ edges { node } }` when no edge data is needed, this field can be used instead. Note that when clients like Relay need to fetch the `cursor` field on edge to enable efficient pagination, this shortcut cannot be used, and the full `{ edges { node } }` version should be used instead. */
  items: Array<Auction>;
  /** Information to aid in pagination. */
  pageInfo: PageInfo;
  /** A count of the total number of objects in this connection, ignoring pagination. This allows a client to fetch the first five objects by passing `5` as the argument to `first`, then fetch the total count so it could display `5 of 83`, for example. */
  totalCount: Scalars['Int'];
};

/** An edge in a connection from an object to another object of a particular type. */
export type AuctionEdge = {
  __typename?: 'AuctionEdge';
  /** A cursor for use in pagination */
  cursor: Scalars['String'];
  /** The item at the end of the edge */
  node: Auction;
};

/** An event triggered by a block with a timestamp greater or equal to an auction's end. */
export type AuctionEndEvent = Event & Node & TezosDocument & {
  __typename?: 'AuctionEndEvent';
  /** Block in which last change happened. */
  block: Block;
  /** The domain name. */
  domainName: Scalars['String'];
  /** Unique global ID of the object. */
  id: Scalars['ID'];
  /** The auction bidders' addresses. */
  participants: Array<AuctionParticipant>;
  /** An address that triggered the event. */
  sourceAddress: Scalars['Address'];
  /** `ReverseRecord` corresponding to `sourceAddress`. */
  sourceAddressReverseRecord?: Maybe<ReverseRecord>;
  /** Event type. */
  type: EventType;
  /** The winning bid amount. */
  winningBid: Scalars['Mutez'];
};

/** Ordering options for `Auction`-s. */
export type AuctionOrder = {
  /** The ordering direction. Default is `ASC`. */
  direction?: InputMaybe<OrderDirection>;
  /** The field to order by. */
  field: AuctionOrderField;
};

/** Properties by which Auctions are ordered. */
export enum AuctionOrderField {
  /** Order auctions by the sum of bids' amounts and then by the last bid timestamp. */
  BidAmountSum = 'BID_AMOUNT_SUM',
  /** Order auctions by the total number of bids and then by the last bid timestamp. */
  BidCount = 'BID_COUNT',
  /** Order auctions by the domain name. */
  DomainName = 'DOMAIN_NAME',
  /** Order auctions by the auction end `DateTime`. */
  EndsAt = 'ENDS_AT',
  /** Order auctions by the current highest bid amount and then by timestamp. */
  HighestBidAmount = 'HIGHEST_BID_AMOUNT',
  /** Order auctions by the current highest bid timestamp. */
  HighestBidTimestamp = 'HIGHEST_BID_TIMESTAMP',
  /** Order auctions by id. */
  Id = 'ID'
}

/** An auction participant. */
export type AuctionParticipant = Node & {
  __typename?: 'AuctionParticipant';
  /** Participant's address */
  address: Scalars['Address'];
  /** Unique global ID of the object. */
  id: Scalars['ID'];
};

/** An event triggered by a user's auction settlement action. */
export type AuctionSettleEvent = Event & Node & TezosDocument & {
  __typename?: 'AuctionSettleEvent';
  /** Block in which last change happened. */
  block: Block;
  /** The data for the won domain. */
  data: Array<DataItem>;
  /** The forward record for the won domain. */
  domainForwardRecordAddress?: Maybe<Scalars['Address']>;
  /** The domain name. */
  domainName: Scalars['String'];
  /** The owner of the won domain. */
  domainOwnerAddress: Scalars['Address'];
  /** Unique global ID of the object. */
  id: Scalars['ID'];
  /** The hash of the operation group that contained this/last change. */
  operationGroupHash: Scalars['String'];
  /** The period in days during which the auction winner owns this domain. This includes the period after the auction ended but was not settled yet. */
  registrationDurationInDays: Scalars['Int'];
  /** An address that triggered the event. */
  sourceAddress: Scalars['Address'];
  /** `ReverseRecord` corresponding to `sourceAddress`. */
  sourceAddressReverseRecord?: Maybe<ReverseRecord>;
  /** Event type. */
  type: EventType;
  /** The price paid for the won domain. */
  winningBid: Scalars['Mutez'];
};

/** The auction state is based on its launch. */
export enum AuctionState {
  /** The auction has its winner who can settle it within a predefined period. */
  CanBeSettled = 'CAN_BE_SETTLED',
  /** The auction is in progress, and users can make a bid. */
  InProgress = 'IN_PROGRESS',
  /** The auction has been settled by its winner. */
  Settled = 'SETTLED',
  /** The auction has not been settled by its winner within a predefined period. */
  SettlementExpired = 'SETTLEMENT_EXPIRED'
}

/** Filters `Auction`-s by their `state`. */
export type AuctionStateFilter = {
  /** Specifies the exact values to be matched. */
  in?: InputMaybe<Array<AuctionState>>;
  /** Specifies the exact values to be excluded. */
  notIn?: InputMaybe<Array<AuctionState>>;
};

/** An event triggered by a user's withdrawal action. */
export type AuctionWithdrawEvent = Event & Node & TezosDocument & {
  __typename?: 'AuctionWithdrawEvent';
  /** Block in which last change happened. */
  block: Block;
  /** Unique global ID of the object. */
  id: Scalars['ID'];
  /** The hash of the operation group that contained this/last change. */
  operationGroupHash: Scalars['String'];
  /** An address that triggered the event. */
  sourceAddress: Scalars['Address'];
  /** `ReverseRecord` corresponding to `sourceAddress`. */
  sourceAddressReverseRecord?: Maybe<ReverseRecord>;
  /** The tld domain name. */
  tldName: Scalars['String'];
  /** Event type. */
  type: EventType;
  /** The withdrawn amount. */
  withdrawnAmount: Scalars['Mutez'];
};

/** Complex object with filter conditions for filtering `Auction`-s. */
export type AuctionsFilter = {
  /** Additional filters of the same type are joined using the `AND` operator. It's joined using AND with other filter properties. */
  and?: InputMaybe<Array<AuctionsFilter>>;
  /** Filters `Auction`-s by their `bidCount`. */
  bidCount?: InputMaybe<IntFilter>;
  /** Filters `Auction`-s by `bidder` addresses of `bids`. */
  bidders?: InputMaybe<AddressArrayFilter>;
  /** Filters `Auction`-s by their `domainName`. */
  domainName?: InputMaybe<StringFilter>;
  /** Filters `Auction`-s by their `endsAtUtc`. */
  endsAtUtc?: InputMaybe<NullableDateTimeFilter>;
  /** Filters `Auction`-s by bidder's address of the `highestBid`. */
  highestBidder?: InputMaybe<AddressFilter>;
  /** Additional filters of the same type are joined using the `OR` operator. It's joined using AND with other filter properties. */
  or?: InputMaybe<Array<AuctionsFilter>>;
  /** Filters `Auction`-s by their `state`. */
  state?: InputMaybe<AuctionStateFilter>;
};

/** A user balance for the specific TLD registrar. */
export type Balance = Node & {
  __typename?: 'Balance';
  /** The withdrawable balance. */
  balance: Scalars['Mutez'];
  /** Unique global ID of the object. */
  id: Scalars['ID'];
  /** The tld registrar's name. */
  tldName: Scalars['String'];
};

/** Auction bid. */
export type Bid = Node & {
  __typename?: 'Bid';
  /** Bid amount. */
  amount: Scalars['Mutez'];
  /** Bidder address. */
  bidder: Scalars['Address'];
  /** `ReverseRecord` corresponding to `bidder`. */
  bidderReverseRecord?: Maybe<ReverseRecord>;
  /** Unique global ID of the object. */
  id: Scalars['ID'];
  /** The hash of the operation group that contained this/last change. */
  operationGroupHash: Scalars['String'];
  /** Bid timestamp. */
  timestamp: Scalars['DateTime'];
};

/** Bidder balances from unused auctions bids. */
export type BidderBalances = Node & {
  __typename?: 'BidderBalances';
  /** Reverse record address. */
  address: Scalars['Address'];
  /** User's withdrawable balances by tld registrars. */
  balances: Array<Balance>;
  /** Unique global ID of the object. */
  id: Scalars['ID'];
  /** The hash of the operation group that contained this/last change. */
  operationGroupHash?: Maybe<Scalars['String']>;
};

/** Block in the chain. */
export type Block = Node & {
  __typename?: 'Block';
  /** Block hash. */
  hash: Scalars['String'];
  /** Unique global ID of the object. */
  id: Scalars['ID'];
  /** Block level. */
  level: Scalars['Int'];
  /** Block timestamp. */
  timestamp: Scalars['DateTime'];
};

/** Filters entities by block history. */
export type BlockHistoryFilter = {
  /** Specifies a point in time using block `level`. */
  level?: InputMaybe<Scalars['Int']>;
  /** Specifies a point in time using an arbitrary timestamp matched against the block's `timestamp`. */
  timestamp?: InputMaybe<Scalars['DateTime']>;
};

/** Domain buy offer. */
export type BuyOffer = Node & {
  __typename?: 'BuyOffer';
  /** Domain buyer address. */
  buyerAddress: Scalars['Address'];
  /** `ReverseRecord` corresponding to `buyerAddress`. */
  buyerAddressReverseRecord?: Maybe<ReverseRecord>;
  /** Offer creation. */
  createdAtUtc: Scalars['DateTime'];
  /** Domain data. */
  domain?: Maybe<DomainData>;
  /** Offer expiration. */
  expiresAtUtc?: Maybe<Scalars['DateTime']>;
  /** Domain's offer fee. */
  fee: Scalars['Mutez'];
  /** Unique global ID of the object. */
  id: Scalars['ID'];
  /** The hash of the operation group that contained this/last change. */
  operationGroupHash: Scalars['String'];
  /** Total price for the domain. */
  price: Scalars['Mutez'];
  /** Amount to buy the domain without a fee. */
  priceWithoutFee: Scalars['Mutez'];
  /** Domain seller address. */
  sellerAddress?: Maybe<Scalars['Address']>;
  /** `ReverseRecord` corresponding to `sellerAddress`. */
  sellerAddressReverseRecord?: Maybe<ReverseRecord>;
  /** Offer state. */
  state: OfferState;
  /** NFT token contract. */
  tokenContract: Scalars['String'];
  /** Domain NFT token id. */
  tokenId: Scalars['Int'];
};

/** A relay connection from an object to a list of objects of a particular type. For more details, see: `https://relay.dev/graphql/connections.htm` */
export type BuyOfferConnection = {
  __typename?: 'BuyOfferConnection';
  /** A list of all of the edges returned in the connection. */
  edges: Array<BuyOfferEdge>;
  /** A list of all of the objects returned in the connection. This is a convenience field provided for quickly exploring the API; rather than querying for `{ edges { node } }` when no edge data is needed, this field can be used instead. Note that when clients like Relay need to fetch the `cursor` field on edge to enable efficient pagination, this shortcut cannot be used, and the full `{ edges { node } }` version should be used instead. */
  items: Array<BuyOffer>;
  /** Information to aid in pagination. */
  pageInfo: PageInfo;
  /** A count of the total number of objects in this connection, ignoring pagination. This allows a client to fetch the first five objects by passing `5` as the argument to `first`, then fetch the total count so it could display `5 of 83`, for example. */
  totalCount: Scalars['Int'];
};

/** An edge in a connection from an object to another object of a particular type. */
export type BuyOfferEdge = {
  __typename?: 'BuyOfferEdge';
  /** A cursor for use in pagination */
  cursor: Scalars['String'];
  /** The item at the end of the edge */
  node: BuyOffer;
};

/** An event triggered by a user accepting an existing offer to buy a domain. */
export type BuyOfferExecutedEvent = Event & Node & TezosDocument & {
  __typename?: 'BuyOfferExecutedEvent';
  /** Block in which last change happened. */
  block: Block;
  /** The buyer of the bought domain. */
  buyerAddress: Scalars['Address'];
  /** `ReverseRecord` corresponding to `buyerAddress`. */
  buyerAddressReverseRecord?: Maybe<ReverseRecord>;
  /** The domain name. */
  domainName: Scalars['String'];
  /** The domain selling fee. */
  fee: Scalars['Mutez'];
  /** Unique global ID of the object. */
  id: Scalars['ID'];
  /** The hash of the operation group that contained this/last change. */
  operationGroupHash: Scalars['String'];
  /** The sold domain price. */
  price: Scalars['Mutez'];
  /** The sold domain price without the fee. */
  priceWithoutFee: Scalars['Mutez'];
  /** An address that triggered the event. */
  sourceAddress: Scalars['Address'];
  /** `ReverseRecord` corresponding to `sourceAddress`. */
  sourceAddressReverseRecord?: Maybe<ReverseRecord>;
  /** The domain token id. */
  tokenId: Scalars['Int'];
  /** Event type. */
  type: EventType;
};

/** An event triggered by a user creating an offer to buy a domain. */
export type BuyOfferPlacedEvent = Event & Node & TezosDocument & {
  __typename?: 'BuyOfferPlacedEvent';
  /** Block in which last change happened. */
  block: Block;
  /** The domain name. */
  domainName: Scalars['String'];
  /** The domain owner. */
  domainOwner: Scalars['String'];
  /** The offer expiration date. */
  expiresAtUtc?: Maybe<Scalars['DateTime']>;
  /** The domain selling fee. */
  fee: Scalars['Mutez'];
  /** Unique global ID of the object. */
  id: Scalars['ID'];
  /** The hash of the operation group that contained this/last change. */
  operationGroupHash: Scalars['String'];
  /** The price to buy the domain. */
  price: Scalars['Mutez'];
  /** The price part which goes to the seller. */
  priceWithoutFee: Scalars['Mutez'];
  /** An address that triggered the event. */
  sourceAddress: Scalars['Address'];
  /** `ReverseRecord` corresponding to `sourceAddress`. */
  sourceAddressReverseRecord?: Maybe<ReverseRecord>;
  /** The domain token id. */
  tokenId: Scalars['Int'];
  /** Event type. */
  type: EventType;
};

/** An event triggered by a user removing his offer to buy a domain. */
export type BuyOfferRemovedEvent = Event & Node & TezosDocument & {
  __typename?: 'BuyOfferRemovedEvent';
  /** Block in which last change happened. */
  block: Block;
  /** The domain name. */
  domainName: Scalars['String'];
  /** Unique global ID of the object. */
  id: Scalars['ID'];
  /** The hash of the operation group that contained this/last change. */
  operationGroupHash: Scalars['String'];
  /** An address that triggered the event. */
  sourceAddress: Scalars['Address'];
  /** `ReverseRecord` corresponding to `sourceAddress`. */
  sourceAddressReverseRecord?: Maybe<ReverseRecord>;
  /** The domain token id. */
  tokenId: Scalars['Int'];
  /** Event type. */
  type: EventType;
};

/** Complex object with filter conditions for filtering `Offer`-s. */
export type BuyOffersFilter = {
  /** Additional filters of the same type are joined using the `AND` operator. It's joined using AND with other filter properties. */
  and?: InputMaybe<Array<BuyOffersFilter>>;
  /** Filters `Offer`-s by their `buyerAddress`. */
  buyerAddress?: InputMaybe<AddressFilter>;
  /** Filters `Offer`-s by their `createdAtUtc`. */
  createdAtUtc?: InputMaybe<DateTimeFilter>;
  /** Filters `Offer`-s by `name` of offered `domain`. */
  domainName?: InputMaybe<StringFilter>;
  /** Filters `Offer`-s by their `expiresAtUtc`. */
  endsAtUtc?: InputMaybe<NullableDateTimeFilter>;
  /** Filters `Offer`-s by their `id`. */
  id?: InputMaybe<IdEqualityFilter>;
  /** Additional filters of the same type are joined using the `OR` operator. It's joined using AND with other filter properties. */
  or?: InputMaybe<Array<BuyOffersFilter>>;
  /** Filters `Offer`-s by their `price`. */
  price?: InputMaybe<MutezFilter>;
  /** Filters `Offer`-s by their `sellerAddress`. */
  sellerAddress?: InputMaybe<AddressFilter>;
  /** Filters `Offer`-s by their `state`. */
  state?: InputMaybe<OfferStateFilter>;
  /** Filters `Offer`-s by their `tokenId`. */
  tokenId?: InputMaybe<IntFilter>;
};

/** An item of arbitrary data associated with the record. */
export type DataItem = Node & {
  __typename?: 'DataItem';
  /** Unique global ID of the object. */
  id: Scalars['ID'];
  /** The unique key of the data item. */
  key: Scalars['String'];
  /** The raw value of the data item. */
  rawValue: Scalars['String'];
  /** The parsed value of the data item. The value is `null` if the on-chain bytes don't represent a valid JSON. */
  value?: Maybe<Scalars['Json']>;
};

/** Filter conditions for filtering associated property of the type `DateTime!`. */
export type DateTimeFilter = {
  /** Specifies the exact value to match. */
  equalTo?: InputMaybe<Scalars['DateTime']>;
  /** Matches values greater than the specified value. */
  greaterThan?: InputMaybe<Scalars['DateTime']>;
  /** Matches values greater than or equal to the specified value. */
  greaterThanOrEqualTo?: InputMaybe<Scalars['DateTime']>;
  /** Specifies a list of exact values to match. */
  in?: InputMaybe<Array<Scalars['DateTime']>>;
  /** Matches values less than the specified value. */
  lessThan?: InputMaybe<Scalars['DateTime']>;
  /** Matches values less than or equal to the specified value. */
  lessThanOrEqualTo?: InputMaybe<Scalars['DateTime']>;
  /** Specifies the exact value NOT to match. */
  notEqualTo?: InputMaybe<Scalars['DateTime']>;
  /** Specifies a list of exact values NOT to match. */
  notIn?: InputMaybe<Array<Scalars['DateTime']>>;
};

/** Domain - forward record. */
export type Domain = Node & {
  __typename?: 'Domain';
  /** Domain address. */
  address?: Maybe<Scalars['Address']>;
  /** `ReverseRecord` corresponding to `address`. */
  addressReverseRecord?: Maybe<ReverseRecord>;
  /** Data associated with this domain. */
  data: Array<DataItem>;
  /** Domain validity. */
  expiresAtUtc?: Maybe<Scalars['DateTime']>;
  /** Unique global ID of the object. */
  id: Scalars['ID'];
  /** First segment of domain name. */
  label: Scalars['String'];
  /** Last `Auction` of the domain. */
  lastAuction?: Maybe<Auction>;
  /** Domain level, which is a number of segments in its `name`. */
  level: Scalars['Int'];
  /** Domain name. */
  name: Scalars['String'];
  /** The hash of the operation group that contained this/last change. */
  operationGroupHash: Scalars['String'];
  /** Domain TZIP-12 operators. */
  operators: Array<DomainOperator>;
  /** Owner address. */
  owner: Scalars['Address'];
  /** `ReverseRecord` corresponding to `owner`. */
  ownerReverseRecord?: Maybe<ReverseRecord>;
  /** Parent domain `name`. */
  parentName?: Maybe<Scalars['String']>;
  /** Parent domain `owner` address. */
  parentOwner?: Maybe<Scalars['String']>;
  /** `ReverseRecord` corresponding to `parentOwner`. */
  parentOwnerReverseRecord?: Maybe<ReverseRecord>;
  /** `ReverseRecord` record corresponding to both the `address` and the `name` of this domain. */
  reverseRecord?: Maybe<ReverseRecord>;
  /** Subdomains of the domain e.g. `foo.bar.tez` is subdomain of `bar.tez`. */
  subdomains: DomainConnection;
  /** TZIP-12 NFT Token id. */
  tokenId?: Maybe<Scalars['Int']>;
};


/** Domain - forward record. */
export type DomainSubdomainsArgs = {
  after?: InputMaybe<Scalars['String']>;
  before?: InputMaybe<Scalars['String']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  onlyDirectChildren?: InputMaybe<Scalars['Boolean']>;
  order?: InputMaybe<DomainOrder>;
};

/** An event triggered by a user buying a domain. */
export type DomainBuyEvent = Event & Node & TezosDocument & {
  __typename?: 'DomainBuyEvent';
  /** Block in which last change happened. */
  block: Block;
  /** The data for the bought domain. */
  data: Array<DataItem>;
  /** The forward record for the bought domain. */
  domainForwardRecordAddress?: Maybe<Scalars['Address']>;
  /** The domain name. */
  domainName: Scalars['String'];
  /** The owner of the bought domain. */
  domainOwnerAddress: Scalars['Address'];
  /** The duration of the bought domain. */
  durationInDays: Scalars['Int'];
  /** Unique global ID of the object. */
  id: Scalars['ID'];
  /** The hash of the operation group that contained this/last change. */
  operationGroupHash: Scalars['String'];
  /** The price paid for the domain. */
  price: Scalars['Mutez'];
  /** An address that triggered the event. */
  sourceAddress: Scalars['Address'];
  /** `ReverseRecord` corresponding to `sourceAddress`. */
  sourceAddressReverseRecord?: Maybe<ReverseRecord>;
  /** Event type. */
  type: EventType;
};

/** An event triggered by a user claiming a DNS domain. */
export type DomainClaimEvent = Event & Node & TezosDocument & {
  __typename?: 'DomainClaimEvent';
  /** Block in which last change happened. */
  block: Block;
  /** The forward record of the claimed domain. */
  domainForwardRecordAddress?: Maybe<Scalars['Address']>;
  /** The domain name. */
  domainName: Scalars['String'];
  /** The owner of the claimed domain. */
  domainOwnerAddress: Scalars['Address'];
  /** Unique global ID of the object. */
  id: Scalars['ID'];
  /** The hash of the operation group that contained this/last change. */
  operationGroupHash: Scalars['String'];
  /** An address that triggered the event. */
  sourceAddress: Scalars['Address'];
  /** `ReverseRecord` corresponding to `sourceAddress`. */
  sourceAddressReverseRecord?: Maybe<ReverseRecord>;
  /** Event type. */
  type: EventType;
};

/** An event triggered by a user's commit transaction during the domain buying process. */
export type DomainCommitEvent = Event & Node & TezosDocument & {
  __typename?: 'DomainCommitEvent';
  /** Block in which last change happened. */
  block: Block;
  /** Commitment hash which allows for the domain to be bought. */
  commitmentHash: Scalars['String'];
  /** Unique global ID of the object. */
  id: Scalars['ID'];
  /** The hash of the operation group that contained this/last change. */
  operationGroupHash: Scalars['String'];
  /** An address that triggered the event. */
  sourceAddress: Scalars['Address'];
  /** `ReverseRecord` corresponding to `sourceAddress`. */
  sourceAddressReverseRecord?: Maybe<ReverseRecord>;
  /** Event type. */
  type: EventType;
};

/** A relay connection from an object to a list of objects of a particular type. For more details, see: `https://relay.dev/graphql/connections.htm` */
export type DomainConnection = {
  __typename?: 'DomainConnection';
  /** A list of all of the edges returned in the connection. */
  edges: Array<DomainEdge>;
  /** A list of all of the objects returned in the connection. This is a convenience field provided for quickly exploring the API; rather than querying for `{ edges { node } }` when no edge data is needed, this field can be used instead. Note that when clients like Relay need to fetch the `cursor` field on edge to enable efficient pagination, this shortcut cannot be used, and the full `{ edges { node } }` version should be used instead. */
  items: Array<Domain>;
  /** Information to aid in pagination. */
  pageInfo: PageInfo;
  /** A count of the total number of objects in this connection, ignoring pagination. This allows a client to fetch the first five objects by passing `5` as the argument to `first`, then fetch the total count so it could display `5 of 83`, for example. */
  totalCount: Scalars['Int'];
};

/** Offer's domain data. */
export type DomainData = Node & {
  __typename?: 'DomainData';
  /** Data associated with an offered domain. */
  data: Array<DataItem>;
  /** Domain expiration. */
  expiresAtUtc?: Maybe<Scalars['DateTime']>;
  /** Unique global ID of the object. */
  id: Scalars['ID'];
  /** Domain name. */
  name: Scalars['String'];
  /** Domain operators. */
  operators: Array<Scalars['Address']>;
  /** Domain owner. */
  owner: Scalars['Address'];
};

/** An edge in a connection from an object to another object of a particular type. */
export type DomainEdge = {
  __typename?: 'DomainEdge';
  /** A cursor for use in pagination */
  cursor: Scalars['String'];
  /** The item at the end of the edge */
  node: Domain;
};

/** An event triggered by a sunrise period granting a domain. */
export type DomainGrantEvent = Event & Node & TezosDocument & {
  __typename?: 'DomainGrantEvent';
  /** Block in which last change happened. */
  block: Block;
  /** The data of the granted domain. */
  data: Array<DataItem>;
  /** The forward record of the granted domain. */
  domainForwardRecordAddress?: Maybe<Scalars['Address']>;
  /** The domain name. */
  domainName: Scalars['String'];
  /** The owner of the granted domain. */
  domainOwnerAddress: Scalars['Address'];
  /** The duration of the granted domain. If null, then granted forever. */
  durationInDays?: Maybe<Scalars['Int']>;
  /** Unique global ID of the object. */
  id: Scalars['ID'];
  /** The hash of the operation group that contained this/last change. */
  operationGroupHash: Scalars['String'];
  /** An address that triggered the event. */
  sourceAddress: Scalars['Address'];
  /** `ReverseRecord` corresponding to `sourceAddress`. */
  sourceAddressReverseRecord?: Maybe<ReverseRecord>;
  /** Event type. */
  type: EventType;
};

/** Filters `Domain`-s by their `label`. */
export type DomainLabelFilter = {
  /** Specifies the domain label composition (letters, numbers, dash) to match. */
  composition?: InputMaybe<LabelCompositionFilter>;
  /** Specifies the suffix of the value to match. */
  endsWith?: InputMaybe<Scalars['String']>;
  /** Specifies the exact value to match. */
  equalTo?: InputMaybe<Scalars['String']>;
  /** Specifies a list of exact values to match. */
  in?: InputMaybe<Array<Scalars['String']>>;
  /** Specifies the value length to match. */
  length?: InputMaybe<StringLengthFilter>;
  /** Specifies the similar string to the value to match. */
  like?: InputMaybe<Scalars['String']>;
  /** Specifies the exact value NOT to match. */
  notEqualTo?: InputMaybe<Scalars['String']>;
  /** Specifies a list of exact values NOT to match. */
  notIn?: InputMaybe<Array<Scalars['String']>>;
  /** Specifies the prefix of the value to match. */
  startsWith?: InputMaybe<Scalars['String']>;
};

/** Domain TZIP-12 operator. */
export type DomainOperator = Node & {
  __typename?: 'DomainOperator';
  /** Operator's address. */
  address: Scalars['String'];
  /** Unique global ID of the object. */
  id: Scalars['ID'];
  /** `ReverseRecord` corresponding to `address`. */
  reverseRecord?: Maybe<ReverseRecord>;
};

/** Ordering options for `Domain`-s. */
export type DomainOrder = {
  /** The ordering direction. Default is `ASC`. */
  direction?: InputMaybe<OrderDirection>;
  /** The field to order by. */
  field: DomainOrderField;
};

/** Properties by which Domains are ordered. */
export enum DomainOrderField {
  /** Order domains by address. */
  Address = 'ADDRESS',
  /** Order domains by name and level. */
  Domain = 'DOMAIN',
  /** Order domains by validity. */
  ExpiresAt = 'EXPIRES_AT',
  /** Order domains by level. */
  Level = 'LEVEL',
  /** Order domains by name. */
  Name = 'NAME',
  /** Order domains by owner. */
  Owner = 'OWNER'
}

/** An event triggered by a user renewing a domain. */
export type DomainRenewEvent = Event & Node & TezosDocument & {
  __typename?: 'DomainRenewEvent';
  /** Block in which last change happened. */
  block: Block;
  /** The domain name. */
  domainName: Scalars['String'];
  /** The duration of the renewed domain. */
  durationInDays: Scalars['Int'];
  /** Unique global ID of the object. */
  id: Scalars['ID'];
  /** The hash of the operation group that contained this/last change. */
  operationGroupHash: Scalars['String'];
  /** The price paid for the renewal. */
  price: Scalars['Mutez'];
  /** An address that triggered the event. */
  sourceAddress: Scalars['Address'];
  /** `ReverseRecord` corresponding to `sourceAddress`. */
  sourceAddressReverseRecord?: Maybe<ReverseRecord>;
  /** Event type. */
  type: EventType;
};

/** An event triggered by a user creating a subdomain. */
export type DomainSetChildRecordEvent = Event & Node & TezosDocument & {
  __typename?: 'DomainSetChildRecordEvent';
  /** Block in which last change happened. */
  block: Block;
  /** The data for the bought domain. */
  data: Array<DataItem>;
  /** The forward record for the bought domain. */
  domainForwardRecordAddress?: Maybe<Scalars['Address']>;
  /** The domain name. */
  domainName: Scalars['String'];
  /** The owner of the bought domain. */
  domainOwnerAddress: Scalars['Address'];
  /** Unique global ID of the object. */
  id: Scalars['ID'];
  /** When `true`, a new subdomain is created. When `false`, a subdomain was updated by the owner of the parent domain. */
  isNewRecord: Scalars['Boolean'];
  /** The hash of the operation group that contained this/last change. */
  operationGroupHash: Scalars['String'];
  /** An address that triggered the event. */
  sourceAddress: Scalars['Address'];
  /** `ReverseRecord` corresponding to `sourceAddress`. */
  sourceAddressReverseRecord?: Maybe<ReverseRecord>;
  /** Event type. */
  type: EventType;
};

/** An event triggered by a user transferring a domain. */
export type DomainTransferEvent = Event & Node & TezosDocument & {
  __typename?: 'DomainTransferEvent';
  /** Block in which last change happened. */
  block: Block;
  /** The domain name. */
  domainName: Scalars['String'];
  /** Unique global ID of the object. */
  id: Scalars['ID'];
  /** The new owner of the transferred domain. */
  newOwner: Scalars['Address'];
  /** `ReverseRecord` corresponding to `newOwner`. */
  newOwnerReverseRecord?: Maybe<ReverseRecord>;
  /** The hash of the operation group that contained this/last change. */
  operationGroupHash: Scalars['String'];
  /** An address that triggered the event. */
  sourceAddress: Scalars['Address'];
  /** `ReverseRecord` corresponding to `sourceAddress`. */
  sourceAddressReverseRecord?: Maybe<ReverseRecord>;
  /** Event type. */
  type: EventType;
};

/** An event triggered by a user updating domain's data. */
export type DomainUpdateEvent = Event & Node & TezosDocument & {
  __typename?: 'DomainUpdateEvent';
  /** Block in which last change happened. */
  block: Block;
  /** The data for the bought domain. */
  data: Array<DataItem>;
  /** The forward record for the bought domain. */
  domainForwardRecordAddress?: Maybe<Scalars['Address']>;
  /** The domain name. */
  domainName: Scalars['String'];
  /** The owner of the bought domain. */
  domainOwnerAddress: Scalars['Address'];
  /** Unique global ID of the object. */
  id: Scalars['ID'];
  /** The hash of the operation group that contained this/last change. */
  operationGroupHash: Scalars['String'];
  /** An address that triggered the event. */
  sourceAddress: Scalars['Address'];
  /** `ReverseRecord` corresponding to `sourceAddress`. */
  sourceAddressReverseRecord?: Maybe<ReverseRecord>;
  /** Event type. */
  type: EventType;
};

/** An event triggered by a user updating operators for a domain. */
export type DomainUpdateOperatorsEvent = Event & Node & TezosDocument & {
  __typename?: 'DomainUpdateOperatorsEvent';
  /** Block in which last change happened. */
  block: Block;
  /** The domain name. */
  domainName: Scalars['String'];
  /** Unique global ID of the object. */
  id: Scalars['ID'];
  /** The hash of the operation group that contained this/last change. */
  operationGroupHash: Scalars['String'];
  /** The domain TZIP-12 operators. */
  operators: Array<Scalars['Address']>;
  /** An address that triggered the event. */
  sourceAddress: Scalars['Address'];
  /** `ReverseRecord` corresponding to `sourceAddress`. */
  sourceAddressReverseRecord?: Maybe<ReverseRecord>;
  /** Event type. */
  type: EventType;
};

/** Complex object with filter conditions for filtering `Domain`-s. */
export type DomainsFilter = {
  /** Filters `Domain`-s by their `address`. */
  address?: InputMaybe<NullableAddressFilter>;
  /** Filters `Domain`-s by their `ancestors`. */
  ancestors?: InputMaybe<StringArrayFilter>;
  /** Additional filters of the same type are joined using the `AND` operator. It's joined using AND with other filter properties. */
  and?: InputMaybe<Array<DomainsFilter>>;
  /** Filters items by their `expiresAtUtc`. */
  expiresAtUtc?: InputMaybe<NullableDateTimeFilter>;
  /** Filters `Domain`-s by their `label`. */
  label?: InputMaybe<DomainLabelFilter>;
  /** Filters `Domain`-s by their `level`. */
  level?: InputMaybe<IntFilter>;
  /** Filters `Domain`-s by their `name`. */
  name?: InputMaybe<StringFilter>;
  /** Filters `Domain`-s by their `operators`. */
  operators?: InputMaybe<StringArrayFilter>;
  /** Additional filters of the same type are joined using the `OR` operator. It's joined using AND with other filter properties. */
  or?: InputMaybe<Array<DomainsFilter>>;
  /** Filters `Domain`-s by their `owner`. */
  owner?: InputMaybe<AddressFilter>;
  /** Filters `Domain`-s by their `parentOwner`. */
  parentOwner?: InputMaybe<AddressFilter>;
  /** Filters validity of the entity based on its validity `DateTime`. Default is `VALID`. */
  validity?: InputMaybe<RecordValidity>;
};

/** An action in Tezos Domains describes an interaction with a domain, a reverse record, or an auction. */
export type Event = {
  /** Block in which last change happened. */
  block: Block;
  /** Unique global ID of the object. */
  id: Scalars['ID'];
  /** An address that triggered the event. */
  sourceAddress: Scalars['Address'];
  /** `ReverseRecord` corresponding to `sourceAddress`. */
  sourceAddressReverseRecord?: Maybe<ReverseRecord>;
  /** Event type. */
  type: EventType;
};

/** A relay connection from an object to a list of objects of a particular type. For more details, see: `https://relay.dev/graphql/connections.htm` */
export type EventConnection = {
  __typename?: 'EventConnection';
  /** A list of all of the edges returned in the connection. */
  edges: Array<EventEdge>;
  /** A list of all of the objects returned in the connection. This is a convenience field provided for quickly exploring the API; rather than querying for `{ edges { node } }` when no edge data is needed, this field can be used instead. Note that when clients like Relay need to fetch the `cursor` field on edge to enable efficient pagination, this shortcut cannot be used, and the full `{ edges { node } }` version should be used instead. */
  items: Array<Event>;
  /** Information to aid in pagination. */
  pageInfo: PageInfo;
  /** A count of the total number of objects in this connection, ignoring pagination. This allows a client to fetch the first five objects by passing `5` as the argument to `first`, then fetch the total count so it could display `5 of 83`, for example. */
  totalCount: Scalars['Int'];
};

/** An edge in a connection from an object to another object of a particular type. */
export type EventEdge = {
  __typename?: 'EventEdge';
  /** A cursor for use in pagination */
  cursor: Scalars['String'];
  /** The item at the end of the edge */
  node: Event;
};

/** Ordering options for `Event`-s. */
export type EventOrder = {
  /** The ordering direction. Default is `ASC`. */
  direction?: InputMaybe<OrderDirection>;
  /** The field to order by. */
  field: EventOrderField;
};

/** Properties by which Events are ordered. */
export enum EventOrderField {
  /** Order events by the timestamp of the associated block. */
  Timestamp = 'TIMESTAMP'
}

/** Event types. */
export enum EventType {
  /** An event triggered by a user's bid to an auction. */
  AuctionBidEvent = 'AUCTION_BID_EVENT',
  /** An event triggered by a block with a timestamp greater or equal to an auction's end. */
  AuctionEndEvent = 'AUCTION_END_EVENT',
  /** An event triggered by a user's auction settlement action. */
  AuctionSettleEvent = 'AUCTION_SETTLE_EVENT',
  /** An event triggered by a user's withdrawal action. */
  AuctionWithdrawEvent = 'AUCTION_WITHDRAW_EVENT',
  /** An event triggered by a user accepting an existing offer to buy a domain. */
  BuyOfferExecutedEvent = 'BUY_OFFER_EXECUTED_EVENT',
  /** An event triggered by a user creating an offer to buy a domain. */
  BuyOfferPlacedEvent = 'BUY_OFFER_PLACED_EVENT',
  /** An event triggered by a user removing his offer to buy a domain. */
  BuyOfferRemovedEvent = 'BUY_OFFER_REMOVED_EVENT',
  /** An event triggered by a user buying a domain. */
  DomainBuyEvent = 'DOMAIN_BUY_EVENT',
  /** An event triggered by a user claiming a DNS domain. */
  DomainClaimEvent = 'DOMAIN_CLAIM_EVENT',
  /** An event triggered by a user's commit transaction during the domain buying process. */
  DomainCommitEvent = 'DOMAIN_COMMIT_EVENT',
  /** An event triggered by a sunrise period granting a domain. */
  DomainGrantEvent = 'DOMAIN_GRANT_EVENT',
  /** An event triggered by a user renewing a domain. */
  DomainRenewEvent = 'DOMAIN_RENEW_EVENT',
  /** An event triggered by a user creating a subdomain. */
  DomainSetChildRecordEvent = 'DOMAIN_SET_CHILD_RECORD_EVENT',
  /** An event triggered by a user transferring a domain. */
  DomainTransferEvent = 'DOMAIN_TRANSFER_EVENT',
  /** An event triggered by a user updating domain's data. */
  DomainUpdateEvent = 'DOMAIN_UPDATE_EVENT',
  /** An event triggered by a user updating operators for a domain. */
  DomainUpdateOperatorsEvent = 'DOMAIN_UPDATE_OPERATORS_EVENT',
  /** An event triggered by a user accepting an existing offer to buy a domain. */
  OfferExecutedEvent = 'OFFER_EXECUTED_EVENT',
  /** An event triggered by a user creating an offer to sell a domain. */
  OfferPlacedEvent = 'OFFER_PLACED_EVENT',
  /** An event triggered by a user removing his offer to sell a domain. */
  OfferRemovedEvent = 'OFFER_REMOVED_EVENT',
  /** An event triggered by a user updating an offer to sell a domain. */
  OfferUpdatedEvent = 'OFFER_UPDATED_EVENT',
  /** An event triggered by a user claiming a reverse record. */
  ReverseRecordClaimEvent = 'REVERSE_RECORD_CLAIM_EVENT',
  /** An event triggered by a user updating a reverse record. */
  ReverseRecordUpdateEvent = 'REVERSE_RECORD_UPDATE_EVENT'
}

/** Filters `Event`-s by their `type`. */
export type EventTypeFilter = {
  /** Specifies the exact values to be matched. */
  in?: InputMaybe<Array<EventType>>;
  /** Specifies the exact values to be excluded. */
  notIn?: InputMaybe<Array<EventType>>;
};

/** Complex object with filter conditions for filtering `Event`-s. */
export type EventsFilter = {
  /** Filters `Event`-s by related address (`sourceAddress`, `previousBidderAddress`, `participants`, `newOwner`, `sellerAddress` or `buyerAddress`). */
  address?: InputMaybe<AddressFilter>;
  /** Additional filters of the same type are joined using the `AND` operator. It's joined using AND with other filter properties. */
  and?: InputMaybe<Array<EventsFilter>>;
  /** Filters `Event`-s by block in which they were created. */
  block?: InputMaybe<AssociatedBlockFilter>;
  /** Filters `Event`-s by their `domainName`. */
  domainName?: InputMaybe<StringFilter>;
  /** Additional filters of the same type are joined using the `OR` operator. It's joined using AND with other filter properties. */
  or?: InputMaybe<Array<EventsFilter>>;
  /** Filters `Event`-s by their `price` (bought, sold, auctioned domain). */
  price?: InputMaybe<MutezFilter>;
  /** Filters `Event`-s by their `type`. */
  type?: InputMaybe<EventTypeFilter>;
};

/** Filter conditions for filtering `id` property. */
export type IdEqualityFilter = {
  /** Specifies the exact value to match. */
  equalTo?: InputMaybe<Scalars['String']>;
  /** Specifies a list of exact values to match. */
  in?: InputMaybe<Array<Scalars['String']>>;
  /** Specifies the exact value NOT to match. */
  notEqualTo?: InputMaybe<Scalars['String']>;
  /** Specifies a list of exact values NOT to match. */
  notIn?: InputMaybe<Array<Scalars['String']>>;
};

/** Filter conditions for filtering associated property of the type `Int!`. */
export type IntFilter = {
  /** Specifies the exact value to match. */
  equalTo?: InputMaybe<Scalars['Int']>;
  /** Matches values greater than the specified value. */
  greaterThan?: InputMaybe<Scalars['Int']>;
  /** Matches values greater than or equal to the specified value. */
  greaterThanOrEqualTo?: InputMaybe<Scalars['Int']>;
  /** Specifies a list of exact values to match. */
  in?: InputMaybe<Array<Scalars['Int']>>;
  /** Matches values less than the specified value. */
  lessThan?: InputMaybe<Scalars['Int']>;
  /** Matches values less than or equal to the specified value. */
  lessThanOrEqualTo?: InputMaybe<Scalars['Int']>;
  /** Specifies the exact value NOT to match. */
  notEqualTo?: InputMaybe<Scalars['Int']>;
  /** Specifies a list of exact values NOT to match. */
  notIn?: InputMaybe<Array<Scalars['Int']>>;
};

/** Filters `Domain`-s by characters within their `Label`. */
export type LabelCompositionFilter = {
  /** Specifies the exact values to be matched. */
  in?: InputMaybe<Array<LabelCompositionType>>;
  /** Specifies the exact values to be excluded. */
  notIn?: InputMaybe<Array<LabelCompositionType>>;
};

export enum LabelCompositionType {
  Dash = 'DASH',
  Letters = 'LETTERS',
  Numbers = 'NUMBERS'
}

/** Filter conditions for filtering associated property of the type `Mutez!`. */
export type MutezFilter = {
  /** Specifies the exact value to match. */
  equalTo?: InputMaybe<Scalars['Mutez']>;
  /** Matches values greater than the specified value. */
  greaterThan?: InputMaybe<Scalars['Mutez']>;
  /** Matches values greater than or equal to the specified value. */
  greaterThanOrEqualTo?: InputMaybe<Scalars['Mutez']>;
  /** Specifies a list of exact values to match. */
  in?: InputMaybe<Array<Scalars['Mutez']>>;
  /** Matches values less than the specified value. */
  lessThan?: InputMaybe<Scalars['Mutez']>;
  /** Matches values less than or equal to the specified value. */
  lessThanOrEqualTo?: InputMaybe<Scalars['Mutez']>;
  /** Specifies the exact value NOT to match. */
  notEqualTo?: InputMaybe<Scalars['Mutez']>;
  /** Specifies a list of exact values NOT to match. */
  notIn?: InputMaybe<Array<Scalars['Mutez']>>;
};

/** Common interface with a globally unique ID. */
export type Node = {
  /** Unique global ID of the object. */
  id: Scalars['ID'];
};

/** Filter conditions for filtering associated property of the type `Address`. */
export type NullableAddressFilter = {
  /** Specifies the exact value to match. */
  equalTo?: InputMaybe<Scalars['Address']>;
  /** Specifies a list of exact values to match. */
  in?: InputMaybe<Array<Scalars['Address']>>;
  /** Matches `null` value if `true` is specified. Matches not `null` values if `false` is specified. Matches everything if nothing is specified. */
  isNull?: InputMaybe<Scalars['Boolean']>;
  /** Specifies the exact value NOT to match. */
  notEqualTo?: InputMaybe<Scalars['Address']>;
  /** Specifies a list of exact values NOT to match. */
  notIn?: InputMaybe<Array<Scalars['Address']>>;
  /** Specifies a prefix of the `Address` to match. */
  startsWith?: InputMaybe<AddressPrefix>;
};

/** Filter conditions for filtering associated property of the type `DateTime`. */
export type NullableDateTimeFilter = {
  /** Specifies the exact value to match. */
  equalTo?: InputMaybe<Scalars['DateTime']>;
  /** Matches values greater than the specified value. */
  greaterThan?: InputMaybe<Scalars['DateTime']>;
  /** Matches values greater than or equal to the specified value. */
  greaterThanOrEqualTo?: InputMaybe<Scalars['DateTime']>;
  /** Specifies a list of exact values to match. */
  in?: InputMaybe<Array<Scalars['DateTime']>>;
  /** Matches `null` value if `true` is specified. Matches not `null` values if `false` is specified. Matches everything if nothing is specified. */
  isNull?: InputMaybe<Scalars['Boolean']>;
  /** Matches values less than the specified value. */
  lessThan?: InputMaybe<Scalars['DateTime']>;
  /** Matches values less than or equal to the specified value. */
  lessThanOrEqualTo?: InputMaybe<Scalars['DateTime']>;
  /** Specifies the exact value NOT to match. */
  notEqualTo?: InputMaybe<Scalars['DateTime']>;
  /** Specifies a list of exact values NOT to match. */
  notIn?: InputMaybe<Array<Scalars['DateTime']>>;
};

/** Filter conditions for filtering associated property of the type `String`. */
export type NullableStringFilter = {
  /** Specifies the suffix of the value to match. */
  endsWith?: InputMaybe<Scalars['String']>;
  /** Specifies the exact value to match. */
  equalTo?: InputMaybe<Scalars['String']>;
  /** Specifies a list of exact values to match. */
  in?: InputMaybe<Array<Scalars['String']>>;
  /** Matches `null` value if `true` is specified. Matches not `null` values if `false` is specified. Matches everything if nothing is specified. */
  isNull?: InputMaybe<Scalars['Boolean']>;
  /** Specifies the value length to match. */
  length?: InputMaybe<StringLengthFilter>;
  /** Specifies the similar string to the value to match. */
  like?: InputMaybe<Scalars['String']>;
  /** Specifies the exact value NOT to match. */
  notEqualTo?: InputMaybe<Scalars['String']>;
  /** Specifies a list of exact values NOT to match. */
  notIn?: InputMaybe<Array<Scalars['String']>>;
  /** Specifies the prefix of the value to match. */
  startsWith?: InputMaybe<Scalars['String']>;
};

/** Domain sell offer. */
export type Offer = Node & {
  __typename?: 'Offer';
  /** Domain buyer address. */
  buyerAddress?: Maybe<Scalars['Address']>;
  /** `ReverseRecord` corresponding to `buyerAddress`. */
  buyerAddressReverseRecord?: Maybe<ReverseRecord>;
  /** Offer creation. */
  createdAtUtc: Scalars['DateTime'];
  /** Domain data. */
  domain?: Maybe<DomainData>;
  /** Offer expiration. */
  expiresAtUtc?: Maybe<Scalars['DateTime']>;
  /** Domain's offer fee. */
  fee: Scalars['Mutez'];
  /** Unique global ID of the object. */
  id: Scalars['ID'];
  /** The hash of the operation group that contained this/last change. */
  operationGroupHash: Scalars['String'];
  /** Total price for the domain. */
  price: Scalars['Mutez'];
  /** Amount to buy the domain without a fee. */
  priceWithoutFee: Scalars['Mutez'];
  /** Domain seller address. */
  sellerAddress: Scalars['Address'];
  /** `ReverseRecord` corresponding to `sellerAddress`. */
  sellerAddressReverseRecord?: Maybe<ReverseRecord>;
  /** Offer state. */
  state: OfferState;
  /** NFT token contract. */
  tokenContract: Scalars['String'];
  /** Domain NFT token id. */
  tokenId: Scalars['Int'];
};

/** A relay connection from an object to a list of objects of a particular type. For more details, see: `https://relay.dev/graphql/connections.htm` */
export type OfferConnection = {
  __typename?: 'OfferConnection';
  /** A list of all of the edges returned in the connection. */
  edges: Array<OfferEdge>;
  /** A list of all of the objects returned in the connection. This is a convenience field provided for quickly exploring the API; rather than querying for `{ edges { node } }` when no edge data is needed, this field can be used instead. Note that when clients like Relay need to fetch the `cursor` field on edge to enable efficient pagination, this shortcut cannot be used, and the full `{ edges { node } }` version should be used instead. */
  items: Array<Offer>;
  /** Information to aid in pagination. */
  pageInfo: PageInfo;
  /** A count of the total number of objects in this connection, ignoring pagination. This allows a client to fetch the first five objects by passing `5` as the argument to `first`, then fetch the total count so it could display `5 of 83`, for example. */
  totalCount: Scalars['Int'];
};

/** An edge in a connection from an object to another object of a particular type. */
export type OfferEdge = {
  __typename?: 'OfferEdge';
  /** A cursor for use in pagination */
  cursor: Scalars['String'];
  /** The item at the end of the edge */
  node: Offer;
};

/** An event triggered by a user accepting an existing offer to buy a domain. */
export type OfferExecutedEvent = Event & Node & TezosDocument & {
  __typename?: 'OfferExecutedEvent';
  /** Block in which last change happened. */
  block: Block;
  /** The domain name. */
  domainName: Scalars['String'];
  /** The domain selling fee. */
  fee: Scalars['Mutez'];
  /** Unique global ID of the object. */
  id: Scalars['ID'];
  /** The hash of the operation group that contained this/last change. */
  operationGroupHash: Scalars['String'];
  /** The sold domain price. */
  price: Scalars['Mutez'];
  /** The sold domain price without the fee. */
  priceWithoutFee: Scalars['Mutez'];
  /** The seller of the bought domain. */
  sellerAddress: Scalars['Address'];
  /** `ReverseRecord` corresponding to `sellerAddress`. */
  sellerAddressReverseRecord?: Maybe<ReverseRecord>;
  /** An address that triggered the event. */
  sourceAddress: Scalars['Address'];
  /** `ReverseRecord` corresponding to `sourceAddress`. */
  sourceAddressReverseRecord?: Maybe<ReverseRecord>;
  /** The domain token id. */
  tokenId: Scalars['Int'];
  /** Event type. */
  type: EventType;
};

/** Ordering options for `Offer`-s. */
export type OfferOrder = {
  /** The ordering direction. Default is `ASC`. */
  direction?: InputMaybe<OrderDirection>;
  /** The field to order by. */
  field: OfferOrderField;
};

/** Properties by which Offers are ordered. */
export enum OfferOrderField {
  /** Order offers by offer creation `DateTime`. */
  CreatedAt = 'CREATED_AT',
  /** Order offers by the domain name. */
  DomainName = 'DOMAIN_NAME',
  /** Order offers by offer expiration `DateTime`. */
  EndsAt = 'ENDS_AT',
  /** Order offers by id. */
  Id = 'ID',
  /** Order offers by price. */
  Price = 'PRICE',
  /** Order offers by token id. */
  TokenId = 'TOKEN_ID'
}

/** An event triggered by a user creating an offer to sell a domain. */
export type OfferPlacedEvent = Event & Node & TezosDocument & {
  __typename?: 'OfferPlacedEvent';
  /** Block in which last change happened. */
  block: Block;
  /** The domain name. */
  domainName: Scalars['String'];
  /** The offer expiration date. */
  expiresAtUtc?: Maybe<Scalars['DateTime']>;
  /** The domain selling fee. */
  fee: Scalars['Mutez'];
  /** Unique global ID of the object. */
  id: Scalars['ID'];
  /** The hash of the operation group that contained this/last change. */
  operationGroupHash: Scalars['String'];
  /** The price to buy the domain. */
  price: Scalars['Mutez'];
  /** The price part which goes to the seller. */
  priceWithoutFee: Scalars['Mutez'];
  /** An address that triggered the event. */
  sourceAddress: Scalars['Address'];
  /** `ReverseRecord` corresponding to `sourceAddress`. */
  sourceAddressReverseRecord?: Maybe<ReverseRecord>;
  /** The domain token id. */
  tokenId: Scalars['Int'];
  /** Event type. */
  type: EventType;
};

/** An event triggered by a user removing his offer to sell a domain. */
export type OfferRemovedEvent = Event & Node & TezosDocument & {
  __typename?: 'OfferRemovedEvent';
  /** Block in which last change happened. */
  block: Block;
  /** The domain name. */
  domainName: Scalars['String'];
  /** Unique global ID of the object. */
  id: Scalars['ID'];
  /** The hash of the operation group that contained this/last change. */
  operationGroupHash: Scalars['String'];
  /** An address that triggered the event. */
  sourceAddress: Scalars['Address'];
  /** `ReverseRecord` corresponding to `sourceAddress`. */
  sourceAddressReverseRecord?: Maybe<ReverseRecord>;
  /** The domain token id. */
  tokenId: Scalars['Int'];
  /** Event type. */
  type: EventType;
};

export enum OfferState {
  Active = 'ACTIVE',
  DomainDoesNotExist = 'DOMAIN_DOES_NOT_EXIST',
  DomainExpired = 'DOMAIN_EXPIRED',
  DomainIsExpiringSoon = 'DOMAIN_IS_EXPIRING_SOON',
  DomainOperatorsContractMissing = 'DOMAIN_OPERATORS_CONTRACT_MISSING',
  Executed = 'EXECUTED',
  OfferExpired = 'OFFER_EXPIRED',
  OfferSellerDomainOwnerMismatch = 'OFFER_SELLER_DOMAIN_OWNER_MISMATCH',
  Removed = 'REMOVED'
}

/** Filters `Offer`-s by their `state`. */
export type OfferStateFilter = {
  /** Specifies the exact values to be matched. */
  in?: InputMaybe<Array<OfferState>>;
  /** Specifies the exact values to be excluded. */
  notIn?: InputMaybe<Array<OfferState>>;
};

/** An event triggered by a user updating an offer to sell a domain. */
export type OfferUpdatedEvent = Event & Node & TezosDocument & {
  __typename?: 'OfferUpdatedEvent';
  /** Block in which last change happened. */
  block: Block;
  /** The domain name. */
  domainName: Scalars['String'];
  /** The offer expiration date. */
  expiresAtUtc?: Maybe<Scalars['DateTime']>;
  /** The domain selling fee. */
  fee: Scalars['Mutez'];
  /** Unique global ID of the object. */
  id: Scalars['ID'];
  /** The hash of the operation group that contained this/last change. */
  operationGroupHash: Scalars['String'];
  /** The price to buy the domain. */
  price: Scalars['Mutez'];
  /** The price part which goes to the seller. */
  priceWithoutFee: Scalars['Mutez'];
  /** An address that triggered the event. */
  sourceAddress: Scalars['Address'];
  /** `ReverseRecord` corresponding to `sourceAddress`. */
  sourceAddressReverseRecord?: Maybe<ReverseRecord>;
  /** The domain token id. */
  tokenId: Scalars['Int'];
  /** Event type. */
  type: EventType;
};

/** Complex object with filter conditions for filtering `Offer`-s. */
export type OffersFilter = {
  /** Additional filters of the same type are joined using the `AND` operator. It's joined using AND with other filter properties. */
  and?: InputMaybe<Array<OffersFilter>>;
  /** Filters `Offer`-s by their `buyerAddress`. */
  buyerAddress?: InputMaybe<AddressFilter>;
  /** Filters `Offer`-s by their `createdAtUtc`. */
  createdAtUtc?: InputMaybe<DateTimeFilter>;
  /** Filters `Offer`-s by `name` of offered `domain`. */
  domainName?: InputMaybe<StringFilter>;
  /** Filters `Offer`-s by their `expiresAtUtc`. */
  endsAtUtc?: InputMaybe<NullableDateTimeFilter>;
  /** Additional filters of the same type are joined using the `OR` operator. It's joined using AND with other filter properties. */
  or?: InputMaybe<Array<OffersFilter>>;
  /** Filters `Offer`-s by their `price`. */
  price?: InputMaybe<MutezFilter>;
  /** Filters `Offer`-s by their `sellerAddress`. */
  sellerAddress?: InputMaybe<AddressFilter>;
  /** Filters `Offer`-s by their `state`. */
  state?: InputMaybe<OfferStateFilter>;
  /** Filters `Offer`-s by their `tokenId`. */
  tokenId?: InputMaybe<IntFilter>;
};

/** Possible directions in which to order a list of items when provided. Default is: `ASC`. */
export enum OrderDirection {
  /** Specifies an ascending order for a given `orderBy` argument. */
  Asc = 'ASC',
  /** Specifies a descending order for a given `orderBy` argument. */
  Desc = 'DESC'
}

/** Information about pagination in a connection. */
export type PageInfo = {
  __typename?: 'PageInfo';
  /** When paginating forwards, the cursor to continue. */
  endCursor?: Maybe<Scalars['String']>;
  /** When paginating forwards, are there more items? */
  hasNextPage: Scalars['Boolean'];
  /** When paginating backwards, are there more items? */
  hasPreviousPage: Scalars['Boolean'];
  /** When paginating backwards, the cursor to continue. */
  startCursor?: Maybe<Scalars['String']>;
};

/** Root node with entry queries. */
export type Query = {
  __typename?: 'Query';
  /** Finds a single `Auction` by its unique `domainName` and `startedAtLevel`. */
  auction?: Maybe<Auction>;
  /** Finds all `Auction`-s corresponding to specified filters. */
  auctions: AuctionConnection;
  /** Finds bidder balances for the given address. */
  bidderBalances: BidderBalances;
  /** Finds the latest indexed `Block` (head) or a specific `Block` by its `hash` or `level` when supplied. Note: Only blocks starting with the origination of the smart contracts are available. */
  block?: Maybe<Block>;
  /** Finds a single `Offer` by its `buyerAddress`, `tokenId`, and offer creation block level. */
  buyOffer?: Maybe<BuyOffer>;
  /** Finds all `Offer`-s corresponding to specified filters. */
  buyOffers: BuyOfferConnection;
  /** Finds a single `Auction` by its `domainName` in current/specified time. */
  currentAuction?: Maybe<Auction>;
  /** Finds a single valid `Offer` by its `domain` name. */
  currentBuyOffer?: Maybe<BuyOffer>;
  /** Finds a single valid `Offer` by its `domain` name. */
  currentOffer?: Maybe<Offer>;
  /** Finds a single `Domain`` by its unique `name` and specified filters. */
  domain?: Maybe<Domain>;
  /** Finds all `Domain`-s corresponding to specified filters. */
  domains: DomainConnection;
  /** Finds all `Event`-s corresponding to specified filters. */
  events: EventConnection;
  /** Finds a single `Offer` by its `sellerAddress`, `tokenId`, and offer creation block level. */
  offer?: Maybe<Offer>;
  /** Finds all `Offer`-s corresponding to specified filters. */
  offers: OfferConnection;
  /** Finds a single ReverseRecord by its unique `address` and specified filters. */
  reverseRecord?: Maybe<ReverseRecord>;
  /** Finds all `ReverseRecord`-s corresponding to specified filters. */
  reverseRecords: ReverseRecordConnection;
};


/** Root node with entry queries. */
export type QueryAuctionArgs = {
  atBlock?: InputMaybe<BlockHistoryFilter>;
  domainName: Scalars['String'];
  startedAtLevel: Scalars['Int'];
};


/** Root node with entry queries. */
export type QueryAuctionsArgs = {
  after?: InputMaybe<Scalars['String']>;
  atBlock?: InputMaybe<BlockHistoryFilter>;
  before?: InputMaybe<Scalars['String']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  order?: InputMaybe<AuctionOrder>;
  where?: InputMaybe<AuctionsFilter>;
};


/** Root node with entry queries. */
export type QueryBidderBalancesArgs = {
  address: Scalars['String'];
  atBlock?: InputMaybe<BlockHistoryFilter>;
};


/** Root node with entry queries. */
export type QueryBlockArgs = {
  hash?: InputMaybe<Scalars['String']>;
  level?: InputMaybe<Scalars['Int']>;
};


/** Root node with entry queries. */
export type QueryBuyOfferArgs = {
  atBlock?: InputMaybe<BlockHistoryFilter>;
  buyerAddress: Scalars['Address'];
  startedAtLevel: Scalars['Int'];
  tokenId: Scalars['Int'];
};


/** Root node with entry queries. */
export type QueryBuyOffersArgs = {
  after?: InputMaybe<Scalars['String']>;
  atBlock?: InputMaybe<BlockHistoryFilter>;
  before?: InputMaybe<Scalars['String']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  order?: InputMaybe<OfferOrder>;
  where?: InputMaybe<BuyOffersFilter>;
};


/** Root node with entry queries. */
export type QueryCurrentAuctionArgs = {
  atBlock?: InputMaybe<BlockHistoryFilter>;
  domainName: Scalars['String'];
};


/** Root node with entry queries. */
export type QueryCurrentBuyOfferArgs = {
  atBlock?: InputMaybe<BlockHistoryFilter>;
  buyerAddress: Scalars['Address'];
  domainName: Scalars['String'];
};


/** Root node with entry queries. */
export type QueryCurrentOfferArgs = {
  atBlock?: InputMaybe<BlockHistoryFilter>;
  domainName: Scalars['String'];
};


/** Root node with entry queries. */
export type QueryDomainArgs = {
  atBlock?: InputMaybe<BlockHistoryFilter>;
  name: Scalars['String'];
  validity?: InputMaybe<RecordValidity>;
};


/** Root node with entry queries. */
export type QueryDomainsArgs = {
  after?: InputMaybe<Scalars['String']>;
  atBlock?: InputMaybe<BlockHistoryFilter>;
  before?: InputMaybe<Scalars['String']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  order?: InputMaybe<DomainOrder>;
  where?: InputMaybe<DomainsFilter>;
};


/** Root node with entry queries. */
export type QueryEventsArgs = {
  after?: InputMaybe<Scalars['String']>;
  before?: InputMaybe<Scalars['String']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  order?: InputMaybe<EventOrder>;
  where?: InputMaybe<EventsFilter>;
};


/** Root node with entry queries. */
export type QueryOfferArgs = {
  atBlock?: InputMaybe<BlockHistoryFilter>;
  sellerAddress: Scalars['Address'];
  startedAtLevel: Scalars['Int'];
  tokenId: Scalars['Int'];
};


/** Root node with entry queries. */
export type QueryOffersArgs = {
  after?: InputMaybe<Scalars['String']>;
  atBlock?: InputMaybe<BlockHistoryFilter>;
  before?: InputMaybe<Scalars['String']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  order?: InputMaybe<OfferOrder>;
  where?: InputMaybe<OffersFilter>;
};


/** Root node with entry queries. */
export type QueryReverseRecordArgs = {
  address: Scalars['String'];
  atBlock?: InputMaybe<BlockHistoryFilter>;
  validity?: InputMaybe<RecordValidity>;
};


/** Root node with entry queries. */
export type QueryReverseRecordsArgs = {
  after?: InputMaybe<Scalars['String']>;
  atBlock?: InputMaybe<BlockHistoryFilter>;
  before?: InputMaybe<Scalars['String']>;
  first?: InputMaybe<Scalars['Int']>;
  last?: InputMaybe<Scalars['Int']>;
  order?: InputMaybe<ReverseRecordOrder>;
  where?: InputMaybe<ReverseRecordsFilter>;
};

/** Validity of the record based on its validity `DateTime`. */
export enum RecordValidity {
  /** All records regardless of their validity `DateTime`. */
  All = 'ALL',
  /** Validity `DateTime` is in the past. */
  Expired = 'EXPIRED',
  /** Validity `DateTime` is in the future or null (domain does not expire at all). */
  Valid = 'VALID'
}

/** Reverse record. */
export type ReverseRecord = Node & {
  __typename?: 'ReverseRecord';
  /** Reverse record address. */
  address: Scalars['Address'];
  /** `Domain` corresponding to the `name` of this record. */
  domain?: Maybe<Domain>;
  /** Reverse record validity. */
  expiresAtUtc?: Maybe<Scalars['DateTime']>;
  /** Unique global ID of the object. */
  id: Scalars['ID'];
  /** The hash of the operation group that contained this/last change. */
  operationGroupHash: Scalars['String'];
  /** Reverse record owner. */
  owner: Scalars['Address'];
  /** `ReverseRecord` corresponding to `owner`. */
  ownerReverseRecord?: Maybe<ReverseRecord>;
};

/** An event triggered by a user claiming a reverse record. */
export type ReverseRecordClaimEvent = Event & Node & TezosDocument & {
  __typename?: 'ReverseRecordClaimEvent';
  /** Block in which last change happened. */
  block: Block;
  /** Unique global ID of the object. */
  id: Scalars['ID'];
  /** The domain name. */
  name?: Maybe<Scalars['String']>;
  /** The hash of the operation group that contained this/last change. */
  operationGroupHash: Scalars['String'];
  /** The owner of the bought domain. */
  reverseRecordOwnerAddress: Scalars['Address'];
  /** An address that triggered the event. */
  sourceAddress: Scalars['Address'];
  /** `ReverseRecord` corresponding to `sourceAddress`. */
  sourceAddressReverseRecord?: Maybe<ReverseRecord>;
  /** Event type. */
  type: EventType;
};

/** A relay connection from an object to a list of objects of a particular type. For more details, see: `https://relay.dev/graphql/connections.htm` */
export type ReverseRecordConnection = {
  __typename?: 'ReverseRecordConnection';
  /** A list of all of the edges returned in the connection. */
  edges: Array<ReverseRecordEdge>;
  /** A list of all of the objects returned in the connection. This is a convenience field provided for quickly exploring the API; rather than querying for `{ edges { node } }` when no edge data is needed, this field can be used instead. Note that when clients like Relay need to fetch the `cursor` field on edge to enable efficient pagination, this shortcut cannot be used, and the full `{ edges { node } }` version should be used instead. */
  items: Array<ReverseRecord>;
  /** Information to aid in pagination. */
  pageInfo: PageInfo;
  /** A count of the total number of objects in this connection, ignoring pagination. This allows a client to fetch the first five objects by passing `5` as the argument to `first`, then fetch the total count so it could display `5 of 83`, for example. */
  totalCount: Scalars['Int'];
};

/** An edge in a connection from an object to another object of a particular type. */
export type ReverseRecordEdge = {
  __typename?: 'ReverseRecordEdge';
  /** A cursor for use in pagination */
  cursor: Scalars['String'];
  /** The item at the end of the edge */
  node: ReverseRecord;
};

/** Ordering options for `ReverseRecord`-s. */
export type ReverseRecordOrder = {
  /** The ordering direction. Default is `ASC`. */
  direction?: InputMaybe<OrderDirection>;
  /** The field to order by. */
  field: ReverseRecordOrderField;
};

/** Properties by which ReverseRecords are ordered. */
export enum ReverseRecordOrderField {
  /** Order reverse records by address. */
  Address = 'ADDRESS',
  /** Order reverse records by validity. */
  ExpiresAt = 'EXPIRES_AT',
  /** Order reverse records by the domain name. */
  Name = 'NAME',
  /** Order reverse records by owner. */
  Owner = 'OWNER'
}

/** An event triggered by a user updating a reverse record. */
export type ReverseRecordUpdateEvent = Event & Node & TezosDocument & {
  __typename?: 'ReverseRecordUpdateEvent';
  /** Block in which last change happened. */
  block: Block;
  /** Unique global ID of the object. */
  id: Scalars['ID'];
  /** The domain name. */
  name?: Maybe<Scalars['String']>;
  /** The hash of the operation group that contained this/last change. */
  operationGroupHash: Scalars['String'];
  /** The forward record for the bought domain. */
  reverseRecordAddress: Scalars['Address'];
  /** The owner of the bought domain. */
  reverseRecordOwnerAddress: Scalars['Address'];
  /** An address that triggered the event. */
  sourceAddress: Scalars['Address'];
  /** `ReverseRecord` corresponding to `sourceAddress`. */
  sourceAddressReverseRecord?: Maybe<ReverseRecord>;
  /** Event type. */
  type: EventType;
};

/** Complex object with filter conditions for filtering `ReverseRecord`-s. */
export type ReverseRecordsFilter = {
  /** Filters `ReverseRecord`-s by their `address`. */
  address?: InputMaybe<AddressFilter>;
  /** Additional filters of the same type are joined using the `AND` operator. It's joined using AND with other filter properties. */
  and?: InputMaybe<Array<ReverseRecordsFilter>>;
  /** Filters items by their `expiresAtUtc`. */
  expiresAtUtc?: InputMaybe<NullableDateTimeFilter>;
  /** Filters `ReverseRecord`-s by their `name`. */
  name?: InputMaybe<NullableStringFilter>;
  /** Additional filters of the same type are joined using the `OR` operator. It's joined using AND with other filter properties. */
  or?: InputMaybe<Array<ReverseRecordsFilter>>;
  /** Filters `ReverseRecord`-s by their `owner`. */
  owner?: InputMaybe<AddressFilter>;
  /** Filters validity of the entity based on its validity `DateTime`. Default is `VALID`. */
  validity?: InputMaybe<RecordValidity>;
};

/** Filter conditions for filtering associated property of the type `[String!]`. */
export type StringArrayFilter = {
  /** Specifies a value that should be included in the array of the associated property. */
  include?: InputMaybe<Scalars['String']>;
};

/** Filter conditions for filtering block `hash` property. */
export type StringEqualityFilter = {
  /** Specifies the exact value to match. */
  equalTo?: InputMaybe<Scalars['String']>;
  /** Specifies a list of exact values to match. */
  in?: InputMaybe<Array<Scalars['String']>>;
  /** Specifies the exact value NOT to match. */
  notEqualTo?: InputMaybe<Scalars['String']>;
  /** Specifies a list of exact values NOT to match. */
  notIn?: InputMaybe<Array<Scalars['String']>>;
};

/** Filter conditions for filtering associated property of the type `String!`. */
export type StringFilter = {
  /** Specifies the suffix of the value to match. */
  endsWith?: InputMaybe<Scalars['String']>;
  /** Specifies the exact value to match. */
  equalTo?: InputMaybe<Scalars['String']>;
  /** Specifies a list of exact values to match. */
  in?: InputMaybe<Array<Scalars['String']>>;
  /** Specifies the value length to match. */
  length?: InputMaybe<StringLengthFilter>;
  /** Specifies the similar string to the value to match. */
  like?: InputMaybe<Scalars['String']>;
  /** Specifies the exact value NOT to match. */
  notEqualTo?: InputMaybe<Scalars['String']>;
  /** Specifies a list of exact values NOT to match. */
  notIn?: InputMaybe<Array<Scalars['String']>>;
  /** Specifies the prefix of the value to match. */
  startsWith?: InputMaybe<Scalars['String']>;
};

/** Filter conditions for filtering length of associated property of type `String`. */
export type StringLengthFilter = {
  /** Specifies a string length to match. */
  equalTo?: InputMaybe<Scalars['Int']>;
  /** Specifies a longer string than. */
  greaterThanOrEqualTo?: InputMaybe<Scalars['Int']>;
  /** Specifies a shorter string than. */
  lessThanOrEqualTo?: InputMaybe<Scalars['Int']>;
};

/** An action in Tezos Domains describing an interaction with a domain or an auction. */
export type TezosDocument = {
  /** `Block` in which the last change happened. */
  block: Block;
};

export type DomainExistsQueryVariables = Exact<{
  name: Scalars['String'];
}>;


export type DomainExistsQuery = { __typename?: 'Query', domain?: { __typename?: 'Domain', name: string, address?: string | null } | null };

export type ClaimableTlDsListQueryVariables = Exact<{
  where: DomainsFilter;
}>;


export type ClaimableTlDsListQuery = { __typename?: 'Query', domains: { __typename?: 'DomainConnection', edges: Array<{ __typename?: 'DomainEdge', node: { __typename?: 'Domain', name: string } }> } };

export type DomainDetailQueryVariables = Exact<{
  name: Scalars['String'];
  domainValidity?: InputMaybe<RecordValidity>;
}>;


export type DomainDetailQuery = { __typename?: 'Query', domain?: { __typename?: 'Domain', id: string, name: string, address?: string | null, owner: string, parentOwner?: string | null, tokenId?: number | null, expires?: dayjs.Dayjs | null, operators: Array<{ __typename?: 'DomainOperator', id: string, address: string }>, data: Array<{ __typename?: 'DataItem', id: string, key: string, rawValue: string }>, subdomains: { __typename?: 'DomainConnection', totalCount: number }, reverseRecord?: { __typename?: 'ReverseRecord', id: string, owner: string, address: string } | null, ownerReverseRecord?: { __typename?: 'ReverseRecord', id: string, domain?: { __typename?: 'Domain', id: string, name: string, data: Array<{ __typename?: 'DataItem', id: string, key: string, rawValue: string }> } | null } | null, addressReverseRecord?: { __typename?: 'ReverseRecord', id: string, domain?: { __typename?: 'Domain', id: string, name: string, data: Array<{ __typename?: 'DataItem', id: string, key: string, rawValue: string }> } | null } | null } | null, auction?: { __typename?: 'Auction', id: string, state: AuctionState, endsAt: dayjs.Dayjs, ownedUntil: dayjs.Dayjs, highestBid: { __typename?: 'Bid', id: string, amount: BigNumber, bidder: string }, bids: Array<{ __typename?: 'Bid', id: string, amount: BigNumber, bidder: string, timestamp: dayjs.Dayjs, bidderReverseRecord?: { __typename?: 'ReverseRecord', id: string, domain?: { __typename?: 'Domain', id: string, name: string, data: Array<{ __typename?: 'DataItem', id: string, key: string, rawValue: string }> } | null } | null }> } | null, lastAuction: { __typename?: 'AuctionConnection', edges: Array<{ __typename?: 'AuctionEdge', node: { __typename?: 'Auction', id: string, state: AuctionState, endsAt: dayjs.Dayjs, ownedUntil: dayjs.Dayjs, highestBid: { __typename?: 'Bid', id: string, amount: BigNumber, bidder: string } } }> }, offer?: { __typename?: 'Offer', id: string, tokenContract: string, tokenId: number, price: BigNumber, state: OfferState, seller: string, expiration?: dayjs.Dayjs | null, domain?: { __typename?: 'DomainData', id: string, name: string, owner: string, expires?: dayjs.Dayjs | null, data: Array<{ __typename?: 'DataItem', id: string, key: string, rawValue: string }> } | null, sellerReverseRecord?: { __typename?: 'ReverseRecord', id: string, domain?: { __typename?: 'Domain', id: string, name: string, data: Array<{ __typename?: 'DataItem', id: string, key: string, rawValue: string }> } | null } | null } | null };

export type CurrentBuyOfferQueryVariables = Exact<{
  name: Scalars['String'];
  address: Scalars['Address'];
}>;


export type CurrentBuyOfferQuery = { __typename?: 'Query', currentBuyOffer?: { __typename?: 'BuyOffer', id: string, tokenContract: string, tokenId: number, price: BigNumber, priceWithoutFee: BigNumber, state: OfferState, seller?: string | null, buyer: string, expiration?: dayjs.Dayjs | null, domain?: { __typename?: 'DomainData', id: string, name: string, owner: string, expires?: dayjs.Dayjs | null, data: Array<{ __typename?: 'DataItem', id: string, key: string, rawValue: string }> } | null, buyerReverseRecord?: { __typename?: 'ReverseRecord', id: string, domain?: { __typename?: 'Domain', id: string, name: string, data: Array<{ __typename?: 'DataItem', id: string, key: string, rawValue: string }> } | null } | null } | null };

export type AuctionDetailQueryVariables = Exact<{
  name: Scalars['String'];
  startedAtLevel: Scalars['Int'];
}>;


export type AuctionDetailQuery = { __typename?: 'Query', auction?: { __typename?: 'Auction', id: string, endsAtUtc: dayjs.Dayjs, state: AuctionState, highestBid: { __typename?: 'Bid', id: string, amount: BigNumber, bidder: string, bidderReverseRecord?: { __typename?: 'ReverseRecord', id: string, domain?: { __typename?: 'Domain', id: string, name: string, data: Array<{ __typename?: 'DataItem', id: string, key: string, rawValue: string }> } | null } | null }, bids: Array<{ __typename?: 'Bid', id: string, amount: BigNumber, bidder: string, timestamp: dayjs.Dayjs, bidderReverseRecord?: { __typename?: 'ReverseRecord', id: string, domain?: { __typename?: 'Domain', id: string, name: string, data: Array<{ __typename?: 'DataItem', id: string, key: string, rawValue: string }> } | null } | null }> } | null };

export type ReverseRecordDetailQueryVariables = Exact<{
  address: Scalars['String'];
}>;


export type ReverseRecordDetailQuery = { __typename?: 'Query', reverseRecord?: { __typename?: 'ReverseRecord', id: string, address: string, owner: string, domain?: { __typename?: 'Domain', id: string, name: string, data: Array<{ __typename?: 'DataItem', id: string, key: string, rawValue: string }> } | null, ownerReverseRecord?: { __typename?: 'ReverseRecord', id: string, domain?: { __typename?: 'Domain', id: string, name: string, data: Array<{ __typename?: 'DataItem', id: string, key: string, rawValue: string }> } | null } | null } | null };

export type DomainsListQueryVariables = Exact<{
  where: DomainsFilter;
  order?: InputMaybe<DomainOrder>;
  first?: InputMaybe<Scalars['Int']>;
  after?: InputMaybe<Scalars['String']>;
}>;


export type DomainsListQuery = { __typename?: 'Query', domains: { __typename?: 'DomainConnection', edges: Array<{ __typename?: 'DomainEdge', cursor: string, node: { __typename?: 'Domain', id: string, name: string, level: number, owner: string, address?: string | null, tokenId?: number | null, parentOwner?: string | null, expires?: dayjs.Dayjs | null, data: Array<{ __typename?: 'DataItem', id: string, key: string, rawValue: string }>, operators: Array<{ __typename?: 'DomainOperator', id: string, address: string }> } }>, pageInfo: { __typename?: 'PageInfo', startCursor?: string | null, endCursor?: string | null, hasNextPage: boolean, hasPreviousPage: boolean } } };

export type DomainsWithAuctionListQueryVariables = Exact<{
  where: DomainsFilter;
  order?: InputMaybe<DomainOrder>;
  first?: InputMaybe<Scalars['Int']>;
  after?: InputMaybe<Scalars['String']>;
}>;


export type DomainsWithAuctionListQuery = { __typename?: 'Query', domains: { __typename?: 'DomainConnection', edges: Array<{ __typename?: 'DomainEdge', cursor: string, node: { __typename?: 'Domain', id: string, name: string, level: number, owner: string, parentOwner?: string | null, expires?: dayjs.Dayjs | null, operators: Array<{ __typename?: 'DomainOperator', id: string, address: string }>, lastAuction?: { __typename?: 'Auction', id: string, state: AuctionState, endsAt: dayjs.Dayjs, ownedUntil: dayjs.Dayjs, highestBid: { __typename?: 'Bid', id: string, amount: BigNumber, bidder: string } } | null } }>, pageInfo: { __typename?: 'PageInfo', startCursor?: string | null, endCursor?: string | null, hasNextPage: boolean, hasPreviousPage: boolean } } };

export type ReverseRecordListQueryVariables = Exact<{
  where: ReverseRecordsFilter;
  order?: InputMaybe<ReverseRecordOrder>;
  first?: InputMaybe<Scalars['Int']>;
  after?: InputMaybe<Scalars['String']>;
}>;


export type ReverseRecordListQuery = { __typename?: 'Query', reverseRecords: { __typename?: 'ReverseRecordConnection', edges: Array<{ __typename?: 'ReverseRecordEdge', cursor: string, node: { __typename?: 'ReverseRecord', id: string, address: string, owner: string, domain?: { __typename?: 'Domain', id: string, name: string, data: Array<{ __typename?: 'DataItem', id: string, key: string, rawValue: string }> } | null } }>, pageInfo: { __typename?: 'PageInfo', startCursor?: string | null, endCursor?: string | null, hasNextPage: boolean, hasPreviousPage: boolean } } };

export type StatsQueryVariables = Exact<{
  owner?: InputMaybe<Scalars['Address']>;
  expiringThreshold: Scalars['DateTime'];
}>;


export type StatsQuery = { __typename?: 'Query', bought: { __typename?: 'DomainConnection', totalCount: number }, expiring: { __typename?: 'DomainConnection', totalCount: number }, total: { __typename?: 'DomainConnection', totalCount: number } };

export type UserDataQueryVariables = Exact<{
  address: Scalars['Address'];
}>;


export type UserDataQuery = { __typename?: 'Query', domains: { __typename?: 'DomainConnection', totalCount: number }, auctions: { __typename?: 'AuctionConnection', totalCount: number }, buyOffers: { __typename?: 'BuyOfferConnection', totalCount: number } };

export type EventsQueryVariables = Exact<{
  first?: InputMaybe<Scalars['Int']>;
  after?: InputMaybe<Scalars['String']>;
  last?: InputMaybe<Scalars['Int']>;
  before?: InputMaybe<Scalars['String']>;
  where?: InputMaybe<EventsFilter>;
}>;


export type EventsQuery = { __typename?: 'Query', events: { __typename?: 'EventConnection', edges: Array<{ __typename?: 'EventEdge', node: { __typename?: 'AuctionBidEvent', operationGroupHash: string, domainName: string, type: EventType, id: string, sourceAddress: string, amount: BigNumber, balance: BigNumber, block: { __typename?: 'Block', timestamp: dayjs.Dayjs }, sourceAddressReverseRecord?: { __typename?: 'ReverseRecord', id: string, domain?: { __typename?: 'Domain', id: string, name: string, data: Array<{ __typename?: 'DataItem', id: string, key: string, rawValue: string }> } | null } | null } | { __typename?: 'AuctionEndEvent', domainName: string, type: EventType, id: string, sourceAddress: string, amount: BigNumber, sourceAddressReverseRecord?: { __typename?: 'ReverseRecord', id: string, domain?: { __typename?: 'Domain', id: string, name: string, data: Array<{ __typename?: 'DataItem', id: string, key: string, rawValue: string }> } | null } | null, block: { __typename?: 'Block', timestamp: dayjs.Dayjs } } | { __typename?: 'AuctionSettleEvent', operationGroupHash: string, domainName: string, type: EventType, id: string, sourceAddress: string, durationInDays: number, amount: BigNumber, sourceAddressReverseRecord?: { __typename?: 'ReverseRecord', id: string, domain?: { __typename?: 'Domain', id: string, name: string, data: Array<{ __typename?: 'DataItem', id: string, key: string, rawValue: string }> } | null } | null, block: { __typename?: 'Block', timestamp: dayjs.Dayjs } } | { __typename?: 'AuctionWithdrawEvent', tldName: string, operationGroupHash: string, type: EventType, id: string, sourceAddress: string, amount: BigNumber, balance: BigNumber, sourceAddressReverseRecord?: { __typename?: 'ReverseRecord', id: string, domain?: { __typename?: 'Domain', id: string, name: string, data: Array<{ __typename?: 'DataItem', id: string, key: string, rawValue: string }> } | null } | null, block: { __typename?: 'Block', timestamp: dayjs.Dayjs } } | { __typename?: 'BuyOfferExecutedEvent', operationGroupHash: string, type: EventType, id: string, sourceAddress: string, maybeDomainName: string, amount: BigNumber, altBalance: BigNumber, secondaryAddress: string, secondaryReverseRecord?: { __typename?: 'ReverseRecord', id: string, domain?: { __typename?: 'Domain', id: string, name: string, data: Array<{ __typename?: 'DataItem', id: string, key: string, rawValue: string }> } | null } | null, sourceAddressReverseRecord?: { __typename?: 'ReverseRecord', id: string, domain?: { __typename?: 'Domain', id: string, name: string, data: Array<{ __typename?: 'DataItem', id: string, key: string, rawValue: string }> } | null } | null, block: { __typename?: 'Block', timestamp: dayjs.Dayjs } } | { __typename?: 'BuyOfferPlacedEvent', tokenId: number, operationGroupHash: string, type: EventType, id: string, sourceAddress: string, maybeDomainName: string, amount: BigNumber, sourceAddressReverseRecord?: { __typename?: 'ReverseRecord', id: string, domain?: { __typename?: 'Domain', id: string, name: string, data: Array<{ __typename?: 'DataItem', id: string, key: string, rawValue: string }> } | null } | null, block: { __typename?: 'Block', timestamp: dayjs.Dayjs } } | { __typename?: 'BuyOfferRemovedEvent', tokenId: number, operationGroupHash: string, type: EventType, id: string, sourceAddress: string, maybeDomainName: string, sourceAddressReverseRecord?: { __typename?: 'ReverseRecord', id: string, domain?: { __typename?: 'Domain', id: string, name: string, data: Array<{ __typename?: 'DataItem', id: string, key: string, rawValue: string }> } | null } | null, block: { __typename?: 'Block', timestamp: dayjs.Dayjs } } | { __typename?: 'DomainBuyEvent', operationGroupHash: string, domainName: string, durationInDays: number, type: EventType, id: string, sourceAddress: string, amount: BigNumber, balance: BigNumber, sourceAddressReverseRecord?: { __typename?: 'ReverseRecord', id: string, domain?: { __typename?: 'Domain', id: string, name: string, data: Array<{ __typename?: 'DataItem', id: string, key: string, rawValue: string }> } | null } | null, block: { __typename?: 'Block', timestamp: dayjs.Dayjs } } | { __typename?: 'DomainClaimEvent', domainName: string, operationGroupHash: string, type: EventType, id: string, sourceAddress: string, sourceAddressReverseRecord?: { __typename?: 'ReverseRecord', id: string, domain?: { __typename?: 'Domain', id: string, name: string, data: Array<{ __typename?: 'DataItem', id: string, key: string, rawValue: string }> } | null } | null, block: { __typename?: 'Block', timestamp: dayjs.Dayjs } } | { __typename?: 'DomainCommitEvent', operationGroupHash: string, type: EventType, id: string, sourceAddress: string, sourceAddressReverseRecord?: { __typename?: 'ReverseRecord', id: string, domain?: { __typename?: 'Domain', id: string, name: string, data: Array<{ __typename?: 'DataItem', id: string, key: string, rawValue: string }> } | null } | null, block: { __typename?: 'Block', timestamp: dayjs.Dayjs } } | { __typename?: 'DomainGrantEvent', domainName: string, operationGroupHash: string, type: EventType, id: string, sourceAddress: string, maybeDurationInDays?: number | null, sourceAddressReverseRecord?: { __typename?: 'ReverseRecord', id: string, domain?: { __typename?: 'Domain', id: string, name: string, data: Array<{ __typename?: 'DataItem', id: string, key: string, rawValue: string }> } | null } | null, block: { __typename?: 'Block', timestamp: dayjs.Dayjs } } | { __typename?: 'DomainRenewEvent', operationGroupHash: string, domainName: string, durationInDays: number, type: EventType, id: string, sourceAddress: string, amount: BigNumber, balance: BigNumber, sourceAddressReverseRecord?: { __typename?: 'ReverseRecord', id: string, domain?: { __typename?: 'Domain', id: string, name: string, data: Array<{ __typename?: 'DataItem', id: string, key: string, rawValue: string }> } | null } | null, block: { __typename?: 'Block', timestamp: dayjs.Dayjs } } | { __typename?: 'DomainSetChildRecordEvent', domainName: string, operationGroupHash: string, type: EventType, id: string, sourceAddress: string, isNew: boolean, sourceAddressReverseRecord?: { __typename?: 'ReverseRecord', id: string, domain?: { __typename?: 'Domain', id: string, name: string, data: Array<{ __typename?: 'DataItem', id: string, key: string, rawValue: string }> } | null } | null, block: { __typename?: 'Block', timestamp: dayjs.Dayjs } } | { __typename?: 'DomainTransferEvent', domainName: string, operationGroupHash: string, type: EventType, id: string, sourceAddress: string, secondaryAddress: string, secondaryReverseRecord?: { __typename?: 'ReverseRecord', id: string, domain?: { __typename?: 'Domain', id: string, name: string, data: Array<{ __typename?: 'DataItem', id: string, key: string, rawValue: string }> } | null } | null, sourceAddressReverseRecord?: { __typename?: 'ReverseRecord', id: string, domain?: { __typename?: 'Domain', id: string, name: string, data: Array<{ __typename?: 'DataItem', id: string, key: string, rawValue: string }> } | null } | null, block: { __typename?: 'Block', timestamp: dayjs.Dayjs } } | { __typename?: 'DomainUpdateEvent', domainName: string, operationGroupHash: string, type: EventType, id: string, sourceAddress: string, sourceAddressReverseRecord?: { __typename?: 'ReverseRecord', id: string, domain?: { __typename?: 'Domain', id: string, name: string, data: Array<{ __typename?: 'DataItem', id: string, key: string, rawValue: string }> } | null } | null, block: { __typename?: 'Block', timestamp: dayjs.Dayjs } } | { __typename?: 'DomainUpdateOperatorsEvent', domainName: string, operationGroupHash: string, type: EventType, id: string, sourceAddress: string, sourceAddressReverseRecord?: { __typename?: 'ReverseRecord', id: string, domain?: { __typename?: 'Domain', id: string, name: string, data: Array<{ __typename?: 'DataItem', id: string, key: string, rawValue: string }> } | null } | null, block: { __typename?: 'Block', timestamp: dayjs.Dayjs } } | { __typename?: 'OfferExecutedEvent', operationGroupHash: string, type: EventType, id: string, sourceAddress: string, maybeDomainName: string, amount: BigNumber, balance: BigNumber, altBalance: BigNumber, secondaryAddress: string, secondaryReverseRecord?: { __typename?: 'ReverseRecord', id: string, domain?: { __typename?: 'Domain', id: string, name: string, data: Array<{ __typename?: 'DataItem', id: string, key: string, rawValue: string }> } | null } | null, sourceAddressReverseRecord?: { __typename?: 'ReverseRecord', id: string, domain?: { __typename?: 'Domain', id: string, name: string, data: Array<{ __typename?: 'DataItem', id: string, key: string, rawValue: string }> } | null } | null, block: { __typename?: 'Block', timestamp: dayjs.Dayjs } } | { __typename?: 'OfferPlacedEvent', tokenId: number, operationGroupHash: string, type: EventType, id: string, sourceAddress: string, maybeDomainName: string, amount: BigNumber, sourceAddressReverseRecord?: { __typename?: 'ReverseRecord', id: string, domain?: { __typename?: 'Domain', id: string, name: string, data: Array<{ __typename?: 'DataItem', id: string, key: string, rawValue: string }> } | null } | null, block: { __typename?: 'Block', timestamp: dayjs.Dayjs } } | { __typename?: 'OfferRemovedEvent', tokenId: number, operationGroupHash: string, type: EventType, id: string, sourceAddress: string, maybeDomainName: string, sourceAddressReverseRecord?: { __typename?: 'ReverseRecord', id: string, domain?: { __typename?: 'Domain', id: string, name: string, data: Array<{ __typename?: 'DataItem', id: string, key: string, rawValue: string }> } | null } | null, block: { __typename?: 'Block', timestamp: dayjs.Dayjs } } | { __typename?: 'OfferUpdatedEvent', tokenId: number, operationGroupHash: string, type: EventType, id: string, sourceAddress: string, maybeDomainName: string, sourceAddressReverseRecord?: { __typename?: 'ReverseRecord', id: string, domain?: { __typename?: 'Domain', id: string, name: string, data: Array<{ __typename?: 'DataItem', id: string, key: string, rawValue: string }> } | null } | null, block: { __typename?: 'Block', timestamp: dayjs.Dayjs } } | { __typename?: 'ReverseRecordClaimEvent', name?: string | null, operationGroupHash: string, type: EventType, id: string, sourceAddress: string, reverseRecordAddress: string, sourceAddressReverseRecord?: { __typename?: 'ReverseRecord', id: string, domain?: { __typename?: 'Domain', id: string, name: string, data: Array<{ __typename?: 'DataItem', id: string, key: string, rawValue: string }> } | null } | null, block: { __typename?: 'Block', timestamp: dayjs.Dayjs } } | { __typename?: 'ReverseRecordUpdateEvent', name?: string | null, operationGroupHash: string, reverseRecordAddress: string, type: EventType, id: string, sourceAddress: string, sourceAddressReverseRecord?: { __typename?: 'ReverseRecord', id: string, domain?: { __typename?: 'Domain', id: string, name: string, data: Array<{ __typename?: 'DataItem', id: string, key: string, rawValue: string }> } | null } | null, block: { __typename?: 'Block', timestamp: dayjs.Dayjs } } }>, pageInfo: { __typename?: 'PageInfo', startCursor?: string | null, endCursor?: string | null, hasNextPage: boolean, hasPreviousPage: boolean } } };

export type BlockQueryVariables = Exact<{
  hash?: InputMaybe<Scalars['String']>;
  level?: InputMaybe<Scalars['Int']>;
}>;


export type BlockQuery = { __typename?: 'Query', block?: { __typename?: 'Block', hash: string, level: number, timestamp: dayjs.Dayjs } | null };

export type AuctionListQueryVariables = Exact<{
  where: AuctionsFilter;
  order?: InputMaybe<AuctionOrder>;
  first?: InputMaybe<Scalars['Int']>;
  after?: InputMaybe<Scalars['String']>;
}>;


export type AuctionListQuery = { __typename?: 'Query', auctions: { __typename?: 'AuctionConnection', edges: Array<{ __typename?: 'AuctionEdge', node: { __typename?: 'Auction', id: string, domainName: string, startedAtLevel: number, endsAtUtc: dayjs.Dayjs, state: AuctionState, bidCount: number, highestBid: { __typename?: 'Bid', id: string, amount: BigNumber, bidder: string } } }>, pageInfo: { __typename?: 'PageInfo', startCursor?: string | null, endCursor?: string | null, hasNextPage: boolean, hasPreviousPage: boolean } } };

export type OfferListQueryVariables = Exact<{
  where: OffersFilter;
  order?: InputMaybe<OfferOrder>;
  first?: InputMaybe<Scalars['Int']>;
  after?: InputMaybe<Scalars['String']>;
}>;


export type OfferListQuery = { __typename?: 'Query', offers: { __typename?: 'OfferConnection', edges: Array<{ __typename?: 'OfferEdge', node: { __typename?: 'Offer', id: string, tokenContract: string, tokenId: number, price: BigNumber, state: OfferState, seller: string, expiration?: dayjs.Dayjs | null, domain?: { __typename?: 'DomainData', id: string, name: string, owner: string, expires?: dayjs.Dayjs | null, data: Array<{ __typename?: 'DataItem', id: string, key: string, rawValue: string }> } | null, sellerReverseRecord?: { __typename?: 'ReverseRecord', id: string, domain?: { __typename?: 'Domain', id: string, name: string, data: Array<{ __typename?: 'DataItem', id: string, key: string, rawValue: string }> } | null } | null } }>, pageInfo: { __typename?: 'PageInfo', startCursor?: string | null, endCursor?: string | null, hasNextPage: boolean, hasPreviousPage: boolean } } };

export type BuyOfferListQueryVariables = Exact<{
  where: BuyOffersFilter;
  order?: InputMaybe<OfferOrder>;
  first?: InputMaybe<Scalars['Int']>;
  after?: InputMaybe<Scalars['String']>;
}>;


export type BuyOfferListQuery = { __typename?: 'Query', buyOffers: { __typename?: 'BuyOfferConnection', edges: Array<{ __typename?: 'BuyOfferEdge', node: { __typename?: 'BuyOffer', id: string, tokenContract: string, tokenId: number, price: BigNumber, priceWithoutFee: BigNumber, state: OfferState, seller?: string | null, buyer: string, expiration?: dayjs.Dayjs | null, domain?: { __typename?: 'DomainData', id: string, name: string, owner: string, expires?: dayjs.Dayjs | null, data: Array<{ __typename?: 'DataItem', id: string, key: string, rawValue: string }> } | null, buyerReverseRecord?: { __typename?: 'ReverseRecord', id: string, domain?: { __typename?: 'Domain', id: string, name: string, data: Array<{ __typename?: 'DataItem', id: string, key: string, rawValue: string }> } | null } | null } }>, pageInfo: { __typename?: 'PageInfo', startCursor?: string | null, endCursor?: string | null, hasNextPage: boolean, hasPreviousPage: boolean } } };

export type BidderBalanceQueryVariables = Exact<{
  address: Scalars['String'];
}>;


export type BidderBalanceQuery = { __typename?: 'Query', bidderBalances: { __typename?: 'BidderBalances', id: string, balances: Array<{ __typename?: 'Balance', id: string, balance: BigNumber, tldName: string }> } };

export const DomainExistsDocument = gql`
    query DomainExists($name: String!) {
  domain(name: $name) {
    name
    address
  }
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class DomainExistsGQL extends Apollo.Query<DomainExistsQuery, DomainExistsQueryVariables> {
    document = DomainExistsDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const ClaimableTlDsListDocument = gql`
    query ClaimableTLDsList($where: DomainsFilter!) {
  domains(where: $where) {
    edges {
      node {
        name
      }
    }
  }
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class ClaimableTlDsListGQL extends Apollo.Query<ClaimableTlDsListQuery, ClaimableTlDsListQueryVariables> {
    document = ClaimableTlDsListDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const DomainDetailDocument = gql`
    query DomainDetail($name: String!, $domainValidity: RecordValidity = ALL) {
  domain: domain(name: $name, validity: $domainValidity) {
    id
    name
    address
    owner
    parentOwner
    tokenId
    operators {
      id
      address
    }
    data {
      id
      key
      rawValue
    }
    expires: expiresAtUtc
    subdomains {
      totalCount
    }
    reverseRecord {
      id
      owner
      address
    }
    ownerReverseRecord {
      id
      domain {
        id
        name
        data {
          id
          key
          rawValue
        }
      }
    }
    addressReverseRecord {
      id
      domain {
        id
        name
        data {
          id
          key
          rawValue
        }
      }
    }
  }
  auction: currentAuction(domainName: $name) {
    id
    endsAt: endsAtUtc
    ownedUntil: ownedUntilUtc
    state
    highestBid {
      id
      amount
      bidder
    }
    bids {
      id
      amount
      bidder
      bidderReverseRecord {
        id
        domain {
          id
          name
          data {
            id
            key
            rawValue
          }
        }
      }
      timestamp
    }
  }
  lastAuction: auctions(
    where: {domainName: {equalTo: $name}}
    order: {field: ENDS_AT, direction: DESC}
    first: 1
  ) {
    edges {
      node {
        id
        endsAt: endsAtUtc
        ownedUntil: ownedUntilUtc
        state
        highestBid {
          id
          amount
          bidder
        }
      }
    }
  }
  offer: currentOffer(domainName: $name) {
    id
    tokenContract
    tokenId
    domain {
      id
      name
      expires: expiresAtUtc
      owner
      data {
        id
        key
        rawValue
      }
    }
    seller: sellerAddress
    sellerReverseRecord: sellerAddressReverseRecord {
      id
      domain {
        id
        name
        data {
          id
          key
          rawValue
        }
      }
    }
    expiration: expiresAtUtc
    price
    state
  }
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class DomainDetailGQL extends Apollo.Query<DomainDetailQuery, DomainDetailQueryVariables> {
    document = DomainDetailDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const CurrentBuyOfferDocument = gql`
    query CurrentBuyOffer($name: String!, $address: Address!) {
  currentBuyOffer(domainName: $name, buyerAddress: $address) {
    id
    domain {
      id
      name
      owner
      expires: expiresAtUtc
      data {
        id
        key
        rawValue
      }
    }
    tokenContract
    tokenId
    seller: sellerAddress
    buyer: buyerAddress
    buyerReverseRecord: buyerAddressReverseRecord {
      id
      domain {
        id
        name
        data {
          id
          key
          rawValue
        }
      }
    }
    expiration: expiresAtUtc
    price
    priceWithoutFee
    state
  }
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class CurrentBuyOfferGQL extends Apollo.Query<CurrentBuyOfferQuery, CurrentBuyOfferQueryVariables> {
    document = CurrentBuyOfferDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const AuctionDetailDocument = gql`
    query AuctionDetail($name: String!, $startedAtLevel: Int!) {
  auction: auction(domainName: $name, startedAtLevel: $startedAtLevel) {
    id
    endsAtUtc
    state
    highestBid {
      id
      amount
      bidder
      bidderReverseRecord {
        id
        domain {
          id
          name
          data {
            id
            key
            rawValue
          }
        }
      }
    }
    bids {
      id
      amount
      bidder
      bidderReverseRecord {
        id
        domain {
          id
          name
          data {
            id
            key
            rawValue
          }
        }
      }
      timestamp
    }
  }
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class AuctionDetailGQL extends Apollo.Query<AuctionDetailQuery, AuctionDetailQueryVariables> {
    document = AuctionDetailDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const ReverseRecordDetailDocument = gql`
    query ReverseRecordDetail($address: String!) {
  reverseRecord: reverseRecord(address: $address) {
    id
    address
    domain {
      id
      name
      data {
        id
        key
        rawValue
      }
    }
    owner
    ownerReverseRecord {
      id
      domain {
        id
        name
        data {
          id
          key
          rawValue
        }
      }
    }
  }
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class ReverseRecordDetailGQL extends Apollo.Query<ReverseRecordDetailQuery, ReverseRecordDetailQueryVariables> {
    document = ReverseRecordDetailDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const DomainsListDocument = gql`
    query DomainsList($where: DomainsFilter!, $order: DomainOrder, $first: Int, $after: String) {
  domains(where: $where, order: $order, first: $first, after: $after) {
    edges {
      node {
        id
        name
        level
        owner
        address
        tokenId
        data {
          id
          key
          rawValue
        }
        operators {
          id
          address
        }
        parentOwner
        expires: expiresAtUtc
      }
      cursor
    }
    pageInfo {
      startCursor
      endCursor
      hasNextPage
      hasPreviousPage
    }
  }
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class DomainsListGQL extends Apollo.Query<DomainsListQuery, DomainsListQueryVariables> {
    document = DomainsListDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const DomainsWithAuctionListDocument = gql`
    query DomainsWithAuctionList($where: DomainsFilter!, $order: DomainOrder, $first: Int, $after: String) {
  domains(where: $where, order: $order, first: $first, after: $after) {
    edges {
      node {
        id
        name
        level
        owner
        operators {
          id
          address
        }
        parentOwner
        lastAuction {
          id
          endsAt: endsAtUtc
          ownedUntil: ownedUntilUtc
          state
          highestBid {
            id
            amount
            bidder
          }
        }
        expires: expiresAtUtc
      }
      cursor
    }
    pageInfo {
      startCursor
      endCursor
      hasNextPage
      hasPreviousPage
    }
  }
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class DomainsWithAuctionListGQL extends Apollo.Query<DomainsWithAuctionListQuery, DomainsWithAuctionListQueryVariables> {
    document = DomainsWithAuctionListDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const ReverseRecordListDocument = gql`
    query ReverseRecordList($where: ReverseRecordsFilter!, $order: ReverseRecordOrder, $first: Int, $after: String) {
  reverseRecords(where: $where, order: $order, first: $first, after: $after) {
    edges {
      node {
        id
        address
        owner
        domain {
          id
          name
          data {
            id
            key
            rawValue
          }
        }
      }
      cursor
    }
    pageInfo {
      startCursor
      endCursor
      hasNextPage
      hasPreviousPage
    }
  }
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class ReverseRecordListGQL extends Apollo.Query<ReverseRecordListQuery, ReverseRecordListQueryVariables> {
    document = ReverseRecordListDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const StatsDocument = gql`
    query Stats($owner: Address, $expiringThreshold: DateTime!) {
  bought: domains(where: {owner: {equalTo: $owner}, level: {equalTo: 2}}) {
    totalCount
  }
  expiring: domains(
    where: {owner: {equalTo: $owner}, expiresAtUtc: {lessThan: $expiringThreshold}, level: {equalTo: 2}}
  ) {
    totalCount
  }
  total: domains(where: {}) {
    totalCount
  }
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class StatsGQL extends Apollo.Query<StatsQuery, StatsQueryVariables> {
    document = StatsDocument;
    client = 'stats';
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const UserDataDocument = gql`
    query UserData($address: Address!) {
  domains: domains(where: {owner: {equalTo: $address}, level: {equalTo: 2}}) {
    totalCount
  }
  auctions: auctions(where: {bidders: {include: $address}}) {
    totalCount
  }
  buyOffers: buyOffers(
    where: {or: [{buyerAddress: {in: [$address]}}, {sellerAddress: {in: [$address]}}]}
  ) {
    totalCount
  }
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class UserDataGQL extends Apollo.Query<UserDataQuery, UserDataQueryVariables> {
    document = UserDataDocument;
    client = 'stats';
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const EventsDocument = gql`
    query Events($first: Int, $after: String, $last: Int, $before: String, $where: EventsFilter) {
  events(
    first: $first
    after: $after
    last: $last
    before: $before
    where: $where
  ) {
    edges {
      node {
        type
        id
        sourceAddress
        sourceAddressReverseRecord {
          id
          domain {
            id
            name
            data {
              id
              key
              rawValue
            }
          }
        }
        block {
          timestamp
        }
        ... on AuctionBidEvent {
          operationGroupHash
          amount: bidAmount
          balance: transactionAmount
          domainName
          block {
            timestamp
          }
        }
        ... on AuctionWithdrawEvent {
          amount: withdrawnAmount
          balance: withdrawnAmount
          tldName
          operationGroupHash
        }
        ... on AuctionSettleEvent {
          operationGroupHash
          domainName
          durationInDays: registrationDurationInDays
          amount: winningBid
        }
        ... on AuctionEndEvent {
          domainName
          amount: winningBid
        }
        ... on DomainBuyEvent {
          operationGroupHash
          domainName
          amount: price
          balance: price
          durationInDays
        }
        ... on DomainRenewEvent {
          operationGroupHash
          domainName
          durationInDays
          amount: price
          balance: price
        }
        ... on DomainCommitEvent {
          operationGroupHash
        }
        ... on DomainUpdateEvent {
          domainName
          operationGroupHash
        }
        ... on DomainSetChildRecordEvent {
          domainName
          operationGroupHash
          isNew: isNewRecord
        }
        ... on ReverseRecordUpdateEvent {
          name
          operationGroupHash
          reverseRecordAddress
        }
        ... on ReverseRecordClaimEvent {
          name
          operationGroupHash
          reverseRecordAddress: sourceAddress
        }
        ... on DomainGrantEvent {
          domainName
          maybeDurationInDays: durationInDays
          operationGroupHash
        }
        ... on DomainTransferEvent {
          domainName
          operationGroupHash
          secondaryAddress: newOwner
          secondaryReverseRecord: newOwnerReverseRecord {
            id
            domain {
              id
              name
              data {
                id
                key
                rawValue
              }
            }
          }
        }
        ... on OfferPlacedEvent {
          tokenId
          maybeDomainName: domainName
          amount: price
          operationGroupHash
        }
        ... on OfferUpdatedEvent {
          tokenId
          maybeDomainName: domainName
          operationGroupHash
        }
        ... on OfferExecutedEvent {
          maybeDomainName: domainName
          amount: price
          balance: price
          altBalance: priceWithoutFee
          secondaryAddress: sellerAddress
          secondaryReverseRecord: sellerAddressReverseRecord {
            id
            domain {
              id
              name
              data {
                id
                key
                rawValue
              }
            }
          }
          operationGroupHash
        }
        ... on OfferRemovedEvent {
          tokenId
          maybeDomainName: domainName
          operationGroupHash
        }
        ... on BuyOfferPlacedEvent {
          tokenId
          maybeDomainName: domainName
          amount: priceWithoutFee
          operationGroupHash
        }
        ... on BuyOfferRemovedEvent {
          tokenId
          maybeDomainName: domainName
          operationGroupHash
        }
        ... on BuyOfferExecutedEvent {
          maybeDomainName: domainName
          amount: priceWithoutFee
          altBalance: priceWithoutFee
          secondaryAddress: buyerAddress
          secondaryReverseRecord: buyerAddressReverseRecord {
            id
            domain {
              id
              name
              data {
                id
                key
                rawValue
              }
            }
          }
          operationGroupHash
        }
        ... on DomainUpdateOperatorsEvent {
          domainName
          operationGroupHash
        }
        ... on DomainClaimEvent {
          domainName
          operationGroupHash
        }
      }
    }
    pageInfo {
      startCursor
      endCursor
      hasNextPage
      hasPreviousPage
    }
  }
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class EventsGQL extends Apollo.Query<EventsQuery, EventsQueryVariables> {
    document = EventsDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const BlockDocument = gql`
    query Block($hash: String, $level: Int) {
  block(hash: $hash, level: $level) {
    hash
    level
    timestamp
  }
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class BlockGQL extends Apollo.Query<BlockQuery, BlockQueryVariables> {
    document = BlockDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const AuctionListDocument = gql`
    query AuctionList($where: AuctionsFilter!, $order: AuctionOrder, $first: Int, $after: String) {
  auctions(where: $where, order: $order, first: $first, after: $after) {
    edges {
      node {
        id
        domainName
        startedAtLevel
        endsAtUtc
        state
        highestBid {
          id
          amount
          bidder
        }
        bidCount
      }
    }
    pageInfo {
      startCursor
      endCursor
      hasNextPage
      hasPreviousPage
    }
  }
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class AuctionListGQL extends Apollo.Query<AuctionListQuery, AuctionListQueryVariables> {
    document = AuctionListDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const OfferListDocument = gql`
    query OfferList($where: OffersFilter!, $order: OfferOrder, $first: Int, $after: String) {
  offers(where: $where, order: $order, first: $first, after: $after) {
    edges {
      node {
        id
        domain {
          id
          name
          owner
          expires: expiresAtUtc
          data {
            id
            key
            rawValue
          }
        }
        tokenContract
        tokenId
        seller: sellerAddress
        sellerReverseRecord: sellerAddressReverseRecord {
          id
          domain {
            id
            name
            data {
              id
              key
              rawValue
            }
          }
        }
        expiration: expiresAtUtc
        price
        state
      }
    }
    pageInfo {
      startCursor
      endCursor
      hasNextPage
      hasPreviousPage
    }
  }
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class OfferListGQL extends Apollo.Query<OfferListQuery, OfferListQueryVariables> {
    document = OfferListDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const BuyOfferListDocument = gql`
    query BuyOfferList($where: BuyOffersFilter!, $order: OfferOrder, $first: Int, $after: String) {
  buyOffers(where: $where, order: $order, first: $first, after: $after) {
    edges {
      node {
        id
        domain {
          id
          name
          owner
          expires: expiresAtUtc
          data {
            id
            key
            rawValue
          }
        }
        tokenContract
        tokenId
        seller: sellerAddress
        buyer: buyerAddress
        buyerReverseRecord: buyerAddressReverseRecord {
          id
          domain {
            id
            name
            data {
              id
              key
              rawValue
            }
          }
        }
        expiration: expiresAtUtc
        price
        priceWithoutFee
        state
      }
    }
    pageInfo {
      startCursor
      endCursor
      hasNextPage
      hasPreviousPage
    }
  }
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class BuyOfferListGQL extends Apollo.Query<BuyOfferListQuery, BuyOfferListQueryVariables> {
    document = BuyOfferListDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const BidderBalanceDocument = gql`
    query BidderBalance($address: String!) {
  bidderBalances(address: $address) {
    id
    balances {
      id
      balance
      tldName
    }
  }
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class BidderBalanceGQL extends Apollo.Query<BidderBalanceQuery, BidderBalanceQueryVariables> {
    document = BidderBalanceDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }