import { TestBed, fakeAsync, tick } from '@angular/core/testing';
import { InMemoryCache } from '@apollo/client/core';
import { TezosAddress, TezosAddress2, getDomainByIndex, getDomains, prepareGQLDomainListResponse } from '@td/test';
import { APOLLO_TESTING_CACHE, ApolloTestingController, ApolloTestingModule } from 'apollo-angular/testing';
import { GraphQLError } from 'graphql';
import { chunk, cloneDeep } from 'lodash-es';
import { instance, mock, when } from 'ts-mockito';

import { Subject } from 'rxjs';
import { AppService } from '../app-service';
import { DomainListRecord, DomainTableDataSource } from './domain-table-data-source';
import { DomainOrderField, DomainsListDocument, DomainsListGQL, OrderDirection } from './graphql.generated';
import { typePolicies } from './graphql.module';

describe('DomainTableDataSource', () => {
    let dataSource: DomainTableDataSource;
    let apollo: ApolloTestingController;
    let dataSpy: jasmine.Spy;
    let loadingSpy: jasmine.Spy;
    let initialLoadingSpy: jasmine.Spy;
    let errorSpy: jasmine.Spy;
    let emptySpy: jasmine.Spy;
    let appServiceMock: AppService;
    let offlineToOnline: Subject<boolean>;

    beforeEach(() => {
        appServiceMock = mock(AppService);
        dataSpy = jasmine.createSpy('dataSpy');
        loadingSpy = jasmine.createSpy('loadingSpy');
        errorSpy = jasmine.createSpy('errorSpy');
        emptySpy = jasmine.createSpy('emptySpy');
        initialLoadingSpy = jasmine.createSpy('initialLoadingSpy');
        offlineToOnline = new Subject();

        when(appServiceMock.goingOnlineFromOffline).thenReturn(offlineToOnline);

        TestBed.configureTestingModule({
            imports: [ApolloTestingModule],
            providers: [
                {
                    provide: APOLLO_TESTING_CACHE,
                    useFactory: () => {
                        return new InMemoryCache({
                            typePolicies,
                            addTypename: false,
                        });
                    },
                },
            ],
        });

        const query = TestBed.inject(DomainsListGQL);
        dataSource = new DomainTableDataSource({
            query,
            appService: instance(appServiceMock),
        });

        dataSource.connect().subscribe(dataSpy);
        dataSource.loading$.subscribe(loadingSpy);
        dataSource.initialLoading$.subscribe(initialLoadingSpy);
        dataSource.empty$.subscribe(emptySpy);
        dataSource.error$.subscribe(errorSpy);

        resetSpies();

        apollo = TestBed.inject(ApolloTestingController);
    });

    afterEach(() => {
        apollo.verify();
    });

    describe('load()', () => {
        it('should create query', fakeAsync(() => {
            dataSource.load({
                where: { owner: { equalTo: TezosAddress } },
                order: { field: DomainOrderField.Domain, direction: OrderDirection.Asc },
            });
            tick();

            expect(loadingSpy).toHaveBeenCalledWith(true);
            expect(initialLoadingSpy).toHaveBeenCalledWith(true);
            expect(errorSpy).toHaveBeenCalledWith(null);
            resetSpies();

            const op = apollo.expectOne(DomainsListDocument);

            expect(op.operation.variables.first).toBe(30);
            expect(op.operation.variables.where).toEqual({ owner: { equalTo: TezosAddress } });
            expect(op.operation.variables.order).toEqual({ field: DomainOrderField.Domain, direction: OrderDirection.Asc });

            op.flush({
                data: {
                    domains: prepareGQLDomainListResponse(getDomains('necroskillz.tez', 'random.tez')),
                },
            });

            tick();

            expect(loadingSpy).toHaveBeenCalledWith(false);
            expect(initialLoadingSpy).toHaveBeenCalledWith(false);
            expect(emptySpy).toHaveBeenCalledWith(false);
            checkData(getDomains('necroskillz.tez', 'random.tez'));
        }));

        it('should create new query when called again', fakeAsync(() => {
            dataSource.load({
                where: { owner: { equalTo: TezosAddress } },
                order: { field: DomainOrderField.Domain, direction: OrderDirection.Asc },
            });

            const queryRef = dataSource['queryRef'];

            apollo.expectOne(DomainsListDocument).flush({
                data: {
                    domains: prepareGQLDomainListResponse(getDomains('necroskillz.tez')),
                },
            });
            tick();

            resetSpies();
            dataSource.load({
                where: { owner: { equalTo: TezosAddress2 } },
                order: { field: DomainOrderField.Domain, direction: OrderDirection.Asc },
            });
            apollo.expectOne(DomainsListDocument).flush({
                data: {
                    domains: prepareGQLDomainListResponse(getDomains('martin.tez')),
                },
            });
            tick();

            checkData(getDomains('martin.tez'));
            expect(loadingSpy).toHaveBeenCalledWith(false);
            expect(initialLoadingSpy).toHaveBeenCalledWith(false);

            expect(dataSource['queryRef']).not.toBe(queryRef);
        }));

        it('should emit error if api returns error', fakeAsync(() => {
            dataSource.load({
                where: { owner: { equalTo: TezosAddress } },
                order: { field: DomainOrderField.Domain, direction: OrderDirection.Asc },
            });

            const error = new GraphQLError('gql error');
            apollo.expectOne(DomainsListDocument).graphqlErrors([error, new GraphQLError('gql error2')]);
            tick();

            expect(errorSpy).toHaveBeenCalledWith(error);
            expect(loadingSpy).toHaveBeenCalledWith(false);
            expect(initialLoadingSpy).toHaveBeenCalledWith(false);
            expect(emptySpy).not.toHaveBeenCalled();
        }));

        it('should emit error if network returns error', fakeAsync(() => {
            dataSource.load({
                where: { owner: { equalTo: TezosAddress } },
                order: { field: DomainOrderField.Domain, direction: OrderDirection.Asc },
            });

            const error = new Error('gql error');
            apollo.expectOne(DomainsListDocument).networkError(error);
            tick();

            expect(errorSpy).toHaveBeenCalledWith(error);
            expect(loadingSpy).toHaveBeenCalledWith(false);
            expect(initialLoadingSpy).toHaveBeenCalledWith(false);
            expect(emptySpy).not.toHaveBeenCalled();
        }));

        it('should emit empty when 0 records are returned', fakeAsync(() => {
            dataSource.load({
                where: { owner: { equalTo: TezosAddress } },
                order: { field: DomainOrderField.Domain, direction: OrderDirection.Asc },
            });

            apollo.expectOne(DomainsListDocument).flush({
                data: {
                    domains: prepareGQLDomainListResponse([]),
                },
            });

            tick();

            expect(emptySpy).toHaveBeenCalled();
        }));

        describe('fill', () => {
            const domains: DomainListRecord[] = [
                {
                    id: 'bb.aa.tez',
                    name: 'bb.aa.tez',
                    owner: TezosAddress,
                    parentOwner: null,
                    address: null,
                    tokenId: null,
                    level: 3,
                    expires: null,
                    operators: [],
                    data: [],
                },
                ...getDomains('necroskillz.tez'),
                {
                    id: 'a.b.necroskillz.tez',
                    name: 'a.b.necroskillz.tez',
                    parentOwner: null,
                    address: null,
                    tokenId: null,
                    owner: TezosAddress,
                    level: 4,
                    expires: null,
                    operators: [],
                    data: [],
                },
                {
                    id: 'lol.wtf.tez',
                    name: 'lol.wtf.tez',
                    parentOwner: null,
                    address: null,
                    tokenId: null,
                    owner: TezosAddress,
                    level: 3,
                    expires: null,
                    operators: [],
                    data: [],
                },
            ];

            const filledData: DomainListRecord[] = [
                {
                    id: 'aa.tez',
                    name: 'aa.tez',
                    parentOwner: null,
                    address: null,
                    tokenId: null,
                    owner: 'generated',
                    level: 2,
                    expires: null,
                    operators: [],
                    data: [],
                },
                {
                    id: 'bb.aa.tez',
                    name: 'bb.aa.tez',
                    parentOwner: null,
                    address: null,
                    tokenId: null,
                    owner: TezosAddress,
                    level: 3,
                    expires: null,
                    operators: [],
                    data: [],
                },
                ...getDomains('necroskillz.tez'),
                {
                    id: 'b.necroskillz.tez',
                    name: 'b.necroskillz.tez',
                    owner: 'generated',
                    level: 3,
                    parentOwner: null,
                    address: null,
                    tokenId: null,
                    expires: null,
                    operators: [],
                    data: [],
                },
                {
                    id: 'a.b.necroskillz.tez',
                    name: 'a.b.necroskillz.tez',
                    owner: TezosAddress,
                    level: 4,
                    parentOwner: null,
                    address: null,
                    tokenId: null,
                    expires: null,
                    operators: [],
                    data: [],
                },
                {
                    id: 'wtf.tez',
                    name: 'wtf.tez',
                    owner: 'generated',
                    level: 2,
                    parentOwner: null,
                    address: null,
                    tokenId: null,
                    expires: null,
                    operators: [],
                    data: [],
                },
                {
                    id: 'lol.wtf.tez',
                    name: 'lol.wtf.tez',
                    parentOwner: null,
                    address: null,
                    tokenId: null,
                    owner: TezosAddress,
                    level: 3,
                    expires: null,
                    operators: [],
                    data: [],
                },
            ];

            it('should fill missing hierarchy data', fakeAsync(() => {
                dataSource.load(
                    {
                        where: { owner: { equalTo: TezosAddress } },
                    },
                    undefined,
                    { baseLevel: 2 }
                );

                apollo.expectOne(DomainsListDocument).flush({
                    data: {
                        domains: prepareGQLDomainListResponse(domains),
                    },
                });

                tick();

                checkData(filledData);
            }));

            it('should fill missing hierarchy data in reverse', fakeAsync(() => {
                dataSource.load(
                    {
                        where: { owner: { equalTo: TezosAddress } },
                        order: { field: DomainOrderField.Domain, direction: OrderDirection.Desc },
                    },
                    undefined,
                    { baseLevel: 2 }
                );

                apollo.expectOne(DomainsListDocument).flush({
                    data: {
                        domains: prepareGQLDomainListResponse(domains.slice(0).reverse()),
                    },
                });

                tick();

                checkData(filledData.slice(0).reverse());
            }));

            it('should not fill missing hierarchy when filtered', fakeAsync(() => {
                dataSource.load(
                    {
                        where: { owner: { equalTo: TezosAddress }, name: { like: 'aa' } },
                    },
                    undefined,
                    { baseLevel: 2 }
                );

                apollo.expectOne(DomainsListDocument).flush({
                    data: {
                        domains: prepareGQLDomainListResponse(domains),
                    },
                });

                tick();

                checkData(domains);
            }));

            it('should not fill missing hierarchy when explicitly disabled', fakeAsync(() => {
                dataSource.load(
                    {
                        where: { owner: { equalTo: TezosAddress } },
                    },
                    undefined,
                    { baseLevel: 2, autofillHierarchy: false }
                );

                apollo.expectOne(DomainsListDocument).flush({
                    data: {
                        domains: prepareGQLDomainListResponse(domains),
                    },
                });

                tick();

                checkData(domains);
            }));

            it('should fill missing hierarchy data up to base level', fakeAsync(() => {
                const subdomains: DomainListRecord[] = [
                    {
                        id: 'a.b.necroskillz.tez',
                        name: 'a.b.necroskillz.tez',
                        parentOwner: null,
                        address: null,
                        tokenId: null,
                        owner: TezosAddress,
                        level: 4,
                        expires: null,
                        operators: [],
                        data: [],
                    },
                ];

                const filledSubdomains: DomainListRecord[] = [
                    {
                        id: 'b.necroskillz.tez',
                        name: 'b.necroskillz.tez',
                        parentOwner: null,
                        address: null,
                        tokenId: null,
                        owner: 'generated',
                        level: 3,
                        expires: null,
                        operators: [],
                        data: [],
                    },
                    ...subdomains,
                ];

                dataSource.load(
                    {
                        where: { owner: { equalTo: TezosAddress } },
                    },
                    undefined,
                    { baseLevel: 3 }
                );

                apollo.expectOne(DomainsListDocument).flush({
                    data: {
                        domains: prepareGQLDomainListResponse(subdomains),
                    },
                });

                tick();

                checkData(filledSubdomains);
            }));
        });
    });

    describe('loadMore()', () => {
        let pages: DomainListRecord[][];

        beforeEach(() => {
            const domains: DomainListRecord[] = [];
            for (let i = 0; i < 70; i++) {
                const domain = cloneDeep(getDomainByIndex((i % 4) + 1));
                domain.name = domain.name + i;
                domains.push(domain);
            }
            pages = chunk(domains, 30);
        });

        it('should throw when called before load', () => {
            expect(() => dataSource.loadMore()).toThrowError();
        });

        it('should load next page', fakeAsync(() => {
            dataSource.load({
                where: { owner: { equalTo: TezosAddress } },
                order: { field: DomainOrderField.Domain, direction: OrderDirection.Asc },
            });

            apollo.expectOne(DomainsListDocument).flush({
                data: {
                    domains: prepareGQLDomainListResponse(pages[0], true),
                },
            });
            tick();

            checkData(pages[0]);

            resetSpies();

            dataSource.loadMore();

            const moreOp = apollo.expectOne(DomainsListDocument);

            expect(moreOp.operation.variables.first).toBe(30);
            expect(moreOp.operation.variables.after).toBe(`${pages[0][29].name}_cursor`);
            expect(moreOp.operation.variables.where).toEqual({ owner: { equalTo: TezosAddress } });
            expect(moreOp.operation.variables.order).toEqual({ field: DomainOrderField.Domain, direction: OrderDirection.Asc });

            moreOp.flush({
                data: {
                    domains: prepareGQLDomainListResponse(pages[1]),
                },
            });
            tick();

            expect(initialLoadingSpy).not.toHaveBeenCalled();
            checkData(pages[0].concat(pages[1]));
            apollo.match(DomainsListDocument); // HACK: started failing for no reason after adding parentOwner
        }));

        it('should not load more after last page', fakeAsync(() => {
            dataSource.load({
                where: { owner: { equalTo: TezosAddress } },
                order: { field: DomainOrderField.Domain, direction: OrderDirection.Asc },
            });

            apollo.expectOne(DomainsListDocument).flush({
                data: {
                    domains: prepareGQLDomainListResponse(pages[0], true),
                },
            });
            tick();

            dataSource.loadMore();

            apollo.expectOne(DomainsListDocument).flush({
                data: {
                    domains: prepareGQLDomainListResponse(pages[1], true),
                },
            });
            tick();
            apollo.match(DomainsListDocument); // HACK: started failing for no reason after adding parentOwner

            dataSource.loadMore();

            const moreOp = apollo.expectOne(DomainsListDocument);

            expect(moreOp.operation.variables.first).toBe(30);
            expect(moreOp.operation.variables.after).toBe(`${pages[1][29].name}_cursor`);
            expect(moreOp.operation.variables.where).toEqual({ owner: { equalTo: TezosAddress } });
            expect(moreOp.operation.variables.order).toEqual({ field: DomainOrderField.Domain, direction: OrderDirection.Asc });

            moreOp.flush({
                data: {
                    domains: prepareGQLDomainListResponse(pages[2]),
                },
            });
            tick();

            checkData(pages[0].concat(pages[1]).concat(pages[2]));

            dataSource.loadMore();

            apollo.expectNone(DomainsListDocument);
        }));
    });

    describe('disconnect', () => {
        it('should stop query', fakeAsync(() => {
            dataSource.load({
                where: { owner: { equalTo: TezosAddress } },
                order: { field: DomainOrderField.Domain, direction: OrderDirection.Asc },
            });

            apollo.expectOne(DomainsListDocument).flush({
                data: {
                    domains: prepareGQLDomainListResponse(getDomains('necroskillz.tez')),
                },
            });
            tick();

            dataSource.disconnect();
            resetSpies();

            dataSource.load({
                where: { owner: { equalTo: TezosAddress2 } },
                order: { field: DomainOrderField.Domain, direction: OrderDirection.Asc },
            });

            apollo.expectOne(DomainsListDocument).flush({
                data: {
                    domains: prepareGQLDomainListResponse(getDomains('martin.tez')),
                },
            });
            tick();

            expect(dataSpy).not.toHaveBeenCalled();
            expect(loadingSpy).not.toHaveBeenCalled();
            expect(errorSpy).not.toHaveBeenCalled();
            expect(emptySpy).not.toHaveBeenCalled();
        }));
    });

    function resetSpies() {
        dataSpy.calls.reset();
        loadingSpy.calls.reset();
        errorSpy.calls.reset();
        emptySpy.calls.reset();
        initialLoadingSpy.calls.reset();
    }

    function checkData(domains: DomainListRecord[]) {
        const data: DomainListRecord[] = dataSpy.calls.mostRecent().args[0];
        expect(data.map(d => d.name)).toEqual(domains.map(d => d.name));
        expect(data.map(d => d.owner)).toEqual(domains.map(d => d.owner));
    }
});
