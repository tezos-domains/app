import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { IdenticonComponent } from './identicon.component';

describe('IdenticonComponent', () => {
  let component: IdenticonComponent;
  let fixture: ComponentFixture<IdenticonComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ IdenticonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IdenticonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
