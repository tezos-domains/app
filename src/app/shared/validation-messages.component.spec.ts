import { AbstractControl, FormControlStatus } from '@angular/forms';
import { TranslocoModule } from '@ngneat/transloco';
import { transloco } from '@td/test';
import { MockBuilder, MockedComponentFixture, MockRender, ngMocks } from 'ng-mocks';
import { Subject } from 'rxjs';
import { anyString, anything, instance, mock, when } from 'ts-mockito';
import { SharedModule } from './shared.module';
import { ValidationMessagesComponent } from './validation-messages.component';

describe('ValidationMessagesComponent', () => {
    let fixture: MockedComponentFixture<ValidationMessagesComponent, Partial<ValidationMessagesComponent>>;
    let controlMock: AbstractControl;
    let statusChanges: Subject<FormControlStatus>;
    let context: any;

    const expectedMessages = {
        default: 'Name must be valid.',
        required: 'Name is required.',
        tezosAddress: 'Name must be a valid Tezos address.',
        labelTezUnsupported: `Name must only contain letters (a-z), numbers or '-'.`,
        labelDefault: 'Name must be a valid label.',
    };

    beforeEach(() => MockBuilder(ValidationMessagesComponent, SharedModule).keep(TranslocoModule, { export: true }).keep(transloco(), { export: true }));

    beforeEach(() => {
        controlMock = mock<AbstractControl>();
        context = { name: 'Name' };
        statusChanges = new Subject<FormControlStatus>();

        when(controlMock.errors).thenReturn({ required: true });
        when(controlMock.statusChanges).thenReturn(statusChanges);
        when(controlMock.getError(anyString())).thenCall(e => instance(controlMock).errors![e]);
    });

    function init() {
        fixture = MockRender(ValidationMessagesComponent, {
            control: instance(controlMock),
            context,
        });
    }

    it('generate messages for control errors', () => {
        init();

        verifyMessages([expectedMessages.required]);
    });

    it('should not show messages if control has no errors', () => {
        when(controlMock.errors).thenReturn(null);

        init();

        verifyMessages([]);
    });

    it('should pick error message from multiple choices', () => {
        when(controlMock.errors).thenReturn({ label: { type: 'tez-UNSUPPORTED_CHARACTERS' } });

        init();

        verifyMessages([expectedMessages.labelTezUnsupported]);
    });

    it('should react to changes', () => {
        init();

        verifyMessages([expectedMessages.required]);

        when(controlMock.errors).thenReturn(null);
        statusChanges.next(anything());
        fixture.detectChanges();

        verifyMessages([]);

        when(controlMock.errors).thenReturn({ label: { type: 'tez-UNSUPPORTED_CHARACTERS' } });
        statusChanges.next(anything());
        fixture.detectChanges();

        verifyMessages([expectedMessages.labelTezUnsupported]);
    });

    function verifyMessages(messages: string[]) {
        fixture.detectChanges();

        const elements = ngMocks.findAll(fixture.point, 'span');

        expect(elements.length).toBe(messages.length);

        for (let i = 0; i < messages.length; i++) {
            expect(elements[i].nativeElement.textContent).toBe(messages[i]);
        }
    }
});
