import { TruncatePipe } from './truncate.pipe';

describe('ShortenAddressPipe', () => {
    let pipe: TruncatePipe;

    beforeEach(() => {
        pipe = new TruncatePipe();
    });

    it('should shorten value', () => {
        expect(pipe.transform('abcdefghijkl', 3, 2, 4, '-.-')).toBe('ab-.-ijkl');
    });

    it('should not shorten values if length is under max', () => {
        expect(pipe.transform('abc', 10)).toBe('abc');
    });

    it('should throw if it would duplicate it parts', () => {
        expect(() => pipe.transform('abcde', 4, 3, 3)).toThrowError();
    });

    it('should default to half of max length and ...', () => {
        expect(pipe.transform('abcdefghijkl', 6)).toBe('abc...jkl');
    });

    it('should return empty string for empty input', () => {
        expect(pipe.transform('')).toBe('');
        expect(pipe.transform(null)).toBe('');
        expect(pipe.transform(undefined)).toBe('');
    });
});
