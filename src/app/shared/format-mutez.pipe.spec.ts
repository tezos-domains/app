import { TestBed } from '@angular/core/testing';
import { provider } from '@td/test';
import BigNumber from 'bignumber.js';
import { mock, when } from 'ts-mockito';
import { LocaleService } from '../i18n/locale.service';
import { FormatMutezPipe } from './format-mutez.pipe';
import { FromMutezPipe } from './from-mutez.pipe';

describe('FormatMutezPipe', () => {
    let pipe: FormatMutezPipe;
    let localeService: LocaleService;

    beforeEach(() => {
        localeService = mock(LocaleService);
        when(localeService.getLocale()).thenReturn('en-US');

        TestBed.configureTestingModule({
            providers: [provider(localeService), FormatMutezPipe, FromMutezPipe],
        });
        pipe = TestBed.inject(FormatMutezPipe);
    });

    it('should allow null or undefined', () => {
        expect(pipe.transform(null)).toBe('');
        expect(pipe.transform(undefined)).toBe('');
    });

    it('should format various values', () => {
        expect(pipe.transform(2.23 * 1e6)).toBe('2.23');
        expect(pipe.transform(2.235 * 1e6)).toBe('2.24');
        expect(pipe.transform(2.2 * 1e6, '1.2-2')).toBe('2.20');
        expect(pipe.transform(0)).toBe('0');

        expect(pipe.transform(BigNumber(2.345 * 1e6))).toBe('2.35');
        expect(pipe.transform(BigNumber(2.3 * 1e6))).toBe('2.3');
    });
});
