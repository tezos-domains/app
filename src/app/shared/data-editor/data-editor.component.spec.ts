import { UntypedFormBuilder, UntypedFormGroup } from '@angular/forms';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { TranslocoModule } from '@ngneat/transloco';
import { provider, transloco } from '@td/test';
import { StandardRecordMetadataKey } from '@tezos-domains/core';
import { DynamicModule } from 'ng-dynamic-component';
import { MockBuilder, MockedComponentFixture, MockRender } from 'ng-mocks';
import { mock, when } from 'ts-mockito';

import { Component, NgModule } from '@angular/core';
import { DataItem } from '../../graphql/graphql.generated';
import { SharedModule } from '../shared.module';
import { DataEditorComponent, Key } from './data-editor.component';
import { DataFieldEditorDescriptor, DataFieldsService } from './data-fields.service';
import { EditorComponentBase } from './editors/editor-component-base';
import { UnknownComponent } from './editors/unknown.component';

@Component({
    selector: 'td-test-editor',
    template: '<div></div>',
})
export class TestEditorComponent extends EditorComponentBase<any> {}

@NgModule({
    declarations: [TestEditorComponent],
})
export class DataEditorTestModule {}

describe('DataEditorComponent', () => {
    let fixture: MockedComponentFixture<DataEditorComponent, Partial<DataEditorComponent>>;
    let component: DataEditorComponent;
    let dataFieldsServiceMock: DataFieldsService;
    let testFields: Map<string, DataFieldEditorDescriptor>;

    beforeEach(() => {
        dataFieldsServiceMock = mock(DataFieldsService);

        testFields = new Map();
        testFields.set(StandardRecordMetadataKey.TTL, {
            key: StandardRecordMetadataKey.TTL,
            displayName: 'TTL',
            editorComponent: TestEditorComponent,
            editorComponentArgs: { min: 0 },
        });
        testFields.set(StandardRecordMetadataKey.OPENID_NAME, {
            key: StandardRecordMetadataKey.OPENID_NAME,
            displayName: 'Name',
            editorComponent: TestEditorComponent,
        });

        when(dataFieldsServiceMock.getFields()).thenReturn(testFields);

        return MockBuilder(DataEditorComponent, SharedModule)
            .keep(TranslocoModule, { export: true })
            .keep(DynamicModule, { export: true })
            .keep(NoopAnimationsModule, { export: true })
            .keep(DataEditorTestModule, { export: true })
            .provide(UntypedFormBuilder)
            .keep(transloco(), { export: true })
            .provide([provider(dataFieldsServiceMock)]);
    });

    function init(data: Pick<DataItem, 'key' | 'rawValue'>[]) {
        fixture = MockRender(DataEditorComponent, {
            data,
            form: new UntypedFormGroup({}),
        });
        component = fixture.point.componentInstance;
    }

    it('should initialize form and properties based on data', () => {
        init([{ key: StandardRecordMetadataKey.TTL, rawValue: '3639' }]);

        expect(component.form.get(StandardRecordMetadataKey.TTL)!.value).toBe('3639');
        checkKeys(component.keys, [StandardRecordMetadataKey.TTL]);
        checkKeys(component.availableKeys, [StandardRecordMetadataKey.OPENID_NAME]);
    });

    describe('addKey()', () => {
        it('should add new control', () => {
            init([]);

            checkKeys(component.keys, []);
            checkKeys(component.availableKeys, [StandardRecordMetadataKey.TTL, StandardRecordMetadataKey.OPENID_NAME]);

            component.addKeyForm.setValue({ key: key(StandardRecordMetadataKey.OPENID_NAME) });
            component.addKey();

            expect(component.addKeyForm.value).toEqual({ key: null });
            expect(component.form.get(StandardRecordMetadataKey.OPENID_NAME)!.value).toBe(null);
            checkKeys(component.keys, [StandardRecordMetadataKey.OPENID_NAME]);
            checkKeys(component.availableKeys, [StandardRecordMetadataKey.TTL]);
        });

        it('should not add empty key', () => {
            init([]);

            component.addKeyForm.setValue({ key: { name: '' } });
            component.addKey();

            expect(Object.keys(component.form.controls).length).toBe(0);
            checkKeys(component.keys, []);
            checkKeys(component.availableKeys, [StandardRecordMetadataKey.TTL, StandardRecordMetadataKey.OPENID_NAME]);
        });

        it('should add custom key', () => {
            init([]);

            component.addKeyForm.setValue({ key: 'custom' });
            component.addKey();

            expect(Object.keys(component.form.controls).length).toBe(1);
            checkKeys(component.keys, ['custom']);
            checkKeys(component.availableKeys, [StandardRecordMetadataKey.TTL, StandardRecordMetadataKey.OPENID_NAME]);
        });

        it('should filter autocomplete keys', () => {
            init([]);
            checkKeys(component.filteredFields, [StandardRecordMetadataKey.TTL, StandardRecordMetadataKey.OPENID_NAME]);

            component.addKeyForm.setValue({ key: key(StandardRecordMetadataKey.TTL) });
            checkKeys(component.filteredFields, [StandardRecordMetadataKey.TTL]);

            component.addKeyForm.setValue({ key: null });
            checkKeys(component.filteredFields, [StandardRecordMetadataKey.TTL, StandardRecordMetadataKey.OPENID_NAME]);
        });

        it('should not add duplicate key', () => {
            init([{ key: StandardRecordMetadataKey.TTL, rawValue: '3639' }]);

            component.addKeyForm.setValue({ key: key(StandardRecordMetadataKey.TTL) });
            component.addKey();

            expect(Object.keys(component.form.controls).length).toBe(1);
            checkKeys(component.keys, [StandardRecordMetadataKey.TTL]);
            checkKeys(component.availableKeys, [StandardRecordMetadataKey.OPENID_NAME]);
        });

        it('should disable add form when parent form is disabled', () => {
            init([]);

            expect(component.addKeyForm.disabled).toBeFalse();

            component.form.disable();

            expect(component.addKeyForm.disabled).toBeTrue();

            component.form.enable();

            expect(component.addKeyForm.disabled).toBeFalse();
        });
    });

    describe('removeKey()', () => {
        it('should remove specified key', () => {
            init([{ key: StandardRecordMetadataKey.TTL, rawValue: '3639' }]);
            checkKeys(component.availableKeys, [StandardRecordMetadataKey.OPENID_NAME]);
            checkKeys(component.filteredFields, [StandardRecordMetadataKey.OPENID_NAME]);

            component.removeKey(StandardRecordMetadataKey.TTL);

            expect(Object.keys(component.form.controls).length).toBe(0);
            checkKeys(component.keys, []);
            checkKeys(component.availableKeys, [StandardRecordMetadataKey.TTL, StandardRecordMetadataKey.OPENID_NAME]);
            checkKeys(component.filteredFields, [StandardRecordMetadataKey.TTL, StandardRecordMetadataKey.OPENID_NAME]);
        });
    });

    describe('getFieldDescriptor', () => {
        it('should return configured editor for standard fields', () => {
            init([]);

            expect(component.getFieldDescriptor(StandardRecordMetadataKey.TTL)).toBe(testFields.get(StandardRecordMetadataKey.TTL)!);
        });

        it('should return default editor for custom fields', () => {
            init([]);

            expect(component.getFieldDescriptor('custom')).toEqual({ key: 'custom', displayName: 'custom', editorComponent: UnknownComponent });
        });
    });

    function checkKeys(array: Key[], expected: string[]) {
        expect(array).toEqual(expected.map(e => key(e)));
    }

    function key(key: string): Key {
        return { name: key, displayName: testFields.get(key)?.displayName || key };
    }
});
