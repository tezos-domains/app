import { DataFieldsService } from './data-fields.service';
import { NumberComponent } from './editors/number.component';
import { StringComponent } from './editors/string.component';
import { JsonComponent } from './editors/json.component';
import { TestBed } from '@angular/core/testing';
import { transloco } from '@td/test';
import { StandardRecordMetadataKey } from '@tezos-domains/core';

describe('DataFieldsService', () => {
    let service: DataFieldsService;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [transloco()],
            providers: [DataFieldsService],
        });
        service = TestBed.inject(DataFieldsService);
    });

    it('should register field editors', () => {
        service.registerNumberField('tn', { min: 1 });
        service.registerStringField('ts');
        service.registerJsonField('tj');

        const fields = service.getFields();

        const tn = fields.get('tn')!;
        expect(tn.key).toBe('tn');
        expect(tn.editorComponent).toBe(NumberComponent);
        expect(tn.editorComponentArgs).toEqual({ min: 1 });

        const ts = fields.get('ts')!;
        expect(ts.key).toBe('ts');
        expect(ts.editorComponent).toBe(StringComponent);
        expect(ts.editorComponentArgs).toBeUndefined();

        const tj = fields.get('tj')!;
        expect(tj.key).toBe('tj');
        expect(tj.editorComponent).toBe(JsonComponent);
        expect(tj.editorComponentArgs).toBeUndefined();

        const ttl = fields.get(StandardRecordMetadataKey.TTL)!;
        expect(ttl.key).toBe(StandardRecordMetadataKey.TTL);
        expect(ttl.displayName).toBe('TTL (seconds)');
        expect(ttl.editorComponent).toBe(NumberComponent);
        expect(ttl.editorComponentArgs).toEqual({ min: 0 });
    });
});
