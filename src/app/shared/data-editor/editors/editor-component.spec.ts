import { Validators, UntypedFormControl } from '@angular/forms';

import { EditorComponentBase } from './editor-component-base';
import { Component, NgModule } from '@angular/core';
import { MockBuilder, MockedComponentFixture, MockRender } from 'ng-mocks';
import { BytesEncoder } from '@tezos-domains/core';
import { fakeAsync, tick } from '@angular/core/testing';

@Component({
    selector: 'td-test-editor',
    template: '',
})
export class TestEditorComponent extends EditorComponentBase<any> {
    protected createValidators() {
        return [Validators.email];
    }
}

@NgModule({
    declarations: [TestEditorComponent],
})
export class TestEditorModule {}

const e = new BytesEncoder().encode;

describe('EditorComponentBase', () => {
    let fixture: MockedComponentFixture<TestEditorComponent, Partial<TestEditorComponent>>;
    let component: TestEditorComponent;

    beforeEach(() => {
        return MockBuilder(TestEditorComponent, TestEditorModule);
    });

    function init(value: any) {
        fixture = MockRender(TestEditorComponent, {
            key: 'test',
            args: { arg: 1 },
            control: new UntypedFormControl(value),
        });
        component = fixture.point.componentInstance;
    }

    it('should setup inner control', fakeAsync(() => {
        init(e('"69@a.com"'));
        tick();

        expect(component.innerControl.value).toBe('69@a.com');
        expect(component.control.valid).toBeTrue();

        component.innerControl.setValue('aa');

        expect(component.innerControl.hasError('email')).toBeTrue();

        component.innerControl.setValue(null);

        expect(component.innerControl.hasError('required')).toBeTrue();
    }));

    it('should propagate encoded value to outer control', () => {
        init(null);

        component.innerControl.setValue('a@a.com');

        expect(component.control.value).toBe(e('"a@a.com"'));
    });

    it('should not propagate invalid value to outer control', () => {
        init(null);

        component.innerControl.setValue('420');

        expect(component.control.value).toBeNull();
    });

    it('should propagate validation error to outer control', fakeAsync(() => {
        init(null);
        tick();

        expect(component.control.invalid).toBeTrue();

        component.innerControl.setValue('a@a.com');

        expect(component.control.valid).toBeTrue();
    }));

    it('should propagate disabled state from outer control', () => {
        init(null);

        expect(component.innerControl.enabled).toBeTrue();

        component.control.disable();
        expect(component.innerControl.enabled).toBeFalse();

        component.control.enable();
        expect(component.innerControl.enabled).toBeTrue();
    });
});
