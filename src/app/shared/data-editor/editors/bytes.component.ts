import { Component } from '@angular/core';

import { EditorComponentBase } from './editor-component-base';
import { TdValidators } from '../../../utils/form-validators';

@Component({
    selector: 'td-bytes',
    templateUrl: './bytes.component.html',
    styleUrls: ['./bytes.component.scss'],
})
export class BytesComponent extends EditorComponentBase<any> {
    protected encode(value: any) {
        return value;
    }

    protected decode(rawValue: string) {
        return rawValue;
    }

    protected createValidators() {
        return [TdValidators.bytesString];
    }
}
