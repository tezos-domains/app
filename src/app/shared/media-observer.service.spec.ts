import { TestBed } from '@angular/core/testing';

import { MediaObserver } from './media-observer.service';

describe('MediaObserverService', () => {
    let service: MediaObserver;

    beforeEach(() => {
        TestBed.configureTestingModule({});
        service = TestBed.inject(MediaObserver);
    });

    it('should be created', () => {
        expect(service).toBeTruthy();
    });
});
