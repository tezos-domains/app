import BigNumber from 'bignumber.js';
import { FromMutezPipe } from './from-mutez.pipe';

describe('FromMutezPipe', () => {
    it('should convert from number', () => {
        const pipe = new FromMutezPipe();
        expect(pipe.transform(2 * 1e6)).toBe(2);
    });

    it('should convert from BigNumber', () => {
        const pipe = new FromMutezPipe();
        expect(pipe.transform(BigNumber(2 * 1e6))).toBe(2);
    });
});
