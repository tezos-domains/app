import { TranslocoModule } from '@ngneat/transloco';
import { MockBuilder, MockRender, MockedComponentFixture, ngMocks } from 'ng-mocks';
import { transloco } from '@td/test';

import { ActionInputComponent } from './action-input.component';
import { SharedModule } from './shared.module';

describe('ActionInputComponent', () => {
    let fixture: MockedComponentFixture<ActionInputComponent, Partial<ActionInputComponent>>;
    let component: ActionInputComponent;
    let spy: jasmine.Spy;

    beforeEach(() => MockBuilder(ActionInputComponent, SharedModule).keep(TranslocoModule, { export: true }).keep(transloco(), { export: true }));

    beforeEach(() => {
        spy = jasmine.createSpy();
        fixture = MockRender(ActionInputComponent);
        component = fixture.point.componentInstance;
        component.action.subscribe(spy);
    });

    describe('search()', () => {
        it('should emit action when form is submitted', () => {
            component.input = 'whatever';

            component.submit();

            expect(spy).toHaveBeenCalledWith('whatever');
        });

        it('should trim whitespace', () => {
            component.input = '      whatever    ';

            component.submit();

            expect(spy).toHaveBeenCalledWith('whatever');
        });

        it('should do nothing if query is empty or whitespace', () => {
            component.submit();

            expect(spy).not.toHaveBeenCalledWith('whatever');

            component.input = '     ';

            component.submit();

            expect(spy).not.toHaveBeenCalledWith('whatever');
        });
    });

    describe('focus()', () => {
        it('should focus input element', () => {
            component.focus();

            const input = ngMocks.find(fixture.point, 'input').nativeElement;
            expect(document.activeElement).toBe(input);
        });
    });

    describe('clear()', () => {
        it('should set input to undefined and blur', () => {
            component.input = 'aaa';
            component.focus();

            const input = ngMocks.find(fixture.point, 'input').nativeElement;
            expect(document.activeElement).toBe(input);

            component.clear();

            expect(component.input).toBeUndefined();
            expect(document.activeElement).not.toBe(input);
        });
    });
});
