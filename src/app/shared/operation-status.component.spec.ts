import { fakeAsync, tick } from '@angular/core/testing';
import { TransactionWalletOperation } from '@taquito/taquito';
import { TranslocoModule } from '@ngneat/transloco';
import { of, Subject } from 'rxjs';
import { mock, instance, when, verify, deepEqual } from 'ts-mockito';
import { MockedComponentFixture, MockBuilder, MockRender, ngMocks } from 'ng-mocks';
import { transloco, provider, TestNetwork } from '@td/test';

import { OperationStatusComponent } from './operation-status.component';
import { SharedModule } from './shared.module';
import { MessageService } from './message-service.service';
import { AlertComponent } from './alert.component';
import {
    SmartContractOperationEvent,
    SmartContractOperationConfirmationEvent,
    SmartContractOperationCompletedEvent,
    SmartContractOperationPermissionsGrantedEvent,
    SmartContractOperationSentEvent,
} from '../tezos/tezos.service';
import { DynamicHtmlDirective } from './dynamic-html.directive';
import { TezosError, TezosErrorCode } from '../tezos/models';
import { TezosNetworkService } from '../tezos/tezos-network.service';

describe('OperationStatusComponent', () => {
    let fixture: MockedComponentFixture<OperationStatusComponent, Partial<OperationStatusComponent>>;
    let component: OperationStatusComponent;
    let messageServiceMock: MessageService;
    let tezosNetworkServiceMock: TezosNetworkService;
    let operation: Subject<SmartContractOperationEvent>;
    let doneSpy: jasmine.Spy;
    let op: TransactionWalletOperation;
    const states = {
        initial: { message: null, state: 'info' },
        request: { message: 'Confirm the operation in your wallet to proceed.', spinner: true, state: 'info' },
        requestTimeout: { message: 'Confirm the operation in your wallet to proceed.', buttons: ['redo'], spinner: true, state: 'info' },
        suspended: {
            message: 'You have requested to redo this operation. Make sure to check and cancel any pending requests in your wallet before trying again.',
            icon: 'exclamation-triangle',
            state: 'warning',
        },
        sent: { message: 'Waiting for the operation to be included on the blockchain...', buttons: ['view'], state: 'info', spinner: true, link: true },
        confirmation1: { message: 'Waiting for confirmation...', buttons: ['view'], state: 'info', spinner: true, link: true },
        confirmation2: { message: 'Waiting for confirmation...', buttons: ['view'], state: 'info', spinner: true, link: true },
        indexer: { message: 'Operation confirmed. Waiting for indexer to catch up...', buttons: ['view'], state: 'info', spinner: true, link: true },
        success: {
            message: 'Operation completed successfully.',
            buttons: ['view'],
            spinner: false,
            state: 'success',
            link: true,
        },
        error: { message: 'An error occurred: err', icon: 'exclamation-circle', spinner: false, state: 'danger' },
        errorBidLow: { message: 'Your bid was lower than the minimum bid.', icon: 'exclamation-circle', spinner: false, state: 'danger' },
        errorAfterSent: {
            message: 'An error occurred: err',
            buttons: ['view'],
            spinner: false,
            state: 'danger',
            link: true,
        },
    };

    beforeEach(() => {
        messageServiceMock = mock(MessageService);
        tezosNetworkServiceMock = mock(TezosNetworkService);

        return MockBuilder(OperationStatusComponent, SharedModule)
            .keep(TranslocoModule, { export: true })
            .keep(transloco(), { export: true })
            .provide([provider(messageServiceMock), provider(tezosNetworkServiceMock)]);
    });

    beforeEach(() => {
        operation = new Subject();
        op = mock(TransactionWalletOperation);
        doneSpy = jasmine.createSpy();

        when(op.opHash).thenReturn('opGZRbz3yJj9jTkCYduDPudxuFyhbGpcnESgbAN9esYQyMTjjjs');
        when(tezosNetworkServiceMock.activeNetwork).thenReturn(of(TestNetwork));

        fixture = MockRender(OperationStatusComponent, {
            operation,
            done: doneSpy as any,
        });
        component = fixture.point.componentInstance;
    });

    it('should change state based on operation events', () => {
        verifyState(states.initial);

        operation.next(new SmartContractOperationPermissionsGrantedEvent());

        verifyState(states.request);

        operation.next(new SmartContractOperationSentEvent(instance(op)));

        verifyState(states.sent);

        operation.next(new SmartContractOperationConfirmationEvent(1, 3));

        verifyState(states.confirmation1);

        operation.next(new SmartContractOperationConfirmationEvent(2, 3));

        verifyState(states.confirmation2);

        operation.next(new SmartContractOperationConfirmationEvent(3, 3));

        verifyState(states.indexer);

        operation.next(new SmartContractOperationCompletedEvent());

        verifyState(states.success);
    });

    it('should show error message and emit done if error occurs', () => {
        verifyState(states.initial);

        operation.error(new Error('err'));

        verifyState(states.error);
        expect(doneSpy).toHaveBeenCalledWith({ success: false });
    });

    it('should show error message by error code', () => {
        verifyState(states.initial);

        operation.error(new TezosError('err', TezosErrorCode.BID_TOO_LOW));

        verifyState(states.errorBidLow);
        expect(doneSpy).toHaveBeenCalledWith({ success: false });
    });

    it('should show error message if error occurs after operations was already sent', () => {
        verifyState(states.initial);

        operation.next(new SmartContractOperationSentEvent(instance(op)));
        operation.error(new Error('err'));

        verifyState(states.errorAfterSent);
    });

    it('should switch to a new input operation by resetting the state and unsubscribing from the old one', () => {
        operation.next(new SmartContractOperationSentEvent(instance(op)));
        verifyState(states.sent);

        const newOperation = new Subject<SmartContractOperationEvent>();
        fixture.componentInstance.operation = newOperation;
        verifyState(states.initial);

        operation.next(new SmartContractOperationConfirmationEvent(1, 3)); // should not react to old operation
        verifyState(states.initial);

        newOperation.next(new SmartContractOperationSentEvent(instance(op)));
        verifyState(states.sent);
    });

    it('should reset state when operation is null', () => {
        operation.next(new SmartContractOperationSentEvent(instance(op)));
        verifyState(states.sent);

        fixture.componentInstance.operation = null;
        verifyState(states.initial);
    });

    it('should emit suspend when redo is clicked', fakeAsync(() => {
        // reinit inside of fakeAsync zone
        component.operation = operation;

        operation.next(new SmartContractOperationPermissionsGrantedEvent());
        tick(2000);
        fixture.detectChanges();
        verifyState(states.requestTimeout);

        ngMocks.find(fixture.point, '[name=redo]').triggerEventHandler('click', new Event('click'));

        expect(doneSpy).toHaveBeenCalledWith({ success: false });
        verifyState(states.suspended);
    }));

    it('should emit and display success message when operation is completed', () => {
        operation.next(new SmartContractOperationSentEvent(instance(op)));
        operation.next(new SmartContractOperationCompletedEvent());

        verify(
            messageServiceMock.success(
                deepEqual({
                    messageKey: 'operation.success-toast',
                    messageParameters: {
                        hash: 'opGZ...jjjs',
                        url: 'https://testnet.tzstats.com/opGZRbz3yJj9jTkCYduDPudxuFyhbGpcnESgbAN9esYQyMTjjjs',
                    },
                })
            )
        );
        expect(doneSpy).toHaveBeenCalledWith({ success: true });
    });

    function verifyState(state: { message: string | null; buttons?: string[]; state: string; spinner?: boolean; link?: boolean }) {
        const message = state.message;
        const buttons = state.buttons || [];

        fixture.detectChanges();

        if (message == null) {
            expect(component.message).toBeFalsy();

            expect(ngMocks.findAll(fixture.point, '.operation-status').length).toBe(0);
        } else {
            const alert = ngMocks.find(fixture.point, AlertComponent);

            const label = ngMocks.findInstance(alert, DynamicHtmlDirective);
            expect(label.tdDynamicHtml).toBe(message);

            const b = ngMocks.findAll(alert, 'button,a');
            expect(b.length).toBe(buttons.length);
            buttons.forEach(b => expect(ngMocks.find(alert, `[name=${b}]`)).toBeDefined());

            if (state.spinner) {
                expect(alert.componentInstance.inProgress).toBeTrue();
            } else {
                expect(alert.componentInstance.inProgress).toBeFalse();
            }

            expect(alert.componentInstance.state).toBe(state.state || 'info');

            if (state.link) {
                expect(ngMocks.find(alert, 'a').properties.href).toBe('https://testnet.tzstats.com/opGZRbz3yJj9jTkCYduDPudxuFyhbGpcnESgbAN9esYQyMTjjjs');
            } else {
                expect(ngMocks.findAll(alert, 'a').length).toBe(0);
            }
        }
    }
});
