import { TranslocoModule } from '@ngneat/transloco';
import { transloco } from '@td/test';
import { MockBuilder, MockedComponentFixture, MockRender, ngMocks } from 'ng-mocks';

import { CopyToClipboardComponent } from './copy-to-clipboard.component';
import { DomainComponent } from './domain.component';
import { SharedModule } from './shared.module';
import { TruncatePipe } from './truncate.pipe';

describe('DomainComponent', () => {
    let fixture: MockedComponentFixture<DomainComponent, Partial<DomainComponent>>;

    beforeEach(() => MockBuilder(DomainComponent, SharedModule).keep(TranslocoModule, { export: true }).keep(TruncatePipe).keep(transloco(), { export: true }));

    beforeEach(() => {
        fixture = MockRender(DomainComponent, {
            domain: 'ble.tez',
        });
    });

    it('should display domain as a link', () => {
        const link = ngMocks.find(fixture.point, 'a');
        const text = ngMocks.findAll(fixture.point, 'span');

        expect(ngMocks.input(link, 'routerLink')).toEqual(['/domain', 'ble.tez']);
        expect(link.nativeElement.innerText).toEqual('ble.tez');
        expect(text.length).toBe(0);
    });

    it('should display address as a text', () => {
        fixture.point.componentInstance.link = false;
        fixture.detectChanges();

        const link = ngMocks.findAll(fixture.point, 'a');
        const text = ngMocks.find(fixture.point, 'span');

        expect(text.nativeElement.innerText).toEqual('ble.tez');
        expect(link.length).toBe(0);
    });

    it('should not display copy paste', () => {
        const copy = ngMocks.findInstances(fixture.point, CopyToClipboardComponent);

        expect(copy.length).toBe(0);
    });

    it('should display copy paste if enabled', () => {
        fixture.point.componentInstance.copy = true;
        fixture.detectChanges();

        const copy = ngMocks.findInstance(fixture.point, CopyToClipboardComponent);

        expect(copy.text).toBe('ble.tez');
    });
});
