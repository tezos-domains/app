import { EscapePipe } from './escape.pipe';

describe('EscapePipe', () => {
    let pipe: EscapePipe;

    beforeEach(() => {
        pipe = new EscapePipe();
    });

    const TEST_CASES = [
        [undefined, ''],
        [null, ''],
        ['aa', 'aa'],
        ['<s>aa', '&lt;s&gt;aa'],
        ['<a%20%20onclick%3D"alert%28\'fuck\'%29%3B">clickme<%2Fa>', '&lt;a%20%20onclick%3D&quot;alert%28&#39;fuck&#39;%29%3B&quot;&gt;clickme&lt;%2Fa&gt;'],
    ];

    TEST_CASES.forEach(t => {
        it(`should encode ${t[0]}`, () => {
            expect(pipe.transform(t[0])).toBe(t[1] as any);
        });
    });
});
