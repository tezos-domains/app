import { fakeAsync } from '@angular/core/testing';
import { MockBuilder, MockedComponentFixture, MockRender, ngMocks } from 'ng-mocks';
import { Observable, Subject } from 'rxjs';
import { ProgressButtonComponent } from './progress-button.component';
import { SharedModule } from './shared.module';

describe('ProgressButtonComponent', () => {
    let fixture: MockedComponentFixture<ProgressButtonComponent, Partial<ProgressButtonComponent>>;

    beforeEach(async () => {
        return MockBuilder(ProgressButtonComponent, SharedModule);
    });

    function init(stream$: Observable<boolean>) {
        fixture = MockRender(ProgressButtonComponent, { loading: stream$ });
        fixture.detectChanges();
    }

    it('should show spinner after click', fakeAsync(() => {
        const loading$ = new Subject<boolean>();
        init(loading$);

        let spinners = ngMocks.findAll('mat-spinner');
        expect(spinners.length).toBe(0);

        loading$.next(true);
        fixture.detectChanges();

        spinners = ngMocks.findAll('mat-spinner');
        expect(spinners.length).toBe(1);

        loading$.next(false);
        fixture.detectChanges();

        spinners = ngMocks.findAll('mat-spinner');
        expect(spinners.length).toBe(0);
    }));
});
