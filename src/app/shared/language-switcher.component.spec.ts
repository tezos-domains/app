import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { TranslocoModule } from '@ngneat/transloco';
import { FaIconComponent } from '@fortawesome/angular-fontawesome';
import { MockBuilder, MockRender, MockedComponentFixture, ngMocks } from 'ng-mocks';
import { anyOfClass, anything, mock, objectContaining, verify } from 'ts-mockito';
import { transloco, provider, TestConfig } from '@td/test';

import { LanguageSwitcherComponent } from './language-switcher.component';
import { LocaleService } from '../i18n/locale.service';
import { SharedModule } from './shared.module';
import { CookieService } from 'ngx-cookie';
import { Configuration } from '../configuration';

describe('LanguageSwitcherComponent', () => {
    let fixture: MockedComponentFixture<LanguageSwitcherComponent, Partial<LanguageSwitcherComponent>>;
    let localeServiceMock: LocaleService;
    let cookieServiceMock: CookieService;

    beforeEach(() => {
        localeServiceMock = mock(LocaleService);
        cookieServiceMock = mock(CookieService);

        return MockBuilder(LanguageSwitcherComponent, SharedModule)
            .keep(NoopAnimationsModule, { export: true })
            .keep(TranslocoModule, { export: true })
            .keep(transloco(), { export: true })
            .provide([provider(localeServiceMock), provider(cookieServiceMock), { provide: Configuration, useValue: TestConfig }]);
    });

    beforeEach(() => {
        fixture = MockRender(LanguageSwitcherComponent);
    });

    it('should display current language', () => {
        const button = ngMocks.find(fixture.point, 'button');
        expect(button.nativeElement.textContent).toBe('English');
    });

    it('should change language', () => {
        const button = ngMocks.find(fixture.point, 'button');
        const menu = ngMocks.find(fixture.point, 'mat-menu');
        const menuButtons = ngMocks.findAll(menu, 'button');
        const en = menuButtons.find(b => b.nativeElement.textContent === 'English')!;
        expect(en).toBeDefined();
        const currentIcon = ngMocks.find(en, FaIconComponent);
        expect(currentIcon.componentInstance.icon).toEqual(['far', 'dot-circle']);

        const cs = menuButtons.find(b => b.nativeElement.textContent === 'Čeština')!;
        expect(cs).toBeDefined();
        const otherIcon = ngMocks.find(cs, FaIconComponent);
        expect(otherIcon.componentInstance.icon).toEqual(['far', 'circle']);

        cs.triggerEventHandler('click', new Event('click'));
        fixture.detectChanges();

        verify(localeServiceMock.loadLocale('cs')).called();
        verify(cookieServiceMock.put(anything(), anything(), anything())).called();
        verify(
            cookieServiceMock.put(
                'user_preferred_language',
                'cs',
                objectContaining({
                    domain: TestConfig.cookieDomain,
                })
            )
        ).called();
        expect(button.nativeElement.textContent).toBe('Čeština');
        expect(ngMocks.find(cs, FaIconComponent).componentInstance.icon).toEqual(['far', 'dot-circle']);
        expect(ngMocks.find(en, FaIconComponent).componentInstance.icon).toEqual(['far', 'circle']);
    });
});
