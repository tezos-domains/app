/* eslint-disable jasmine/no-focused-tests */
import { fakeAsync, tick } from '@angular/core/testing';
import { TranslocoModule } from '@ngneat/transloco';
import { RecordMetadata, StandardRecordMetadataKey } from '@tezos-domains/core';
import { NameResolver } from '@tezos-domains/resolver';
import { TaquitoTezosDomainsClient } from '@tezos-domains/taquito-client';
import { MockBuilder, MockRender, MockedComponentFixture, ngMocks } from 'ng-mocks';
import { transloco, provider, TezosAddress, TestWallet } from '@td/test';
import { mock, instance, when } from 'ts-mockito';
import { of, ReplaySubject } from 'rxjs';

import { AddressComponent, AddressComponentReverseRecord } from './address.component';
import { SharedModule } from './shared.module';
import { CopyToClipboardComponent } from './copy-to-clipboard.component';
import { TezosDomainsClientService } from '../tezos/integration/tezos-domains-client.service';
import { ReverseRecord, ReverseRecordService } from '../domains/reverse-record.service';
import { TezosService } from '../tezos/tezos.service';
import { TezosWallet } from '../tezos/models';
import { TezosNetworkService } from '../tezos/tezos-network.service';
import { TestNetwork } from '../../test/data';

const ReverseRecords: AddressComponentReverseRecord[] = [
    {
        domain: {
            id: 'ID:current.tez',
            name: 'current.tez',
            data: [
                {
                    id: 'ID:DataItem:current.tez:1',
                    key: StandardRecordMetadataKey.GRAVATAR_HASH,
                    rawValue: '226122',
                },
                {
                    id: 'ID:DataItem:current.tez:2',
                    key: StandardRecordMetadataKey.OPENID_EMAIL,
                    rawValue: '226222',
                },
            ],
        },
    },
    {
        domain: {
            id: 'ID:input.tez',
            name: 'input.tez',
            data: [
                {
                    id: 'ID:DataItem:input.tez:1',
                    key: StandardRecordMetadataKey.GRAVATAR_HASH,
                    rawValue: '226322',
                },
                {
                    id: 'ID:DataItem:input.tez:2',
                    key: StandardRecordMetadataKey.OPENID_EMAIL,
                    rawValue: '226422',
                },
            ],
        },
    },
];

describe('AddressComponent', () => {
    let fixture: MockedComponentFixture<AddressComponent, Partial<AddressComponent>>;
    let tezosDomainsClientServiceMock: TezosDomainsClientService;
    let tezosDomainsMock: TaquitoTezosDomainsClient;
    let tezosDomainsResolverMock: NameResolver;
    let reverseRecordServiceMock: ReverseRecordService;
    let tezosServiceMock: TezosService;
    let tezosNetworkServiceMock: TezosNetworkService;
    let currentReverseRecord: ReplaySubject<ReverseRecord | null>;
    let wallet: ReplaySubject<TezosWallet | null>;

    beforeEach(() => {
        tezosDomainsClientServiceMock = mock(TezosDomainsClientService);
        tezosDomainsMock = mock(TaquitoTezosDomainsClient);
        tezosDomainsResolverMock = mock<NameResolver>();
        reverseRecordServiceMock = mock(ReverseRecordService);
        tezosServiceMock = mock(TezosService);
        tezosNetworkServiceMock = mock(TezosNetworkService);

        currentReverseRecord = new ReplaySubject(1);
        wallet = new ReplaySubject(1);

        when(tezosDomainsClientServiceMock.current).thenReturn(of(instance(tezosDomainsMock)));
        when(tezosDomainsMock.resolver).thenReturn(instance(tezosDomainsResolverMock));

        const data = new RecordMetadata();
        data.setJson(StandardRecordMetadataKey.GRAVATAR_HASH, 'e');
        data.setJson(StandardRecordMetadataKey.OPENID_EMAIL, 'f');
        when(tezosDomainsResolverMock.resolveReverseRecord('tz1xxx')).thenResolve({
            name: 'client.tez',
            address: TezosAddress,
            data,
            expiry: null,
        });
        when(reverseRecordServiceMock.current).thenReturn(currentReverseRecord);
        when(tezosServiceMock.activeWallet).thenReturn(wallet);
        when(tezosNetworkServiceMock.activeNetwork).thenReturn(of(TestNetwork));

        currentReverseRecord.next(ReverseRecords[0] as any);
        wallet.next(TestWallet);

        return MockBuilder(AddressComponent, SharedModule)
            .keep(TranslocoModule, { export: true })
            .keep(transloco(), { export: true })
            .provide([
                provider(tezosDomainsClientServiceMock),
                provider(reverseRecordServiceMock),
                provider(tezosServiceMock),
                provider(tezosNetworkServiceMock),
            ]);
    });

    beforeEach(() => {
        // tests bellow are changing `stats` and other properties so we need to initialize them
        // due to the way ng-mocks works for triggering ngOnChanges hook
        fixture = MockRender(AddressComponent, {
            address: 'tz1xxx',
            stats: undefined,
            resolveReverseRecord: undefined,
            reverseRecord: undefined,
            link: undefined,
        });
    });

    it('should display address as a link', () => {
        const link = ngMocks.find(fixture.point, 'a');
        const text = ngMocks.find(fixture.point, 'a span');

        expect(ngMocks.input(link, 'routerLink')).toEqual(['/address', 'tz1xxx']);
        expect(text.nativeElement.innerText).toEqual('tz1xxx');
    });

    it('should display address as a text', () => {
        fixture.componentInstance.link = false;
        fixture.detectChanges();

        const link = ngMocks.findAll(fixture.point, 'a');
        const text = ngMocks.find(fixture.point, 'span');

        expect(text.nativeElement.innerText).toEqual('tz1xxx');
        expect(link.length).toBe(0);
    });

    it('should display copy paste', () => {
        const copy = ngMocks.findInstance(fixture.point, CopyToClipboardComponent);

        expect(copy.text).toBe('tz1xxx');
    });

    it('should display stats link', () => {
        fixture.componentInstance.stats = true;
        fixture.detectChanges();

        const statsLink = ngMocks.find(fixture.point, '.stats-link');

        expect(statsLink).toBeDefined();
    });

    it('should display reverse record from input', () => {
        fixture.componentInstance.reverseRecord = ReverseRecords[1];
        fixture.detectChanges();

        const reverseNameText = ngMocks.find(fixture.point, 'span.rr');

        expect(reverseNameText.nativeElement.innerText).toBe('input.tez');
        expect(fixture.point.componentInstance.gravatarHash).toBe('c');
        expect(fixture.point.componentInstance.gravatarEmail).toBe('d');
    });

    it('should display reverse record from current', () => {
        fixture.componentInstance.address = TezosAddress;
        fixture.componentInstance.resolveReverseRecord = true;
        fixture.detectChanges();

        const reverseNameText = ngMocks.find(fixture.point, 'span.rr');

        expect(reverseNameText.nativeElement.innerText).toBe('current.tez');
        expect(fixture.point.componentInstance.gravatarHash).toBe('a');
        expect(fixture.point.componentInstance.gravatarEmail).toBe('b');
    });

    it('should display reverse record from client', fakeAsync(() => {
        fixture.componentInstance.resolveReverseRecord = true;
        fixture.detectChanges();
        tick();
        fixture.detectChanges();

        const reverseNameText = ngMocks.find(fixture.point, 'span.rr');

        expect(reverseNameText.nativeElement.innerText).toBe('client.tez');
        expect(fixture.point.componentInstance.gravatarHash).toBe('e');
        expect(fixture.point.componentInstance.gravatarEmail).toBe('f');
    }));
});
