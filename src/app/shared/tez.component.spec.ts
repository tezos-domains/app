import { mock, when, anyNumber } from 'ts-mockito';
import { MockBuilder, MockRender, MockedComponentFixture, ngMocks } from 'ng-mocks';
import { of } from 'rxjs';
import BigNumber from 'bignumber.js';

import { TezComponent } from './tez.component';
import { SharedModule } from './shared.module';
import { XTZPriceService } from '../tezos/integration/xtzprice.service';
import { provider } from '../../test/mocks';

describe('TezComponent', () => {
    let fixture: MockedComponentFixture<TezComponent, Partial<TezComponent>>;
    let priceService: XTZPriceService;

    beforeEach(() => {
        priceService = mock(XTZPriceService);

        when(priceService.convert(anyNumber())).thenCall(n => of(n * 2));

        return MockBuilder(TezComponent, SharedModule).provide(provider(priceService));
    });

    function init(value: number | BigNumber | null, mutez = false, showUsd: boolean | 'only' = false) {
        fixture = MockRender(TezComponent, {
            value,
            mutez,
            showUsd,
        });
    }

    it('add format number (number)', () => {
        init(1000);

        expect(getRenderedValue()).toBe('1,000');
        expect(getRenderedUsd()).toBeNull();
    });

    it('add format number (BigNumber)', () => {
        init(new BigNumber(1000));

        expect(getRenderedValue()).toBe('1,000');
        expect(getRenderedUsd()).toBeNull();
    });

    it('should convert from mutez', () => {
        init(new BigNumber(1000000000), true);

        expect(getRenderedValue()).toBe('1,000');
        expect(getRenderedUsd()).toBeNull();
    });

    it('add display 0 for undefined or null', () => {
        init(null, true);

        expect(getRenderedValue()).toBe('0');
        expect(getRenderedUsd()).toBeNull();
    });

    it('should render 6 decimal places', () => {
        init(new BigNumber('123456789'), true);

        expect(getRenderedValue()).toBe('123.456789');
        expect(getRenderedUsd()).toBeNull();
    });

    it('add display usd', () => {
        init(1000, false, true);

        expect(getRenderedValue()).toBe('1,000');
        expect(getRenderedUsd()).toBe('($2,000.00)');
    });

    it('add display only usd', () => {
        init(1000, false, 'only');

        expect(getRenderedValue()).toBeNull();
        expect(getRenderedUsd()).toBe('($2,000.00)');
    });

    it('should update value when it changes', () => {
        init(1000, false, true);

        expect(getRenderedValue()).toBe('1,000');
        expect(getRenderedUsd()).toBe('($2,000.00)');

        fixture.componentInstance.value = 500;
        fixture.detectChanges();

        expect(getRenderedValue()).toBe('500');
        expect(getRenderedUsd()).toBe('($1,000.00)');

        fixture.componentInstance.mutez = true;
        fixture.detectChanges();

        expect(getRenderedValue()).toBe('0.0005');
        expect(getRenderedUsd()).toBe('($0.00)');
    });

    function getRenderedValue() {
        const texts = ngMocks.findAll('span.tez-value');
        if (texts.length) {
            return texts[0].nativeElement.textContent;
        }

        return null;
    }

    function getRenderedUsd() {
        const texts = ngMocks.findAll('span.usd-value');
        if (texts.length) {
            return texts[0].nativeElement.textContent;
        }

        return null;
    }
});
