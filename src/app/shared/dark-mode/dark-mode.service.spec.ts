import { CookieService } from 'ngx-cookie';
import { firstValueFrom, of } from 'rxjs';
import { first } from 'rxjs/operators';
import { anyOfClass, instance, mock, verify, when } from 'ts-mockito';
import { WindowRef } from '../../browser/window-ref';
import { StorageSchema } from '../../utils/storage';
import { BrowserDarkModeService } from './browser-dark-mode.service';
import { ThemeType } from './dark-mode.model';
import { DarkModeService } from './dark-mode.service';

describe('DarkModeService', () => {
    let service: DarkModeService;
    let windowRefMock: WindowRef;
    let cookieServiceMock: CookieService;
    let browserDarkModeMock: BrowserDarkModeService;

    let addClass: jasmine.Spy;
    let removeClass: jasmine.Spy;
    let querySelector: jasmine.Spy;

    beforeEach(() => {
        windowRefMock = mock(WindowRef);
        cookieServiceMock = mock(CookieService);
        browserDarkModeMock = mock(BrowserDarkModeService);

        addClass = jasmine.createSpy('addClass');
        removeClass = jasmine.createSpy('removeClass');
        querySelector = jasmine.createSpy('querySelector');

        when(windowRefMock.nativeWindow).thenReturn({
            document: { documentElement: { classList: { add: addClass, remove: removeClass } }, querySelector },
        } as any);
        when(browserDarkModeMock.mode).thenReturn(of('dark'));
    });

    describe('when theme cookie is set', () => {
        ['light', 'dark'].forEach(cookieTheme => {
            it(`should return cookie theme: ${cookieTheme}`, async () => {
                when(cookieServiceMock.get(StorageSchema.userTheme.key)).thenReturn(cookieTheme);
                init();

                const theme = await firstValueFrom(service.theme$.pipe(first()));
                expect(theme).toBe(cookieTheme);
            });
        });
    });

    describe('when theme cookie is NOT set', () => {
        ['light', 'dark'].forEach(browserTheme => {
            it(`should return browser theme: ${browserTheme}`, async () => {
                when(browserDarkModeMock.mode).thenReturn(of(browserTheme as ThemeType));
                init();

                const theme = await firstValueFrom(service.theme$);
                expect(theme).toBe(browserTheme);
            });
        });
    });

    describe('toggleTheme', () => {
        it('should change to other theme', async () => {
            init();

            let theme = await firstValueFrom(service.theme$);
            expect(theme).toBe('dark');

            service.toggleTheme();

            theme = await firstValueFrom(service.theme$);
            expect(theme).toBe('light');

            service.toggleTheme();

            theme = await firstValueFrom(service.theme$);
            expect(theme).toBe('dark');
            verify(cookieServiceMock.put(StorageSchema.userTheme.key, { expires: anyOfClass(Date) } as any));
        });
    });

    function init() {
        service = new DarkModeService(instance(windowRefMock), instance(cookieServiceMock), instance(browserDarkModeMock));
    }
});
