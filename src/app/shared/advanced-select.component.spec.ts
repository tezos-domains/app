import { tick, fakeAsync } from '@angular/core/testing';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { CommonModule } from '@angular/common';
import { FormsModule, ValidatorFn } from '@angular/forms';
import { MatMenuModule } from '@angular/material/menu';
import { TranslocoModule } from '@ngneat/transloco';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { StorageMap } from '@ngx-pwa/local-storage';
import { MockedComponentFixture, MockBuilder, MockRender, ngMocks } from 'ng-mocks';
import { mock, when, verify, anything, instance, objectContaining, deepEqual } from 'ts-mockito';
import { provider, transloco } from '@td/test';
import { ReplaySubject, Subject, of } from 'rxjs';
import { first } from 'rxjs/operators';

import { AdvancedSelectComponent, AdvancedSelectOption } from './advanced-select.component';
import { AdvancedSelectAddComponent } from './advanced-select-add.component';
import { StorageSchema } from '../utils/storage';

interface WrapperComponent {
    value: string;
    change: jasmine.Spy;
    options: AdvancedSelectOption[];
    validators: ValidatorFn[];
}

describe('AdvancedSelectComponent', () => {
    let fixture: MockedComponentFixture<WrapperComponent, WrapperComponent>;
    let wrapperComponent: WrapperComponent;
    let storageMapMock: StorageMap;
    let dialogMock: MatDialog;
    let dialogRefMock: MatDialogRef<AdvancedSelectAddComponent>;
    let dialogResult: Subject<AdvancedSelectOption | null>;
    let storedOptions: ReplaySubject<AdvancedSelectOption[] | null>;
    let params: WrapperComponent;

    beforeEach(() => {
        storageMapMock = mock(StorageMap);
        dialogMock = mock(MatDialog);
        dialogRefMock = mock<MatDialogRef<any, any>>(MatDialogRef);
        dialogResult = new Subject();

        params = {
            value: 'default',
            options: [
                { label: 'A', value: 'val' },
                { label: 'B', value: 'default' },
            ],
            change: jasmine.createSpy(),
            validators: [jasmine.createSpy()],
        };

        storedOptions = new ReplaySubject(1);
        storedOptions.next(null);

        when(storageMapMock.watch(StorageSchema.customOptions('some_key').key, StorageSchema.customOptions('some_key').schema)).thenReturn(storedOptions);
        when(storageMapMock.get(StorageSchema.customOptions('some_key').key, StorageSchema.customOptions('some_key').schema)).thenCall(() =>
            storedOptions.pipe(first())
        );
        when(storageMapMock.set(anything(), anything(), anything())).thenReturn(of(void 0));

        when(dialogMock.open(anything(), anything())).thenReturn(instance(dialogRefMock));

        when(dialogRefMock.afterClosed()).thenReturn(dialogResult);

        return MockBuilder(AdvancedSelectComponent)
            .keep(FormsModule)
            .keep(CommonModule)
            .keep(TranslocoModule, { export: true })
            .keep(transloco(), { export: true })
            .mock(MatMenuModule)
            .mock(FontAwesomeModule)
            .provide([provider(storageMapMock), provider(dialogMock)]);
    });

    function init() {
        fixture = MockRender<WrapperComponent, WrapperComponent>(
            '<td-advanced-select [(ngModel)]="value" (valueChange)="change($event)" key="some_key" [options]="options" [valueValidators]="validators"></td-advanced-select>',
            params,
            {
                detectChanges: false,
            }
        );

        fixture.detectChanges();
        tick();
        fixture.detectChanges();

        wrapperComponent = fixture.componentInstance;
    }

    it('should render options and display initial value', fakeAsync(() => {
        init();

        verifyState('default', params.options);
    }));

    it('should render custom options', fakeAsync(() => {
        const customOptions = [{ label: 'C', value: 'custom' }];
        storedOptions.next(customOptions);
        params.value = 'custom';

        init();

        verifyState('custom', params.options.concat(customOptions));
    }));

    it('should update value when selected', fakeAsync(() => {
        init();

        ngMocks.findAll(fixture.point, 'button.advanced-select-option')[0].triggerEventHandler('click', new Event('click'));

        fixture.detectChanges();

        expect(params.change).toHaveBeenCalledWith('val');

        tick();

        verifyState('val', params.options);
        expect(wrapperComponent.value).toBe('val');
    }));

    describe('custom options management', () => {
        let customOption: AdvancedSelectOption;

        beforeEach(() => {
            customOption = { label: 'C', value: 'custom' };
            storedOptions.next([customOption]);
        });

        describe('addOption()', () => {
            it('should show dialog then add resulting custom option to storage and select it', fakeAsync(() => {
                init();

                ngMocks.find(fixture.point, 'button.advanced-select-add').triggerEventHandler('click', new Event('click'));
                fixture.detectChanges();

                verify(
                    dialogMock.open(
                        AdvancedSelectAddComponent,
                        objectContaining({
                            data: {
                                existingValues: ['val', 'default', 'custom'],
                                valueValidators: wrapperComponent.validators,
                            },
                        })
                    )
                ).called();

                const newOption = { label: 'D', value: 'new' };
                dialogResult.next(newOption);
                verify(
                    storageMapMock.set(
                        StorageSchema.customOptions('some_key').key,
                        deepEqual([customOption, newOption]),
                        StorageSchema.customOptions('some_key').schema
                    )
                ).called();
                storedOptions.next([customOption, newOption]);

                fixture.detectChanges();
                tick();

                verifyState('new', params.options.concat([customOption, newOption]));
            }));

            it('should seed options if storage is empty', fakeAsync(() => {
                storedOptions.next(null);
                init();

                ngMocks.find(fixture.point, 'button.advanced-select-add').triggerEventHandler('click', new Event('click'));
                fixture.detectChanges();

                const newOption = { label: 'D', value: 'new' };
                dialogResult.next(newOption);
                verify(
                    storageMapMock.set(StorageSchema.customOptions('some_key').key, deepEqual([newOption]), StorageSchema.customOptions('some_key').schema)
                ).called();
            }));
        });

        describe('removeOption()', () => {
            it('should remove option', fakeAsync(() => {
                init();

                ngMocks
                    .find(fixture.point, 'button.advanced-select-option:nth-child(3) .advanced-select-remove')
                    .triggerEventHandler('click', new Event('click'));
                fixture.detectChanges();

                verify(storageMapMock.set(StorageSchema.customOptions('some_key').key, deepEqual([]), StorageSchema.customOptions('some_key').schema)).called();
            }));

            it('should set value to null if removing selected option', fakeAsync(() => {
                params.value = 'custom';
                init();

                ngMocks
                    .find(fixture.point, 'button.advanced-select-option:nth-child(3) .advanced-select-remove')
                    .triggerEventHandler('click', new Event('click'));

                fixture.detectChanges();
                tick();

                expect(getSelectedText()).toBe('Not Set');
            }));
        });
    });

    function verifyState(value: string, options: AdvancedSelectOption[]) {
        const selectedOption = options.find(o => o.value === value)!;

        expect(getSelectedText()).toBe(`${selectedOption.label} (${selectedOption.value})`);
        expect(getRenderedOptions()).toEqual(options);

        const elements = ngMocks.findAll(fixture.point, 'button.advanced-select-option div');
        for (const element of elements) {
            if (ngMocks.find(element, 'small').nativeElement.textContent === value) {
                expect(element.classes['accent']).toBeTrue();
            }
        }
    }

    function getRenderedOptions(): AdvancedSelectOption[] {
        const options = ngMocks.findAll(fixture.point, 'button.advanced-select-option');
        return options.map(o => ({ label: ngMocks.find(o, 'span').nativeElement.textContent, value: ngMocks.find(o, 'small').nativeElement.textContent }));
    }

    function getSelectedText() {
        const selectedSpan = ngMocks.find(fixture.point, 'button.advanced-select-value span');

        return selectedSpan.nativeElement.textContent;
    }
});
