import { ReactiveFormsModule } from '@angular/forms';
import { mock, deepEqual, verify } from 'ts-mockito';
import { MockBuilder, MockRender, MockedComponentFixture } from 'ng-mocks';
import { provider } from '@td/test';

import { SharedModule } from '../shared/shared.module';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { AdvancedSelectAddComponent } from './advanced-select-add.component';

describe('AdvancedSelectAddComponent', () => {
    let fixture: MockedComponentFixture<AdvancedSelectAddComponent, Partial<AdvancedSelectAddComponent>>;
    let component: AdvancedSelectAddComponent;
    let dialogRefMock: MatDialogRef<AdvancedSelectAddComponent>;

    beforeEach(() => {
        dialogRefMock = mock<MatDialogRef<any, any>>(MatDialogRef);

        return MockBuilder(AdvancedSelectAddComponent, SharedModule)
            .provide([provider(dialogRefMock)])
            .keep(ReactiveFormsModule);
    });

    beforeEach(() => {
        fixture = MockRender(
            AdvancedSelectAddComponent,
            {},
            {
                providers: [{ provide: MAT_DIALOG_DATA, useValue: { existingValues: ['a', 'b'], valueValidators: [jasmine.createSpy()] } }],
                detectChanges: true,
            }
        );

        component = fixture.point.componentInstance;
    });

    describe('form', () => {
        it('should create form', () => {
            expect(component.form.get('label')?.value).toBe('');
            expect(component.form.get('value')?.value).toBe('');
        });
    });

    describe('add()', () => {
        it('should close dialog with form data', () => {
            const data = { label: 'xxx', value: 'c' };
            component.form.setValue(data);

            component.add();

            verify(dialogRefMock.close(deepEqual(data))).called();
        });
    });

    describe('cancel()', () => {
        it('should close the modal', () => {
            component.cancel();

            verify(dialogRefMock.close(null)).called();
        });
    });
});
