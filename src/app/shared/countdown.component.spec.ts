import { fakeAsync, discardPeriodicTasks, tick } from '@angular/core/testing';
import { TranslocoModule } from '@ngneat/transloco';
import { MockBuilder, MockedComponentFixture, MockRender } from 'ng-mocks';
import { transloco, mockNow, resetNow } from '@td/test';
import dayjs from 'dayjs';

import { CountdownComponent } from './countdown.component';
import { SharedModule } from './shared.module';

describe('CountdownComponent', () => {
    let fixture: MockedComponentFixture<CountdownComponent, Partial<CountdownComponent>>;

    beforeEach(() => {
        mockNow(dayjs('2020-06-01T20:05:00Z'));

        return MockBuilder(CountdownComponent, SharedModule).keep(TranslocoModule, { export: true }).keep(transloco(), { export: true });
    });

    afterEach(() => {
        resetNow();
    });

    function init(date?: dayjs.Dayjs) {
        fixture = MockRender(CountdownComponent, {
            value: date || dayjs('2020-06-02T21:06:01Z'),
        });
    }

    it('should display countdown relative to now', fakeAsync(() => {
        init();

        expect(fixture.point.nativeElement.textContent.trim()).toBe('1d 1h 1m 1s');

        discardPeriodicTasks();
    }));

    it('should update every second', fakeAsync(() => {
        init();

        expect(fixture.point.nativeElement.textContent.trim()).toBe('1d 1h 1m 1s');

        mockNow(dayjs('2020-06-01T20:05:01Z'));
        tick(1000);
        fixture.detectChanges();

        expect(fixture.point.nativeElement.textContent.trim()).toBe('1d 1h 1m 0s');

        discardPeriodicTasks();
    }));

    it('should emit an event when it reaches 0', fakeAsync(() => {
        init();

        const spy = jasmine.createSpy();
        fixture.point.componentInstance.zero.subscribe(spy);

        mockNow(dayjs('2020-06-02T21:06:01Z'));
        tick(1000);
        fixture.detectChanges();

        expect(spy).toHaveBeenCalled();

        discardPeriodicTasks();
    }));

    const TEST_CASES = [
        { now: dayjs('2020-06-02T21:06:01Z'), expected: '' },
        { now: dayjs('2020-06-02T21:06:00Z'), expected: '1s' },
        { now: dayjs('2020-06-02T21:05:01Z'), expected: '1m 0s' },
        { now: dayjs('2020-06-02T20:06:01Z'), expected: '1h 0m 0s' },
        { now: dayjs('2020-06-01T21:06:01Z'), expected: '1d 0h 0m 0s' },
        { now: dayjs('2020-06-01T21:06:00Z'), expected: '1d 0h 0m 1s' },
        { now: dayjs('2020-06-02T21:06:02Z'), expected: '' },
        { now: dayjs('2020-06-02T22:06:01Z'), expected: '' },
        { now: dayjs('2021-07-03T22:06:00Z'), expected: '' },
    ];

    TEST_CASES.forEach(t => {
        it(`should display correct format ${t.now.format()} ${t.expected}`, fakeAsync(() => {
            mockNow(t.now);

            init();

            expect(fixture.point.nativeElement.textContent.trim()).toBe(t.expected);

            discardPeriodicTasks();
        }));
    });
});
