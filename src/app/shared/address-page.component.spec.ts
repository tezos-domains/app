import { ActivatedRoute } from '@angular/router';
import { TranslocoModule } from '@ngneat/transloco';
import { Subject, ReplaySubject } from 'rxjs';
import { mock, when, verify, anything, deepEqual, instance } from 'ts-mockito';
import { MockBuilder, MockRender, MockedComponentFixture, DefaultRenderComponent } from 'ng-mocks';
import { TestWallet, TezosAddress, provider, TezosAddress2, transloco, TestReverseRecord, TestReverseRecord2 } from '@td/test';

import { SharedModule } from '../shared/shared.module';
import { AddressPageComponent } from './address-page.component';
import { TezosService } from '../tezos/tezos.service';
import { TezosWallet } from '../tezos/models';
import { AppService } from '../app-service';
import { ErrorModule } from '../error/error.module';
import { PageService } from '../browser/page.service';
import { ReverseRecordDetailDataSource } from '../graphql/reverse-record-data-source';
import { DataSourceFactory } from '../graphql/data-source-factory';
import { ReverseRecordDetailQuery } from '../graphql/graphql.generated';
import { ReverseRecord, ReverseRecordService } from '../domains/reverse-record.service';

describe('AddressPageComponent', () => {
    let fixture: MockedComponentFixture<AddressPageComponent, DefaultRenderComponent<AddressPageComponent>>;
    let component: AddressPageComponent;
    let activateRouteMock: ActivatedRoute;
    let tezosServiceMock: TezosService;
    let appServiceMock: AppService;
    let pageServiceMock: PageService;
    let reverseRecordServiceMock: ReverseRecordService;
    let routeParamsMock: Subject<{ [key: string]: string }>;
    let offlineToOnline: Subject<boolean>;
    let wallet: ReplaySubject<TezosWallet | null>;
    let reverseRecordDetailDataSourceMock: ReverseRecordDetailDataSource;
    let dataSourceFactoryMock: DataSourceFactory;
    let getReverseRecord: Subject<ReverseRecordDetailQuery>;
    let currentUserReverseRecord: ReplaySubject<ReverseRecord>;

    beforeEach(() => {
        activateRouteMock = mock(ActivatedRoute);
        tezosServiceMock = mock(TezosService);
        appServiceMock = mock(AppService);
        pageServiceMock = mock(PageService);
        reverseRecordDetailDataSourceMock = mock(ReverseRecordDetailDataSource);
        reverseRecordServiceMock = mock(ReverseRecordService);
        dataSourceFactoryMock = mock(DataSourceFactory);
        routeParamsMock = new Subject();
        offlineToOnline = new Subject();
        wallet = new ReplaySubject(1);
        currentUserReverseRecord = new ReplaySubject(1);
        getReverseRecord = new Subject();

        when(dataSourceFactoryMock.createReverseRecordDetailDataSource()).thenReturn(instance(reverseRecordDetailDataSourceMock));
        when(reverseRecordServiceMock.current).thenReturn(currentUserReverseRecord);
        when(reverseRecordDetailDataSourceMock.data$).thenReturn(getReverseRecord);
        when(activateRouteMock.params).thenReturn(routeParamsMock);
        when(tezosServiceMock.activeWallet).thenReturn(wallet);
        when(appServiceMock.goingOnlineFromOffline).thenReturn(offlineToOnline);

        wallet.next(TestWallet);
        currentUserReverseRecord.next(TestReverseRecord);

        return MockBuilder(AddressPageComponent, SharedModule)
            .keep(TranslocoModule, { export: true })
            .keep(transloco(), { export: true })
            .provide([
                provider(activateRouteMock),
                provider(tezosServiceMock),
                provider(appServiceMock),
                provider(pageServiceMock),
                provider(dataSourceFactoryMock),
                provider(reverseRecordServiceMock),
            ])
            .mock(ErrorModule, { export: true });
    });

    beforeEach(() => {
        fixture = MockRender(`<td-address-page scope="some-scope"></td-address-page>`);

        component = fixture.point.componentInstance;
    });

    it('should init wallet', () => {
        expect(component.wallet).toBe(TestWallet);
    });

    it('should set address for current wallet', () => {
        routeParamsMock.next({ address: TezosAddress });

        expect(component.address).toBe(TezosAddress);
        expect(component.validAddress).toBeTrue();
        expect(component.currentUserReverseRecord).toBe(TestReverseRecord);

        verify(reverseRecordDetailDataSourceMock.load(anything())).never();
    });

    it('should set address for different address than wallet', () => {
        routeParamsMock.next({ address: TezosAddress2 });
        getReverseRecord.next({ reverseRecord: TestReverseRecord2 });

        expect(component.address).toBe(TezosAddress2);
        expect(component.validAddress).toBeTrue();
        expect(component.reverseRecord).toBe(TestReverseRecord2);
    });

    it('should show error message if address is invalid', () => {
        routeParamsMock.next({ address: 'aaa' });

        expect(component.validAddress).toBeFalse();
    });

    it('should set address as url changes', () => {
        routeParamsMock.next({ address: TezosAddress });

        expect(component.address).toBe(TezosAddress);
        expect(component.validAddress).toBeTrue();
        verify(pageServiceMock.setTitle('my-some-scope', anything())).called();

        routeParamsMock.next({ address: 'aaa' });

        expect(component.validAddress).toBeFalse();

        routeParamsMock.next({ address: TezosAddress2 });
        getReverseRecord.next({ reverseRecord: TestReverseRecord2 });

        expect(component.address).toBe(TezosAddress2);
        expect(component.validAddress).toBeTrue();
        expect(component.reverseRecord).toBe(TestReverseRecord2);
        verify(pageServiceMock.setTitle('address-some-scope', deepEqual({ address: TezosAddress2 }))).called();
    });

    it('should update title when wallet changes', () => {
        routeParamsMock.next({ address: TezosAddress });

        verify(pageServiceMock.setTitle('my-some-scope', anything())).called();

        wallet.next(null);

        verify(pageServiceMock.setTitle('address-some-scope', deepEqual({ address: TezosAddress }))).called();
    });

    it('should stop setting when destroyed', () => {
        fixture.destroy();

        routeParamsMock.next({ address: TezosAddress });
        currentUserReverseRecord.next(TestReverseRecord);

        expect(component.address).toBeUndefined();
        expect(component.reverseRecord).toBeUndefined();
    });
});
