import { MockedComponentFixture, MockBuilder, MockRender, ngMocks } from 'ng-mocks';

import { SharedModule } from './shared.module';
import { AlertComponent } from './alert.component';
import { FaIconComponent } from '@fortawesome/angular-fontawesome';
import { MatProgressSpinner } from '@angular/material/progress-spinner';

describe('OperationStatusComponent', () => {
    let fixture: MockedComponentFixture<AlertComponent, Partial<AlertComponent>>;

    beforeEach(() => {
        return MockBuilder(AlertComponent, SharedModule);
    });

    const TEST_CASES = [
        { state: 'info', inProgress: false, color: 'info', icon: 'info-circle' },
        { state: 'warning', inProgress: false, color: 'warning', icon: 'exclamation-triangle' },
        { state: 'danger', inProgress: false, color: 'danger', icon: 'exclamation-circle' },
        { state: 'success', inProgress: false, color: 'success', icon: 'check-double' },
        { state: 'info', inProgress: true, color: 'info', icon: null },
        { state: 'warning', inProgress: true, color: 'warning', icon: null },
        { state: 'danger', inProgress: true, color: 'danger', icon: null },
        { state: 'success', inProgress: true, color: 'success', icon: null },
    ];

    TEST_CASES.forEach(t => {
        it(`should render an alert with state ${t.state} and inProgress ${t.inProgress}`, () => {
            fixture = MockRender(`<td-alert state="${t.state}" [inProgress]="${t.inProgress}"><span>message</span></td-alert>`);

            const message = ngMocks.find(fixture.point, 'span');
            expect(message.nativeElement.textContent).toBe('message');

            if (t.icon) {
                expect(ngMocks.find(fixture.point, FaIconComponent).componentInstance.icon).toBe(t.icon as any);
            } else {
                expect(ngMocks.findAll(fixture.point, FaIconComponent).length).toBe(0);
            }

            if (t.inProgress) {
                expect(ngMocks.find(fixture.point, MatProgressSpinner)).toBeDefined();
            } else {
                expect(ngMocks.findAll(fixture.point, MatProgressSpinner).length).toBe(0);
            }
        });
    });
});
