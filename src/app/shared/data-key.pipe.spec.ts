import { DataKeyPipe } from './data-key.pipe';
import { TestBed } from '@angular/core/testing';
import { transloco } from '@td/test';

describe('DataKeyPipe', () => {
    let pipe: DataKeyPipe;
    let spy: jasmine.Spy;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [transloco()],
            providers: [DataKeyPipe],
        });

        pipe = TestBed.inject(DataKeyPipe);
        spy = jasmine.createSpy();
    });

    it('should return name for data key', () => {
        pipe.transform('td:ttl').subscribe(spy);

        expect(spy).toHaveBeenCalledWith('TTL (seconds)');
    });

    it('should return key if translation does not exist', () => {
        pipe.transform('xxx').subscribe(spy);

        expect(spy).toHaveBeenCalledWith('xxx');
    });
});
