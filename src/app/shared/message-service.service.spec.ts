import { MessageService } from './message-service.service';
import { TestBed } from '@angular/core/testing';
import { TranslocoModule } from '@ngneat/transloco';
import { transloco, provider } from '@td/test';
import { ToastrService } from 'ngx-toastr';
import { mock, verify, anything, objectContaining } from 'ts-mockito';

describe('MessageService', () => {
    let service: MessageService;
    let toastrMock: ToastrService;

    beforeEach(() => {
        toastrMock = mock(ToastrService);

        TestBed.configureTestingModule({
            imports: [TranslocoModule, transloco()],
            providers: [MessageService, provider(toastrMock)],
        });

        service = TestBed.inject(MessageService);
    });

    testType('success', 'Success');
    testType('error', 'Error');

    function testType(type: 'success' | 'error', defaultTitle: string) {
        describe(`${type}()`, () => {
            it('should show toast with specified message', () => {
                service[type]({ message: 'msg' });

                verify(toastrMock[type]('msg', defaultTitle, anything())).called();
            });

            it('should show toast with specified messageKey and parameters', () => {
                service[type]({ messageKey: 'operation.success-toast', messageParameters: { url: 'url', hash: 'hash' } });

                verify(
                    toastrMock[type](
                        `Operation hash completed successfully. <a href="url" target="_blank">Click here to view it</a>.`,
                        defaultTitle,
                        anything()
                    )
                ).called();
            });

            it('should show toast with specified options', () => {
                service[type]({ message: 'msg', positionClass: 'custom' });

                verify(toastrMock[type]('msg', defaultTitle, objectContaining({ positionClass: 'custom' }))).called();
            });

            it('should show toast with custom title', () => {
                service[type]({ message: 'msg', title: 'title' });

                verify(toastrMock[type]('msg', 'title', anything())).called();
            });

            it('should show toast with custom titleKey', () => {
                service[type]({ message: 'msg', titleKey: 'domain-form.title-edit', titleParameters: { name: 'test' } });

                verify(toastrMock[type]('msg', 'Edit test', anything())).called();
            });

            it('should throw if message and messageKey are not specified', () => {
                expect(() => service[type]({})).toThrowError();
            });

            it('should throw if title and titleKey are not specified', () => {
                expect(() => service[type]({ titleKey: undefined })).toThrowError();
            });
        });
    }
});
