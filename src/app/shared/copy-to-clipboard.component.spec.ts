import { fakeAsync, tick } from '@angular/core/testing';
import { Clipboard } from '@angular/cdk/clipboard';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { TranslocoModule } from '@ngneat/transloco';
import { mock, anyString, when, verify } from 'ts-mockito';
import { MockBuilder, MockRender, MockedComponentFixture, ngMocks } from 'ng-mocks';
import { provider, transloco } from '@td/test';

import { SharedModule } from './shared.module';
import { CopyToClipboardComponent } from './copy-to-clipboard.component';

describe('CopyToClipboardComponent', () => {
    let fixture: MockedComponentFixture<CopyToClipboardComponent, Partial<CopyToClipboardComponent>>;
    let component: CopyToClipboardComponent;
    let clipboardMock: Clipboard;

    beforeEach(() => {
        clipboardMock = mock(Clipboard);

        when(clipboardMock.copy(anyString())).thenReturn(true);

        return MockBuilder(CopyToClipboardComponent, SharedModule)
            .keep(NoopAnimationsModule, { export: true })
            .keep(TranslocoModule, { export: true })
            .keep(transloco(), { export: true })
            .provide(provider(clipboardMock));
    });

    beforeEach(() => {
        fixture = MockRender(CopyToClipboardComponent, {
            text: 'txt',
        });
        component = fixture.point.componentInstance;
    });

    it('should copy text to clipboard when clicked and show success text for 2 seconds', fakeAsync(() => {
        expect(component.copyDone).toBeFalsy();
        expect(ngMocks.findAll(fixture.point, 'span').length).toBe(0);

        const copy = ngMocks.find(fixture.point, '.copy-to-clipboard');

        copy.triggerEventHandler('click', new Event('click'));

        verify(clipboardMock.copy('txt')).called();

        fixture.detectChanges();

        expect(component.copyDone).toBeTrue();
        expect(ngMocks.findAll(fixture.point, '.txt-success').length).toBe(1);

        tick(2000);
        fixture.detectChanges();
        tick();

        expect(component.copyDone).toBeFalse();
        expect(ngMocks.findAll(fixture.point, '.txt-success').length).toBe(0);
    }));

    it('should not show success text if copy fails', fakeAsync(() => {
        when(clipboardMock.copy(anyString())).thenReturn(false);

        const copy = ngMocks.find(fixture.point, '.copy-to-clipboard');

        copy.triggerEventHandler('click', new Event('click'));

        verify(clipboardMock.copy('txt')).called();

        fixture.detectChanges();
        expect(ngMocks.findAll(fixture.point, 'span').length).toBe(0);
    }));
});
