import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { TezosWallet } from '../tezos/models';
import { TezosService } from '../tezos/tezos.service';
import { DomainSummaryTableComponent } from '../domains/domain-summary-table.component';

@Component({
    selector: 'td-home-domains',
    templateUrl: './domains.component.html',
    styleUrls: ['./domains.component.scss'],
})
export class DomainsComponent implements OnInit, OnDestroy {
    wallet: TezosWallet;

    @ViewChild(DomainSummaryTableComponent) table: DomainSummaryTableComponent;

    private unsubscribe = new Subject<void>();

    constructor(private tezosService: TezosService) {}

    ngOnInit() {
        this.tezosService.activeWallet.pipe(takeUntil(this.unsubscribe)).subscribe(w => (this.wallet = w!));
    }

    ngOnDestroy() {
        this.unsubscribe.next();
        this.unsubscribe.complete();
    }
}
