import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { TezosWallet } from '../tezos/models';
import { TezosService } from '../tezos/tezos.service';
import { AuctionSummaryTableComponent } from '../auctions/auction-summary-table.component';

@Component({
    selector: 'td-home-auctions',
    templateUrl: './auctions.component.html',
    styleUrls: ['./auctions.component.scss'],
})
export class AuctionsComponent implements OnInit, OnDestroy {
    wallet: TezosWallet;

    @ViewChild(AuctionSummaryTableComponent) table: AuctionSummaryTableComponent;

    private unsubscribe = new Subject<void>();

    constructor(private tezosService: TezosService) {}

    ngOnInit() {
        this.tezosService.activeWallet.pipe(takeUntil(this.unsubscribe)).subscribe(w => (this.wallet = w!));
    }

    ngOnDestroy() {
        this.unsubscribe.next();
        this.unsubscribe.complete();
    }
}
