import { fakeAsync, tick } from '@angular/core/testing';
import { StorageMap } from '@ngx-pwa/local-storage';
import { TezosToolkit } from '@taquito/taquito';
import { RpcClient } from '@taquito/rpc';
import { BehaviorSubject, of } from 'rxjs';
import { mock, when, instance, anyString, anything, verify, deepEqual } from 'ts-mockito';
import { TestNetwork, TestConfig, TestPromise } from '@td/test';

import { TezosNetworkService } from './tezos-network.service';
import { TezosToolkitService } from './integration/tezos-toolkit.service';
import { StorageSchema } from '../utils/storage';

describe('TezosNetworkService', () => {
    let service: TezosNetworkService;
    let storageMapMock: StorageMap;
    let tezosToolkitServiceMock: TezosToolkitService;
    let tezosToolkitMock: TezosToolkit;
    let rpcClientMock: RpcClient;
    let customRpcUrl: BehaviorSubject<string | undefined>;
    let chainId: TestPromise<string>;
    let spy: jasmine.Spy;

    beforeEach(() => {
        storageMapMock = mock(StorageMap);
        tezosToolkitServiceMock = mock(TezosToolkitService);
        tezosToolkitMock = mock(TezosToolkit);
        rpcClientMock = mock(RpcClient);
        chainId = new TestPromise();
        customRpcUrl = new BehaviorSubject<string | undefined>(undefined);
        spy = jasmine.createSpy();

        when(storageMapMock.watch(StorageSchema.rpcUrl.key, deepEqual(StorageSchema.rpcUrl.schema))).thenReturn(customRpcUrl);
        when(storageMapMock.set(anyString(), anything(), anything())).thenReturn(of());
        when(storageMapMock.delete(anyString())).thenReturn(of());

        when(tezosToolkitServiceMock.new('crpc')).thenReturn(instance(tezosToolkitMock));
        when(tezosToolkitMock.rpc).thenReturn(instance(rpcClientMock));
        when(rpcClientMock.getChainId()).thenReturn(chainId);

        service = new TezosNetworkService(instance(storageMapMock), TestConfig, instance(tezosToolkitServiceMock));
    });

    describe('activeNetwork', () => {
        it('should get network from config and emit it', () => {
            service.activeNetwork.subscribe(spy);

            expect(spy).toHaveBeenCalledWith({ ...TestNetwork, rpcUrl: 'https://testnet-tezos.giganode.io' });
            expect(service.customRpcUrl).toBeNull();
        });

        it('should ignore disabled networks in config', () => {
            const testNetwork = {
                ...TestNetwork,
                rpcUrls: [
                    { name: 'Giganode1', url: 'https://testnet-tezos.giganode1.io', enabled: false },
                    { name: 'Giganode2', url: 'https://testnet-tezos.giganode2.io', enabled: <any>'false' },
                    ...TestNetwork.rpcUrls,
                ],
            };

            const testConfig = {
                ...TestConfig,
                network: testNetwork,
            };

            service = new TezosNetworkService(instance(storageMapMock), testConfig, instance(tezosToolkitServiceMock));

            service.activeNetwork.subscribe(spy);

            expect(spy).toHaveBeenCalledWith({ ...TestNetwork, rpcUrl: 'https://testnet-tezos.giganode.io' });
            expect(service.customRpcUrl).toBeNull();
        });

        it('should get network from config and emit it with overridden rpcUrl from storage if chain id on the url is valid for current network', fakeAsync(() => {
            customRpcUrl.next('crpc');

            service.activeNetwork.subscribe(spy);

            chainId.resolve(TestNetwork.chainId);
            tick();

            expect(spy).toHaveBeenCalledWith({ ...TestNetwork, rpcUrl: 'crpc' });
            expect(service.customRpcUrl).toBe('crpc');
        }));

        it('should get network from config and emit it default rpcUrl from storage if chain id on the url is not valid for current network', fakeAsync(() => {
            customRpcUrl.next('crpc');

            service.activeNetwork.subscribe(spy);

            chainId.resolve('ble');
            tick();

            expect(spy).not.toHaveBeenCalledWith(jasmine.objectContaining({ rpcUrl: 'crpc' }));
            expect(service.customRpcUrl).toBe('crpc');
        }));

        it('should get network from config and emit it default rpcUrl from storage if custom url returns an error', fakeAsync(() => {
            customRpcUrl.next('crpc');

            service.activeNetwork.subscribe(spy);

            chainId.reject('ble');
            tick();

            expect(spy).not.toHaveBeenCalledWith(jasmine.objectContaining({ rpcUrl: 'crpc' }));
            expect(service.customRpcUrl).toBe('crpc');
        }));
    });

    describe('setCustomRpcUrl()', () => {
        it('should store custom rpc url', () => {
            service.setCustomRpcUrl('crpc');

            verify(storageMapMock.set(StorageSchema.rpcUrl.key, 'crpc', StorageSchema.rpcUrl.schema)).called();
        });

        it('should delete custom rpc url from store if null is specified', () => {
            service.setCustomRpcUrl(null);

            verify(storageMapMock.delete(StorageSchema.rpcUrl.key)).called();
        });
    });
});
