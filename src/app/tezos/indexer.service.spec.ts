import { fakeAsync, TestBed, tick } from '@angular/core/testing';
import { TestConfig, transloco } from '@td/test';
import { ApolloTestingController, ApolloTestingModule } from 'apollo-angular/testing';
import { GraphQLError } from 'graphql';
import { Configuration } from '../configuration';
import { BlockDocument } from '../graphql/graphql.generated';
import { IndexerService } from './indexer.service';
import { TezosErrorCode } from './models';

describe('IndexerService', () => {
    let service: IndexerService;
    let apollo: ApolloTestingController;
    let spy: jasmine.Spy;

    beforeEach(() => {
        spy = jasmine.createSpy();

        TestBed.configureTestingModule({
            imports: [ApolloTestingModule.withClients(['gvpool']), transloco()],
            providers: [{ provide: Configuration, useValue: TestConfig }],
        });

        service = TestBed.inject(IndexerService);

        apollo = TestBed.inject(ApolloTestingController);
    });

    afterEach(() => {
        apollo.verify();
    });

    describe('whenBlockIndexed()', () => {
        it('should check if block is indexed periodically and emit when it does', fakeAsync(() => {
            service.whenBlockIndexed('hash', false).subscribe(spy);

            const op = apollo.expectOne(BlockDocument);

            expect(op.operation.variables.hash).toBe('hash');

            op.flushData({ block: null });
            tick();

            expect(spy).not.toHaveBeenCalled();

            tick(2000);
            apollo.expectOne(BlockDocument).flushData({ block: { hash: 'hash', timestamp: new Date().toISOString(), level: 1 } });
            tick();

            expect(spy).toHaveBeenCalled();
        }));

        it('should throw when timeout is reached', fakeAsync(() => {
            const errorSpy = jasmine.createSpy();
            service.whenBlockIndexed('hash', false).subscribe({ next: spy, error: errorSpy });

            for (let i = 0; i < TestConfig.network.blockIndexedTimeoutMs / 2000; i++) {
                apollo.expectOne(BlockDocument).flushData({ block: null });
                tick();
                tick(2000);
            }

            expect(spy).not.toHaveBeenCalled();

            expect(errorSpy.calls.mostRecent().args[0].message).toBe('Timeout while waiting for indexer.');
            expect(errorSpy.calls.mostRecent().args[0].errorCode).toBe(TezosErrorCode.INDEXER_TIMEOUT);
        }));

        it('should rethrow error', fakeAsync(() => {
            const errorSpy = jasmine.createSpy();
            service.whenBlockIndexed('hash', false).subscribe({ next: spy, error: errorSpy });

            const error = new GraphQLError('err');
            apollo.expectOne(BlockDocument).graphqlErrors([error]);
            tick();

            expect(spy).not.toHaveBeenCalled();
            expect(errorSpy).toHaveBeenCalledWith(error);
        }));
    });
});
