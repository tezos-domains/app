import { discardPeriodicTasks, fakeAsync, TestBed, tick } from '@angular/core/testing';
import { RpcClient } from '@taquito/rpc';
import { TezosToolkit } from '@taquito/taquito';
import { provider, TestConfig, TestPromise, TezosAddress } from '@td/test';
import { ApolloTestingController, ApolloTestingModule } from 'apollo-angular/testing';
import BigNumber from 'bignumber.js';
import { of } from 'rxjs';
import { anything, deepEqual, instance, mock, verify, when } from 'ts-mockito';
import { Configuration } from '../configuration';
import { BidderBalanceDocument } from '../graphql/graphql.generated';
import { ErrorPresenterService } from '../utils/error-presenter.service';
import { TezosToolkitService } from './integration/tezos-toolkit.service';
import { TezosWallet } from './models';
import { TezosWalletService } from './tezos-wallet.service';

describe('TezosWalletService', () => {
    let service: TezosWalletService;
    let tezosToolkitServiceMock: TezosToolkitService;
    let tezosToolkitMock: TezosToolkit;
    let apollo: ApolloTestingController;
    let errorPresenterServiceMock: ErrorPresenterService;
    let rpcClientMock: RpcClient;
    let getBalance: TestPromise<BigNumber>;
    let getBalance2: TestPromise<BigNumber>;
    let wallet: TezosWallet;
    let balanceSpy: jasmine.Spy;
    let lowSpy: jasmine.Spy;
    let bidderBalanceSpy: jasmine.Spy;

    beforeEach(() => {
        tezosToolkitServiceMock = mock(TezosToolkitService);
        tezosToolkitMock = mock(TezosToolkit);
        rpcClientMock = mock(RpcClient);
        errorPresenterServiceMock = mock(ErrorPresenterService);
        getBalance = new TestPromise();
        getBalance2 = new TestPromise();
        balanceSpy = jasmine.createSpy();
        lowSpy = jasmine.createSpy();
        bidderBalanceSpy = jasmine.createSpy();

        when(tezosToolkitServiceMock.current).thenReturn(of(instance(tezosToolkitMock)));
        when(tezosToolkitMock.rpc).thenReturn(instance(rpcClientMock));

        when(rpcClientMock.getBalance(TezosAddress)).thenReturn(getBalance).thenReturn(getBalance2);

        TestBed.configureTestingModule({
            imports: [ApolloTestingModule],
            providers: [
                TezosWalletService,
                provider(tezosToolkitServiceMock),
                provider(errorPresenterServiceMock),
                { provide: Configuration, useValue: TestConfig },
            ],
        });

        service = TestBed.inject(TezosWalletService);

        apollo = TestBed.inject(ApolloTestingController);
    });

    afterEach(() => {
        apollo.verify();
    });

    function activate() {
        wallet = service.activateWallet({ address: TezosAddress })!;
        wallet.balance.subscribe(balanceSpy);
        wallet.isBalanceLow.subscribe(lowSpy);
        wallet.bidderBalances.subscribe(bidderBalanceSpy);
        tick();
    }

    it('should fetch balances', fakeAsync(() => {
        activate();

        getBalance.resolve(new BigNumber(7654321));
        const op = apollo.expectOne(BidderBalanceDocument);
        expect(op.operation.variables.address).toBe(TezosAddress);
        op.flushData({ bidderBalances: { id: '1', balances: [{ id: '1', tldName: 'tez', balance: new BigNumber(2000000) }] } });
        tick();

        expect(wallet.address).toBe(TezosAddress);
        expect(balanceSpy).toHaveBeenCalledWith(7654321);
        expect(lowSpy).toHaveBeenCalledWith(false);
        expect(bidderBalanceSpy).toHaveBeenCalledWith([{ tld: 'tez', balance: 2000000 }]);

        discardPeriodicTasks();
    }));

    it('should present error', fakeAsync(() => {
        activate();

        const error = new Error();
        const gqlError = new Error('err');
        getBalance.reject(error);
        apollo.expectOne(BidderBalanceDocument).networkError(gqlError);
        tick();

        verify(errorPresenterServiceMock.nodeErrorToast('balance-refresh', error)).called();
        verify(errorPresenterServiceMock.apiErrorToast('bidder-balance-refresh', deepEqual(gqlError))).called();

        discardPeriodicTasks();
    }));

    it('should emit low balance if below threshold', fakeAsync(() => {
        activate();

        getBalance.resolve(new BigNumber(300000));
        tick();

        expect(balanceSpy).toHaveBeenCalledWith(300000);
        expect(lowSpy).toHaveBeenCalledWith(true);
        apollo.expectOne(BidderBalanceDocument);

        discardPeriodicTasks();
    }));

    it('should poll balance', fakeAsync(() => {
        activate();

        getBalance.resolve(new BigNumber(7654321));
        apollo
            .expectOne(BidderBalanceDocument)
            .flushData({ bidderBalances: { id: '1', balances: [{ id: '2', tldName: 'tez', balance: new BigNumber(2000000) }] } });
        tick();

        expect(balanceSpy).toHaveBeenCalledWith(7654321);
        expect(lowSpy).toHaveBeenCalledWith(false);
        expect(bidderBalanceSpy).toHaveBeenCalledWith([{ tld: 'tez', balance: 2000000 }]);

        tick(5 * 60 * 1000);

        getBalance2.resolve(new BigNumber(300000));
        apollo
            .expectOne(BidderBalanceDocument)
            .flushData({ bidderBalances: { id: '1', balances: [{ id: '1', tldName: 'tez', balance: new BigNumber(4000000) }] } });
        tick();

        expect(balanceSpy).toHaveBeenCalledWith(300000);
        expect(lowSpy).toHaveBeenCalledWith(true);
        expect(bidderBalanceSpy).toHaveBeenCalledWith([{ tld: 'tez', balance: 4000000 }]);

        discardPeriodicTasks();
    }));

    it('should deactivate polling when another wallet is activated', fakeAsync(() => {
        activate();
        getBalance.resolve(new BigNumber(7654321));
        apollo.expectOne(BidderBalanceDocument);
        tick();
        verify(rpcClientMock.getBalance(anything())).once();

        service.activateWallet(null);

        tick(5 * 60 * 1000);

        verify(rpcClientMock.getBalance(anything())).once();
        apollo.expectNone(BidderBalanceDocument);
    }));

    describe('refreshBalance()', () => {
        it('should refresh balance', fakeAsync(() => {
            activate();
            getBalance.resolve(new BigNumber(7654321));
            apollo.expectOne(BidderBalanceDocument);
            tick();

            service.refreshBalance();

            getBalance2.resolve(new BigNumber(300000));
            tick();

            expect(balanceSpy).toHaveBeenCalledWith(300000);
            expect(lowSpy).toHaveBeenCalledWith(true);

            discardPeriodicTasks();
        }));

        it('should throw if no wallet is activated', fakeAsync(() => {
            expect(() => service.refreshBalance()).toThrowError();
        }));
    });

    describe('refreshBidderBalance()', () => {
        it('should refresh balance', fakeAsync(() => {
            activate();

            apollo.expectOne(BidderBalanceDocument).flushData({
                bidderBalances: {
                    id: '1',
                    balances: [
                        { id: '1', tldName: 'tez', balance: new BigNumber(2000000) },
                        { id: '2', tldName: 'tez2', balance: new BigNumber(4000000) },
                    ],
                },
            });
            tick();

            service.refreshBidderBalance();

            apollo
                .expectOne(BidderBalanceDocument)
                .flushData({ bidderBalances: { id: '1', balances: [{ id: '1', tldName: 'tez', balance: new BigNumber(3000000) }] } });
            tick();

            expect(bidderBalanceSpy).toHaveBeenCalledWith([{ tld: 'tez', balance: 3000000 }]);

            discardPeriodicTasks();
        }));

        it('should throw if no wallet is activated', fakeAsync(() => {
            expect(() => service.refreshBidderBalance()).toThrowError();
        }));
    });
});
