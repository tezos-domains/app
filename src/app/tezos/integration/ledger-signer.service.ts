import { LedgerSigner } from '@taquito/ledger-signer';
import { Injectable } from '@angular/core';
import { DeviceDetectorService } from 'ngx-device-detector';
import Transport from '@ledgerhq/hw-transport';
import TransportU2F from '@ledgerhq/hw-transport-u2f';
import TransportWebHID from '@ledgerhq/hw-transport-webhid';

@Injectable({
    providedIn: 'root',
})
export class LedgerSignerService {
    get isChromeNix() {
        return this.deviceDetector.browser === 'Chrome' && this.deviceDetector.os !== 'Windows';
    }

    constructor(private deviceDetector: DeviceDetectorService) {}

    async createSigner(options?: { derivationPath?: string; storedKeys?: { pk: string; pkh: string } }): Promise<[Transport, LedgerSigner]> {
        const transport = this.isChromeNix ? await TransportWebHID.create() : await TransportU2F.create();
        const signer = new LedgerSigner(transport, options?.derivationPath);

        return [transport, signer];
    }

    async isSupported() {
        return this.isChromeNix ? await TransportWebHID.isSupported() : await TransportU2F.isSupported();
    }
}
