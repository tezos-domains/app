import { ContractAbstraction, TezosToolkit, Wallet, WalletContract } from '@taquito/taquito';
import { mockNow, resetNow, TestNetwork } from '@td/test';
import BigNumber from 'bignumber.js';
import { of } from 'rxjs';
import { anyString, anything, instance, mock, verify, when } from 'ts-mockito';

import { discardPeriodicTasks, fakeAsync, tick } from '@angular/core/testing';
import dayjs from 'dayjs';
import { Logger } from '../../browser/logger';
import { TezosNetworkService } from '../tezos-network.service';
import { TezosService } from '../tezos.service';
import { XTZPriceService, YouvesOracleResult } from './xtzprice.service';

describe('XTZPriceService', () => {
    let service: XTZPriceService;
    let tezosServiceMock: TezosService;
    let tezosNetworkServiceMock: TezosNetworkService;
    let tezosToolkitMock: TezosToolkit;
    let loggerMock: Logger;
    let walletMock: Wallet;
    let contractMock: WalletContract;
    let priceResult: YouvesOracleResult;

    let spy: jasmine.Spy;
    let getPriceSpy: jasmine.Spy;
    let executeViewSpy: jasmine.Spy;

    beforeEach(() => {
        tezosServiceMock = mock(TezosService);
        tezosNetworkServiceMock = mock(TezosNetworkService);
        tezosToolkitMock = mock(TezosToolkit);
        contractMock = mock(ContractAbstraction);
        walletMock = mock(Wallet);
        loggerMock = mock(Logger);
        spy = jasmine.createSpy();

        getPriceSpy = jasmine.createSpy('getPriceSpy');
        executeViewSpy = jasmine.createSpy('executeViewSpy');
        priceResult = { last_update_timestamp: getHackedDate('2021-02-01T09:00:00.000Z'), price: new BigNumber(2_500_000) };

        mockNow(dayjs('2021-02-01T09:15:00.000Z'));

        when(tezosServiceMock.tezosToolkit).thenReturn(of(instance(tezosToolkitMock)));
        when(tezosToolkitMock.wallet).thenReturn(instance(walletMock));
        when(walletMock.at(TestNetwork.youvesOracleContract)).thenResolve(instance(contractMock));
        when(contractMock.contractViews).thenReturn({
            get_price_with_timestamp: getPriceSpy,
        });
        getPriceSpy.and.returnValue({ executeView: executeViewSpy });
        executeViewSpy.and.resolveTo(priceResult);

        when(tezosNetworkServiceMock.activeNetwork).thenReturn(of(TestNetwork));

        service = new XTZPriceService(instance(tezosServiceMock), instance(tezosNetworkServiceMock), instance(loggerMock));
    });

    afterEach(() => {
        resetNow();
    });

    it('should convert XTZ to USD', fakeAsync(() => {
        service.convert(2).subscribe(spy);
        tick();

        expect(spy).toHaveBeenCalledWith(5);

        discardPeriodicTasks();
    }));

    it('should return 0 if contract is not configured', fakeAsync(() => {
        when(tezosNetworkServiceMock.activeNetwork).thenReturn(of({ ...TestNetwork, youvesOracleContract: '' }));

        service.convert(2).subscribe(spy);
        tick();

        expect(spy).toHaveBeenCalledWith(0);

        discardPeriodicTasks();
    }));

    it('should fetch price once', fakeAsync(() => {
        verify(walletMock.at(anything())).never();

        service.convert(2).subscribe(spy);
        tick();
        service.convert(4).subscribe(spy);
        tick();

        expect(spy).toHaveBeenCalledWith(5);
        expect(spy).toHaveBeenCalledWith(10);

        discardPeriodicTasks();
    }));

    it('should periodically check and update price', fakeAsync(() => {
        service.convert(2).subscribe(spy);
        tick();

        expect(spy).toHaveBeenCalledWith(5);
        spy.calls.reset();

        priceResult.price = new BigNumber(5000000);

        tick(10 * 60 * 1000);

        expect(spy).toHaveBeenCalledWith(10);

        discardPeriodicTasks();
    }));

    it('should ignore outdated price and print a warning', fakeAsync(() => {
        priceResult.last_update_timestamp = getHackedDate('2021-02-01T06:50:00.000Z');

        service.convert(2).subscribe(spy);
        tick();

        expect(spy).not.toHaveBeenCalled();
        verify(loggerMock.warn(anyString())).called();

        discardPeriodicTasks();
    }));
});

// because taquito handles the date in a weird way, we have to replicate the madness
function getHackedDate(val: string) {
    return new Date(new Date(val).getTime() * 1000).toISOString();
}
