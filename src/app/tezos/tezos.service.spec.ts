import { fakeAsync, tick } from '@angular/core/testing';
import {
    OpKind,
    OperationContentsAndResult,
    OperationContentsAndResultMetadataTransaction,
    OperationContentsAndResultTransaction,
    TransactionOperationParameter,
} from '@taquito/rpc';
import { BigMapAbstraction, ContractAbstraction, TezosToolkit, TransactionWalletOperation, Wallet, WalletContract } from '@taquito/taquito';
import { LowTestWallet, TestNetwork, TestPromise, TestWallet } from '@td/test';
import { TaquitoTezosDomainsClient } from '@tezos-domains/taquito-client';
import { BehaviorSubject, ReplaySubject, Subject, of } from 'rxjs';
import { anyNumber, anyString, anything, deepEqual, instance, mock, verify, when } from 'ts-mockito';
import { Logger } from '../browser/logger';
import { TrackingService } from '../browser/tracking.service';
import { ClaimableTldsService } from './claimable-tlds.service';
import { TezosBeaconWalletManager } from './connector/tezos-wallet-manager';
import { IndexerService } from './indexer.service';
import { TezosDomainsClientService } from './integration/tezos-domains-client.service';
import { TezosToolkitService } from './integration/tezos-toolkit.service';
import { TezosBeaconWallet, TezosErrorCode, TezosNetwork } from './models';
import { TezosNetworkService } from './tezos-network.service';
import { TezosWalletService } from './tezos-wallet.service';
import {
    SmartContractOperationCompletedEvent,
    SmartContractOperationConfirmationEvent,
    SmartContractOperationEvent,
    SmartContractOperationPermissionsGrantedEvent,
    SmartContractOperationSentEvent,
    TezosService,
} from './tezos.service';

class OP implements OperationContentsAndResultTransaction {
    kind: OpKind.TRANSACTION;
    source: string;
    fee: string;
    counter: string;
    gas_limit: string;
    storage_limit: string;
    amount: string;
    destination: string;
    parameters?: TransactionOperationParameter | undefined;
    metadata: OperationContentsAndResultMetadataTransaction;
}

describe('TezosService', () => {
    let service: TezosService;
    let tezosNetworkServiceMock: TezosNetworkService;
    let tezosBeaconWalletManagerMock: TezosBeaconWalletManager;
    let tezosToolkitServiceMock: TezosToolkitService;
    let tezosToolkitMock: TezosToolkit;
    let tezosDomainsClientServiceMock: TezosDomainsClientService;
    let tezosDomainsMock: TaquitoTezosDomainsClient;
    let tezosWalletServiceMock: TezosWalletService;
    let indexerServiceMock: IndexerService;
    let loggerMock: Logger;
    let trackingServiceMock: TrackingService;
    let walletMock: Wallet;
    let operation: TransactionWalletOperation;
    let activeNetwork: ReplaySubject<TezosNetwork>;
    let confirmations: Subject<{
        expectedConfirmation: number;
        currentConfirmation: number;
        completed: boolean;
        block: { hash: string };
    }>;
    let send: TestPromise<TransactionWalletOperation>;
    let spy: jasmine.Spy;
    let walletContractMock: WalletContract;
    let storage: {
        bm: BigMapAbstraction;
        val: number;
    };
    let bigMapGet: TestPromise<any>;
    let block: Subject<void>;
    let operationResults: OperationContentsAndResult[];
    let transactionOperation: OperationContentsAndResultTransaction;
    let metadata: OperationContentsAndResultMetadataTransaction;
    let claimableTldsServiceMock: ClaimableTldsService;
    let activeWallet: ReplaySubject<TezosBeaconWallet | null>;

    beforeEach(() => {
        tezosNetworkServiceMock = mock(TezosNetworkService);
        tezosBeaconWalletManagerMock = mock(TezosBeaconWalletManager);
        tezosToolkitServiceMock = mock(TezosToolkitService);
        tezosToolkitMock = mock(TezosToolkit);
        tezosDomainsMock = mock(TaquitoTezosDomainsClient);
        indexerServiceMock = mock(IndexerService);
        claimableTldsServiceMock = mock(ClaimableTldsService);
        walletMock = mock(Wallet);
        operation = mock(TransactionWalletOperation);
        walletContractMock = mock(ContractAbstraction);
        tezosDomainsClientServiceMock = mock(TezosDomainsClientService);
        tezosWalletServiceMock = mock(TezosWalletService);
        loggerMock = mock(Logger);
        trackingServiceMock = mock(TrackingService);
        activeNetwork = new ReplaySubject(1);
        activeWallet = new ReplaySubject(1);
        confirmations = new Subject();
        send = new TestPromise();
        spy = jasmine.createSpy();
        bigMapGet = new TestPromise();
        block = new Subject();
        transactionOperation = mock(OP);
        metadata = {
            operation_result: {
                status: 'applied',
            },
            balance_updates: [],
            internal_operation_results: [
                {
                    result: {
                        status: 'applied',
                        errors: [],
                    },
                } as any,
            ],
        };

        operationResults = [instance(transactionOperation)];

        const bigMap = mock<BigMapAbstraction>();
        when(bigMap.get(deepEqual({ key: 'key', converted: true } as any))).thenReturn(bigMapGet);
        storage = {
            bm: instance(bigMap),
            val: 1,
        };

        when(claimableTldsServiceMock.getTlds()).thenReturn(of(['com']));

        when(tezosNetworkServiceMock.activeNetwork).thenReturn(activeNetwork);
        activeNetwork.next(TestNetwork);

        when(tezosBeaconWalletManagerMock.activeWallet).thenReturn(activeWallet);
        when(tezosBeaconWalletManagerMock.disconnect()).thenReturn(of());

        when(tezosToolkitServiceMock.current).thenReturn(of(instance(tezosToolkitMock)));
        when(tezosToolkitMock.wallet).thenReturn(instance(walletMock));

        when(tezosDomainsClientServiceMock.current).thenReturn(of(instance(tezosDomainsMock)));

        when(walletMock.transfer(anything())).thenReturn({ send: () => send });
        when(walletMock.at(anyString())).thenResolve(instance(walletContractMock));

        when(walletContractMock.storage()).thenResolve(storage);

        when(operation.confirmationObservable(anyNumber())).thenReturn(confirmations as any);
        when(operation.opHash).thenReturn('hash');
        when(operation.operationResults()).thenResolve(operationResults);

        when(transactionOperation.metadata).thenReturn(metadata);

        when(tezosWalletServiceMock.activateWallet(anything())).thenCall(w => {
            if (w) w.activated = true;
            return w;
        });

        when(indexerServiceMock.whenBlockIndexed('hash', false)).thenReturn(block);

        service = new TezosService(
            instance(tezosNetworkServiceMock),
            instance(tezosBeaconWalletManagerMock),
            instance(tezosToolkitServiceMock),
            instance(tezosDomainsClientServiceMock),
            instance(tezosWalletServiceMock),
            instance(indexerServiceMock),
            instance(loggerMock),
            instance(trackingServiceMock),
            instance(claimableTldsServiceMock)
        );
    });

    describe('tezosToolkit', () => {
        it('should expose tezos toolkit', () => {
            activeWallet.next(TestWallet);

            service.tezosToolkit.subscribe(spy);

            expect(spy).toHaveBeenCalledWith(instance(tezosToolkitMock));
        });
    });

    describe('activeWallet', () => {
        it('should not throw if connector is not set', () => {
            expect(() => service.activeWallet.subscribe(spy)).not.toThrow();
        });

        it('should activate active wallet from connector', () => {
            service.activeWallet.subscribe(spy);

            activeWallet.next(TestWallet);

            expect(spy).toHaveBeenCalledWith({ ...TestWallet, activated: true });
        });

        it('should emit null when connector is set to null', () => {
            service.activeWallet.subscribe(spy);

            activeWallet.next(null);

            expect(spy).toHaveBeenCalledWith(null);
        });
    });

    describe('execute()', () => {
        let fn: () => Promise<TransactionWalletOperation>;

        beforeEach(() => {
            fn = () => send;
        });

        it('should call smart contract and report progress', fakeAsync(() => {
            const completeSpy = jasmine.createSpy('complete');
            activeWallet.next(TestWallet);

            service.execute(fn).subscribe({ next: spy, complete: completeSpy });

            expect(event()).toBeInstanceOf(SmartContractOperationPermissionsGrantedEvent);

            send.resolve(instance(operation));
            tick();

            const sentEvent = event<SmartContractOperationSentEvent>();
            expect(sentEvent).toBeInstanceOf(SmartContractOperationSentEvent);
            expect(sentEvent.operation).toBe(instance(operation));

            confirmations.next({ currentConfirmation: 1, expectedConfirmation: 3, completed: false, block: { hash: 'hash' } });
            tick();
            const confirmation1Event = event<SmartContractOperationConfirmationEvent>();
            expect(confirmation1Event).toBeInstanceOf(SmartContractOperationConfirmationEvent);
            expect(confirmation1Event.currentConfirmation).toBe(1);
            expect(confirmation1Event.expectedConfirmations).toBe(3);

            confirmations.next({ currentConfirmation: 2, expectedConfirmation: 3, completed: false, block: { hash: 'hash2' } });
            tick();
            const confirmation2Event = event<SmartContractOperationConfirmationEvent>();
            expect(confirmation2Event).toBeInstanceOf(SmartContractOperationConfirmationEvent);
            expect(confirmation2Event.currentConfirmation).toBe(2);
            expect(confirmation2Event.expectedConfirmations).toBe(3);

            verify(indexerServiceMock.whenBlockIndexed(anything(), anything())).never();

            confirmations.next({ currentConfirmation: 3, expectedConfirmation: 3, completed: true, block: { hash: 'hash3' } });
            tick();
            const confirmation3Event = event<SmartContractOperationConfirmationEvent>();
            expect(confirmation3Event).toBeInstanceOf(SmartContractOperationConfirmationEvent);
            expect(confirmation3Event.currentConfirmation).toBe(3);
            expect(confirmation3Event.expectedConfirmations).toBe(3);

            block.next();
            tick();
            expect(event()).toBeInstanceOf(SmartContractOperationCompletedEvent);
            expect(completeSpy).toHaveBeenCalled();
        }));

        it('should report error', fakeAsync(() => {
            const error = new Error('err');
            activeWallet.next(TestWallet);

            service.execute(fn).subscribe({ error: spy });

            send.reject(error);
            tick();

            expect(spy).toHaveBeenCalledWith(error);
        }));

        it('should report error in case of backtracked operation', fakeAsync(() => {
            activeWallet.next(TestWallet);

            metadata.operation_result.status = 'backtracked';
            metadata.operation_result.errors = [{ id: 'proto.009-PsFLoren.storage_exhausted.operation', kind: 'temporary' }];
            metadata.internal_operation_results![0].result = {
                status: 'backtracked',
            };

            service.execute(fn).subscribe({ error: spy });

            send.resolve(instance(operation));
            tick();

            confirmations.next({ currentConfirmation: 1, expectedConfirmation: 1, completed: false, block: { hash: 'hash' } });
            tick();

            expect(spy.calls.mostRecent().args[0].message).toBe('Operation was not applied (statuses: backtracked).');
            expect(spy.calls.mostRecent().args[0].errorCode).toBe(TezosErrorCode.STORAGE_EXHAUSTED_OPERATION);
        }));

        it('should report error in case of backtracked operation because of storage', fakeAsync(() => {
            activeWallet.next(TestWallet);

            metadata.operation_result.status = 'backtracked';
            metadata.internal_operation_results![0].result = {
                status: 'failed',
                errors: [{} as any, { with: { string: 'BID_TOO_LOW' } }],
            };

            service.execute(fn).subscribe({ error: spy });

            send.resolve(instance(operation));
            tick();

            confirmations.next({ currentConfirmation: 1, expectedConfirmation: 1, completed: false, block: { hash: 'hash' } });
            tick();

            expect(spy.calls.mostRecent().args[0].message).toBe('Operation was not applied (statuses: backtracked).');
            expect(spy.calls.mostRecent().args[0].errorCode).toBe(TezosErrorCode.BID_TOO_LOW);
        }));

        it('should throw error if balance is low after double checking', fakeAsync(() => {
            activeWallet.next(LowTestWallet);

            service.execute(fn).subscribe({ error: spy });

            verify(tezosWalletServiceMock.refreshBalance()).called();

            (<BehaviorSubject<boolean>>LowTestWallet.isBalanceLow).next(true);

            expect(spy.calls.mostRecent().args[0].message).toBe('Insufficient balance.');
            expect(spy.calls.mostRecent().args[0].errorCode).toBe(TezosErrorCode.CONTRACT_BALANCE_TOO_LOW);
        }));

        function event<T extends SmartContractOperationEvent>(): T {
            expect(spy).toHaveBeenCalled();

            return spy.calls.mostRecent().args[0];
        }
    });
});
