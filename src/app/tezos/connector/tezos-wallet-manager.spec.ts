import { DAppClient } from '@airgap/beacon-dapp';
import { AccountInfo, BeaconEvent, ColorMode, NetworkType, PeerInfo, PermissionResponseOutput, PermissionScope } from '@airgap/beacon-sdk';
import { fakeAsync, tick } from '@angular/core/testing';
import { BeaconWallet } from '@taquito/beacon-wallet';
import { TestConfig, TestNetwork, TezosAddress, mockNow, resetNow } from '@td/test';
import dayjs from 'dayjs';
import { CookieService } from 'ngx-cookie';
import { BehaviorSubject, of } from 'rxjs';
import { anyFunction, anyString, anything, deepEqual, instance, mock, verify, when } from 'ts-mockito';

import { TrackingService } from '../../browser/tracking.service';
import { DarkModeService } from '../../shared/dark-mode/dark-mode.service';
import { StorageSchema } from '../../utils/storage';
import { BeaconService } from '../integration/beacon.service';
import { TezosNetworkService } from '../tezos-network.service';
import { TezosBeaconWalletManager } from './tezos-wallet-manager';

describe('tezosBeaconWalletManager', () => {
    let manager: TezosBeaconWalletManager;
    let trackingServiceMock: TrackingService;
    let tezosNetworkMock: TezosNetworkService;
    let beaconServiceMock: BeaconService;
    let cookieServiceMock: CookieService;
    let darkModeMock: DarkModeService;

    let beaconWalletMock: BeaconWallet;
    let dAppClientMock: DAppClient;
    let activeWalletSpy: jasmine.Spy;
    let accountInfo: AccountInfo;
    let darkMode$: BehaviorSubject<'dark' | 'light'>;

    beforeEach(() => {
        beaconServiceMock = mock(BeaconService);
        trackingServiceMock = mock(TrackingService);
        cookieServiceMock = mock(CookieService);
        tezosNetworkMock = mock(TezosNetworkService);
        darkModeMock = mock(DarkModeService);

        beaconWalletMock = mock(BeaconWallet);
        dAppClientMock = mock(DAppClient);
        activeWalletSpy = jasmine.createSpy('activeWalletSpy');
        darkMode$ = new BehaviorSubject('dark');
        mockNow(dayjs('2020-05-01T20:05:00Z'));

        when(tezosNetworkMock.activeNetwork).thenReturn(of({ ...TestNetwork, name: NetworkType.MAINNET }));
        when(beaconServiceMock.createWallet(NetworkType.MAINNET)).thenReturn(instance(beaconWalletMock));
        when(beaconWalletMock.client).thenReturn(instance(dAppClientMock));

        when(darkModeMock.theme$).thenReturn(darkMode$);
        when(dAppClientMock.getPeers()).thenResolve([{ icon: 'icon', name: 'awesome temple', senderId: '112' } as unknown as PeerInfo]);

        accountInfo = { address: TezosAddress, senderId: '112' } as AccountInfo;
    });

    afterEach(() => {
        resetNow();
    });

    function init() {
        manager = new TezosBeaconWalletManager(
            instance(tezosNetworkMock),
            instance(beaconServiceMock),
            instance(trackingServiceMock),
            instance(darkModeMock),
            instance(cookieServiceMock),
            TestConfig
        );
        manager.activeWallet.subscribe(activeWalletSpy);
    }

    describe('connect()', () => {
        describe('when already connected', () => {
            beforeEach(() => {
                when(dAppClientMock.subscribeToEvent(BeaconEvent.ACTIVE_ACCOUNT_SET, anyFunction())).thenCall((_, callback) => {
                    callback(accountInfo);
                });
            });

            describe('and on happy path', () => {
                it('should emit the connected wallet', fakeAsync(() => {
                    init();
                    tick();

                    expect(activeWalletSpy).toHaveBeenCalledOnceWith({ address: accountInfo.address, icon: 'icon', walletName: 'awesome temple' });
                }));

                it('should emit the connected wallet when subscribing later', fakeAsync(() => {
                    init();
                    tick();

                    const spy = jasmine.createSpy('lateWalletSubscription');
                    manager.activeWallet.subscribe(spy);

                    expect(spy).toHaveBeenCalledOnceWith({ address: accountInfo.address, icon: 'icon', walletName: 'awesome temple' });
                }));

                it('should track login', fakeAsync(() => {
                    init();
                    tick();
                    verify(trackingServiceMock.event('login', deepEqual({ method: 'beacon-sdk' }))).once();
                }));

                it('should save cookie', fakeAsync(() => {
                    init();
                    tick();

                    verify(
                        cookieServiceMock.put(
                            StorageSchema.connectStatus.key,
                            'connected',
                            deepEqual({ domain: TestConfig.cookieDomain, expires: dayjs().add(90, 'days').toDate() })
                        )
                    ).once();
                }));
            });

            it('should emit NULL when no peers connected', fakeAsync(() => {
                when(dAppClientMock.getPeers()).thenResolve([]);

                init();
                tick();

                expect(activeWalletSpy).toHaveBeenCalledOnceWith(null);
                verify(cookieServiceMock.remove(StorageSchema.connectStatus.key, deepEqual({ domain: TestConfig.cookieDomain }))).once();
            }));

            it('should set color mode', fakeAsync(() => {
                init();
                tick();

                verify(dAppClientMock.setColorMode(ColorMode.DARK)).once();

                darkMode$.next('light');

                verify(dAppClientMock.setColorMode(ColorMode.LIGHT)).once();
            }));
        });

        describe('when not already connected', () => {
            beforeEach(() => {
                when(
                    dAppClientMock.requestPermissions(
                        deepEqual({
                            scopes: [PermissionScope.OPERATION_REQUEST, PermissionScope.SIGN],
                            network: { type: NetworkType.MAINNET, rpcUrl: undefined },
                        })
                    )
                ).thenResolve(<PermissionResponseOutput>{ address: TezosAddress });
            });

            it('should intialize beacon wallet only once', fakeAsync(() => {
                init();
                tick();
                manager.connect().subscribe();
                tick();

                verify(beaconServiceMock.createWallet(anything())).once();
            }));

            it('should request permission and emit wallet', fakeAsync(() => {
                // eslint-disable-next-line @typescript-eslint/no-empty-function
                let activeAccountCallback = () => {};
                when(dAppClientMock.subscribeToEvent(BeaconEvent.ACTIVE_ACCOUNT_SET, anyFunction())).thenCall((_, callback) => {
                    activeAccountCallback = () => callback(accountInfo);
                });

                init();
                tick();
                activeWalletSpy.calls.reset();

                manager.connect().subscribe();
                activeAccountCallback();
                tick();

                expect(activeWalletSpy).toHaveBeenCalledOnceWith({ address: TezosAddress, icon: 'icon', walletName: 'awesome temple' });
            }));

            it('should emit null initially', fakeAsync(() => {
                init();
                tick();

                expect(activeWalletSpy).toHaveBeenCalledOnceWith(null);
            }));
        });
    });

    describe('disconnect()', () => {
        it('should emit null after disconnecting', fakeAsync(() => {
            when(dAppClientMock.subscribeToEvent(BeaconEvent.ACTIVE_ACCOUNT_SET, anyFunction())).thenCall((_, callback) => {
                callback(accountInfo);
            });
            when(beaconWalletMock.clearActiveAccount()).thenResolve();

            init();
            tick();
            activeWalletSpy.calls.reset();

            manager.disconnect().subscribe();
            tick();

            verify(beaconWalletMock.clearActiveAccount()).once();
            expect(activeWalletSpy).toHaveBeenCalledOnceWith(null);
            verify(cookieServiceMock.remove(StorageSchema.connectStatus.key, deepEqual({ domain: TestConfig.cookieDomain }))).once();
        }));
    });
});
