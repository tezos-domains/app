import { StorageMap } from '@ngx-pwa/local-storage';
import { BehaviorSubject, of } from 'rxjs';
import { mock, when, instance, anyString, anything, verify, deepEqual } from 'ts-mockito';
import { TestConfig } from '@td/test';

import { HardwareWalletSettingsService } from './hardware-wallet-settings.service';
import { StorageSchema } from '../utils/storage';

describe('HardwareWalletSettingsService', () => {
    let service: HardwareWalletSettingsService;
    let storageMapMock: StorageMap;
    let customDerivationPath: BehaviorSubject<string | undefined>;
    let spy: jasmine.Spy;

    beforeEach(() => {
        storageMapMock = mock(StorageMap);
        customDerivationPath = new BehaviorSubject<string | undefined>(undefined);
        spy = jasmine.createSpy();

        when(storageMapMock.watch(StorageSchema.derivationPath.key, deepEqual(StorageSchema.derivationPath.schema))).thenReturn(customDerivationPath);
        when(storageMapMock.set(anyString(), anything(), anything())).thenReturn(of());
        when(storageMapMock.delete(anyString())).thenReturn(of());

        service = new HardwareWalletSettingsService(instance(storageMapMock), TestConfig);
    });

    describe('current', () => {
        it('should get network from config and emit it', () => {
            service.activeSettings.subscribe(spy);

            expect(spy).toHaveBeenCalledWith(TestConfig.hardwareWallet);
        });

        it('should get network from config and emit it with overridden derivationPath from storage', () => {
            customDerivationPath.next('cdp');

            service.activeSettings.subscribe(spy);

            expect(spy).toHaveBeenCalledWith({ ...TestConfig.hardwareWallet, derivationPath: 'cdp' });
        });
    });

    describe('setDerivationPath()', () => {
        it('should store custom derivation path', () => {
            service.setDerivationPath('cdp');

            verify(storageMapMock.set(StorageSchema.derivationPath.key, 'cdp', StorageSchema.derivationPath.schema)).called();
        });

        it('should delete custom derivation path from store if null is specified', () => {
            service.setDerivationPath(null);

            verify(storageMapMock.delete(StorageSchema.derivationPath.key)).called();
        });
    });
});
