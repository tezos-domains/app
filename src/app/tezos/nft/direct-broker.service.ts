import { Injectable } from '@angular/core';
import { OpKind } from '@taquito/rpc';
import { RecordMetadata } from '@tezos-domains/core';
import { TezosNetwork, TezosWallet } from '../models';
import { TezosNetworkService } from '../tezos-network.service';
import { TezosService } from '../tezos.service';

export interface PlaceOfferRequest {
    tokenId: number;
    price: number;
    expiration: Date | null;
}

export interface RemoveOfferRequest {
    tokenId: number;
    removeOperator: boolean;
}

export interface ExecuteOfferRequest {
    tokenId: number;
    seller: string;
    price: number;
    domain?: {
        name: string;
        address: string | null;
        data: RecordMetadata;
        owner: string;
    };
}

@Injectable({
    providedIn: 'root',
})
export class DirectBrokerService {
    private network: TezosNetwork;
    private wallet: TezosWallet;

    constructor(private tezosService: TezosService, private tezosNetworkService: TezosNetworkService) {
        this.tezosService.activeWallet.subscribe(w => (this.wallet = w!));
        this.tezosNetworkService.activeNetwork.subscribe(n => (this.network = n));
    }

    placeOffer(request: PlaceOfferRequest) {
        return this.tezosService.execute(async (_, tezos) => {
            const contract = await tezos.wallet.at(this.network.directBrokerContract);
            const tokenContract = await tezos.wallet.at(this.network.tokenContract);

            const ops = [
                contract.methods.place_offer(tokenContract.address, request.tokenId, request.price, request.expiration).toTransferParams({ storageLimit: 100 }),
                tokenContract.methods
                    .update_operators([{ add_operator: { owner: this.wallet.address, token_id: request.tokenId, operator: contract.address } }])
                    .toTransferParams({ storageLimit: 100 }),
            ];

            return tezos.wallet.batch(ops.map(o => ({ ...o, kind: OpKind.TRANSACTION }))).send();
        });
    }

    removeOffer(request: RemoveOfferRequest | RemoveOfferRequest[]) {
        const removeItems = !isArray(request) ? [request] : request;

        return this.tezosService.execute(async (_, tezos) => {
            const contract = await tezos.wallet.at(this.network.directBrokerContract);
            const tokenContract = await tezos.wallet.at(this.network.tokenContract);
            const ops = removeItems.flatMap(item => {
                const innerOps = [];

                const removeOfferOp = contract.methods.remove_offer(tokenContract.address, item.tokenId).toTransferParams({ storageLimit: 0 });
                innerOps.push(removeOfferOp);

                if (item.removeOperator) {
                    innerOps.push(
                        tokenContract.methods
                            .update_operators([{ remove_operator: { owner: this.wallet.address, token_id: item.tokenId, operator: contract.address } }])
                            .toTransferParams({ storageLimit: 0 })
                    );
                }

                return innerOps;
            });

            return tezos.wallet.batch(ops.map(o => ({ ...o, kind: OpKind.TRANSACTION }))).send();
        });
    }

    executeOffer(request: ExecuteOfferRequest) {
        return this.tezosService.execute(async (client, tezos) => {
            const contract = await tezos.wallet.at(this.network.directBrokerContract);

            return client.manager.batch(async b => {
                const ops = [
                    contract.methods
                        .execute_offer(this.network.tokenContract, request.tokenId, request.seller)
                        .toTransferParams({ amount: request.price, mutez: true, storageLimit: 0 }),
                ];

                if (request.domain) {
                    ops.push(
                        await b.updateRecord(
                            {
                                name: request.domain.name,
                                address: request.domain.address,
                                owner: request.domain.owner,
                                data: request.domain.data,
                            },
                            { storageLimit: 0 }
                        )
                    );
                }
                return ops;
            });
        });
    }
}
function isArray<T>(val: T | T[]): val is T[] {
    return Array.isArray(val);
}
