import { mock, when, instance, anyFunction, anyString, anything, verify } from 'ts-mockito';
import { BuyOfferBrokerService } from './buy-offer-broker.service';
import { TezosService } from '../tezos.service';
import { TezosNetworkService } from '../tezos-network.service';
import { ContractAbstraction, TezosToolkit, Wallet, WalletOperationBatch } from '@taquito/taquito';
import { TestNetwork, TestWallet } from '@td/test';
import { of, ReplaySubject } from 'rxjs';
import { TezosWallet } from '../models';
import { fakeAsync, tick } from '@angular/core/testing';

describe('BuyOfferBrokerService', () => {
    let service: BuyOfferBrokerService;
    let tezosService: TezosService;
    let tezosNetworkService: TezosNetworkService;
    let tezosToolkit: TezosToolkit;

    let taquitoWalletMock: Wallet;
    let placeOfferSpy: jasmine.Spy;
    let removeOfferSpy: jasmine.Spy;
    let updateOperatorsSpy: jasmine.Spy;
    let toTransferParamsPlaceSpy: jasmine.Spy;
    let toTransferParamsRemoveSpy: jasmine.Spy;
    let toTransferParamsUpdateOperatorsSpy: jasmine.Spy;
    let wallet: ReplaySubject<TezosWallet | null>;
    let walletOperationBatch: WalletOperationBatch;

    beforeEach(() => {
        tezosService = mock(TezosService);
        tezosNetworkService = mock(TezosNetworkService);

        tezosToolkit = mock(TezosToolkit);
        taquitoWalletMock = mock(Wallet);
        walletOperationBatch = mock(WalletOperationBatch);

        wallet = new ReplaySubject(1);

        placeOfferSpy = jasmine.createSpy('placeOfferSpy');
        removeOfferSpy = jasmine.createSpy('placeOfferSpy');
        updateOperatorsSpy = jasmine.createSpy('updateOperatorsSpy');
        toTransferParamsPlaceSpy = jasmine.createSpy('toTransferParamsPlaceSpy');
        toTransferParamsRemoveSpy = jasmine.createSpy('toTransferParamsRemoveSpy');
        toTransferParamsUpdateOperatorsSpy = jasmine.createSpy('toTransferParamsUpdateOperatorsSpy');

        when(tezosService.execute(anyFunction())).thenCall(fn => {
            fn(null, instance(tezosToolkit));
            return of();
        });

        when(tezosService.activeWallet).thenReturn(wallet);
        when(tezosNetworkService.activeNetwork).thenReturn(of(TestNetwork));

        when(tezosToolkit.wallet).thenReturn(instance(taquitoWalletMock));
        when(taquitoWalletMock.batch(anything())).thenReturn(instance(walletOperationBatch));

        when(taquitoWalletMock.at(anyString())).thenCall(address => {
            return <ContractAbstraction<Wallet>>{
                address,
                methods: <any>{
                    place_offer: placeOfferSpy.and.returnValue({ toTransferParams: toTransferParamsPlaceSpy }),
                    remove_offer: removeOfferSpy.and.returnValue({ toTransferParams: toTransferParamsRemoveSpy }),
                    update_operators: updateOperatorsSpy.and.returnValue({ toTransferParams: toTransferParamsUpdateOperatorsSpy }),
                },
            };
        });

        wallet.next(TestWallet);

        service = new BuyOfferBrokerService(instance(tezosService), instance(tezosNetworkService));
    });

    describe('placeOffer', () => {
        it('should call methods with correct params', fakeAsync(() => {
            service.placeOffer({ tokenId: 22, price: 123, priceWithFee: 124, expiration: null });
            tick();

            verify(taquitoWalletMock.batch(anything())).once();
            verify(walletOperationBatch.send()).once();

            expect(placeOfferSpy).toHaveBeenCalledOnceWith(TestNetwork.tokenContract, 22, 123, null);
            expect(toTransferParamsPlaceSpy).toHaveBeenCalledOnceWith({ amount: 124, mutez: true, storageLimit: 100 });
        }));
    });

    describe('removeOffer', () => {
        it('should call methods with correct params', fakeAsync(() => {
            service.removeOffer(22);
            tick();

            verify(taquitoWalletMock.batch(anything())).once();
            verify(walletOperationBatch.send()).once();

            expect(removeOfferSpy).toHaveBeenCalledOnceWith(TestNetwork.tokenContract, 22, TestWallet.address);
        }));
    });
});
