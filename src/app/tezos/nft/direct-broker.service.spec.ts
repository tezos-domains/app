import { fakeAsync, tick } from '@angular/core/testing';
import { ContractAbstraction, TezosToolkit, Wallet, WalletOperationBatch } from '@taquito/taquito';
import { TestNetwork, TestWallet } from '@td/test';
import { of, ReplaySubject } from 'rxjs';
import { anyFunction, anyString, anything, instance, mock, verify, when } from 'ts-mockito';
import { TezosWallet } from '../models';
import { TezosNetworkService } from '../tezos-network.service';
import { TezosService } from '../tezos.service';
import { DirectBrokerService } from './direct-broker.service';

describe('DirectBrokerService', () => {
    let service: DirectBrokerService;
    let tezosService: TezosService;
    let tezosNetworkService: TezosNetworkService;
    let tezosToolkit: TezosToolkit;

    let taquitoWalletMock: Wallet;
    let placeOfferSpy: jasmine.Spy;
    let removeOfferSpy: jasmine.Spy;
    let updateOperatorsSpy: jasmine.Spy;
    let wallet: ReplaySubject<TezosWallet | null>;
    let walletOperationBatch: WalletOperationBatch;

    beforeEach(() => {
        tezosService = mock(TezosService);
        tezosNetworkService = mock(TezosNetworkService);

        tezosToolkit = mock(TezosToolkit);
        taquitoWalletMock = mock(Wallet);
        walletOperationBatch = mock(WalletOperationBatch);

        wallet = new ReplaySubject(1);

        placeOfferSpy = jasmine.createSpy('placeOfferSpy');
        removeOfferSpy = jasmine.createSpy('placeOfferSpy');
        updateOperatorsSpy = jasmine.createSpy('updateOperatorsSpy');

        when(tezosService.execute(anyFunction())).thenCall(fn => {
            fn(null, instance(tezosToolkit));
            return of();
        });

        when(tezosService.activeWallet).thenReturn(wallet);
        when(tezosNetworkService.activeNetwork).thenReturn(of(TestNetwork));

        when(tezosToolkit.wallet).thenReturn(instance(taquitoWalletMock));
        when(taquitoWalletMock.batch(anything())).thenReturn(instance(walletOperationBatch));

        when(taquitoWalletMock.at(anyString())).thenCall(address => {
            return <ContractAbstraction<Wallet>>{
                address,
                methods: <any>{
                    place_offer: placeOfferSpy.and.returnValue({ toTransferParams: jasmine.createSpy() }),
                    remove_offer: removeOfferSpy.and.returnValue({ toTransferParams: jasmine.createSpy() }),
                    update_operators: updateOperatorsSpy.and.returnValue({ toTransferParams: jasmine.createSpy() }),
                },
            };
        });

        wallet.next(TestWallet);

        service = new DirectBrokerService(instance(tezosService), instance(tezosNetworkService));
    });

    describe('placeOffer', () => {
        it('should call methods with correct params', fakeAsync(() => {
            service.placeOffer({ tokenId: 22, price: 123, expiration: null });
            tick();

            verify(taquitoWalletMock.batch(anything())).once();
            verify(walletOperationBatch.send()).once();

            expect(placeOfferSpy).toHaveBeenCalledOnceWith(TestNetwork.tokenContract, 22, 123, null);
            expect(updateOperatorsSpy).toHaveBeenCalledOnceWith([
                { add_operator: { owner: TestWallet.address, token_id: 22, operator: TestNetwork.directBrokerContract } },
            ]);
        }));
    });

    describe('removeOffer', () => {
        it('should call methods with correct params', fakeAsync(() => {
            service.removeOffer({ tokenId: 22, removeOperator: true });
            tick();

            verify(taquitoWalletMock.batch(anything())).once();
            verify(walletOperationBatch.send()).once();

            expect(removeOfferSpy).toHaveBeenCalledOnceWith(TestNetwork.tokenContract, 22);
            expect(updateOperatorsSpy).toHaveBeenCalledOnceWith([
                { remove_operator: { owner: TestWallet.address, token_id: 22, operator: TestNetwork.directBrokerContract } },
            ]);
        }));

        it('should not remove operator when not needed', fakeAsync(() => {
            service.removeOffer({ tokenId: 22, removeOperator: false });
            tick();

            verify(taquitoWalletMock.batch(anything())).once();
            verify(walletOperationBatch.send()).once();

            expect(removeOfferSpy).toHaveBeenCalledOnceWith(TestNetwork.tokenContract, 22);
            expect(updateOperatorsSpy).not.toHaveBeenCalled();
        }));
    });
});
