import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { StorageMap } from '@ngx-pwa/local-storage';
import { TestConfig, TezosAddress, provider } from '@td/test';
import { mock, when, anyString, anything, verify, deepEqual } from 'ts-mockito';

import { NotificationsService, NotificationsSubscription } from './notifications.service';
import { Configuration } from '../configuration';
import { of, ReplaySubject } from 'rxjs';
import { StorageSchema } from '../utils/storage';
import { TezosService } from '../tezos/tezos.service';
import { TezosWallet } from '../tezos/models';
import { TestWallet } from '../../test/data';

describe('NotificationsService', () => {
    let service: NotificationsService;
    let httpMock: HttpTestingController;
    let storageMapMock: StorageMap;
    let tezosServiceMock: TezosService;
    let storedSubscription: ReplaySubject<NotificationsSubscription | undefined>;
    let wallet: ReplaySubject<TezosWallet | null>;
    let spy: jasmine.Spy;
    const storage = StorageSchema.notificationStatus(TezosAddress);

    beforeEach(() => {
        storageMapMock = mock(StorageMap);
        tezosServiceMock = mock(TezosService);
        storedSubscription = new ReplaySubject(1);
        wallet = new ReplaySubject(1);
        spy = jasmine.createSpy();

        TestBed.configureTestingModule({
            imports: [HttpClientTestingModule],
            providers: [NotificationsService, { provide: Configuration, useValue: TestConfig }, provider(storageMapMock), provider(tezosServiceMock)],
        });

        when(storageMapMock.set(anyString(), anything(), anything())).thenReturn(of(void 0));
        when(storageMapMock.delete(anyString())).thenReturn(of(void 0));
        when(storageMapMock.watch<NotificationsSubscription>(storage.key, storage.schema)).thenReturn(storedSubscription);

        when(tezosServiceMock.activeWallet).thenReturn(wallet);

        service = TestBed.inject(NotificationsService);
        httpMock = TestBed.inject(HttpTestingController);
    });

    describe('currentSubscription', () => {
        it('should emit subscription for wallet', () => {
            service.currentSubscription.subscribe(spy);

            wallet.next(TestWallet);

            storedSubscription.next({ email: 'wutface@tezos.domains', status: 'unconfirmed' });

            expect(spy).toHaveBeenCalledWith({ email: 'wutface@tezos.domains', status: 'unconfirmed' });
        });

        it('should emit not-subscribed status if storage is empty', () => {
            service.currentSubscription.subscribe(spy);

            wallet.next(TestWallet);

            storedSubscription.next(undefined);

            expect(spy).toHaveBeenCalledWith({ email: null, status: 'not-subscribed' });
        });

        it('should not emit if no wallet', () => {
            service.currentSubscription.subscribe(spy);

            wallet.next(null);

            verify(storageMapMock.watch(anything(), anything())).never();
            expect(spy).not.toHaveBeenCalled();
        });
    });

    describe('subscribe()', () => {
        it('should call api', () => {
            service.subscribe({ address: TezosAddress, email: 'wutface@tezos.domains' }).subscribe(spy);

            const request = httpMock.expectOne('https://mail.tezos.domains/subscribe');
            expect(request.request.method).toBe('POST');
            expect(request.request.body).toEqual({ address: TezosAddress, email: 'wutface@tezos.domains' });

            request.flush(null);

            verify(storageMapMock.set(storage.key, deepEqual({ email: 'wutface@tezos.domains', status: 'unconfirmed' }), storage.schema)).called();
            expect(spy).toHaveBeenCalledWith({ status: 'OK', data: null });
        });

        it('should call api and report error', () => {
            service.subscribe({ address: TezosAddress, email: 'wutface@tezos.domains' }).subscribe(spy);

            const request = httpMock.expectOne('https://mail.tezos.domains/subscribe');

            request.flush(
                {
                    reason: 'invalid',
                },
                {
                    status: 400,
                    statusText: 'Bad Request',
                }
            );

            verify(storageMapMock.set(anything(), anything(), anything())).never();
            expect(spy).toHaveBeenCalledWith({ status: 'ERROR', failReason: 'invalid' });
        });

        it('should call api and report general error', () => {
            service.subscribe({ address: TezosAddress, email: 'wutface@tezos.domains' }).subscribe(spy);

            const request = httpMock.expectOne('https://mail.tezos.domains/subscribe');

            request.flush(null, {
                status: 500,
                statusText: 'Internal Server Error',
            });

            verify(storageMapMock.set(anything(), anything(), anything())).never();
            expect(spy).toHaveBeenCalledWith({ status: 'ERROR', failReason: undefined });
        });
    });

    describe('confirm()', () => {
        it('should call api', () => {
            service.confirm('key').subscribe(spy);

            const request = httpMock.expectOne('https://mail.tezos.domains/confirm');
            expect(request.request.method).toBe('POST');
            expect(request.request.body).toEqual({ key: 'key' });

            request.flush({ address: TezosAddress, email: 'wutface@tezos.domains' });

            verify(storageMapMock.set(storage.key, deepEqual({ email: 'wutface@tezos.domains', status: 'confirmed' }), storage.schema)).called();
            expect(spy).toHaveBeenCalledWith({ status: 'OK', data: { address: TezosAddress, email: 'wutface@tezos.domains' } });
        });

        it('should call api and report error', () => {
            service.confirm('key').subscribe(spy);

            const request = httpMock.expectOne('https://mail.tezos.domains/confirm');

            request.flush(
                {
                    reason: 'invalid',
                },
                {
                    status: 400,
                    statusText: 'Bad Request',
                }
            );

            verify(storageMapMock.set(anything(), anything(), anything())).never();
            expect(spy).toHaveBeenCalledWith({ status: 'ERROR', failReason: 'invalid' });
        });

        it('should call api and report general error', () => {
            service.confirm('key').subscribe(spy);

            const request = httpMock.expectOne('https://mail.tezos.domains/confirm');

            request.flush(null, {
                status: 500,
                statusText: 'Internal Server Error',
            });

            verify(storageMapMock.set(anything(), anything(), anything())).never();
            expect(spy).toHaveBeenCalledWith({ status: 'ERROR', failReason: undefined });
        });
    });

    describe('unsubscribe()', () => {
        it('should call api', () => {
            service.unsubscribe('key').subscribe(spy);

            const request = httpMock.expectOne('https://mail.tezos.domains/unsubscribe');
            expect(request.request.method).toBe('POST');
            expect(request.request.body).toEqual({ key: 'key' });

            request.flush({ address: TezosAddress });

            verify(storageMapMock.delete(storage.key)).called();
            expect(spy).toHaveBeenCalledWith({ status: 'OK', data: { address: TezosAddress } });
        });

        it('should call api and report error', () => {
            service.unsubscribe('key').subscribe(spy);

            const request = httpMock.expectOne('https://mail.tezos.domains/unsubscribe');

            request.flush(
                {
                    reason: 'invalid',
                },
                {
                    status: 400,
                    statusText: 'Bad Request',
                }
            );

            verify(storageMapMock.delete(anything())).never();
            expect(spy).toHaveBeenCalledWith({ status: 'ERROR', failReason: 'invalid' });
        });

        it('should call api and report general error', () => {
            service.unsubscribe('key').subscribe(spy);

            const request = httpMock.expectOne('https://mail.tezos.domains/unsubscribe');

            request.flush(null, {
                status: 500,
                statusText: 'Internal Server Error',
            });

            verify(storageMapMock.delete(anything())).never();
            expect(spy).toHaveBeenCalledWith({ status: 'ERROR', failReason: undefined });
        });
    });
});
