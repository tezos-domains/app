import { Component, OnInit, OnChanges, OnDestroy, Input } from '@angular/core';
import { ReplaySubject, Subject } from 'rxjs';
import { delay, takeUntil } from 'rxjs/operators';

import { DataSourceFactory } from '../graphql/data-source-factory';
import { AuctionState, OrderDirection, AuctionOrderField } from '../graphql/graphql.generated';
import { AuctionTableDataSource } from '../graphql/auction-table-data-source';

@Component({
    selector: 'td-hot-auction-summary-table',
    templateUrl: './hot-auction-summary-table.component.html',
    styleUrls: ['./hot-auction-summary-table.component.scss'],
})
export class HotAuctionSummaryTableComponent implements OnInit, OnChanges, OnDestroy {
    @Input() noShadow: boolean;

    dataSource: AuctionTableDataSource;
    states = AuctionState;

    private emptyStream = new ReplaySubject<boolean>(1);
    private unsubscribe = new Subject<void>();

    get empty$() {
        return this.emptyStream;
    }

    constructor(private dataSourceFactory: DataSourceFactory) {}

    ngOnInit() {
        this.dataSource = this.dataSourceFactory.createAuctionTableDataSource();
        this.dataSource.empty$.pipe(takeUntil(this.unsubscribe), delay(0)).subscribe(this.emptyStream);
        this.load();
    }

    ngOnChanges() {
        if (this.dataSource) {
            this.load();
        }
    }

    ngOnDestroy() {
        this.unsubscribe.next();
        this.unsubscribe.complete();
    }

    private load() {
        this.dataSource.load(
            {
                where: {
                    state: { in: [AuctionState.InProgress] },
                    bidCount: { greaterThanOrEqualTo: 1 },
                },
                order: { field: AuctionOrderField.BidAmountSum, direction: OrderDirection.Desc },
                first: 10,
            },
            {
                pollInterval: 10 * 60 * 1000,
            }
        );
    }
}
