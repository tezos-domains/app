import { Component } from '@angular/core';

@Component({
    selector: 'td-auction-history-list',
    templateUrl: './auction-history-list.component.html',
    styleUrls: ['./auction-history-list.component.scss'],
})
export class AuctionHistoryListComponent {}
