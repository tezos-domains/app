import { Component } from '@angular/core';

@Component({
    selector: 'td-hot-auction-list',
    templateUrl: './hot-auction-list.component.html',
    styleUrls: ['./hot-auction-list.component.scss'],
})
export class HotAuctionListComponent {}
