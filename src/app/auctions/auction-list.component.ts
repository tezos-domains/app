import { Component } from '@angular/core';

@Component({
    selector: 'td-auction-list',
    templateUrl: './auction-list.component.html',
    styleUrls: ['./auction-list.component.scss'],
})
export class AuctionListComponent {}
