import { Component, Input, OnChanges, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatSort, SortDirection } from '@angular/material/sort';
import mergeObject from 'lodash-es/merge';
import { InfiniteScrollDirective } from 'ngx-infinite-scroll';
import { BehaviorSubject, merge } from 'rxjs';
import { debounceTime } from 'rxjs/operators';

import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { HistoryStateService } from '../browser/history-state.service';
import { AuctionTableDataSource } from '../graphql/auction-table-data-source';
import { DataSourceFactory } from '../graphql/data-source-factory';
import { AuctionListQueryVariables, AuctionOrder, AuctionOrderField, AuctionsFilter, AuctionState } from '../graphql/graphql.generated';
import { TezosWallet } from '../tezos/models';
import { TezosService } from '../tezos/tezos.service';

type FilterCategory = {
    name: AuctionTableFilterCategory;
    predicateBuilder: () => AuctionsFilter;
};

export type AuctionTableFilterCategory = 'all' | 'won' | 'outbid';

export type AuctionTableState = {
    auctionTable?: {
        sort: {
            field: string;
            direction: SortDirection;
        };
        form: any;
    };
};

type AuctionCategories = 'all' | 'won' | 'outbid';

@UntilDestroy()
@Component({
    selector: 'td-auction-table',
    templateUrl: './auction-table.component.html',
    styleUrls: ['./auction-table.component.scss'],
})
export class AuctionTableComponent implements OnInit, OnChanges {
    @Input() address: string;

    dataSource: AuctionTableDataSource;
    form: FormGroup<{
        filter: FormControl<string>;
        category: FormControl<AuctionCategories>;
    }>;
    categories: FilterCategory[];

    sortField: string;
    sortDirection: SortDirection;
    hideResults = new BehaviorSubject<boolean>(true);
    states = AuctionState;
    columns: string[] = [];
    wallet: TezosWallet | null;

    @ViewChild(MatSort) sorter: MatSort;
    @ViewChild(InfiniteScrollDirective) infiniteScroll: InfiniteScrollDirective;

    constructor(
        private dataSourceFactory: DataSourceFactory,
        private formBuilder: FormBuilder,
        private historyStateService: HistoryStateService,
        private tezosService: TezosService
    ) {}

    ngOnInit(): void {
        this.dataSource = this.dataSourceFactory.createAuctionTableDataSource();

        this.categories = [
            {
                name: 'all',
                predicateBuilder: () => ({
                    or: [
                        {
                            bidders: { include: this.address },
                            state: { in: [AuctionState.InProgress] },
                        },
                        {
                            highestBidder: { equalTo: this.address },
                            state: { in: [AuctionState.CanBeSettled] },
                        },
                    ],
                }),
            },
            {
                name: 'won',
                predicateBuilder: () => ({
                    highestBidder: { equalTo: this.address },
                    state: { in: [AuctionState.CanBeSettled] },
                }),
            },
            {
                name: 'outbid',
                predicateBuilder: () => ({
                    highestBidder: { notEqualTo: this.address },
                    bidders: { include: this.address },
                    state: { in: [AuctionState.InProgress] },
                }),
            },
        ];

        this.form = this.formBuilder.group({
            filter: this.formBuilder.control('', { nonNullable: true }),
            category: this.formBuilder.control<AuctionCategories>('all', { nonNullable: true }),
        });

        merge(this.form.get('filter')!.valueChanges.pipe(debounceTime(300)), this.form.get('category')!.valueChanges.pipe(debounceTime(0)))
            .pipe(untilDestroyed(this))
            .subscribe(() => {
                this.hideResults.next(true);
                this.reload();
            });

        this.dataSource.initialLoading$.pipe(untilDestroyed(this)).subscribe(l => {
            if (!l) {
                this.hideResults.next(false);
            }
        });

        this.restoreState();

        this.tezosService.activeWallet.pipe(untilDestroyed(this)).subscribe(w => {
            this.wallet = w;
            if (this.wallet?.address === this.address) {
                this.columns = ['name', 'timeLeft', 'status', 'currentBid', 'actions'];
            } else {
                this.columns = ['name', 'timeLeft', 'currentBid'];
            }
        });
    }

    ngOnChanges() {
        // check because changes fire before init
        if (this.categories && this.form) {
            this.restoreState();
        }
    }

    scrolled() {
        this.dataSource.loadMore();
    }

    reload() {
        if (this.infiniteScroll) {
            this.infiniteScroll.destroyScroller();
            this.infiniteScroll.setup();
        }

        const where: AuctionsFilter = mergeObject(
            {
                domainName: { like: this.form.value.filter },
            },
            this.getCategoryFilter()
        );

        const variables: AuctionListQueryVariables = {
            where,
            order: this.getSort(),
        };

        this.historyStateService.merge<AuctionTableState>({
            auctionTable: { form: this.form.value, sort: { field: this.sorter.active, direction: this.sorter.direction } },
        });

        this.dataSource.load(variables);
    }

    private restoreState() {
        this.hideResults.next(true);
        const state = this.historyStateService.get<AuctionTableState>();

        if (state.auctionTable) {
            this.form.setValue(state.auctionTable.form, { emitEvent: false });
            this.sortField = state.auctionTable.sort.field;
            this.sortDirection = state.auctionTable.sort.direction;
        } else {
            this.sortField = AuctionOrderField.EndsAt;
            this.sortDirection = 'asc';
        }

        setTimeout(() => this.reload());
    }

    private getCategoryFilter() {
        const categoryName = this.form.value.category;
        const category = this.categories.find(c => c.name === categoryName)!;
        return category.predicateBuilder();
    }

    private getSort(): AuctionOrder {
        return {
            field: this.sorter.active as any,
            direction: (this.sorter.direction.toUpperCase() as any) || null,
        };
    }
}
