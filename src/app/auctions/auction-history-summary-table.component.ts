import { Component, Input, OnInit, OnChanges, OnDestroy } from '@angular/core';
import { ReplaySubject, Subject } from 'rxjs';
import { delay, takeUntil } from 'rxjs/operators';

import { DataSourceFactory } from '../graphql/data-source-factory';
import { AuctionState, OrderDirection, AuctionOrderField } from '../graphql/graphql.generated';
import { AuctionTableDataSource } from '../graphql/auction-table-data-source';
import { TezosService } from '../tezos/tezos.service';
import dayjs from 'dayjs';

@Component({
    selector: 'td-auction-history-summary-table',
    templateUrl: './auction-history-summary-table.component.html',
    styleUrls: ['./auction-history-summary-table.component.scss'],
})
export class AuctionHistorySummaryTableComponent implements OnInit, OnChanges, OnDestroy {
    @Input() address: string;

    dataSource: AuctionTableDataSource;
    totalDataSource: AuctionTableDataSource;
    states = AuctionState;
    columns: string[] = [];

    private emptyStream = new ReplaySubject<boolean>(1);
    private unsubscribe = new Subject<void>();

    get empty$() {
        return this.emptyStream;
    }

    constructor(private dataSourceFactory: DataSourceFactory, private tezosService: TezosService) {}

    ngOnInit() {
        this.dataSource = this.dataSourceFactory.createAuctionTableDataSource();
        this.totalDataSource = this.dataSourceFactory.createAuctionTableDataSource();
        this.dataSource.empty$.pipe(takeUntil(this.unsubscribe), delay(0)).subscribe(this.emptyStream);

        this.tezosService.activeWallet.pipe(takeUntil(this.unsubscribe)).subscribe(w => {
            if (w?.address === this.address) {
                this.columns = ['name', 'ended', 'status', 'highestBid', 'actions'];
            } else {
                this.columns = ['name', 'ended', 'highestBid'];
            }
        });

        this.load();
    }

    ngOnChanges() {
        if (this.dataSource) {
            this.load();
        }
    }

    ngOnDestroy() {
        this.unsubscribe.next();
        this.unsubscribe.complete();
    }

    private load() {
        this.dataSource.load(
            {
                where: {
                    and: [
                        {
                            bidders: { include: this.address },
                            endsAtUtc: { greaterThanOrEqualTo: dayjs().subtract(7, 'days') },
                        },
                        {
                            or: [
                                {
                                    highestBidder: { notEqualTo: this.address },
                                    state: { in: [AuctionState.CanBeSettled] },
                                },
                                {
                                    state: { in: [AuctionState.Settled, AuctionState.SettlementExpired] },
                                },
                            ],
                        },
                    ],
                },
                order: { field: AuctionOrderField.EndsAt, direction: OrderDirection.Desc },
                first: 4,
            },
            {
                pollInterval: 15 * 60 * 1000,
            }
        );

        this.totalDataSource.load(
            {
                where: {
                    and: [
                        {
                            bidders: { include: this.address },
                        },
                        {
                            or: [
                                {
                                    highestBidder: { notEqualTo: this.address },
                                    state: { in: [AuctionState.CanBeSettled] },
                                },
                                {
                                    state: { in: [AuctionState.Settled, AuctionState.SettlementExpired] },
                                },
                            ],
                        },
                    ],
                },
                first: 1,
            },
            {
                pollInterval: 15 * 60 * 1000,
            }
        );
    }
}
