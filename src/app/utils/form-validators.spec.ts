import { DomainNameValidationResult, DomainNameValidator } from '@tezos-domains/core';
import { fakeAsync, tick } from '@angular/core/testing';
import { UntypedFormControl, ValidationErrors } from '@angular/forms';
import { TaquitoTezosDomainsClient } from '@tezos-domains/taquito-client';
import { NameResolver } from '@tezos-domains/resolver';
import { mock, when, instance, anyString, verify, anything } from 'ts-mockito';
import { TezosAddress, TestPromise } from '@td/test';
import { Observable, of, Subject } from 'rxjs';

import { TdValidators, TdAsyncValidatorsFactory } from './form-validators';
import { GraphQLError } from 'graphql';
import { TezosDomainsClientService } from '../tezos/integration/tezos-domains-client.service';
import { DomainService } from '../domains/domain.service';

describe('TdValidators', () => {
    describe('tezosAddress', () => {
        it('should not error for undefined', () => {
            expect(TdValidators.tezosAddress()(new UntypedFormControl(undefined))).toBeNull();
        });

        it('should not error for null', () => {
            expect(TdValidators.tezosAddress()(new UntypedFormControl(null))).toBeNull();
        });

        it('should not error for empty string', () => {
            expect(TdValidators.tezosAddress()(new UntypedFormControl(''))).toBeNull();
        });

        it('should not error for valid address', () => {
            expect(TdValidators.tezosAddress()(new UntypedFormControl(TezosAddress))).toBeNull();
        });

        it('should error for invalid address', () => {
            expect(TdValidators.tezosAddress()(new UntypedFormControl(TezosAddress.replace('tz1', 'xxx')))).toEqual({
                tezosAddress: { type: 'invalid' },
            });
        });

        it('should not allow entrypoint at the end of an address if specified', () => {
            expect(TdValidators.tezosAddress(false)(new UntypedFormControl('KT19pwVfLTxoLw9vwYRtUT7fbAVf3itB9Yq2%aaaa'))).toEqual({
                tezosAddress: { type: 'entrypoint' },
            });
        });
    });

    describe('label()', () => {
        let tezosDomainsValidatorMock: DomainNameValidator;

        beforeEach(() => {
            tezosDomainsValidatorMock = mock<DomainNameValidator>();

            when(tezosDomainsValidatorMock.isValidWithKnownTld(anyString())).thenReturn(DomainNameValidationResult.VALID);
        });

        it('should not error for undefined', () => {
            expect(TdValidators.label('necroskillz.tez', instance(tezosDomainsValidatorMock))(new UntypedFormControl(undefined))).toBeNull();
        });

        it('should not error for null', () => {
            expect(TdValidators.label('necroskillz.tez', instance(tezosDomainsValidatorMock))(new UntypedFormControl(null))).toBeNull();
        });

        it('should not error for empty string', () => {
            expect(TdValidators.label('necroskillz.tez', instance(tezosDomainsValidatorMock))(new UntypedFormControl(''))).toBeNull();
        });

        it('should not error for valid label', () => {
            expect(TdValidators.label('necroskillz.tez', instance(tezosDomainsValidatorMock))(new UntypedFormControl('aaa'))).toBeNull();
        });

        it('should error for invalid label', () => {
            when(tezosDomainsValidatorMock.isValidWithKnownTld('-aa.necroskillz.tez')).thenReturn(DomainNameValidationResult.INVALID_NAME);

            expect(TdValidators.label('necroskillz.tez', instance(tezosDomainsValidatorMock))(new UntypedFormControl('-aa'))).toEqual({
                label: { type: `tez-${DomainNameValidationResult.INVALID_NAME}` },
            });
        });

        it('should error if label contains a dot', () => {
            expect(TdValidators.label('necroskillz.tez', instance(tezosDomainsValidatorMock))(new UntypedFormControl('aa.aa'))).toEqual({
                label: { type: `tez-${DomainNameValidationResult.UNSUPPORTED_CHARACTERS}` },
            });
        });
    });

    describe('except()', () => {
        it('should not error for undefined', () => {
            expect(TdValidators.except(['a', 'b'])(new UntypedFormControl(undefined))).toBeNull();
        });

        it('should not error for null', () => {
            expect(TdValidators.except(['a', 'b'])(new UntypedFormControl(null))).toBeNull();
        });

        it('should not error for empty string', () => {
            expect(TdValidators.except(['a', 'b'])(new UntypedFormControl(''))).toBeNull();
        });

        it('should not error for value outside the set', () => {
            expect(TdValidators.except(['a', 'b'])(new UntypedFormControl('c'))).toBeNull();
        });

        it('should error for value inside the set', () => {
            expect(TdValidators.except(['a', 'b'])(new UntypedFormControl('b'))).toEqual({ except: { existing: 'a, b' } });
        });
    });

    describe('url', () => {
        it('should not error for undefined', () => {
            expect(TdValidators.url()(new UntypedFormControl(undefined))).toBeNull();
        });

        it('should not error for null', () => {
            expect(TdValidators.url()(new UntypedFormControl(null))).toBeNull();
        });

        it('should not error for empty string', () => {
            expect(TdValidators.url()(new UntypedFormControl(''))).toBeNull();
        });

        it('should not error for valid url', () => {
            expect(TdValidators.url()(new UntypedFormControl('https://www.google.com'))).toBeNull();
        });

        it('should not error for valid https url', () => {
            expect(TdValidators.url(true)(new UntypedFormControl('https://www.google.com'))).toBeNull();
        });

        it('should error for not https url', () => {
            expect(TdValidators.url(true)(new UntypedFormControl('http://www.google.com'))).toEqual({ url: true });
        });

        it('should error for invalid url', () => {
            expect(TdValidators.url()(new UntypedFormControl('-aa'))).toEqual({ url: true });
        });
    });

    describe('md5', () => {
        it('should not error for undefined', () => {
            expect(TdValidators.md5(new UntypedFormControl(undefined))).toBeNull();
        });

        it('should not error for null', () => {
            expect(TdValidators.md5(new UntypedFormControl(null))).toBeNull();
        });

        it('should not error for empty string', () => {
            expect(TdValidators.md5(new UntypedFormControl(''))).toBeNull();
        });

        it('should not error for valid md5', () => {
            expect(TdValidators.md5(new UntypedFormControl('efdbd34e1b065d092fca88e8f91649ce'))).toBeNull();
        });

        it('should error for invalid md5', () => {
            expect(TdValidators.md5(new UntypedFormControl('bleh'))).toEqual({ md5: true });
        });
    });

    describe('json', () => {
        it('should not error for undefined', () => {
            expect(TdValidators.json(new UntypedFormControl(undefined))).toBeNull();
        });

        it('should not error for null', () => {
            expect(TdValidators.json(new UntypedFormControl(null))).toBeNull();
        });

        it('should not error for empty string', () => {
            expect(TdValidators.json(new UntypedFormControl(''))).toBeNull();
        });

        it('should not error for valid json', () => {
            expect(TdValidators.json(new UntypedFormControl('{ "hello": "world" }'))).toBeNull();
        });

        it('should error for invalid json', () => {
            expect(TdValidators.json(new UntypedFormControl('{ "hell'))).toEqual({ json: true });
        });
    });

    describe('derivationPath', () => {
        it('should not error for undefined', () => {
            expect(TdValidators.derivationPath(new UntypedFormControl(undefined))).toBeNull();
        });

        it('should not error for null', () => {
            expect(TdValidators.derivationPath(new UntypedFormControl(null))).toBeNull();
        });

        it('should not error for empty string', () => {
            expect(TdValidators.derivationPath(new UntypedFormControl(''))).toBeNull();
        });

        it('should not error for valid derivation path', () => {
            expect(TdValidators.derivationPath(new UntypedFormControl(`44'/1729'/0'/0'`))).toBeNull();
        });

        it('should error for invalid derivation path', () => {
            expect(TdValidators.derivationPath(new UntypedFormControl('aa'))).toEqual({ derivationPath: true });
        });
    });

    describe('bytesString', () => {
        it('should not error for undefined', () => {
            expect(TdValidators.bytesString(new UntypedFormControl(undefined))).toBeNull();
        });

        it('should not error for null', () => {
            expect(TdValidators.bytesString(new UntypedFormControl(null))).toBeNull();
        });

        it('should not error for empty string', () => {
            expect(TdValidators.bytesString(new UntypedFormControl(''))).toBeNull();
        });

        it('should not error for valid bytes', () => {
            expect(TdValidators.bytesString(new UntypedFormControl('a59b95'))).toBeNull();
        });

        it('should error for invalid bytes', () => {
            expect(TdValidators.bytesString(new UntypedFormControl('0F'))).toEqual({ bytesString: true });
            expect(TdValidators.bytesString(new UntypedFormControl('123'))).toEqual({ bytesString: true });
            expect(TdValidators.bytesString(new UntypedFormControl('1s'))).toEqual({ bytesString: true });
        });
    });
});

describe('TdAsyncValidatorFactory', () => {
    let factory: TdAsyncValidatorsFactory;
    let domainServiceMock: DomainService;
    let tezosDomainsClientServiceMock: TezosDomainsClientService;
    let tezosDomainsMock: TaquitoTezosDomainsClient;
    let tezosDomainsResolverMock: NameResolver;
    let tezosDomainsValidatorMock: DomainNameValidator;
    let spy: jasmine.Spy;
    let getAddress: TestPromise<string | null>;
    let isDomainAvailable: Subject<boolean>;
    let hasAddress: Subject<boolean>;

    beforeEach(() => {
        tezosDomainsClientServiceMock = mock(TezosDomainsClientService);
        tezosDomainsMock = mock(TaquitoTezosDomainsClient);
        tezosDomainsResolverMock = mock<NameResolver>();
        domainServiceMock = mock(DomainService);
        tezosDomainsValidatorMock = mock<DomainNameValidator>();
        getAddress = new TestPromise();
        isDomainAvailable = new Subject();
        hasAddress = new Subject();
        spy = jasmine.createSpy();

        when(tezosDomainsClientServiceMock.current).thenReturn(of(instance(tezosDomainsMock)));
        when(tezosDomainsMock.resolver).thenReturn(instance(tezosDomainsResolverMock));
        when(tezosDomainsMock.validator).thenReturn(instance(tezosDomainsValidatorMock));
        when(tezosDomainsResolverMock.resolveNameToAddress('necroskillz.tez')).thenReturn(getAddress);
        when(domainServiceMock.isDomainAvailable(anyString())).thenReturn(isDomainAvailable);
        when(domainServiceMock.hasAddress('necroskillz.tez', TezosAddress)).thenReturn(hasAddress);
        when(tezosDomainsValidatorMock.isValidWithKnownTld(anyString())).thenReturn(DomainNameValidationResult.VALID);

        factory = new TdAsyncValidatorsFactory(instance(domainServiceMock), instance(tezosDomainsClientServiceMock));
    });

    describe('subdomainNameAvailable()', () => {
        it('should check if domain already exists and return null if it doesnt', fakeAsync(() => {
            const validation = factory.subdomainNameAvailable('necroskillz.tez')(
                new UntypedFormControl('sub')
            ) as unknown as Observable<ValidationErrors | null>;
            validation.subscribe(spy);
            tick(300);

            verify(domainServiceMock.isDomainAvailable('sub.necroskillz.tez')).called();
            isDomainAvailable.next(true);

            expect(spy).toHaveBeenCalledWith(null);
        }));

        it('not validate empty value', fakeAsync(() => {
            const validation = factory.subdomainNameAvailable('necroskillz.tez')(new UntypedFormControl('')) as unknown as Observable<ValidationErrors | null>;
            validation.subscribe(spy);
            tick(300);

            verify(domainServiceMock.isDomainAvailable(anything())).never();

            expect(spy).toHaveBeenCalledWith(null);
        }));

        it('should check if domain already exists and return error if it does', fakeAsync(() => {
            const validation = factory.subdomainNameAvailable('necroskillz.tez')(
                new UntypedFormControl('sub')
            ) as unknown as Observable<ValidationErrors | null>;
            validation.subscribe(spy);
            tick(300);

            verify(domainServiceMock.isDomainAvailable('sub.necroskillz.tez')).called();
            isDomainAvailable.next(false);

            expect(spy).toHaveBeenCalledWith({ duplicateSubdomain: true });
        }));

        it('should debounce', fakeAsync(() => {
            const validation = factory.subdomainNameAvailable('necroskillz.tez')(
                new UntypedFormControl('sub')
            ) as unknown as Observable<ValidationErrors | null>;
            validation.subscribe(spy);
            tick();

            verify(domainServiceMock.isDomainAvailable(anything())).never();

            tick(300);

            verify(domainServiceMock.isDomainAvailable('sub.necroskillz.tez')).called();
            isDomainAvailable.next(true);

            expect(spy).toHaveBeenCalledWith(null);
        }));

        it('should return generic error if request fails with', fakeAsync(() => {
            const validation = factory.subdomainNameAvailable('necroskillz.tez')(
                new UntypedFormControl('sub')
            ) as unknown as Observable<ValidationErrors | null>;
            validation.subscribe(spy);
            tick(300);

            isDomainAvailable.error(new GraphQLError('msg'));
            tick();

            expect(spy).toHaveBeenCalledWith({ asyncError: { message: 'msg' } });
        }));
    });

    describe('validRecipient()', () => {
        it('should return null valid for valid address', () => {
            const validation = factory.validRecipient()(new UntypedFormControl(TezosAddress)) as unknown as Observable<ValidationErrors | null>;
            validation.subscribe(spy);

            expect(spy).toHaveBeenCalledWith(null);
        });

        it('should return kt not allowed for KT1 address if disallowed', () => {
            const validation = factory.validRecipient({ disallowKT: true })(
                new UntypedFormControl('KT1VzcRCm6XH7mUHFCLnFp6XNDpLqrMRKcmq')
            ) as unknown as Observable<ValidationErrors | null>;
            validation.subscribe(spy);

            expect(spy).toHaveBeenCalledWith({ validRecipient: { type: 'kt-not-allowed' } });
        });

        it('should return invalid input for invalid domain name', () => {
            when(tezosDomainsValidatorMock.isValidWithKnownTld(anyString())).thenReturn(DomainNameValidationResult.UNSUPPORTED_CHARACTERS);

            const validation = factory.validRecipient()(new UntypedFormControl('ble')) as unknown as Observable<ValidationErrors | null>;
            validation.subscribe(spy);

            expect(spy).toHaveBeenCalledWith({ validRecipient: { type: 'invalid-input' } });
        });

        it('not validate empty value', () => {
            const validation = factory.validRecipient()(new UntypedFormControl('')) as unknown as Observable<ValidationErrors | null>;
            validation.subscribe(spy);

            expect(spy).toHaveBeenCalledWith(null);
        });

        it('should return valid for domain that resolves to an address', fakeAsync(() => {
            const validation = factory.validRecipient()(new UntypedFormControl('necroskillz.tez')) as unknown as Observable<ValidationErrors | null>;
            validation.subscribe(spy);
            tick(300);

            getAddress.resolve(TezosAddress);
            tick();

            expect(spy).toHaveBeenCalledWith(null);
        }));

        it('should return no address for domain that does not resolve to an address', fakeAsync(() => {
            const validation = factory.validRecipient()(new UntypedFormControl('necroskillz.tez')) as unknown as Observable<ValidationErrors | null>;
            validation.subscribe(spy);
            tick(300);

            getAddress.resolve(null);
            tick();

            expect(spy).toHaveBeenCalledWith({ validRecipient: { type: 'no-address' } });
        }));

        it('should return generic error resolving of address fails', fakeAsync(() => {
            const validation = factory.validRecipient()(new UntypedFormControl('necroskillz.tez')) as unknown as Observable<ValidationErrors | null>;
            validation.subscribe(spy);
            tick(300);

            getAddress.reject(new Error('msg'));
            tick();

            expect(spy).toHaveBeenCalledWith({ asyncError: { message: 'msg' } });
        }));
    });

    describe('domainWithAddress()', () => {
        it('should return invalid input for invalid domain', () => {
            when(tezosDomainsValidatorMock.isValidWithKnownTld(anyString())).thenReturn(DomainNameValidationResult.UNSUPPORTED_CHARACTERS);

            const validation = factory.domainWithAddress(TezosAddress)(new UntypedFormControl('ble')) as unknown as Observable<ValidationErrors | null>;
            validation.subscribe(spy);

            expect(spy).toHaveBeenCalledWith({ domainWithAddress: { type: 'invalid-input' } });
        });

        it('not validate empty value', () => {
            const validation = factory.domainWithAddress(TezosAddress)(new UntypedFormControl('')) as unknown as Observable<ValidationErrors | null>;
            validation.subscribe(spy);

            expect(spy).toHaveBeenCalledWith(null);
        });

        it('should return valid for domain that has matching address', fakeAsync(() => {
            const validation = factory.domainWithAddress(TezosAddress)(
                new UntypedFormControl('necroskillz.tez')
            ) as unknown as Observable<ValidationErrors | null>;
            validation.subscribe(spy);
            tick(300);

            hasAddress.next(true);
            tick();

            expect(spy).toHaveBeenCalledWith(null);
        }));

        it('should return no address for domain that does not resolve to an address', fakeAsync(() => {
            const validation = factory.domainWithAddress(TezosAddress)(
                new UntypedFormControl('necroskillz.tez')
            ) as unknown as Observable<ValidationErrors | null>;
            validation.subscribe(spy);
            tick(300);

            hasAddress.next(false);
            tick();

            expect(spy).toHaveBeenCalledWith({ domainWithAddress: { type: 'address-mismatch' } });
        }));

        it('should return generic error resolving fails', fakeAsync(() => {
            const validation = factory.domainWithAddress(TezosAddress)(
                new UntypedFormControl('necroskillz.tez')
            ) as unknown as Observable<ValidationErrors | null>;
            validation.subscribe(spy);
            tick(300);

            hasAddress.error(new Error('msg'));
            tick();

            expect(spy).toHaveBeenCalledWith({ asyncError: { message: 'msg' } });
        }));
    });
});
