import { Router } from '@angular/router';
import { DomainNameValidationResult, DomainNameValidator } from '@tezos-domains/core';
import { TaquitoTezosDomainsClient } from '@tezos-domains/taquito-client';
import { of, Subject } from 'rxjs';
import { anyString, instance, mock, when } from 'ts-mockito';

import { TrackingService } from '../browser/tracking.service';
import { DomainService } from '../domains/domain.service';
import { TezosDomainsClientService } from '../tezos/integration/tezos-domains-client.service';
import { SearchService } from './search.service';

describe('SearchService', () => {
    let service: SearchService;
    let tezosDomainsClientServiceMock: TezosDomainsClientService;
    let tezosDomainsClientMock: TaquitoTezosDomainsClient;
    let tezosDomainsValidatorMock: DomainNameValidator;
    let routerMock: Router;
    let trackingServiceMock: TrackingService;
    let domainServiceMock: DomainService;
    let isAvailable: Subject<boolean>;

    beforeEach(() => {
        tezosDomainsClientServiceMock = mock(TezosDomainsClientService);
        tezosDomainsClientMock = mock(TaquitoTezosDomainsClient);
        tezosDomainsValidatorMock = mock<DomainNameValidator>();
        routerMock = mock(Router);
        trackingServiceMock = mock(TrackingService);
        domainServiceMock = mock(DomainService);

        isAvailable = new Subject();

        when(routerMock.url).thenReturn('');
        when(tezosDomainsClientServiceMock.current).thenReturn(of(instance(tezosDomainsClientMock)));
        when(tezosDomainsClientMock.validator).thenReturn(instance(tezosDomainsValidatorMock));
        when(tezosDomainsValidatorMock.supportedTLDs).thenReturn(['tez', 'com']);
        when(tezosDomainsValidatorMock.isValidWithKnownTld(anyString())).thenCall(s =>
            s.endsWith('.tez') ? DomainNameValidationResult.VALID : DomainNameValidationResult.INVALID_NAME
        );
        when(domainServiceMock.isDomainAvailable(anyString())).thenReturn(isAvailable);

        service = new SearchService(instance(tezosDomainsClientServiceMock), instance(routerMock), instance(trackingServiceMock), instance(domainServiceMock));
    });

    const TEST_CASES = [
        { domain: 'necroskillz.tez', sanitized: 'necroskillz.tez' },
        { domain: 'necroskillz.dev', sanitized: 'necroskillz.tez' },
        { domain: 'necroskillz.com', sanitized: 'necroskillz.com' },
        { domain: 'com', sanitized: 'com.tez' },
        { domain: 'necroskillz', sanitized: 'necroskillz.tez' },
        { domain: 'NEcrOSkiLLz', sanitized: 'necroskillz.tez' },
        { domain: 'NEcrOSkiLLz.TEZ', sanitized: 'necroskillz.tez' },
        { domain: 'necros.killz', sanitized: 'necros.tez' },
        { domain: '-n-.-e-.croskillz', sanitized: 'ne.tez' },
        { domain: ' \t\n\r-necroskillz-\t\r\n ', sanitized: 'necroskillz.tez' },
        { domain: 'ne$c()ro^!skěišlčlřz=', sanitized: 'necroskillz.tez' },
        { domain: '', sanitized: '' },
        { domain: '¯\\_(ツ)_/¯', sanitized: '' },
        { domain: 'tez', sanitized: 'tez.tez' },
        { domain: 'tez.', sanitized: 'tez.tez' },
        { domain: '.tez', sanitized: 'tez.tez' },
        { domain: '.', sanitized: '' },
    ];

    TEST_CASES.forEach(t => {
        it(`should sanitize: ${t.domain} => ${t.sanitized}`, () => {
            const spy = jasmine.createSpy();

            const result = service.sanitizeDomainName(instance(tezosDomainsClientMock), t.domain);

            expect(result).toBe(t.sanitized);
        });
    });
});
