import { isTezosAddress } from './validate';

describe('isTezosAddress', () => {
    it('should return true for tezos addresses', () => {
        expect(isTezosAddress('tz1VxMudmADssPp6FPDGRsvJXE41DD6i9g6n')).toBeTrue();
        expect(isTezosAddress('tz2TSvNTh2epDMhZHrw73nV9piBX7kLZ9K9m')).toBeTrue();
        expect(isTezosAddress('tz3WMqdzXqRWXwyvj5Hp2H7QEepaUuS7vd9K')).toBeTrue();
        expect(isTezosAddress('KT1TRHzT3HdLe3whe35q6rNxavGx8WVFHSpH')).toBeTrue();
    });

    it('should return false for not tezos addresses', () => {
        expect(isTezosAddress('')).toBeFalse();
        expect(isTezosAddress('tz1VxMudmADssPp6FPDGRsvJXE41DD6i9g6')).toBeFalse();
        expect(isTezosAddress('tz1VxMudmADssPp6FPDGRsvJXE41DD6i9gXX')).toBeFalse();
        expect(isTezosAddress('tz1')).toBeFalse();
        expect(isTezosAddress('aaa')).toBeFalse();
    });
});
