import { emptyStringToNull } from './convert';

describe('convert', () => {
    describe('emptyStringToNull()', () => {
        it('should return null for empty values', () => {
            expect(emptyStringToNull(undefined)).toBe(null);
            expect(emptyStringToNull(null)).toBe(null);
            expect(emptyStringToNull('')).toBe(null);
        });

        it('should return same string for non empty values', () => {
            expect(emptyStringToNull('aa')).toBe('aa');
        });
    });
});
