export type Unarray<T> = T extends Array<infer U> ? U : T;

export function notNil<T>(value: T | null | undefined): value is T {
    return !!value;
}
