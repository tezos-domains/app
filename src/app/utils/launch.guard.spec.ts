import { Router } from '@angular/router';
import { ReplaySubject } from 'rxjs';
import { instance, mock, verify, when, deepEqual } from 'ts-mockito';

import { LaunchGuard } from './launch.guard';
import { TezosService } from '../tezos/tezos.service';
import { TezosWallet } from '../tezos/models';
import { TestWallet, TezosAddress } from '../../test/data';

describe('LaunchGuard', () => {
    let guard: LaunchGuard;
    let routerMock: Router;
    let tezosServiceMock: TezosService;
    let wallet: ReplaySubject<TezosWallet | null>;

    beforeEach(() => {
        routerMock = mock(Router);
        tezosServiceMock = mock(TezosService);

        wallet = new ReplaySubject(1);

        when(tezosServiceMock.activeWallet).thenReturn(wallet);

        guard = new LaunchGuard(instance(routerMock), instance(tezosServiceMock));
    });

    it('should navigate to home if user is not logged in', () => {
        const spy = jasmine.createSpy();

        wallet.next(null);

        guard.canActivate().subscribe(spy);

        verify(routerMock.navigate(deepEqual(['/']), deepEqual({ replaceUrl: true })));
        expect(spy).toHaveBeenCalledWith(false);
    });

    it('should navigate to home if user is logged in', () => {
        const spy = jasmine.createSpy();

        wallet.next(TestWallet);

        guard.canActivate().subscribe(spy);

        verify(routerMock.navigate(deepEqual(['/address', TezosAddress]), deepEqual({ replaceUrl: true })));
        expect(spy).toHaveBeenCalledWith(false);
    });
});
