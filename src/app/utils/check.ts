import { tokenizeDomainName } from '@tezos-domains/core';

export function isSecondLevel(name: string) {
    return tokenizeDomainName(name).length === 2;
}
