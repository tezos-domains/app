import { isSecondLevel } from './check';

describe('check', () => {
    describe('isSecondLevel()', () => {
        it('should return true for second level domain', () => {
            expect(isSecondLevel('necroskillz.tez')).toBeTrue();
        });

        it('should return false for non second level domain', () => {
            expect(isSecondLevel('tez')).toBeFalse();
            expect(isSecondLevel('play.necroskillz.tez')).toBeFalse();
            expect(isSecondLevel('a.play.necroskillz.tez')).toBeFalse();
        });
    });
});
