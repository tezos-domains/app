import { Component } from '@angular/core';

@Component({
    selector: 'td-events-list',
    templateUrl: './events-list.component.html',
    styleUrls: ['./events-list.component.scss'],
})
export class EventsListComponent {}
