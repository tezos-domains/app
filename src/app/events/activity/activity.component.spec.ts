import { TranslocoModule } from '@ngneat/transloco';
import { providers, TestNetwork, TezosAddress, transloco } from '@td/test';
import { TaquitoTezosDomainsClient } from '@tezos-domains/taquito-client';
import BigNumber from 'bignumber.js';
import dayjs from 'dayjs';
import { MockBuilder, MockedComponentFixture, MockRender, ngMocks } from 'ng-mocks';
import { of } from 'rxjs';
import { DataSourceFactory } from 'src/app/graphql/data-source-factory';
import { EventsDataSource } from 'src/app/graphql/events-table-data-source';
import { EventsFilter, EventType } from 'src/app/graphql/graphql.generated';
import { SharedModule } from 'src/app/shared/shared.module';
import { TezosDomainsClientService } from 'src/app/tezos/integration/tezos-domains-client.service';
import { TezosNetworkService } from 'src/app/tezos/tezos-network.service';
import { deepEqual, instance, mock, verify, when } from 'ts-mockito';
import { EventsModule } from '../events.module';
import { ActivityFormComponent } from './activity-form.component';
import { ActivityComponent } from './activity.component';

describe('ActivityComponent', () => {
    let component: ActivityComponent;
    let fixture: MockedComponentFixture<ActivityComponent, Partial<ActivityComponent>>;

    let dataSourceFactory: DataSourceFactory;
    let tezosNetworkServiceMock: TezosNetworkService;
    let tezosDomainsClientMock: TezosDomainsClientService;
    let eventsDataSourceMock: EventsDataSource;
    let tezosDomainsMock: TaquitoTezosDomainsClient;

    beforeEach(() => {
        dataSourceFactory = mock(DataSourceFactory);
        tezosNetworkServiceMock = mock(TezosNetworkService);
        tezosDomainsClientMock = mock(TezosDomainsClientService);
        tezosDomainsMock = mock(TaquitoTezosDomainsClient);

        eventsDataSourceMock = mock(EventsDataSource);
        when(dataSourceFactory.createEventsDataSource()).thenReturn(instance(eventsDataSourceMock));
        when(tezosNetworkServiceMock.activeNetwork).thenReturn(of(TestNetwork));
        when(tezosDomainsClientMock.current).thenReturn(of(instance(tezosDomainsMock)));

        return MockBuilder(ActivityComponent, EventsModule)
            .mock(SharedModule)
            .keep(TranslocoModule, { export: true })
            .keep(transloco(), { export: true })
            .provide([providers(dataSourceFactory, tezosNetworkServiceMock, tezosDomainsClientMock)]);
    });

    describe('when address is provided', () => {
        beforeEach(() => {
            init();
            fixture.detectChanges();
        });

        it('should load data when filter updated', () => {
            const now = dayjs();
            const yesterday = dayjs().subtract(1, 'day');

            const form = ngMocks.findInstance(ActivityFormComponent);
            const fromPrice = new BigNumber(1_000_000);

            form.filterChange.next({
                address: TezosAddress,
                domainName: 'blue-dough.ith',
                types: [EventType.DomainBuyEvent],
                price: { from: fromPrice },
                date: { from: yesterday, to: now },
            });

            const expectedFilter: EventsFilter = {
                type: { in: [EventType.DomainBuyEvent] },
                address: { equalTo: TezosAddress },
                domainName: { equalTo: 'blue-dough.ith' },
                price: { greaterThanOrEqualTo: fromPrice },
                block: { timestamp: { greaterThanOrEqualTo: yesterday, lessThanOrEqualTo: now } },
            };

            verify(eventsDataSourceMock.load(deepEqual({ where: expectedFilter }))).once();
        });
    });

    function init(data?: Partial<ActivityComponent>) {
        fixture = MockRender(ActivityComponent, data ?? {});
        component = fixture.point.componentInstance;
    }
});
