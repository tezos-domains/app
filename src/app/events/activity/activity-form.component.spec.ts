import { fakeAsync, tick } from '@angular/core/testing';
import { TranslocoModule } from '@ngneat/transloco';
import { provider, resetNow, TestConfig, TezosAddress, transloco } from '@td/test';
import BigNumber from 'bignumber.js';
import dayjs from 'dayjs';
import { flatMap } from 'lodash-es';
import { MockBuilder, MockedComponentFixture, MockRender, ngMocks } from 'ng-mocks';
import { FilterHistoryStateService } from 'src/app/browser/filter-history-state.service';
import { AddressFilterComponent } from 'src/app/filters/address-filter.component';
import { DateFilterComponent } from 'src/app/filters/date-filter.component';
import { DomainFilterComponent } from 'src/app/filters/domain-filter.component';
import { ListOptionType } from 'src/app/filters/list-filter.component';
import { PriceFilterComponent } from 'src/app/filters/price-filter.component';
import { EventType } from 'src/app/graphql/graphql.generated';
import { mock, when } from 'ts-mockito';
import { Configuration } from '../../configuration';
import { SharedModule } from '../../shared/shared.module';
import { EventsModule } from '../events.module';
import { ActivityFormComponent, ActivityFormFilterState } from './activity-form.component';
import { ActivityFormFilter } from './model';

describe('ActivityFormComponent', () => {
    let component: ActivityFormComponent;
    let fixture: MockedComponentFixture<ActivityFormComponent, Partial<ActivityFormComponent>>;
    let historyStateMock: FilterHistoryStateService;

    beforeEach(() => {
        historyStateMock = mock(FilterHistoryStateService);

        return MockBuilder(ActivityFormComponent, EventsModule)
            .mock(SharedModule)
            .keep(TranslocoModule, { export: true })
            .keep(transloco(), { export: true })
            .provide([provider(historyStateMock), { provide: Configuration, useValue: TestConfig }]);
    });

    afterEach(() => {
        resetNow();
    });

    describe('onInit()', () => {
        it('should output default filter state', fakeAsync(() => {
            when(historyStateMock.get<ActivityFormFilterState>()).thenReturn({});

            init();

            const onFilterChange = jasmine.createSpy('onFilterChange');
            component.filterChange.subscribe(onFilterChange);

            tick(350);

            expect(onFilterChange).toHaveBeenCalledWith(<ActivityFormFilter>{
                address: '',
                domainName: '',
                types: flatMap(component.eventTypeOptions.filter(x => x.type === ListOptionType.Item).map(x => x.values.map(v => <EventType>v))),
                date: { from: dayjs(TestConfig.minEventsFilterDate).startOf('day'), to: dayjs().endOf('day') },
                price: null,
            });
        }));

        it('should output saved state', fakeAsync(() => {
            when(historyStateMock.get<ActivityFormFilterState>()).thenReturn({
                activityAddress: 'addr',
                activityDate: { from: dayjs().subtract(2, 'days'), to: dayjs().subtract(1, 'day') },
                activityDomain: 'domain',
                activityPrice: { min: 1, max: 89 },
                activityEventType: [EventType.DomainBuyEvent, EventType.DomainUpdateOperatorsEvent],
            });

            init();

            const onFilterChange = jasmine.createSpy('onFilterChange');
            component.filterChange.subscribe(onFilterChange);

            tick(350);

            expect(onFilterChange).toHaveBeenCalledWith(<ActivityFormFilter>{
                address: 'addr',
                domainName: 'domain',
                types: [EventType.DomainBuyEvent],
                date: { from: dayjs().subtract(2, 'days').startOf('day'), to: dayjs().subtract(1, 'day').endOf('day') },
                price: { from: new BigNumber(1_000_000), to: new BigNumber(89_000_000) },
            });
        }));
    });

    describe('on child filter changed', () => {
        let onFilterChange: jasmine.Spy;
        beforeEach(() => {
            when(historyStateMock.get<ActivityFormFilterState>()).thenReturn({});
            init();

            onFilterChange = jasmine.createSpy('onFilterChange');
            component.filterChange.subscribe(onFilterChange);

            fixture.detectChanges();
        });

        it('should emit updated address', fakeAsync(() => {
            const addressFilter = ngMocks.findInstance(AddressFilterComponent);
            addressFilter.addressChange.next(TezosAddress);

            tick(350);

            expect(onFilterChange).toHaveBeenCalledWith(jasmine.objectContaining({ address: TezosAddress }));
        }));

        it('should emit updated domain', fakeAsync(() => {
            const domainFilter = ngMocks.findInstance(DomainFilterComponent);
            domainFilter.domainChange.next('blue-moon.han');

            tick(350);

            expect(onFilterChange).toHaveBeenCalledWith(jasmine.objectContaining({ domainName: 'blue-moon.han' }));
        }));

        it('should emit updated price', fakeAsync(() => {
            const priceFilter = ngMocks.findInstance(PriceFilterComponent);
            priceFilter.priceFilterChange.next({ min: 1, max: 2 });

            tick(350);

            expect(onFilterChange).toHaveBeenCalledWith(jasmine.objectContaining({ price: { from: new BigNumber(1_000_000), to: new BigNumber(2_000_000) } }));

            onFilterChange.calls.reset();
            priceFilter.priceFilterChange.next({ min: 1 });

            tick(350);

            expect(onFilterChange).toHaveBeenCalledWith(jasmine.objectContaining({ price: { from: new BigNumber(1_000_000) } }));
            onFilterChange.calls.reset();

            priceFilter.priceFilterChange.next(null);

            tick(350);

            expect(onFilterChange).toHaveBeenCalledWith(jasmine.objectContaining({ price: null }));
        }));

        it('should emit updated date', fakeAsync(() => {
            const dateFilter = ngMocks.findInstance(DateFilterComponent);

            dateFilter.dateRangeChange.next({ from: dayjs() });

            tick(350);

            expect(onFilterChange).toHaveBeenCalledWith(jasmine.objectContaining({ date: { from: dayjs().startOf('day'), to: undefined } }));
        }));
    });

    function init(data?: Partial<ActivityFormComponent>) {
        fixture = MockRender(ActivityFormComponent, data ?? {});
        component = fixture.point.componentInstance;
    }
});
