import { SortDirection } from '@angular/material/sort';
import BigNumber from 'bignumber.js';
import { EventOrderField, EventType } from '../../graphql/graphql.generated';
import { DateFilter } from '../../filters/model';

export interface ActivityFormFilter {
    date: DateFilter;
    types: EventType[];
    address: string;
    domainName: string;
    price: { from?: BigNumber; to?: BigNumber } | null;
}

export interface ActivitySorting {
    field: EventOrderField;
    direction: SortDirection;
}
