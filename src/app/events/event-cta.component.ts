import { Component, Input, OnInit, OnDestroy, Output, EventEmitter } from '@angular/core';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { EventsRecord } from '../graphql/events-table-data-source';
import { EventType } from '../graphql/graphql.generated';
import { TezosService } from '../tezos/tezos.service';
import { OverlayService } from '../overlay.service';
import { Router } from '@angular/router';
import { getEventType } from './utils';

@Component({
    selector: 'td-event-cta',
    templateUrl: './event-cta.component.html',
    styleUrls: ['./event-cta.component.scss'],
})
export class EventCtaComponent implements OnInit, OnDestroy {
    @Input() event: EventsRecord;
    @Input() buttonStyle: string;

    @Output() update = new EventEmitter<void>();

    action: (() => void) | null;
    type: string;

    private unsubscribe = new Subject<void>();

    constructor(private tezosService: TezosService, private overlayService: OverlayService, private router: Router) {}

    ngOnInit() {
        this.tezosService.activeWallet.pipe(takeUntil(this.unsubscribe)).subscribe(w => {
            let domain = '';
            let reverseRecordAddress = '';
            if ('domainName' in this.event) {
                domain = this.event.domainName;
            }
            if ('reverseRecordAddress' in this.event) {
                reverseRecordAddress = this.event.reverseRecordAddress;
            }

            this.type = getEventType(this.event, w?.address);

            switch (this.event.type) {
                case EventType.AuctionEndEvent:
                    if (this.event.sourceAddress === w?.address) {
                        this.action = () => this.router.navigate(['/domain', domain]);
                    } else {
                        w?.bidderBalances.pipe(takeUntil(this.unsubscribe)).subscribe(bb => {
                            if (bb.reduce((a, b) => a + b.balance, 0)) {
                                this.action = () => {
                                    const dialog = this.overlayService.openWithdraw();
                                    dialog.afterClosed().subscribe(result => {
                                        if (result) {
                                            this.update.next();
                                        }
                                    });
                                };
                            } else {
                                this.action = null;
                            }
                        });
                    }
                    break;
                case EventType.AuctionBidEvent:
                case EventType.AuctionSettleEvent:
                case EventType.DomainBuyEvent:
                case EventType.DomainGrantEvent:
                case EventType.DomainSetChildRecordEvent:
                case EventType.DomainUpdateEvent:
                case EventType.DomainClaimEvent:
                    this.action = () => this.router.navigate(['/domain', domain]);
                    break;
                case EventType.ReverseRecordClaimEvent:
                case EventType.ReverseRecordUpdateEvent:
                    this.action = () => this.router.navigate(['/reverse-record', reverseRecordAddress]);
                    break;
            }
        });
    }

    ngOnDestroy() {
        this.unsubscribe.next();
        this.unsubscribe.complete();
    }
}
