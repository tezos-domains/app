import { Component } from '@angular/core';

@Component({
    selector: 'td-expired-domains',
    templateUrl: './expired-domains.component.html',
    styleUrls: ['./expired-domains.component.scss'],
})
export class ExpiredDomainsComponent {}
