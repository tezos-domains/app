import { fakeAsync, TestBed, tick } from '@angular/core/testing';
import { Router } from '@angular/router';
import { TranslocoModule } from '@ngneat/transloco';
import { getDomain, mockNow, provider, resetNow, TestConfig, transloco } from '@td/test';
import { DomainNameValidator } from '@tezos-domains/core';
import { DomainAcquisitionAuctionInfo, DomainAcquisitionInfo, DomainAcquisitionState, DomainsManager } from '@tezos-domains/manager';
import { TaquitoTezosDomainsClient } from '@tezos-domains/taquito-client';
import { APOLLO_OPTIONS } from 'apollo-angular';
import { ApolloTestingController, ApolloTestingModule } from 'apollo-angular/testing';
import dayjs, { Dayjs } from 'dayjs';
import { MockBuilder, MockedComponentFixture, MockRender, ngMocks } from 'ng-mocks';
import { of, Subject } from 'rxjs';
import { anyNumber, anything, instance, mock, verify, when } from 'ts-mockito';
import { AppService } from '../app-service';
import { Configuration } from '../configuration';
import { ErrorModule } from '../error/error.module';
import { DataSourceFactory } from '../graphql/data-source-factory';
import { DomainDetailQuery, DomainOrderField, DomainsListDocument, DomainsListGQL } from '../graphql/graphql.generated';
import { HumanizedDateComponent } from '../i18n/humanized-date.component';
import { DomainListingComponent } from '../domain-common/domain-listing.component';
import { SharedModule } from '../shared/shared.module';
import { TezosDomainsClientService } from '../tezos/integration/tezos-domains-client.service';
import { TldConfigurationService } from '../tezos/integration/tld-configuration.service';
import { DomainsModule } from './domains.module';
import { ExpiredDomainsTableComponent } from './expired-domains-table.component';
import { DomainNameType } from './expired-domains-filter.component';

describe('ExpiredDomainsTableComponent', () => {
    let fixture: MockedComponentFixture<ExpiredDomainsTableComponent, Partial<ExpiredDomainsTableComponent>>;
    let component: ExpiredDomainsTableComponent;
    let apollo: ApolloTestingController;

    let routerMock: Router;
    let tezosDomainsMock: TaquitoTezosDomainsClient;
    let tezosDomainsValidatorMock: DomainNameValidator;
    let tezosDomainsManagerMock: DomainsManager;
    let tezosDomainsClientServiceMock: TezosDomainsClientService;
    let tldConfigurationService: TldConfigurationService;
    let acquisitionInfo: DomainAcquisitionInfo;
    let appServiceMock: AppService;

    beforeEach(() => {
        routerMock = mock(Router);
        tezosDomainsMock = mock(TaquitoTezosDomainsClient);
        tezosDomainsValidatorMock = mock<DomainNameValidator>();
        tezosDomainsManagerMock = mock<DomainsManager>();
        tezosDomainsClientServiceMock = mock(TezosDomainsClientService);
        tldConfigurationService = mock(TldConfigurationService);
        acquisitionInfo = mock(DomainAcquisitionInfo);
        appServiceMock = mock(AppService);
        const offlineToOnline = new Subject<boolean>();

        when(tezosDomainsClientServiceMock.defaultTld$).thenReturn(of('tez'));
        when(tezosDomainsClientServiceMock.current).thenReturn(of(instance(tezosDomainsMock)));
        when(tezosDomainsClientServiceMock.calculateAcquisitionInfo(anything())).thenReturn(instance(acquisitionInfo));
        when(tezosDomainsMock.validator).thenReturn(instance(tezosDomainsValidatorMock));
        when(tezosDomainsMock.manager).thenReturn(instance(tezosDomainsManagerMock));
        when(tldConfigurationService.get(anything())).thenReturn(of(<any>{}));
        when(appServiceMock.goingOnlineFromOffline).thenReturn(offlineToOnline);

        when(acquisitionInfo.calculatePrice(anyNumber())).thenReturn(1);

        return MockBuilder(ExpiredDomainsTableComponent, DomainsModule)
            .keep(ApolloTestingModule, { export: true })
            .keep(TranslocoModule, { export: true })
            .keep(DataSourceFactory)
            .keep(DomainsListGQL)
            .keep(transloco(), { export: true })
            .provide([
                provider(routerMock),
                provider(tezosDomainsClientServiceMock),
                provider(tldConfigurationService),
                provider(appServiceMock),
                { provide: APOLLO_OPTIONS, useValue: null },
                { provide: Configuration, useValue: TestConfig },
            ])
            .mock(ErrorModule)
            .mock(SharedModule);
    });

    beforeEach(() => {
        initComponent({ showFilter: true, showShadow: true });
    });

    beforeEach(() => {
        mockNow(dayjs('2020-10-01T20:04:42Z'));
    });

    afterEach(() => {
        apollo.verify();
        resetNow();
    });

    describe('when filter not visible', () => {
        beforeEach(() => {
            initComponent({ maxCount: 5, showFilter: false });
        });

        it('should trigger data loading', fakeAsync(() => {
            resolveData({ showFilter: false, acquisitionState: DomainAcquisitionState.CanBeAuctioned, domains: [] });

            tick();
            fixture.detectChanges();

            const listingRows = ngMocks.findInstances(DomainListingComponent);
            expect(listingRows.length).toBe(0);
        }));
    });

    describe('', () => {
        it('should handle auctionable domains', fakeAsync(() => {
            const auctionEnd = dayjs().add(1, 'day');
            const domainData = getDomain('expired.tez');

            resolveData({ showFilter: true, auctionEnd, acquisitionState: DomainAcquisitionState.CanBeAuctioned, domains: [domainData] });

            tick();
            fixture.detectChanges();

            const listingRows = ngMocks.findInstances(DomainListingComponent);
            expect(listingRows.length).toBe(1);

            const listing = listingRows.pop();
            expect(listing?.domainName).toBe('expired.tez');
            expect(listing?.buttonText).toBe('Go to Auction');
            expect(listing?.expiration).toEqual(auctionEnd);
            expect(listing?.buttonDisabled).toBe(false);

            listing?.buttonAction.next();

            verify(routerMock.navigate(['/domain', 'expired.tez']));

            const humanizedDate = ngMocks.findInstances(HumanizedDateComponent).pop();
            expect(humanizedDate?.value).toEqual(domainData.expires);
            expect(humanizedDate?.prefix).toBe('expired');
        }));

        it('should handle domains for registration', fakeAsync(() => {
            const auctionEnd = dayjs().add(1, 'day');
            const domainData = getDomain('expired.tez');

            resolveData({ showFilter: true, auctionEnd, acquisitionState: DomainAcquisitionState.CanBeBought, domains: [domainData] });

            tick();
            fixture.detectChanges();

            const listingRows = ngMocks.findInstances(DomainListingComponent);
            expect(listingRows.length).toBe(1);

            const listing = listingRows.pop();
            expect(listing?.domainName).toBe('expired.tez');
            expect(listing?.buttonText).toBe('Register');
            expect(listing?.expiration).toBeUndefined();
            expect(listing?.buttonDisabled).toBe(false);
        }));

        it('should handle no domains returned', fakeAsync(() => {
            resolveData({ showFilter: true, acquisitionState: DomainAcquisitionState.CanBeAuctioned, domains: [] });

            tick();
            fixture.detectChanges();

            const listingRows = ngMocks.findInstances(DomainListingComponent);
            expect(listingRows.length).toBe(0);
        }));
    });

    function resolveData(data: { showFilter: boolean; acquisitionState: DomainAcquisitionState; domains: DomainDetailQuery['domain'][]; auctionEnd?: Dayjs }) {
        when(acquisitionInfo.acquisitionState).thenReturn(data.acquisitionState);
        if (data.auctionEnd) {
            when(acquisitionInfo.auctionDetails).thenReturn(<DomainAcquisitionAuctionInfo>{ auctionEnd: data.auctionEnd.toDate(), nextMinimumBid: 1.1 });
        }

        if (data.showFilter)
            component.onFilterChange({
                allCategoriesSelected: true,
                sorting: { label: '', value: DomainOrderField.ExpiresAt, direction: 'desc' },
                categories: [],
                domainNameType: DomainNameType.All,
                expirationDate: null,
            });

        const op = apollo.expectOne(DomainsListDocument);

        op.flush({
            data: {
                domains: { edges: data.domains.map(d => ({ node: d })) },
            },
        });
    }

    function initComponent(data: Partial<ExpiredDomainsTableComponent>) {
        ngMocks.flushTestBed();
        fixture = MockRender(ExpiredDomainsTableComponent, data);
        apollo = TestBed.inject(ApolloTestingController);
        component = fixture.point.componentInstance;
    }
});
