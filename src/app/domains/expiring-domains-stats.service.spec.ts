import { discardPeriodicTasks, fakeAsync, TestBed, tick } from '@angular/core/testing';
import { mockNow, provider, resetNow, TestConfig, TestWallet, TezosAddress } from '@td/test';
import { APOLLO_OPTIONS } from 'apollo-angular';
import { ApolloTestingController, ApolloTestingModule } from 'apollo-angular/testing';
import dayjs, { ManipulateType } from 'dayjs';
import { ReplaySubject, Subject } from 'rxjs';
import { mock, when } from 'ts-mockito';
import { AppService } from '../app-service';
import { Configuration } from '../configuration';
import { DataSourceFactory } from '../graphql/data-source-factory';
import { StatsGQL } from '../graphql/graphql.generated';
import { TezosWallet } from '../tezos/models';
import { TezosService } from '../tezos/tezos.service';
import { ExpiringDomainsStatsService } from './expiring-domains-stats.service';

describe('ExpiringDomainsStatsService', () => {
    let service: ExpiringDomainsStatsService;
    let tezosServiceMock: TezosService;
    let apollo: ApolloTestingController;
    let appServiceMock: AppService;
    let wallet: ReplaySubject<TezosWallet | null>;

    beforeEach(() => {
        wallet = new ReplaySubject();
        wallet.next(TestWallet);

        tezosServiceMock = mock(TezosService);
        appServiceMock = mock(AppService);

        when(tezosServiceMock.activeWallet).thenReturn(wallet);
        when(appServiceMock.goingOnlineFromOffline).thenReturn(new Subject());

        TestBed.configureTestingModule({
            providers: [
                ExpiringDomainsStatsService,
                DataSourceFactory,
                StatsGQL,
                provider(appServiceMock),
                provider(tezosServiceMock),
                { provide: APOLLO_OPTIONS, useValue: null },
                { provide: Configuration, useValue: TestConfig },
            ],
            imports: [ApolloTestingModule.withClients(['stats'])],
        });
        mockNow(dayjs('2020-01-01T00:00:00Z'));
    });

    beforeEach(() => {
        apollo = TestBed.inject(ApolloTestingController);
    });

    afterEach(() => {
        apollo.verify();
        resetNow();
    });

    it('should emit when expring later', fakeAsync(() => {
        service = TestBed.inject(ExpiringDomainsStatsService);
        const spy = jasmine.createSpy();

        flushOperation(TestConfig.expiringDomainThreshold, 0);
        flushOperation(TestConfig.expiringDomainEarlyWarningThreshold, 2);

        service.combinedStatsFor$(TezosAddress).subscribe(spy);
        expect(spy).toHaveBeenCalledWith({ hasExpiringLater: true, hasExpiringEarly: false });

        discardPeriodicTasks();
    }));

    it('should emit when expring earlier', fakeAsync(() => {
        service = TestBed.inject(ExpiringDomainsStatsService);
        const spy = jasmine.createSpy();

        flushOperation(TestConfig.expiringDomainThreshold, 2);
        flushOperation(TestConfig.expiringDomainEarlyWarningThreshold, 0);

        service.combinedStatsFor$(TezosAddress).subscribe(spy);
        expect(spy).toHaveBeenCalledWith({ hasExpiringLater: false, hasExpiringEarly: true });

        discardPeriodicTasks();
    }));

    function flushOperation(threshold: { time: number; units: ManipulateType }, expiring: number) {
        const op = apollo.expectOne(x => {
            const targetDate = dayjs().add(threshold.time, threshold.units);
            return x.variables.expiringThreshold.isSame(targetDate, 'day');
        });

        op.flush({
            data: {
                bought: null,
                expiring: { totalCount: expiring },
                total: { totalCount: 1 },
            },
        });
        tick();
    }
});
