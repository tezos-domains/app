import { StorageMap } from '@ngx-pwa/local-storage';
import { mock, instance, when, verify, anyNumber, capture, anyString, anything } from 'ts-mockito';
import { TezosAddress } from '@td/test';
import { of } from 'rxjs';

import { NonceService } from './nonce.service';
import { StorageSchema } from '../utils/storage';

describe('NonceService', () => {
    let storageMapMock: StorageMap;
    let service: NonceService;
    let spy: jasmine.Spy;

    beforeEach(() => {
        storageMapMock = mock(StorageMap);
        spy = jasmine.createSpy();

        when(storageMapMock.get(StorageSchema.nonce('necroskillz', TezosAddress).key, StorageSchema.nonce('necroskillz', TezosAddress).schema)).thenReturn(
            of(42)
        );
        when(storageMapMock.get(StorageSchema.nonce('alice', TezosAddress).key, StorageSchema.nonce('alice', TezosAddress).schema)).thenReturn(of(undefined));
        when(storageMapMock.set(anyString(), anyNumber(), anything())).thenReturn(of(void 0));
        when(storageMapMock.delete(anyString())).thenReturn(of(void 0));

        service = new NonceService(instance(storageMapMock));
    });

    describe('getNonce()', () => {
        it('should generate nonce for parameters and store it', () => {
            service.getNonce('alice', TezosAddress).subscribe(spy);

            verify(storageMapMock.set(StorageSchema.nonce('alice', TezosAddress).key, anyNumber(), StorageSchema.nonce('alice', TezosAddress).schema)).called();
            expect(spy).toHaveBeenCalledWith(capture(storageMapMock.set).last()[1]);
        });

        it('should return stored value', () => {
            service.getNonce('necroskillz', TezosAddress).subscribe(spy);

            verify(storageMapMock.set(anything(), anything(), anything())).never();
            expect(spy).toHaveBeenCalledWith(42);
        });
    });

    describe('clearNonce()', () => {
        it('should delete nonce from storage', () => {
            service.clearNonce('necroskillz', TezosAddress);

            verify(storageMapMock.delete(StorageSchema.nonce('necroskillz', TezosAddress).key)).called();
        });
    });
});
