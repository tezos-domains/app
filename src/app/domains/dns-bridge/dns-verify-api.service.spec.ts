import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { TestConfig } from '@td/test';
import { Configuration } from 'src/app/configuration';
import { DnsVerifyApiService } from './dns-verify-api.service';

describe('DnsVerifyApi', () => {
    let service: DnsVerifyApiService;
    let httpMock: HttpTestingController;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [HttpClientTestingModule],
            providers: [DnsVerifyApiService, { provide: Configuration, useValue: TestConfig }],
        });

        service = TestBed.inject(DnsVerifyApiService);
        httpMock = TestBed.inject(HttpTestingController);
    });

    describe('checkStatus()', () => {
        it('should call api', () => {
            const spy = jasmine.createSpy('checkStatusSubscription');
            service.checkStatus('real-label', 'com', 'tz1xxx').subscribe(spy);

            const request = httpMock.expectOne('https://dns-verify-api/api/verify');
            expect(request.request.method).toBe('POST');
            expect(request.request.body).toEqual({ label: 'real-label', tld: 'com', owner: 'tz1xxx' });

            httpMock.verify();

            const response = { status: 'OK' };
            request.flush(response);

            expect(spy).toHaveBeenCalledWith(response);
        });

        it('should handle api exception', () => {
            const spy = jasmine.createSpy('checkStatusSubscription');
            service.checkStatus('real-label', 'com', 'tz1xxx').subscribe(spy);

            const request = httpMock.expectOne('https://dns-verify-api/api/verify');
            httpMock.verify();

            request.flush({ statusCode: 400, error: 'Bad Request' }, { status: 400, statusText: 'Bad Request' });

            expect(spy).toHaveBeenCalledWith({ status: 'UnknownFailure', error: 'Bad Request' });
        });
    });
});
