import { fakeAsync, flush, tick } from '@angular/core/testing';
import { UntypedFormBuilder, FormGroupDirective } from '@angular/forms';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { TranslocoModule } from '@ngneat/transloco';
import { mockNow, providers, resetNow, TestReverseRecord, TestWallet, TezosAddress, transloco } from '@td/test';
import { RecordMetadata } from '@tezos-domains/core';
import { DomainsManager, TaquitoTezosDomainsOperationFactory } from '@tezos-domains/manager';
import { TLDConfiguration } from '@tezos-domains/manager/dist/src/manager/model';
import { TaquitoTezosDomainsClient } from '@tezos-domains/taquito-client';
import BigNumber from 'bignumber.js';
import dayjs from 'dayjs';
import { MockBuilder, MockedComponentFixture, MockRender, ngMocks } from 'ng-mocks';
import { Observable, of, ReplaySubject, Subject } from 'rxjs';
import { OperationStatusComponent } from 'src/app/shared/operation-status.component';
import { TezosWallet } from 'src/app/tezos/models';
import { SmartContractOperationEvent, TezosService } from 'src/app/tezos/tezos.service';
import { anyFunction, anyOfClass, anyString, anything, deepEqual, instance, mock, verify, when } from 'ts-mockito';
import { TldConfigurationService } from '../../tezos/integration/tld-configuration.service';
import { DomainsModule } from '../domains.module';
import { ReverseRecordService } from '../reverse-record.service';
import { DnsBridgeComponent, DnsBridgeStep } from './dns-bridge.component';
import { DnsVerifyApiService, DomainValidationResponse, ValidationResult } from './dns-verify-api.service';

describe('DnsBridgeComponent', () => {
    let fixture: MockedComponentFixture<DnsBridgeComponent, Partial<DnsBridgeComponent>>;
    let component: DnsBridgeComponent;

    let tezosServiceMock: TezosService;
    let tezosDomainsMock: TaquitoTezosDomainsClient;
    let tezosDomainsManagerMock: DomainsManager;
    let reverseRecordServiceMock: ReverseRecordService;

    let wallet: ReplaySubject<TezosWallet | null>;
    let operation: Observable<SmartContractOperationEvent>;
    let batchOperation: Observable<SmartContractOperationEvent>;
    let stepperNextSpy: jasmine.Spy;
    let operationFactory: TaquitoTezosDomainsOperationFactory;
    let dnsVerifyApiMock: DnsVerifyApiService;
    let tldConfigMock: TldConfigurationService;

    beforeEach(() => {
        tezosServiceMock = mock(TezosService);
        tezosDomainsMock = mock(TaquitoTezosDomainsClient);
        tezosDomainsManagerMock = mock<DomainsManager>();
        reverseRecordServiceMock = mock(ReverseRecordService);
        operationFactory = mock(TaquitoTezosDomainsOperationFactory);
        dnsVerifyApiMock = mock(DnsVerifyApiService);
        tldConfigMock = mock(TldConfigurationService);

        wallet = new ReplaySubject(1);
        operation = new Subject();
        batchOperation = new Subject();
        stepperNextSpy = jasmine.createSpy();

        when(tezosServiceMock.activeWallet).thenReturn(wallet);

        when(tezosDomainsMock.manager).thenReturn(instance(tezosDomainsManagerMock));

        when(operationFactory.claim('signature', anything())).thenResolve('claim' as any);
        when(operationFactory.claimReverseRecord(anything())).thenResolve('claimReverseRecord' as any);
        when(operationFactory.updateRecord(anything())).thenResolve('updateRecord' as any);
        when(tldConfigMock.get(anyString())).thenReturn(of(<TLDConfiguration>{ claimPrice: new BigNumber(1_000_000) }));
        when(tezosServiceMock.execute(anyFunction())).thenCall(fn => {
            fn(instance(tezosDomainsMock));
            return operation;
        });
        when(tezosDomainsManagerMock.batch(anything())).thenCall(fn => {
            fn(instance(operationFactory));
            return batchOperation;
        });
        when(reverseRecordServiceMock.current).thenReturn(of(TestReverseRecord));

        wallet.next(TestWallet);

        mockNow(dayjs('2020-10-01T20:05:00Z'));

        return MockBuilder(DnsBridgeComponent, DomainsModule)
            .keep(TranslocoModule, { export: true })
            .keep(NoopAnimationsModule, { export: true })
            .keep(transloco(), { export: true })
            .provide([providers(tezosServiceMock, reverseRecordServiceMock, dnsVerifyApiMock, tldConfigMock)])
            .provide(UntypedFormBuilder)
            .mock(FormGroupDirective);
    });

    afterEach(() => {
        resetNow();
    });

    function init(validationStatus: ValidationResult = 'OK') {
        when(dnsVerifyApiMock.checkStatus(anyString(), anyString(), anyString())).thenReturn(
            of(<DomainValidationResponse>{ status: validationStatus, signature: 'signature', signedAt: '2022-02-02T02:02:02Z' })
        );

        fixture = MockRender(DnsBridgeComponent, {
            name: 'real-world.com',
        });

        component = fixture.point.componentInstance;
        fixture.detectChanges();

        tick(2);

        component.stepper.next = stepperNextSpy;
    }

    describe('init', () => {
        it('should create form', fakeAsync(() => {
            init();
            expect(component.form.get('address')?.value).toBe(TezosAddress);
            expect(component.form.get('price')?.value).toBe(1);
        }));

        it('should move stepper to correct step', fakeAsync(() => {
            init('FailedDNSSEC');
            expect(component.step).toBe(DnsBridgeStep.DNSSEC);

            init('TXTNoMatch');
            expect(component.step).toBe(DnsBridgeStep.TXTRecord);

            init('OK');
            expect(component.step).toBe(DnsBridgeStep.Claim);
        }));
    });

    describe('claim()', () => {
        it('should disable the form and call claim', fakeAsync(() => {
            when(reverseRecordServiceMock.current).thenReturn(of({ ...TestReverseRecord, domain: undefined }));

            init();

            component.claim();
            fixture.detectChanges();

            const status = ngMocks.find(OperationStatusComponent);
            status.componentInstance.operation!.subscribe();

            tick();
            flush();

            verify(
                operationFactory.claim('signature', deepEqual({ label: 'real-world', tld: 'com', owner: TezosAddress, timestamp: '2022-02-02T02:02:02Z' }))
            ).called();

            verify(
                operationFactory.updateRecord(
                    deepEqual({ data: anyOfClass(RecordMetadata), owner: TezosAddress, address: TezosAddress, name: 'real-world.com' })
                )
            ).called();

            verify(operationFactory.claimReverseRecord(deepEqual({ name: 'real-world.com', owner: TezosAddress }))).called();

            Object.keys(component.form.controls)
                .map(c => component.form.controls[c])
                .forEach(c => expect(c.disabled).toBeTrue());
        }));
    });

    describe('claimDone({ success: true })', () => {
        it('should set phase to DONE and move stepper', fakeAsync(() => {
            init();

            component.claimDone({ success: true });
            tick(300);

            verify(reverseRecordServiceMock.refresh()).called();
            expect(component.stepper?.next).toHaveBeenCalled();
            expect(component.step).toBe(DnsBridgeStep.Done);
        }));
    });

    describe('setNextStep()', () => {
        it('should move to next step', fakeAsync(() => {
            init('NoDNSSEC');

            component.setNextStep();
            tick(300);

            expect(component.stepper?.next).toHaveBeenCalled();
            expect(component.step).toBe(DnsBridgeStep.TXTRecord);
        }));
    });

    describe('done()', () => {
        it('should close trigger event', fakeAsync(() => {
            init();

            const spy = jasmine.createSpy();
            component.finished.subscribe(spy);

            component.done();

            expect(spy).toHaveBeenCalled();
        }));
    });
});
