import { DomainOperator } from '../../graphql/graphql.generated';

export function isOperator(domain: { operators?: DomainOperator[] } | undefined | null, address: string | undefined | null): boolean {
    return (address && domain?.operators?.some(op => op.address === address)) || false;
}
