import { fakeAsync, tick } from '@angular/core/testing';
import { FormBuilder, FormGroupDirective } from '@angular/forms';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { TranslocoModule } from '@ngneat/transloco';
import { mockNow, provider, resetNow, TestConfig, TestPromise, TestReverseRecord, TestWallet, TezosAddress, TezosAddress2, transloco } from '@td/test';
import { RecordMetadata } from '@tezos-domains/core';
import { BuyRequest, CommitmentInfo, CommitmentRequest, DomainsManager, TaquitoTezosDomainsOperationFactory } from '@tezos-domains/manager';
import { TaquitoTezosDomainsClient } from '@tezos-domains/taquito-client';
import dayjs from 'dayjs';
import { MockBuilder, MockedComponentFixture, MockRender, ngMocks } from 'ng-mocks';
import { Observable, of, ReplaySubject, Subject } from 'rxjs';
import { anyFunction, anyOfClass, anyString, anything, deepEqual, instance, mock, verify, when } from 'ts-mockito';
import { Configuration } from '../configuration';
import { NotificationsService, NotificationsSubscription } from '../notifications/notifications.service';
import { OperationStatusComponent } from '../shared/operation-status.component';
import { TezosDomainsClientService } from '../tezos/integration/tezos-domains-client.service';
import { TezosWallet } from '../tezos/models';
import { SmartContractOperationEvent, TezosService } from '../tezos/tezos.service';
import { ErrorPresenterService } from '../utils/error-presenter.service';
import { BuyComponent, BuyFormModel, BuyPhase } from './buy.component';
import { DomainsModule } from './domains.module';
import { NonceService } from './nonce.service';
import { ReverseRecordService } from './reverse-record.service';
import { AffiliateService } from '../affiliates/affiliates.service';
import { AffiliateBuyRequest } from '@tezos-domains/manager/dist/src/manager/model';

describe('BuyComponent', () => {
    let fixture: MockedComponentFixture<BuyComponent, Partial<BuyComponent>>;
    let component: BuyComponent;
    let tezosServiceMock: TezosService;
    let tezosDomainsClientServiceMock: TezosDomainsClientService;
    let tezosDomainsMock: TaquitoTezosDomainsClient;
    let tezosDomainsManagerMock: DomainsManager;
    let reverseRecordServiceMock: ReverseRecordService;
    let nonceServiceMock: NonceService;
    let errorPresenterServiceMock: ErrorPresenterService;
    let wallet: ReplaySubject<TezosWallet | null>;
    let nonce: ReplaySubject<number>;
    let currentSubscription: ReplaySubject<NotificationsSubscription>;
    let promptBlocked$: ReplaySubject<boolean>;
    let getCommitment: TestPromise<CommitmentInfo | null>;
    let operation: Observable<SmartContractOperationEvent>;
    let batchOperation: Observable<SmartContractOperationEvent>;
    let stepperNextSpy: jasmine.Spy;
    let operationFactory: TaquitoTezosDomainsOperationFactory;
    let notificationServiceMock: NotificationsService;

    beforeEach(() => {
        tezosServiceMock = mock(TezosService);
        tezosDomainsClientServiceMock = mock(TezosDomainsClientService);
        tezosDomainsMock = mock(TaquitoTezosDomainsClient);
        tezosDomainsManagerMock = mock<DomainsManager>();
        reverseRecordServiceMock = mock(ReverseRecordService);
        nonceServiceMock = mock(NonceService);
        operationFactory = mock(TaquitoTezosDomainsOperationFactory);
        errorPresenterServiceMock = mock(ErrorPresenterService);
        notificationServiceMock = mock(NotificationsService);
        const affiliateService = mock(AffiliateService);

        wallet = new ReplaySubject(1);
        nonce = new ReplaySubject<number>(1);
        operation = new Subject();
        batchOperation = new Subject();
        getCommitment = new TestPromise();
        stepperNextSpy = jasmine.createSpy();
        currentSubscription = new ReplaySubject(1);
        promptBlocked$ = new ReplaySubject(1);

        when(notificationServiceMock.currentSubscription).thenReturn(currentSubscription);
        when(notificationServiceMock.promptBlocked(anyString())).thenReturn(promptBlocked$);

        when(tezosServiceMock.activeWallet).thenReturn(wallet);

        when(affiliateService.affiliateId$).thenReturn(of(null));

        when(tezosDomainsMock.manager).thenReturn(instance(tezosDomainsManagerMock));
        when(tezosDomainsClientServiceMock.current).thenReturn(of(instance(tezosDomainsMock)));
        when(tezosDomainsManagerMock.commit('tez', anything())).thenResolve('commit' as any);
        when(tezosDomainsManagerMock.buy('tez', anything())).thenResolve('buy' as any);
        when(tezosDomainsManagerMock.getCommitment('tez', anything())).thenCall(() => {
            getCommitment = new TestPromise();
            return getCommitment;
        });
        when(tezosServiceMock.execute(anyFunction())).thenCall(fn => {
            fn(instance(tezosDomainsMock));
            return operation;
        });
        when(tezosDomainsManagerMock.batch(anything())).thenCall(fn => {
            fn(instance(operationFactory));
            return batchOperation;
        });
        when(reverseRecordServiceMock.current).thenReturn(of(TestReverseRecord));
        when(nonceServiceMock.getNonce('necroskillz.tez', TezosAddress)).thenReturn(nonce);
        when(operationFactory.buy('tez', anything())).thenResolve('bbuy' as any);
        when(operationFactory.claimReverseRecord(anything())).thenResolve('bclaim' as any);

        wallet.next(TestWallet);
        nonce.next(42);
        currentSubscription.next({ email: 'e@e.com', status: 'confirmed' });
        promptBlocked$.next(true);

        mockNow(dayjs('2020-10-01T20:05:00Z'));

        return MockBuilder(BuyComponent, DomainsModule)
            .keep(TranslocoModule, { export: true })
            .keep(NoopAnimationsModule, { export: true })
            .keep(transloco(), { export: true })
            .provide([
                provider(tezosServiceMock),
                provider(affiliateService),
                provider(tezosDomainsClientServiceMock),
                provider(reverseRecordServiceMock),
                provider(nonceServiceMock),
                provider(errorPresenterServiceMock),
                provider(notificationServiceMock),
                { provide: Configuration, useValue: TestConfig },
            ])
            .provide(FormBuilder)
            .mock(FormGroupDirective);
    });

    afterEach(() => {
        resetNow();
    });

    function init() {
        const calculatePrice = jasmine.createSpy();
        calculatePrice.and.callFake(d => d * 2);

        fixture = MockRender(BuyComponent, {
            name: 'necroskillz.tez',
            acquisitionInfo: { calculatePrice, buyOrRenewDetails: { pricePerMinDuration: 1e6 } } as any,
        });

        component = fixture.point.componentInstance;

        component.stepper.next = stepperNextSpy;
    }

    describe('init', () => {
        it('should create form', () => {
            init();

            expect(component.buyForm.get('duration')?.value).toBe(1);
            expect(component.buyForm.get('address')?.value).toBe(TezosAddress);
        });

        it('should calculate price', () => {
            init();

            expect(component.buyForm.get('price')?.value).toEqual(730 / 1e6);
        });

        it('should update price when duration changes', () => {
            init();

            component.buyForm.get('duration')?.setValue(2);

            expect(component.buyForm.get('price')?.value).toEqual(1460 / 1e6);
        });

        testCheckForCommitment();
    });

    describe('buy()', () => {
        it('should disable the form and call buy', () => {
            init();

            component.buyForm.get('duration')?.setValue(2);
            component.buyForm.get('address')?.setValue(TezosAddress2);

            component.buy();
            fixture.detectChanges();

            const status = ngMocks.findAll(OperationStatusComponent)[1];
            status.componentInstance.operation!.subscribe();

            verify(tezosDomainsManagerMock.buy('tez', deepEqual(getBuyParams()))).called();
            Object.keys(component.buyForm.controls)
                .map((c: keyof BuyFormModel) => component.buyForm.controls[c])
                .forEach(c => expect(c.disabled).toBeTrue());
        });

        it('should disable the form and call buy and claim reverse record in batch', fakeAsync(() => {
            init();

            component.buyForm.get('duration')?.setValue(2);
            component.buyForm.get('address')?.setValue(TezosAddress2);
            component.buyForm.get('createReverseRecord')?.setValue(true);

            component.buy();
            fixture.detectChanges();

            const status = ngMocks.findAll(OperationStatusComponent)[1];
            status.componentInstance.operation!.subscribe();

            tick();

            verify(operationFactory.buy('tez', deepEqual(getBuyParams()))).called();
            verify(operationFactory.claimReverseRecord(deepEqual({ name: 'necroskillz.tez', owner: TezosAddress }))).called();
            Object.keys(component.buyForm.controls)
                .map((c: keyof BuyFormModel) => component.buyForm.controls[c])
                .forEach(c => expect(c.disabled).toBeTrue());
        }));
    });

    describe('commit()', () => {
        it('should call commit', () => {
            init();

            component.commit();
            fixture.detectChanges();

            const status = ngMocks.findAll(OperationStatusComponent)[0];
            status.componentInstance.operation!.subscribe();

            verify(tezosDomainsManagerMock.commit('tez', deepEqual(getCommitParams()))).called();
        });
    });

    describe('commitDone({ success: true })', () => {
        testCheckForCommitment(() => {
            getCommitment.resolve(null);
            tick();

            component.commitDone({ success: true });
        });
    });

    describe('buyDone({ success: true })', () => {
        it('should set phase to DONE and move stepper', fakeAsync(() => {
            init();

            component.phase = BuyPhase.BUY as any;

            component.buyDone({ success: true }, false);
            tick();

            verify(nonceServiceMock.clearNonce('necroskillz.tez', TezosAddress)).called();
            verify(reverseRecordServiceMock.refresh()).called();
            expect(component.stepper?.next).toHaveBeenCalled();
            expect(component.phase).toBe(BuyPhase.DONE);
        }));
    });

    describe('buyDone({ success: false })', () => {
        it('should hide the operation', () => {
            init();

            component.buy();

            component.buyDone({ success: false }, false);

            expect(component.buyForm.disabled).toBeFalse();
        });
    });

    describe('setNextPhase()', () => {
        it('should set phase to next phase and move stepper', fakeAsync(() => {
            init();

            component.setNextPhase(false);

            tick();

            expect(component.stepper?.next).toHaveBeenCalled();
            expect(component.phase).toBe(BuyPhase.COMMIT);
        }));
    });

    describe('done()', () => {
        it('should close trigger event', () => {
            init();

            const spy = jasmine.createSpy();
            component.bought.subscribe(spy);

            component.done();

            expect(spy).toHaveBeenCalled();
        });
    });

    function getCommitParams(): CommitmentRequest {
        return {
            label: 'necroskillz',
            owner: TezosAddress,
            nonce: 42,
        };
    }

    function getBuyParams(): BuyRequest | AffiliateBuyRequest {
        return {
            ...getCommitParams(),
            duration: component.buyForm.get('duration')!.value * 365,
            address: component.buyForm.get('address')!.value,
            data: anyOfClass(RecordMetadata),
            affiliate: null,
        };
    }

    function testCheckForCommitment(setup?: () => void) {
        it('should check for existing commitment and trigger wait phase if it exists and is valid', fakeAsync(() => {
            init();

            setup && setup();
            verify(tezosDomainsManagerMock.getCommitment('tez', deepEqual(getCommitParams()))).called();

            getCommitment.resolve({
                created: new Date('2020-10-01T20:04:30Z'),
                usableFrom: new Date('2020-10-01T20:05:30Z'),
                usableUntil: new Date('2020-10-02T20:04:30Z'),
            } as any);
            tick();

            expect(component.phase).toBe(BuyPhase.COMMIT);
            expect(component.waitFrom.toISOString()).toBe('2020-10-01T20:04:30.000Z');
            expect(component.waitUntil.toISOString()).toBe('2020-10-01T20:05:30.000Z');

            expect(component.stepperAnimation).toBeFalse();
            tick(1000);
            expect(component.stepperAnimation).toBeTrue();
        }));

        if (setup) {
            it('should check for existing commitment and display error if it doesnt exist after commit done', fakeAsync(() => {
                init();

                setup();
                getCommitment.resolve(null);
                tick(2000);
                getCommitment.resolve(null);
                tick(2000);
                getCommitment.resolve(null);
                tick(2000);
                getCommitment.resolve(null);
                tick(1000);

                verify(errorPresenterServiceMock.nodeErrorToast('commitment-fetch', null)).called();

                expect(component.phase).toBe(BuyPhase.COMMIT);
                expect(component.waitFrom).toBeUndefined();
                expect(component.waitUntil).toBeUndefined();
            }));

            it('should check for existing commitment trigger wait phase if it exists and is valid after retrying', fakeAsync(() => {
                init();

                setup();
                getCommitment.resolve(null);
                tick(2000);
                getCommitment.resolve(null);
                tick(2000);
                getCommitment.resolve({
                    created: new Date('2020-10-01T20:04:30Z'),
                    usableFrom: new Date('2020-10-01T20:05:30Z'),
                    usableUntil: new Date('2020-10-02T20:04:30Z'),
                } as any);
                tick(1000);

                verify(errorPresenterServiceMock.nodeErrorToast(anything(), anything())).never();

                expect(component.phase).toBe(BuyPhase.COMMIT);
                expect(component.waitFrom.toISOString()).toBe('2020-10-01T20:04:30.000Z');
                expect(component.waitUntil.toISOString()).toBe('2020-10-01T20:05:30.000Z');
            }));
        } else {
            it('should check for existing commitment and dont do anything if it doesnt exist on startup', fakeAsync(() => {
                init();

                getCommitment.resolve(null);
                tick(1000);

                expect(component.phase).toBe(BuyPhase.COMMIT);
                expect(component.waitFrom).toBeUndefined();
                expect(component.waitUntil).toBeUndefined();
            }));
        }

        it('should check for existing commitment and clear nonce if it is expired', fakeAsync(() => {
            init();

            setup && setup();
            getCommitment.resolve({
                created: new Date('2020-09-01T20:04:30Z'),
                usableFrom: new Date('2020-09-01T20:05:30Z'),
                usableUntil: new Date('2020-09-02T20:04:30Z'),
            } as any);
            tick(1000);

            expect(component.phase).toBe(BuyPhase.COMMIT);
            verify(nonceServiceMock.clearNonce('necroskillz.tez', TezosAddress)).called();
            expect(component.waitFrom).toBeUndefined();
            expect(component.waitUntil).toBeUndefined();
        }));
    }
});
