import { fakeAsync, TestBed, tick } from '@angular/core/testing';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslocoModule } from '@ngneat/transloco';
import { getDomain, mockNow, provider, resetNow, TestConfig, TestReverseRecord, TestWallet, TestWallet3, transloco } from '@td/test';
import { DomainNameValidationResult, DomainNameValidator } from '@tezos-domains/core';
import { DomainAcquisitionState, DomainsManager } from '@tezos-domains/manager';
import { TaquitoTezosDomainsClient } from '@tezos-domains/taquito-client';
import { APOLLO_OPTIONS } from 'apollo-angular';
import { ApolloTestingController, ApolloTestingModule } from 'apollo-angular/testing';
import dayjs from 'dayjs';
import { GraphQLError } from 'graphql';
import { DefaultRenderComponent, MockBuilder, MockedComponentFixture, MockRender, ngMocks } from 'ng-mocks';
import { of, ReplaySubject, Subject } from 'rxjs';
import { anyFunction, anyString, anything, deepEqual, instance, mock, verify, when } from 'ts-mockito';
import { AppService } from '../app-service';
import { PageService } from '../browser/page.service';
import { WindowRef } from '../browser/window-ref';
import { Configuration } from '../configuration';
import { ErrorModule } from '../error/error.module';
import { DataSourceFactory } from '../graphql/data-source-factory';
import { CurrentBuyOfferDocument, CurrentBuyOfferGQL, DomainDetailDocument, DomainDetailGQL, RecordValidity } from '../graphql/graphql.generated';
import { SubscribeButtonComponent } from '../notifications/subscribe-button.component';
import { ReverseRecordFormComponent } from '../reverse-records/reverse-record-form.component';
import { MediaObserver } from '../shared/media-observer.service';
import { SharedModule } from '../shared/shared.module';
import { TezosDomainsClientService } from '../tezos/integration/tezos-domains-client.service';
import { TezosWallet } from '../tezos/models';
import { TezosService } from '../tezos/tezos.service';
import { DomainDetailComponent } from './domain-detail.component';
import { DomainRecordFormComponent } from './domain-record-form.component';
import { DomainService } from './domain.service';
import { DomainsModule } from './domains.module';
import { RenewComponent } from './renew.component';
import { ReverseRecord, ReverseRecordService } from './reverse-record.service';

describe('DomainDetailComponent', () => {
    let fixture: MockedComponentFixture<DomainDetailComponent, DefaultRenderComponent<DomainDetailComponent>>;
    let component: DomainDetailComponent;
    let activateRouteMock: ActivatedRoute;
    let dialogMock: MatDialog;
    let tezosServiceMock: TezosService;
    let reverseRecordServiceMock: ReverseRecordService;
    let tezosDomainsMock: TaquitoTezosDomainsClient;
    let tezosDomainsValidatorMock: DomainNameValidator;
    let tezosDomainsManagerMock: DomainsManager;
    let tezosDomainsClientServiceMock: TezosDomainsClientService;
    let mediaMock: MediaObserver;
    let windowRefMock: WindowRef;
    let appServiceMock: AppService;
    let pageServiceMock: PageService;
    let modalMock: MatDialogRef<any>;
    let routeParamsMock: Subject<{ [key: string]: string }>;
    let routeQueryParamsMock: Subject<{ [key: string]: string }>;
    let routerMock: Router;
    let wallet: ReplaySubject<TezosWallet | null>;
    let reverseRecord: ReplaySubject<ReverseRecord | null>;
    let dialogClose: Subject<boolean>;
    let offlineToOnline: Subject<boolean>;
    let apollo: ApolloTestingController;

    beforeEach(() => {
        activateRouteMock = mock(ActivatedRoute);
        dialogMock = mock(MatDialog);
        tezosServiceMock = mock(TezosService);
        reverseRecordServiceMock = mock(ReverseRecordService);
        modalMock = mock(MatDialogRef);
        appServiceMock = mock(AppService);
        pageServiceMock = mock(PageService);
        tezosDomainsMock = mock(TaquitoTezosDomainsClient);
        mediaMock = mock(MediaObserver);
        tezosDomainsValidatorMock = mock<DomainNameValidator>();
        tezosDomainsManagerMock = mock<DomainsManager>();
        tezosDomainsClientServiceMock = mock(TezosDomainsClientService);
        routerMock = mock(Router);
        windowRefMock = mock(WindowRef);
        routeParamsMock = new Subject();
        routeQueryParamsMock = new Subject();
        wallet = new ReplaySubject(1);
        reverseRecord = new ReplaySubject(1);
        dialogClose = new Subject();
        offlineToOnline = new Subject();

        when(activateRouteMock.params).thenReturn(routeParamsMock);
        when(activateRouteMock.queryParams).thenReturn(routeQueryParamsMock);
        when(activateRouteMock.snapshot).thenReturn({ queryParams: {} } as any);
        when(windowRefMock.nativeWindow).thenReturn({ location: { href: 'https://url' } } as any);
        when(dialogMock.open(anything(), anything())).thenReturn(instance(modalMock));
        when(modalMock.afterClosed()).thenReturn(dialogClose);
        when(tezosServiceMock.activeWallet).thenReturn(wallet);
        when(reverseRecordServiceMock.current).thenReturn(reverseRecord);
        when(appServiceMock.goingOnlineFromOffline).thenReturn(offlineToOnline);
        when(tezosDomainsClientServiceMock.current).thenReturn(of(instance(tezosDomainsMock)));
        when(tezosDomainsMock.validator).thenReturn(instance(tezosDomainsValidatorMock));
        when(tezosDomainsMock.manager).thenReturn(instance(tezosDomainsManagerMock));
        when(tezosDomainsValidatorMock.supportedTLDs).thenReturn(['tez']);
        when(tezosDomainsValidatorMock.isValidWithKnownTld(anyString())).thenCall(name => {
            if (['💩.tez'].includes(name) || !name.endsWith('tez')) {
                return DomainNameValidationResult.UNSUPPORTED_CHARACTERS;
            }

            return DomainNameValidationResult.VALID;
        });
        when(tezosDomainsClientServiceMock.calculateAcquisitionInfo(anything())).thenReturn({
            acquisitionState: DomainAcquisitionState.Taken,
            calculatePrice: (d: number) => d,
        } as any);
        when(tezosDomainsManagerMock.getTldConfiguration('tez')).thenResolve({} as any);

        wallet.next(TestWallet);

        return MockBuilder(DomainDetailComponent, DomainsModule)
            .keep(ApolloTestingModule, { export: true })
            .keep(TranslocoModule, { export: true })
            .keep(DataSourceFactory)
            .keep(DomainDetailGQL)
            .keep(CurrentBuyOfferGQL)
            .keep(DomainService)
            .keep(transloco(), { export: true })
            .provide([
                provider(activateRouteMock),
                provider(dialogMock),
                provider(tezosServiceMock),
                provider(reverseRecordServiceMock),
                provider(appServiceMock),
                provider(pageServiceMock),
                provider(tezosDomainsClientServiceMock),
                provider(windowRefMock),
                provider(routerMock),
                provider(mediaMock),
                { provide: APOLLO_OPTIONS, useValue: null },
                { provide: Configuration, useValue: TestConfig },
            ])
            .mock(ErrorModule)
            .mock(SharedModule);
    });

    beforeEach(() => {
        fixture = MockRender(DomainDetailComponent);

        apollo = TestBed.inject(ApolloTestingController);

        component = fixture.point.componentInstance;
    });

    beforeEach(() => {
        mockNow(dayjs('2020-10-01T20:04:42Z'));
        // apollo.expectOne(CurrentBuyOfferDocument);
    });

    afterEach(() => {
        apollo.verify();
        resetNow();
    });

    it('should get wallet', () => {
        navigate('necroskillz.tez');
        apollo.expectOne(CurrentBuyOfferDocument);
        apollo.expectOne(DomainDetailDocument);

        expect(component.wallet).toBe(TestWallet);
    });

    it('should load domain data', fakeAsync(() => {
        navigate('necroskillz.tez');
        apollo.expectOne(CurrentBuyOfferDocument);

        const op = apollo.expectOne(DomainDetailDocument);

        expect(op.operation.variables).toEqual({ name: 'necroskillz.tez', domainValidity: RecordValidity.All });

        op.flush({
            data: {
                domain: getDomain('necroskillz.tez'),
                lastAuction: { edges: [] },
            },
        });
        tick();

        reverseRecord.next(TestReverseRecord);

        expect(component.walletReverseRecord).toEqual(TestReverseRecord);
        expect(component.pricePerYear).toBe(365);
        expect(component.domain).toEqual(getDomain('necroskillz.tez'));
        expect(component.subdomainsFilter).toEqual({ ancestors: { include: 'necroskillz.tez' } });
        expect(component.subdomainLevel).toBe(3);
        expect(component.errorMessage).toBeNull();
    }));

    describe('SubscribeButton', () => {
        it('should be visible', fakeAsync(() => {
            navigate('never-expires.tez');
            apollo.expectOne(CurrentBuyOfferDocument);

            const op = apollo.expectOne(DomainDetailDocument);

            op.flush({
                data: {
                    domain: getDomain('necroskillz.tez'),
                    lastAuction: { edges: [] },
                },
            });
            tick();
            fixture.detectChanges();

            const subscribeButton = ngMocks.findInstance(SubscribeButtonComponent);
            expect(subscribeButton).toBeTruthy();
        }));

        it('should be hidden when user is operator', fakeAsync(() => {
            wallet.next(TestWallet3);
            navigate('with-operators.tez');
            apollo.expectOne(CurrentBuyOfferDocument);

            const op = apollo.expectOne(DomainDetailDocument);

            op.flush({
                data: {
                    domain: getDomain('with-operators.tez'),
                    lastAuction: { edges: [] },
                },
            });
            tick();
            fixture.detectChanges();

            const subscribeButton = ngMocks.findInstances(SubscribeButtonComponent).pop();
            expect(subscribeButton).toBeFalsy();
        }));
    });

    it('should not load parent for tld', fakeAsync(() => {
        navigate('tez');
        apollo.expectOne(CurrentBuyOfferDocument);

        const op = apollo.expectOne(DomainDetailDocument);

        expect(op.operation.variables).toEqual({ name: 'tez', domainValidity: RecordValidity.All });

        op.flush({
            data: {
                domain: getDomain('tez'),
                lastAuction: { edges: [] },
            },
        });
        tick();

        reverseRecord.next(TestReverseRecord);

        expect(component.domain).toEqual(getDomain('tez'));
        expect(component.subdomainsFilter).toEqual({ ancestors: { include: 'tez' } });
        expect(component.subdomainLevel).toBe(2);
        expect(component.errorMessage).toBeNull();
    }));

    it('should set error message if api returns error', fakeAsync(() => {
        navigate('necroskillz.tez');
        apollo.expectOne(CurrentBuyOfferDocument);

        apollo.expectOne(DomainDetailDocument).graphqlErrors([new GraphQLError('gql error'), new GraphQLError('gql error2')]);
        tick();

        reverseRecord.next(TestReverseRecord);

        expect(component.walletReverseRecord).toEqual(TestReverseRecord);
        expect(component.domain).toBeNull();
        expect(component.errorMessage).not.toBeNull();
        expect(component.errorTitleKey).toBe('general.error');
    }));

    it('should reload data after returning back online', fakeAsync(() => {
        navigate('necroskillz.tez');
        apollo.expectOne(CurrentBuyOfferDocument);

        apollo.expectOne(DomainDetailDocument).graphqlErrors([new GraphQLError('gql error'), new GraphQLError('gql error2')]);
        tick();

        when(activateRouteMock.snapshot).thenReturn({ params: { name: 'necroskillz.tez' } } as any);
        offlineToOnline.next(true);

        const op = apollo.expectOne(DomainDetailDocument);

        expect(op.operation.variables).toEqual({ name: 'necroskillz.tez', domainValidity: RecordValidity.All });

        op.flush({
            data: {
                domain: getDomain('necroskillz.tez'),
                lastAuction: { edges: [] },
            },
        });
        tick();

        expect(component.domain).toEqual(getDomain('necroskillz.tez'));
        expect(component.errorMessage).toBeNull();
    }));

    it('should load domain data as url changes', fakeAsync(() => {
        navigate('necroskillz.tez');
        apollo.expectOne(CurrentBuyOfferDocument);

        apollo.expectOne(DomainDetailDocument).flush({
            data: {
                domain: getDomain('necroskillz.tez'),
                lastAuction: { edges: [] },
            },
        });
        tick();

        expect(component.domain).toEqual(getDomain('necroskillz.tez'));
        expect(component.errorMessage).toBeNull();
        verify(pageServiceMock.setTitle('domain', deepEqual({ name: 'necroskillz.tez' }))).called();

        navigate('💩.tez');

        apollo.expectNone(DomainDetailDocument);

        expect(component.domain).toBeNull();
        expect(component.errorMessage).not.toBeNull();
        expect(component.errorTitleKey).toBe('domain-detail.title-invalid-domain');
        verify(pageServiceMock.setTitle('domain', deepEqual({ name: '💩.tez' }))).called();

        navigate('happytezos.tez');
        apollo.expectOne(CurrentBuyOfferDocument);

        const op = apollo.expectOne(DomainDetailDocument);
        expect(op.operation.variables).toEqual({ name: 'happytezos.tez', domainValidity: RecordValidity.All });
        op.flush({
            data: {
                domain: getDomain('happytezos.tez'),
                lastAuction: { edges: [] },
            },
        });
        tick();

        expect(component.domain).toEqual(getDomain('happytezos.tez'));
        expect(component.errorMessage).toBeNull();
        verify(pageServiceMock.setTitle('domain', deepEqual({ name: 'happytezos.tez' }))).called();
    }));

    it('setup unowned domain for non existent 2ld', fakeAsync(() => {
        navigate('necroskillz.tez');
        apollo.expectOne(CurrentBuyOfferDocument);

        apollo.expectOne(DomainDetailDocument).flush({
            data: {
                domain: null,
                lastAuction: { edges: [] },
            },
        });
        tick();

        expect(component.domain).toEqual({ name: 'necroskillz.tez' });
        expect(component.errorMessage).toBeNull();
    }));

    it('setup unowned domain for non expired 2ld', fakeAsync(() => {
        navigate('expired.tez');
        apollo.expectOne(CurrentBuyOfferDocument);

        apollo.expectOne(DomainDetailDocument).flush({
            data: {
                domain: getDomain('expired.tez'),
                lastAuction: { edges: [] },
            },
        });
        tick();

        expect(component.domain).toEqual({ name: 'expired.tez' });
        expect(component.errorMessage).toBeNull();
    }));

    it('show error for invalid domain name', () => {
        navigate('💩.tez');

        apollo.expectNone(DomainDetailDocument);

        reverseRecord.next(TestReverseRecord);

        expect(component.walletReverseRecord).toEqual(TestReverseRecord);
        expect(component.domain).toBeNull();
        expect(component.errorMessage).not.toBeNull();
    });

    it('show error non existent subdomain', fakeAsync(() => {
        navigate('play.necroskillz.tez');
        apollo.expectOne(CurrentBuyOfferDocument);

        apollo.expectOne(DomainDetailDocument).flush({
            data: {
                domain: null,
                lastAuction: { edges: [] },
            },
        });
        tick();

        expect(component.errorMessage).not.toBeNull();
    }));

    it('should stop loading domains when destroyed', () => {
        fixture.destroy();

        navigate('necroskillz.tez');

        apollo.expectNone(DomainDetailDocument);

        expect(component.domain).toBeUndefined();
    });

    describe('canEdit', () => {
        it('should be false when user has not connected with wallet', () => {
            wallet.next(null);

            expect(component.canEdit).toBeFalse();
        });

        it('should be false when domain and parent is not loaded', () => {
            expect(component.canEdit).toBeFalse();
        });

        it('should be false when user is not owner of the domain or its parent', fakeAsync(() => {
            navigate('ble.happytezos.tez');
            apollo.expectOne(CurrentBuyOfferDocument);

            apollo.expectOne(DomainDetailDocument).flush({
                data: {
                    domain: getDomain('ble.happytezos.tez'),
                    lastAuction: { edges: [] },
                },
            });
            tick();

            expect(component.domain).toBeDefined();
            expect(component.canEdit).toBeFalse();
        }));

        it('should be true when user is the owner of the domain', fakeAsync(() => {
            navigate('necroskillz.tez');
            apollo.expectOne(CurrentBuyOfferDocument);

            apollo.expectOne(DomainDetailDocument).flush({
                data: {
                    domain: getDomain('necroskillz.tez'),
                    lastAuction: { edges: [] },
                },
            });
            tick();

            expect(component.canEdit).toBeTrue();
        }));

        it('should be true when user is the owner of the parent', fakeAsync(() => {
            navigate('test.necroskillz.tez');
            apollo.expectOne(CurrentBuyOfferDocument);

            apollo.expectOne(DomainDetailDocument).flush({
                data: {
                    domain: getDomain('test.necroskillz.tez'),
                    lastAuction: { edges: [] },
                },
            });
            tick();

            expect(component.canEdit).toBeTrue();
        }));
    });

    describe('granular permissions', () => {
        describe('canChangeOperators, canSellDomain', () => {
            it('should be true when domain has tokenId', fakeAsync(() => {
                navigateToDomain('necroskillz.tez', 8384);

                tick();

                expect(component.canChangeOperators).toBeTrue();
                expect(component.canSellDomain).toBeTrue();
            }));

            it('should be false when domain doesn`t have a tokenId', fakeAsync(() => {
                navigateToDomain('necroskillz.tez');

                tick();

                expect(component.canChangeOperators).toBeFalse();
                expect(component.canSellDomain).toBeFalse();
            }));
        });

        function navigateToDomain(domain: string, tokenId?: number): void {
            navigate(domain);
            apollo.expectOne(CurrentBuyOfferDocument);

            apollo.expectOne(DomainDetailDocument).flush({
                data: {
                    domain: { ...getDomain(domain), tokenId },
                    lastAuction: { edges: [] },
                },
            });
        }
    });

    describe('canOperate', () => {
        it('should be false when user has not connected with wallet', () => {
            wallet.next(null);

            expect(component.canOperate).toBeFalse();
        });

        it('should be false when domain and parent is not loaded', () => {
            expect(component.canOperate).toBeFalse();
        });

        it('should be false when user is not owner of the domain', fakeAsync(() => {
            navigate('ble.happytezos.tez');
            apollo.expectOne(CurrentBuyOfferDocument);

            apollo.expectOne(DomainDetailDocument).flush({
                data: {
                    domain: getDomain('ble.happytezos.tez'),
                    lastAuction: { edges: [] },
                },
            });
            tick();

            expect(component.domain).toBeDefined();
            expect(component.canOperate).toBeFalse();
        }));

        it('should be true when user is the owner of the domain', fakeAsync(() => {
            navigate('necroskillz.tez');
            apollo.expectOne(CurrentBuyOfferDocument);

            apollo.expectOne(DomainDetailDocument).flush({
                data: {
                    domain: getDomain('necroskillz.tez'),
                    lastAuction: { edges: [] },
                },
            });
            tick();

            expect(component.canOperate).toBeTrue();
        }));

        it('should be false when user is the owner of the parent but not of the domain', fakeAsync(() => {
            navigate('play.necroskillz.tez');
            apollo.expectOne(CurrentBuyOfferDocument);

            apollo.expectOne(DomainDetailDocument).flush({
                data: {
                    domain: getDomain('play.necroskillz.tez'),
                    lastAuction: { edges: [] },
                },
            });
            tick();

            expect(component.domain).toBeDefined();
            expect(component.canOperate).toBeFalse();
        }));
    });

    describe('showDetails', () => {
        it('should be true for 3rd level domain', fakeAsync(() => {
            navigate('test.necroskillz.tez');
            apollo.expectOne(CurrentBuyOfferDocument);

            apollo.expectOne(DomainDetailDocument).flush({
                data: {
                    domain: getDomain('test.necroskillz.tez'),
                    lastAuction: { edges: [] },
                },
            });
            tick();

            expect(component.showDetails).toBeTrue();
        }));

        it('should be true for 2nd level domain that is Taken', fakeAsync(() => {
            navigate('necroskillz.tez');
            apollo.expectOne(CurrentBuyOfferDocument);

            apollo.expectOne(DomainDetailDocument).flush({
                data: {
                    domain: getDomain('necroskillz.tez'),
                    lastAuction: { edges: [] },
                },
            });
            tick();

            expect(component.showDetails).toBeTrue();
        }));

        it('should be false for 2nd level domain not Taken', fakeAsync(() => {
            when(tezosDomainsClientServiceMock.calculateAcquisitionInfo(anything())).thenReturn({
                acquisitionState: DomainAcquisitionState.CanBeBought,
                calculatePrice: (d: number) => d,
            } as any);

            navigate('necroskillz.tez');
            apollo.expectOne(CurrentBuyOfferDocument);

            apollo.expectOne(DomainDetailDocument).flush({
                data: {
                    domain: getDomain('necroskillz.tez'),
                    lastAuction: { edges: [] },
                },
            });
            tick();

            expect(component.showDetails).toBeFalse();
        }));
    });

    describe('hasActiveWalletAddress', () => {
        it('should be false when user has not connected with wallet', () => {
            wallet.next(null);

            expect(component.hasActiveWalletAddress).toBeFalse();
        });

        it('should be false when domain is not loaded', () => {
            expect(component.hasActiveWalletAddress).toBeFalse();
        });

        it('should be true when domain address matches wallet address', fakeAsync(() => {
            navigate('necroskillz.tez');
            apollo.expectOne(CurrentBuyOfferDocument);

            apollo.expectOne(DomainDetailDocument).flush({
                data: {
                    domain: getDomain('necroskillz.tez'),
                    lastAuction: { edges: [] },
                },
            });
            tick();

            expect(component.hasActiveWalletAddress).toBeTrue();
        }));

        it('should be false when domain address does not matches wallet address', fakeAsync(() => {
            navigate('happytezos.tez');
            apollo.expectOne(CurrentBuyOfferDocument);

            apollo.expectOne(DomainDetailDocument).flush({
                data: {
                    domain: getDomain('happytezos.tez'),
                    lastAuction: { edges: [] },
                },
            });
            tick();

            expect(component.hasActiveWalletAddress).toBeFalse();
        }));
    });

    describe('canSetupReverseRecord', () => {
        it('should be false reverse record is not loaded', () => {
            expect(component.canSetupReverseRecord).toBeFalse();
        });

        it('should be true when domain address matches wallet address and reverse record is not already set', fakeAsync(() => {
            navigate('mike.tez');
            apollo.expectOne(CurrentBuyOfferDocument);

            apollo.expectOne(DomainDetailDocument).flush({
                data: {
                    domain: getDomain('mike.tez'),
                    lastAuction: { edges: [] },
                },
            });
            tick();

            expect(component.canSetupReverseRecord).toBeTrue();
        }));

        it('should be false when domain address matches wallet address and reverse record already set to this domain', fakeAsync(() => {
            navigate('necroskillz.tez');
            apollo.expectOne(CurrentBuyOfferDocument);

            reverseRecord.next(TestReverseRecord);
            apollo.expectOne(DomainDetailDocument).flush({
                data: {
                    domain: getDomain('necroskillz.tez'),
                    lastAuction: { edges: [] },
                },
            });
            tick();

            expect(component.canSetupReverseRecord).toBeFalse();
        }));
    });

    describe('isCurrentReverseRecord', () => {
        it('should be false reverse record is not loaded', () => {
            expect(component.isCurrentReverseRecord).toBeFalse();
        });

        it('should false when reverse record is not set', fakeAsync(() => {
            navigate('mike.tez');
            apollo.expectOne(CurrentBuyOfferDocument);

            apollo.expectOne(DomainDetailDocument).flush({
                data: {
                    domain: getDomain('mike.tez'),
                    lastAuction: { edges: [] },
                },
            });
            tick();

            expect(component.isCurrentReverseRecord).toBeFalse();
        }));

        it('should be true when domain name matches reverse record name', fakeAsync(() => {
            navigate('necroskillz.tez');
            apollo.expectOne(CurrentBuyOfferDocument);

            apollo.expectOne(DomainDetailDocument).flush({
                data: {
                    domain: getDomain('necroskillz.tez'),
                    lastAuction: { edges: [] },
                },
            });
            tick();

            expect(component.isCurrentReverseRecord).toBeTrue();
        }));

        it('should be false when domain name does not match reverse record name', fakeAsync(() => {
            navigate('martin.tez');

            reverseRecord.next(TestReverseRecord);
            apollo.expectOne(CurrentBuyOfferDocument);
            apollo.expectOne(DomainDetailDocument).flush({
                data: {
                    domain: getDomain('martin.tez'),
                    lastAuction: { edges: [] },
                },
            });
            tick();

            expect(component.isCurrentReverseRecord).toBeFalse();
        }));
    });

    describe('location', () => {
        it('should return current url', () => {
            expect(component.location).toBe('https://url');
        });
    });

    describe('operations', () => {
        let subdomainReloadSpy: jasmine.Spy;

        function init(withRender = false) {
            navigate('necroskillz.tez');
            apollo.expectOne(CurrentBuyOfferDocument);

            apollo.expectOne(DomainDetailDocument).flush({
                data: {
                    domain: getDomain('necroskillz.tez'),
                    lastAuction: { edges: [] },
                },
            });
            tick();

            if (withRender) {
                fixture.detectChanges();

                subdomainReloadSpy = spyOn(component.subdomainsTable, 'reload');
            }
        }

        describe('edit()', () => {
            it('should show dialog', fakeAsync(() => {
                init();
                component.edit();

                verify(
                    dialogMock.open(
                        DomainRecordFormComponent,
                        deepEqual({
                            data: {
                                domain: getDomain('necroskillz.tez'),
                                parent: 'tez',
                            },
                            panelClass: 'big-dialog-pane',
                            height: anyString(),
                        })
                    )
                ).called();
            }));

            it('should reload data after its saved', fakeAsync(() => {
                init();
                component.edit();

                dialogClose.next(true);

                verifyDataReloaded();
            }));

            it('should not reload data after its canceled', fakeAsync(() => {
                init();
                component.edit();

                dialogClose.next(false);

                verifyDataNotReloaded();
            }));
        });

        describe('addSubdomain()', () => {
            it('should show dialog', fakeAsync(() => {
                init();
                component.addSubdomain();

                verify(
                    dialogMock.open(
                        DomainRecordFormComponent,
                        deepEqual({
                            data: { parent: 'necroskillz.tez', domain: {} },
                            panelClass: 'big-dialog-pane',
                            height: anyString(),
                        })
                    )
                ).called();
            }));

            it('should reload data after its saved', fakeAsync(() => {
                init(true);
                component.addSubdomain();

                dialogClose.next(true);

                expect(subdomainReloadSpy).toHaveBeenCalled();
                verifyDataReloaded();
            }));

            it('should not reload data after its canceled', fakeAsync(() => {
                init(true);
                component.addSubdomain();

                dialogClose.next(false);

                expect(subdomainReloadSpy).not.toHaveBeenCalled();
                verifyDataNotReloaded();
            }));
        });

        describe('setReverseRecord()', () => {
            it('should show dialog', fakeAsync(() => {
                init();
                component.setReverseRecord();

                verify(
                    dialogMock.open(
                        ReverseRecordFormComponent,
                        deepEqual({
                            data: { reverseRecord: TestReverseRecord, name: 'necroskillz.tez' },
                            panelClass: 'big-dialog-pane',
                            height: anyString(),
                        })
                    )
                ).called();
            }));

            it('should reload reverse record after its saved', fakeAsync(() => {
                init();
                component.setReverseRecord();

                dialogClose.next(true);

                verifyDataReloaded();
                verify(reverseRecordServiceMock.refresh()).called();
            }));

            it('should not reload reverse record after its canceled', fakeAsync(() => {
                init();
                component.setReverseRecord();

                dialogClose.next(false);

                verifyDataNotReloaded();
                verify(reverseRecordServiceMock.refresh()).never();
            }));
        });

        describe('renew()', () => {
            it('should show dialog', fakeAsync(() => {
                init();
                component.renew();

                verify(
                    dialogMock.open(
                        RenewComponent,
                        deepEqual({
                            data: {
                                domain: getDomain('necroskillz.tez'),
                                acquisitionInfo: { acquisitionState: DomainAcquisitionState.Taken, calculatePrice: anyFunction() },
                            },
                            panelClass: 'big-dialog-pane',
                        })
                    )
                ).called();
            }));

            it('should reload data after its saved', fakeAsync(() => {
                init();
                component.renew();

                dialogClose.next(true);

                verifyDataReloaded();
            }));

            it('should not reload data after its canceled', fakeAsync(() => {
                init();
                component.renew();

                dialogClose.next(false);

                verifyDataNotReloaded();
            }));
        });

        function verifyDataReloaded() {
            apollo.expectOne(DomainDetailDocument).flush({
                data: {
                    domain: getDomain('martin.tez'),
                    lastAuction: { edges: [] },
                },
            });
            tick();

            expect(component.domain).toEqual(getDomain('martin.tez'));
        }

        function verifyDataNotReloaded() {
            apollo.expectNone(DomainDetailDocument);

            expect(component.domain).toEqual(getDomain('necroskillz.tez'));
        }
    });

    function navigate(name: string) {
        when(activateRouteMock.snapshot).thenReturn({ params: { name }, queryParams: {} } as any);
        routeParamsMock.next({ name });
    }
});
