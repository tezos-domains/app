import { TestBed, fakeAsync, tick } from '@angular/core/testing';
import { ApolloTestingController, ApolloTestingModule } from 'apollo-angular/testing';
import { transloco, TezosAddress, getDomain, TezosAddress2 } from '@td/test';

import { DomainService } from './domain.service';
import { DomainExistsDocument } from '../graphql/graphql.generated';

describe('DomainService', () => {
    let service: DomainService;
    let apollo: ApolloTestingController;
    let spy: jasmine.Spy;

    beforeEach(() => {
        spy = jasmine.createSpy();

        TestBed.configureTestingModule({
            imports: [ApolloTestingModule, transloco()],
        });

        service = TestBed.inject(DomainService);

        apollo = TestBed.inject(ApolloTestingController);
    });

    afterEach(() => {
        apollo.verify();
    });

    describe('isDomainAvailable()', () => {
        it('should check if domain already exists and return true if it doesnt', fakeAsync(() => {
            service.isDomainAvailable('necroskillz.tez').subscribe(spy);

            const op = apollo.expectOne(DomainExistsDocument);

            expect(op.operation.variables.name).toBe('necroskillz.tez');

            op.flushData({ domain: null });
            tick();

            expect(spy).toHaveBeenCalledWith(true);
        }));

        it('should check if domain already exists and return false if it does', fakeAsync(() => {
            service.isDomainAvailable('necroskillz.tez').subscribe(spy);

            const op = apollo.expectOne(DomainExistsDocument);

            expect(op.operation.variables.name).toBe('necroskillz.tez');

            op.flushData({ domain: {} });
            tick();

            expect(spy).toHaveBeenCalledWith(false);
        }));
    });

    describe('hasAddress()', () => {
        it('should check if domain exists and return true if its address matches the specified one', fakeAsync(() => {
            service.hasAddress('necroskillz.tez', TezosAddress).subscribe(spy);

            const op = apollo.expectOne(DomainExistsDocument);

            expect(op.operation.variables.name).toBe('necroskillz.tez');

            op.flushData({ domain: getDomain('necroskillz.tez') });
            tick();

            expect(spy).toHaveBeenCalledWith(true);
        }));

        it('should check if domain exists and return false if its address does not match the specified one', fakeAsync(() => {
            service.hasAddress('necroskillz.tez', TezosAddress2).subscribe(spy);

            apollo.expectOne(DomainExistsDocument).flushData({ domain: getDomain('necroskillz.tez') });
            tick();

            expect(spy).toHaveBeenCalledWith(false);
        }));

        it('should return false if domain does not exist', fakeAsync(() => {
            service.hasAddress('necroskillz.tez', TezosAddress).subscribe(spy);

            apollo.expectOne(DomainExistsDocument).flushData({ domain: null });
            tick();

            expect(spy).toHaveBeenCalledWith(false);
        }));
    });
});
