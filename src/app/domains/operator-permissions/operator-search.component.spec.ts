import { fakeAsync, tick } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { TranslocoModule } from '@ngneat/transloco';
import { TezosAddress, transloco } from '@td/test';
import { MockBuilder, MockedComponentFixture, MockRender, ngMocks } from 'ng-mocks';
import { RecipientComponent } from 'src/app/shared/recipient.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { DomainsModule } from '../domains.module';

import { OperatorSearchComponent } from './operator-search.component';

describe('OperatorSearchComponent', () => {
    let component: OperatorSearchComponent;
    let fixture: MockedComponentFixture<OperatorSearchComponent, Partial<OperatorSearchComponent>>;
    let addressOutput: jasmine.Spy;

    beforeEach(() => {
        addressOutput = jasmine.createSpy('addressOutput');

        return MockBuilder(OperatorSearchComponent, DomainsModule)
            .mock(SharedModule)
            .keep(ReactiveFormsModule)
            .keep(TranslocoModule, { export: true })
            .keep(transloco(), { export: true });
    });

    beforeEach(() => {
        fixture = MockRender(OperatorSearchComponent);

        component = fixture.point.componentInstance;
        fixture.detectChanges();
    });

    it('should emit entered address', fakeAsync(() => {
        component.address.subscribe(addressOutput);

        setInputValue(TezosAddress);
        submit();

        expect(addressOutput).toHaveBeenCalledWith(TezosAddress);
        const recipient = ngMocks.find(RecipientComponent);

        expect(addressOutput).toHaveBeenCalledWith(TezosAddress);
        expect(recipient).toBeDefined();
    }));

    function setInputValue(value: string): void {
        component.form.controls.inputAddress.setValue(value);

        fixture.detectChanges();
        tick();
    }

    function submit() {
        ngMocks.find(fixture.point, 'button').triggerEventHandler('click', new Event('click'));
    }
});
