import { fakeAsync, tick } from '@angular/core/testing';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { TranslocoModule } from '@ngneat/transloco';
import { ContractAbstraction, ContractMethod, TezosToolkit, Wallet } from '@taquito/taquito';
import { getDomain, provider, TestNetwork, TestWallet, TestWallet3, TezosAddress, TezosAddress2, TezosAddress3, TezosAddress4, transloco } from '@td/test';
import { MockBuilder, MockedComponentFixture, MockRender, ngMocks } from 'ng-mocks';
import { of, ReplaySubject, Subject } from 'rxjs';
import { DomainDetailQuery } from 'src/app/graphql/graphql.generated';
import { AddressComponent } from 'src/app/shared/address.component';
import { AlertComponent } from 'src/app/shared/alert.component';
import { OperationStatusComponent } from 'src/app/shared/operation-status.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { TezosWallet } from 'src/app/tezos/models';
import { TezosNetworkService } from 'src/app/tezos/tezos-network.service';
import { TezosService } from 'src/app/tezos/tezos.service';
import { anyFunction, anyString, instance, mock, objectContaining, verify, when } from 'ts-mockito';
import { DomainsModule } from '../domains.module';
import { OperatorPermissionsComponent } from './operator-permissions.component';
import { OperatorSearchComponent } from './operator-search.component';

describe('OperatorPermissionsComponent', () => {
    let component: OperatorPermissionsComponent;
    let fixture: MockedComponentFixture<OperatorPermissionsComponent, Partial<OperatorPermissionsComponent>>;
    let dialogRefMock: MatDialogRef<OperatorPermissionsComponent>;
    let tezosServiceMock: TezosService;
    let tezosToolkigMock: TezosToolkit;
    let tezosNetworkServiceMock: TezosNetworkService;
    let walletMock: Wallet;
    let tokenContract: ContractAbstraction<Wallet>;
    let updateOperatorsContractMethod: ContractMethod<Wallet>;
    let update_operators: jasmine.Spy;

    let wallet: ReplaySubject<TezosWallet | null>;
    let operation: Subject<void>;

    beforeEach(() => {
        dialogRefMock = mock<MatDialogRef<any, any>>(MatDialogRef);

        tezosServiceMock = mock(TezosService);
        tezosToolkigMock = mock(TezosToolkit);
        walletMock = mock(Wallet);
        tezosNetworkServiceMock = mock(TezosNetworkService);
        tokenContract = mock(ContractAbstraction);

        updateOperatorsContractMethod = mock(ContractMethod);
        update_operators = jasmine.createSpy('update_operators');

        wallet = new ReplaySubject(1);
        operation = new Subject();

        when(tezosToolkigMock.wallet).thenReturn(instance(walletMock));
        when(walletMock.at(anyString())).thenResolve(instance(tokenContract));

        when(tokenContract.methods).thenReturn({
            update_operators: update_operators.and.returnValue(instance(updateOperatorsContractMethod)),
        });

        when(tezosServiceMock.activeWallet).thenReturn(wallet);
        when(tezosServiceMock.execute(anyFunction())).thenCall(fn => {
            fn(null, instance(tezosToolkigMock));
            return operation;
        });
        when(tezosNetworkServiceMock.activeNetwork).thenReturn(of(TestNetwork));

        wallet.next(TestWallet);

        return MockBuilder(OperatorPermissionsComponent, DomainsModule)
            .mock(SharedModule)
            .keep(TranslocoModule, { export: true })
            .keep(transloco(), { export: true })
            .provide([provider(dialogRefMock), provider(tezosServiceMock), provider(tezosNetworkServiceMock)]);
    });

    function init(domain: Partial<DomainDetailQuery['domain']>) {
        fixture = MockRender(
            OperatorPermissionsComponent,
            {},
            {
                providers: [{ provide: MAT_DIALOG_DATA, useValue: { domain } }],
                detectChanges: true,
            }
        );

        component = fixture.point.componentInstance;
        fixture.detectChanges();

        return domain;
    }

    it('should map operators', () => {
        init(domainWithOperators());

        expect(component.operators).toEqual([
            { address: TezosAddress3, id: '', isDirectBrokerAddress: false },
            { address: TezosAddress2, id: '', isDirectBrokerAddress: false },
        ]);

        const directBrokerAddress = ngMocks.findInstances(AddressComponent).find(c => !!c.customName);
        expect(directBrokerAddress).toBeUndefined();
    });

    it('should handle broker address', () => {
        init(domainWithOperators({ addBrokerAddress: true }));

        expect(component.operators).toEqual([
            { address: TestNetwork.directBrokerContract, id: '', isDirectBrokerAddress: true },
            { address: TezosAddress3, id: '', isDirectBrokerAddress: false },
            { address: TezosAddress2, id: '', isDirectBrokerAddress: false },
        ]);

        const directBrokerAddress = ngMocks.findInstances(AddressComponent).find(c => !!c.customName);
        expect(directBrokerAddress).toBeDefined();

        const addresses = ngMocks.findAll(fixture, AddressComponent);
        expect(addresses.length).toBe(3);
    });

    it('should add new address', () => {
        init(domainWithOperators());

        const addresses = ngMocks.findAll(fixture, AddressComponent);
        expect(addresses.length).toBe(2);

        const search = ngMocks.findInstance(OperatorSearchComponent);
        search.address.emit(TezosAddress4);

        fixture.detectChanges();

        const addressAfterAdd = ngMocks.findAll(fixture, AddressComponent);
        expect(addressAfterAdd.length).toBe(3);
    });

    it('should not add duplicate address', () => {
        init(domainWithOperators());

        const addresses = ngMocks.findAll(fixture, AddressComponent);
        expect(addresses.length).toBe(2);

        const search = ngMocks.findInstance(OperatorSearchComponent);
        search.address.emit(TezosAddress4);
        search.address.emit(TezosAddress4);
        search.address.emit(TezosAddress4);

        fixture.detectChanges();

        const addressAfterAdd = ngMocks.findAll(fixture, AddressComponent);
        expect(addressAfterAdd.length).toBe(3);
    });

    it('should not add owner address', () => {
        init(domainWithOperators());

        const addresses = ngMocks.findAll(fixture, AddressComponent);
        expect(addresses.length).toBe(2);

        const search = ngMocks.findInstance(OperatorSearchComponent);
        search.address.emit(TezosAddress);

        fixture.detectChanges();

        const addressAfterAdd = ngMocks.findAll(fixture, AddressComponent);
        expect(addressAfterAdd.length).toBe(2);
    });

    it('should remove address', () => {
        init(domainWithOperators());

        const addresses = ngMocks.findAll(fixture, AddressComponent);
        expect(addresses.length).toBe(2);

        ngMocks.find(fixture, 'button').triggerEventHandler('click', new Event('click'));
        fixture.detectChanges();

        const addressAfterAdd = ngMocks.findAll(fixture, AddressComponent);
        expect(addressAfterAdd.length).toBe(1);
    });

    it('should persist changes', fakeAsync(() => {
        const domain = init(domainWithOperators());

        //remove first address
        ngMocks.find(fixture, 'button').triggerEventHandler('click', new Event('click'));

        //add new address
        ngMocks.findInstance(OperatorSearchComponent).address.emit(TezosAddress4);
        ngMocks.find(fixture, '.js-submit').triggerEventHandler('click', new Event('click'));

        fixture.detectChanges();

        tick();

        expect(update_operators).toHaveBeenCalledOnceWith([
            {
                add_operator: { owner: TestWallet.address, token_id: domain!.tokenId, operator: TezosAddress4 },
            },
            {
                remove_operator: { owner: TestWallet.address, token_id: domain!.tokenId, operator: TezosAddress3 },
            },
        ]);

        verify(updateOperatorsContractMethod.send(objectContaining({ storageLimit: 100 }))).once();
    }));

    it('should close popup on cancel', () => {
        init(domainWithOperators());

        ngMocks.find(fixture, '.js-cancel-btn').triggerEventHandler('click', new Event('click'));

        verify(dialogRefMock.close(false)).once();
    });

    it('should close when operation is successfull', () => {
        init(domainWithOperators());

        ngMocks.findInstance(fixture, OperationStatusComponent).done.emit({ success: true });

        verify(dialogRefMock.close(true)).once();
    });

    it('should not close when operation failed', () => {
        init(domainWithOperators());

        ngMocks.findInstance(fixture, OperationStatusComponent).done.emit({ success: false });

        verify(dialogRefMock.close(true)).never();
    });

    describe('when user is operator', () => {
        let domain;
        beforeEach(() => {
            wallet.next(TestWallet3);
            domain = domainWithOperators({ extraOperator: TestWallet3.address });
            init(domain);
        });

        it('should show warning that they are removing themselves as operator', () => {
            expect(component.removedSelfAsOperator).toBe(false);

            component.removeOperator({ address: TestWallet3.address, id: '' });

            expect(component.removedSelfAsOperator).toBe(true);
            fixture.detectChanges();

            ngMocks.find(AlertComponent);
        });

        it('should hide warning  that they are removing themselves as operator', () => {
            expect(component.removedSelfAsOperator).toBe(false);

            component.removeOperator({ address: TestWallet3.address, id: '' });

            expect(component.removedSelfAsOperator).toBe(true);

            component.onAddressEntered(TestWallet3.address);

            expect(component.removedSelfAsOperator).toBe(false);
        });
    });
});

function domainWithOperators(options?: { addBrokerAddress?: boolean; extraOperator?: string }) {
    const domain = { ...getDomain('with-operators.tez') };

    if (options?.addBrokerAddress) {
        domain.operators = [...domain.operators, { address: TestNetwork.directBrokerContract, id: '' }];
    }

    if (options?.extraOperator) {
        domain.operators = [...domain.operators, { address: options.extraOperator, id: '' }];
    }

    return domain;
}
