import { TranslocoService } from '@ngneat/transloco';
import { TestConfig } from '@td/test';
import dayjs from 'dayjs';
import { anyString, anything, instance, mock, verify, when } from 'ts-mockito';
import { DayjsDatePipe } from '../i18n/dayjs-date.pipe';
import { NotificationEventGeneratorService } from './notification-event-generator.service';

describe('NotificationEventGeneratorService', () => {
    let service: NotificationEventGeneratorService;
    let translation: TranslocoService;
    let dayjsPipe: DayjsDatePipe;

    beforeEach(() => {
        translation = mock(TranslocoService);
        dayjsPipe = mock(DayjsDatePipe);

        when(translation.translate(anyString())).thenCall(key => key);
        when(translation.translate(anyString(), anything())).thenCall(key => key);

        service = new NotificationEventGeneratorService(instance(translation), instance(dayjsPipe), TestConfig);
    });

    it('should generate event', () => {
        const expiration = dayjs().add(365, 'days');
        const eventStart = expiration.subtract(TestConfig.expiringDomainThreshold.time, TestConfig.expiringDomainThreshold.units).hour(10).minute(0);
        const event = service.generate({ name: 'test.tez', expires: expiration });

        verify(dayjsPipe.transform(expiration)).once();

        expect(event).toEqual({
            title: 'domain-detail.notification-event.title',
            description: 'domain-detail.notification-event.description',
            start: eventStart,
            duration: [15, 'minutes'],
        });
    });
});
