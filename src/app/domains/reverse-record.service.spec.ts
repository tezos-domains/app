import { StorageMap } from '@ngx-pwa/local-storage';
import { TestReverseRecord, TestWallet } from '@td/test';
import { ReplaySubject, Subject } from 'rxjs';
import { anyString, anything, instance, mock, verify, when } from 'ts-mockito';

import { fakeAsync, tick } from '@angular/core/testing';
import { DataSourceFactory } from '../graphql/data-source-factory';
import { ReverseRecordDetailQuery } from '../graphql/graphql.generated';
import { ReverseRecordDetailDataSource } from '../graphql/reverse-record-data-source';
import { TezosWallet } from '../tezos/models';
import { TezosService } from '../tezos/tezos.service';
import { ErrorPresenterService } from '../utils/error-presenter.service';
import { StorageSchema } from '../utils/storage';
import { ReverseRecordService } from './reverse-record.service';

describe('ReverseRecordService', () => {
    let service: ReverseRecordService;
    let tezosServiceMock: TezosService;
    let dataSourceFactoryMock: DataSourceFactory;
    let reverseRecordDetailDataSourceMock: ReverseRecordDetailDataSource;
    let errorPresenterServiceMock: ErrorPresenterService;
    let storageMapMock: StorageMap;
    let storage: Subject<string | undefined>;
    let storageWrite: Subject<any>;
    let wallet: ReplaySubject<TezosWallet | null>;
    let getReverseRecord: ReplaySubject<ReverseRecordDetailQuery>;
    let error: Subject<any>;
    let spy: jasmine.Spy;

    beforeEach(() => {
        tezosServiceMock = mock(TezosService);
        storageMapMock = mock(StorageMap);
        reverseRecordDetailDataSourceMock = mock(ReverseRecordDetailDataSource);
        dataSourceFactoryMock = mock(DataSourceFactory);
        errorPresenterServiceMock = mock(ErrorPresenterService);
        storageWrite = new Subject();
        error = new Subject();
        wallet = new ReplaySubject(1);
        getReverseRecord = new ReplaySubject(1);
        spy = jasmine.createSpy();

        wallet.next(TestWallet);

        when(dataSourceFactoryMock.createReverseRecordDetailDataSource()).thenReturn(instance(reverseRecordDetailDataSourceMock));
        when(storageMapMock.get(StorageSchema.reverseRecord.key, StorageSchema.reverseRecord.schema)).thenCall(() => (storage = new Subject()));
        when(storageMapMock.set(anyString(), anything(), anything())).thenReturn(storageWrite);
        when(storageMapMock.delete(anyString())).thenReturn(storageWrite);
        when(reverseRecordDetailDataSourceMock.data$).thenReturn(getReverseRecord);
        when(reverseRecordDetailDataSourceMock.error$).thenReturn(error);
        when(tezosServiceMock.activeWallet).thenReturn(wallet);

        service = new ReverseRecordService(
            instance(tezosServiceMock),
            instance(storageMapMock),
            instance(errorPresenterServiceMock),
            instance(dataSourceFactoryMock)
        );

        service.current.subscribe(spy);
    });

    it('should set reverse record from storage', () => {
        storage.next(JSON.stringify(TestReverseRecord));

        expect(spy).toHaveBeenCalledWith(TestReverseRecord);
    });

    it('should not emit when storage is empty', () => {
        expect(spy).not.toHaveBeenCalled();
    });

    it('should refresh record at startup', fakeAsync(() => {
        getReverseRecord.next({ reverseRecord: TestReverseRecord });
        tick();

        verify(storageMapMock.set(StorageSchema.reverseRecord.key, JSON.stringify(TestReverseRecord), StorageSchema.reverseRecord.schema)).called();
        storageWrite.next(null);

        storage.next(JSON.stringify(TestReverseRecord));

        expect(spy).toHaveBeenCalledWith(TestReverseRecord);
    }));

    it('should set record to null if wallet is null', () => {
        wallet.next(null);

        verify(storageMapMock.delete(StorageSchema.reverseRecord.key)).called();
        storageWrite.next(null);

        expect(spy).toHaveBeenCalledWith(null);
    });

    it('should set record to null record does not exist', fakeAsync(() => {
        getReverseRecord.next({ reverseRecord: null });
        tick();

        verify(storageMapMock.delete(StorageSchema.reverseRecord.key)).called();
        storageWrite.next(null);

        storage.next(undefined);

        expect(spy).toHaveBeenCalledWith(null);
    }));

    it('should present error', fakeAsync(() => {
        const e = new Error();
        error.next(e);

        verify(errorPresenterServiceMock.apiErrorToast('reverse-record-fetch', e)).called();
    }));

    describe('refresh()', () => {
        it('should refresh reverse record', fakeAsync(() => {
            getReverseRecord.next({ reverseRecord: TestReverseRecord });
            tick();
            storageWrite.next(null);
            storage.next(JSON.stringify(TestReverseRecord));

            service.refresh();

            const newRecord = { reverseRecord: { name: 'alt' } } as any;
            getReverseRecord.next(newRecord);
            tick();
            storageWrite.next(null);
            verify(storageMapMock.set(StorageSchema.reverseRecord.key, JSON.stringify(newRecord.reverseRecord), StorageSchema.reverseRecord.schema)).called();
            storage.next(JSON.stringify(newRecord));

            expect(spy).toHaveBeenCalledWith(newRecord);
        }));
    });
});
