import { discardPeriodicTasks, fakeAsync, tick } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { TranslocoModule } from '@ngneat/transloco';
import { getDomain, provider, TestWallet, transloco } from '@td/test';
import { DomainAcquisitionInfo, DomainsManager, TaquitoTezosDomainsOperationFactory } from '@tezos-domains/manager';
import { TLDConfiguration } from '@tezos-domains/manager/dist/src/manager/model';
import { TaquitoTezosDomainsClient } from '@tezos-domains/taquito-client';
import { MockBuilder, MockedComponentFixture, MockRender, ngMocks } from 'ng-mocks';
import { Observable, of, ReplaySubject, Subject } from 'rxjs';
import { anyFunction, anyNumber, anyString, anything, instance, mock, verify, when } from 'ts-mockito';
import { DayjsDatePipe } from '../i18n/dayjs-date.pipe';
import { SharedModule } from '../shared/shared.module';
import { TruncatePipe } from '../shared/truncate.pipe';
import { TezosDomainsClientService } from '../tezos/integration/tezos-domains-client.service';
import { TezosWallet } from '../tezos/models';
import { SmartContractOperationEvent, TezosService } from '../tezos/tezos.service';

import { AffiliateService } from '../affiliates/affiliates.service';
import { OperationStatusComponent } from '../shared/operation-status.component';
import { BulkRenewComponent } from './bulk-renew.component';
import { DomainsModule } from './domains.module';

describe('BulkRenewComponent', () => {
    let component: BulkRenewComponent;
    let fixture: MockedComponentFixture<BulkRenewComponent, Partial<BulkRenewComponent>>;
    let dialogRefMock: MatDialogRef<BulkRenewComponent>;
    let tezosServiceMock: TezosService;
    let tezosDomainsClientServiceMock: TezosDomainsClientService;
    let tezosDomainsMock: TaquitoTezosDomainsClient;
    let tezosDomainsManagerMock: DomainsManager;
    let operation: Observable<SmartContractOperationEvent>;
    let wallet: ReplaySubject<TezosWallet | null>;
    let operationFactory: TaquitoTezosDomainsOperationFactory;
    let batchOperation: Observable<SmartContractOperationEvent>;

    beforeEach(() => {
        operationFactory = mock(TaquitoTezosDomainsOperationFactory);
        dialogRefMock = mock<MatDialogRef<BulkRenewComponent>>(MatDialogRef);
        tezosServiceMock = mock(TezosService);

        tezosDomainsClientServiceMock = mock(TezosDomainsClientService);
        tezosDomainsMock = mock(TaquitoTezosDomainsClient);
        tezosDomainsManagerMock = mock<DomainsManager>();

        batchOperation = new Subject();
        operation = new Subject();
        wallet = new ReplaySubject(1);
        const affiliateServiceMock = mock(AffiliateService);

        when(operationFactory.renew(anyString(), anything())).thenResolve(<any>null);

        when(tezosDomainsClientServiceMock.current).thenReturn(of(instance(tezosDomainsMock)));
        when(tezosDomainsMock.manager).thenReturn(instance(tezosDomainsManagerMock));
        when(tezosDomainsManagerMock.setChildRecord(anything()));
        when(tezosDomainsManagerMock.updateRecord(anything()));
        when(tezosServiceMock.execute(anyFunction())).thenReturn(operation);
        when(tezosDomainsManagerMock.batch(anything())).thenCall(fn => {
            fn(instance(operationFactory));
            return batchOperation;
        });
        when(tezosDomainsManagerMock.getTldConfiguration(anything())).thenResolve(<TLDConfiguration>{});
        when(affiliateServiceMock.affiliateId$).thenReturn(of(null));

        const acq = mock(DomainAcquisitionInfo);
        when(acq.calculatePrice(anyNumber())).thenReturn(10 * 1e6);
        when(tezosDomainsClientServiceMock.calculateAcquisitionInfo(anything())).thenReturn(instance(acq));

        when(tezosServiceMock.activeWallet).thenReturn(wallet);
        when(tezosServiceMock.execute(anyFunction())).thenCall(fn => {
            fn(instance(tezosDomainsMock));
            return operation;
        });

        wallet.next(TestWallet);

        return MockBuilder(BulkRenewComponent, DomainsModule)
            .mock(SharedModule)
            .mock(DayjsDatePipe, value => value?.toISOString())
            .provide([provider(dialogRefMock), provider(tezosDomainsClientServiceMock), provider(tezosServiceMock), provider(affiliateServiceMock)])
            .keep(ReactiveFormsModule)
            .keep(OperationStatusComponent)
            .keep(FormsModule)
            .keep(TruncatePipe)
            .keep(TranslocoModule, { export: true })
            .keep(transloco(), { export: true });
    });

    describe('init', () => {
        beforeEach(fakeAsync(() => {
            init();
        }));

        it('should set price for renewing the domain', () => {
            expect(component.form.get('price')?.value).toBe(10);
        });

        it('should set price add the default duration', () => {
            expect(component.form.get('duration')?.value).toBe(1);
        });

        it('should show one row with new expiration date', () => {
            const name = ngMocks.find('.js-domain-name');
            const expiration = ngMocks.find('.js-new-expiration');

            const domain = getDomain('expires-soon.tez');
            const newExpirationDate = domain.expires?.add(365, 'days').toISOString();

            expect(expiration.nativeElement.innerHTML).toContain(newExpirationDate);
            expect(name.nativeElement.innerHTML).toContain(domain.name);
        });
    });

    it('should renew the domain', fakeAsync(() => {
        init();
        spyOn(component.form, 'disable').and.callThrough();

        const renewButton = ngMocks.find(fixture, 'button[mat-flat-button]');
        renewButton.triggerEventHandler('click', new Event('click'));
        fixture.detectChanges();

        expect(component.form.disable).toHaveBeenCalled();
        verify(tezosDomainsManagerMock.batch(anything())).called();

        discardPeriodicTasks();
    }));

    describe('operationDone({ success: false })', () => {
        it('should enable form', fakeAsync(() => {
            init();

            component.renew();
            component.operationDone({ success: false });

            expect(component.form.disabled).toBeFalse();
        }));
    });

    describe('operationDone({ success: true })', () => {
        it('should close the dialog', fakeAsync(() => {
            init();

            component.operationDone({ success: true });
            verify(dialogRefMock.close(true)).called();
        }));
    });

    describe('cancel()', () => {
        it('should close the dialog', fakeAsync(() => {
            init();

            component.cancel();

            verify(dialogRefMock.close(false)).called();
        }));
    });

    function init() {
        fixture = MockRender(
            BulkRenewComponent,
            {},
            {
                providers: [{ provide: MAT_DIALOG_DATA, useValue: { domains: [getDomain('expires-soon.tez')] } }],
                detectChanges: true,
            }
        );

        component = fixture.point.componentInstance;
        tick();
        fixture.detectChanges();
    }
});
