import { FormGroupDirective, UntypedFormBuilder, UntypedFormControl, UntypedFormGroup } from '@angular/forms';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { TranslocoModule } from '@ngneat/transloco';
import { TestReverseRecord, TestWallet, TestWallet3, TezosAddress, TezosAddress2, TezosAddress3, getDomain, provider, transloco } from '@td/test';
import { DomainNameValidationResult, DomainNameValidator, RecordMetadata } from '@tezos-domains/core';
import { DomainsManager } from '@tezos-domains/manager';
import { TaquitoTezosDomainsClient } from '@tezos-domains/taquito-client';
import { MockBuilder, MockRender, MockedComponentFixture } from 'ng-mocks';
import { Observable, ReplaySubject, Subject, of } from 'rxjs';
import { anyFunction, anyOfClass, anyString, anything, capture, deepEqual, instance, mock, verify, when } from 'ts-mockito';

import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { DomainDetailQuery } from '../graphql/graphql.generated';
import { TezosDomainsClientService } from '../tezos/integration/tezos-domains-client.service';
import { TezosWallet } from '../tezos/models';
import { SmartContractOperationEvent, TezosService } from '../tezos/tezos.service';
import { TdAsyncValidatorsFactory } from '../utils/form-validators';
import { DomainRecordFormComponent } from './domain-record-form.component';
import { DomainsModule } from './domains.module';
import { ReverseRecordService } from './reverse-record.service';

describe('DomainRecordFormComponent', () => {
    let fixture: MockedComponentFixture<DomainRecordFormComponent, Partial<DomainRecordFormComponent>>;
    let component: DomainRecordFormComponent;
    let dialogRefMock: MatDialogRef<DomainRecordFormComponent>;
    let tezosServiceMock: TezosService;
    let tezosDomainsMock: TaquitoTezosDomainsClient;
    let tezosDomainsManagerMock: DomainsManager;
    let tezosDomainsClientServiceMock: TezosDomainsClientService;
    let tezosDomainsValidatorMock: DomainNameValidator;
    let reverseRecordServiceMock: ReverseRecordService;
    let asyncValidatorFactoryMock: TdAsyncValidatorsFactory;
    let wallet: ReplaySubject<TezosWallet | null>;
    let operation: Observable<SmartContractOperationEvent>;

    beforeEach(() => {
        dialogRefMock = mock<MatDialogRef<any, any>>(MatDialogRef);
        tezosServiceMock = mock(TezosService);
        tezosDomainsMock = mock(TaquitoTezosDomainsClient);
        tezosDomainsManagerMock = mock<DomainsManager>();
        reverseRecordServiceMock = mock(ReverseRecordService);
        tezosDomainsClientServiceMock = mock(TezosDomainsClientService);
        tezosDomainsValidatorMock = mock<DomainNameValidator>();
        asyncValidatorFactoryMock = mock(TdAsyncValidatorsFactory);

        wallet = new ReplaySubject(1);
        operation = new Subject();

        when(tezosServiceMock.activeWallet).thenReturn(wallet);
        when(reverseRecordServiceMock.current).thenReturn(of(TestReverseRecord));

        when(tezosDomainsMock.manager).thenReturn(instance(tezosDomainsManagerMock));
        when(tezosDomainsMock.validator).thenReturn(instance(tezosDomainsValidatorMock));
        when(tezosDomainsManagerMock.setChildRecord(anything()));
        when(tezosDomainsManagerMock.updateRecord(anything()));
        when(tezosServiceMock.execute(anyFunction())).thenCall(fn => {
            fn(instance(tezosDomainsMock));
            return operation;
        });
        when(tezosDomainsClientServiceMock.current).thenReturn(of(instance(tezosDomainsMock)));
        when(tezosDomainsValidatorMock.isValidWithKnownTld(anyString())).thenReturn(DomainNameValidationResult.VALID);
        when(asyncValidatorFactoryMock.subdomainNameAvailable(anyString())).thenReturn(() => of(null));

        wallet.next(TestWallet);

        return MockBuilder(DomainRecordFormComponent, DomainsModule)
            .keep(TranslocoModule, { export: true })
            .keep(NoopAnimationsModule, { export: true })
            .keep(transloco(), { export: true })
            .provide([
                provider(dialogRefMock),
                provider(tezosServiceMock),
                provider(tezosDomainsClientServiceMock),
                provider(reverseRecordServiceMock),
                provider(asyncValidatorFactoryMock),
            ])
            .provide(UntypedFormBuilder)
            .mock(FormGroupDirective);
    });

    function init(domain: Partial<DomainDetailQuery['domain']>, parent: string) {
        fixture = MockRender(
            DomainRecordFormComponent,
            {},
            {
                providers: [{ provide: MAT_DIALOG_DATA, useValue: { domain, parent } }],
                detectChanges: true,
            }
        );

        component = fixture.point.componentInstance;
    }

    describe('form', () => {
        it('should create form for new subdomain', () => {
            init({}, 'necroskillz.tez');

            expect(component.form.get('label')?.value).toBe('');
            expect(component.form.get('address')?.value).toBeNull();
            expect(component.form.get('owner')?.value).toBe(TestWallet.address);
            expect(component.form.get('data')?.value).toEqual({});
        });

        it('should create form for existing domain', () => {
            init(getDomain('necroskillz.tez'), 'tez');

            expect(component.form.get('label')).toBeNull();
            expect(component.form.get('address')?.value).toBe(getDomain('necroskillz.tez').address);
            expect(component.form.get('owner')?.value).toBe(getDomain('necroskillz.tez').owner);
            expect(component.form.get('data')?.value).toEqual({});
        });
    });

    describe('create()', () => {
        beforeEach(() => {
            init({}, 'necroskillz.tez');
        });

        it('should send operation with form data', () => {
            addDataKeyControl('someKey');
            const data = { label: 'xxx', address: TezosAddress, owner: TezosAddress2, data: { someKey: '3638' } };
            component.form.setValue(data);

            component.create();

            verify(
                tezosDomainsManagerMock.setChildRecord(
                    deepEqual({
                        ...data,
                        parent: 'necroskillz.tez',
                        data: anyOfClass(RecordMetadata),
                    })
                )
            ).called();

            const metadata = capture(tezosDomainsManagerMock.setChildRecord).last()[0].data;
            expect(metadata.keys()).toEqual(['someKey']);
            expect(metadata.getJson<number>('someKey')).toEqual(68);

            expect(component.operation).toBe(operation);
            expect(component.form.disabled).toBeTrue();
        });

        it('should coalesce address to null', () => {
            const data = { label: 'xxx', address: '', owner: TezosAddress2, data: {} };
            component.form.setValue(data);

            component.create();

            verify(
                tezosDomainsManagerMock.setChildRecord(
                    deepEqual({
                        ...data,
                        address: null,
                        parent: 'necroskillz.tez',
                        data: anyOfClass(RecordMetadata),
                    })
                )
            ).called();

            expect(component.operation).toBe(operation);
            expect(component.form.disabled).toBeTrue();
        });
    });

    describe('save()', () => {
        it('should send updateRecord operation with form data if user is the owner', () => {
            init(getDomain('necroskillz.tez'), 'tez');
            addDataKeyControl('someKey');

            const data = { address: TezosAddress2, owner: TezosAddress3, data: { someKey: '3638' } };
            component.form.setValue(data);

            component.save();

            verify(
                tezosDomainsManagerMock.updateRecord(
                    deepEqual({
                        ...data,
                        name: 'necroskillz.tez',
                        data: anyOfClass(RecordMetadata),
                    })
                )
            ).called();

            const metadata = capture(tezosDomainsManagerMock.updateRecord).last()[0].data;
            expect(metadata.keys()).toEqual(['someKey']);
            expect(metadata.getJson<number>('someKey')).toEqual(68);

            expect(component.operation).toBe(operation);
            expect(component.form.disabled).toBeTrue();
        });

        it('should send updateRecord operation with form data if user is operator', () => {
            wallet.next(TestWallet3);
            init(getDomain('with-operators.tez'), 'tez');
            addDataKeyControl('someKey');

            const data = { address: TezosAddress2, owner: TezosAddress3, data: { someKey: '3638' } };
            component.form.setValue(data);

            component.save();

            verify(
                tezosDomainsManagerMock.updateRecord(
                    deepEqual({
                        ...data,
                        name: 'with-operators.tez',
                        data: anyOfClass(RecordMetadata),
                    })
                )
            ).called();

            const metadata = capture(tezosDomainsManagerMock.updateRecord).last()[0].data;
            expect(metadata.keys()).toEqual(['someKey']);
            expect(metadata.getJson<number>('someKey')).toEqual(68);

            expect(component.operation).toBe(operation);
            expect(component.form.disabled).toBeTrue();
        });

        it('should coalesce address to null for update', () => {
            init(getDomain('necroskillz.tez'), 'tez');

            const data = { address: '', owner: TezosAddress3, data: {} };
            component.form.setValue(data);

            component.save();

            verify(
                tezosDomainsManagerMock.updateRecord(
                    deepEqual({
                        ...data,
                        address: null,
                        name: 'necroskillz.tez',
                        data: anyOfClass(RecordMetadata),
                    })
                )
            ).called();

            expect(component.operation).toBe(operation);
            expect(component.form.disabled).toBeTrue();
        });

        it('should send setChildRecord operation with form data if user is NOT the owner (= user is the owner of the parent)', () => {
            init(getDomain('play.necroskillz.tez'), 'necroskillz.tez');
            addDataKeyControl('someKey');

            const data = { address: TezosAddress2, owner: TezosAddress3, data: { someKey: '3638' } };
            component.form.setValue(data);

            component.save();

            verify(
                tezosDomainsManagerMock.setChildRecord(
                    deepEqual({
                        ...data,
                        label: 'play',
                        parent: 'necroskillz.tez',
                        data: anyOfClass(RecordMetadata),
                    })
                )
            ).called();

            const metadata = capture(tezosDomainsManagerMock.setChildRecord).last()[0].data;
            expect(metadata.keys()).toEqual(['someKey']);
            expect(metadata.getJson<number>('someKey')).toEqual(68);

            expect(component.operation).toBe(operation);
            expect(component.form.disabled).toBeTrue();
        });

        it('should coalesce address to null for set', () => {
            init(getDomain('play.necroskillz.tez'), 'necroskillz.tez');

            const data = { address: '', owner: TezosAddress3, data: {} };
            component.form.setValue(data);

            component.save();

            verify(
                tezosDomainsManagerMock.setChildRecord(
                    deepEqual({
                        ...data,
                        address: null,
                        label: 'play',
                        parent: 'necroskillz.tez',
                        data: anyOfClass(RecordMetadata),
                    })
                )
            ).called();

            expect(component.operation).toBe(operation);
            expect(component.form.disabled).toBeTrue();
        });
    });

    describe('cancel()', () => {
        it('should close the modal', () => {
            init({}, 'necroskillz.tez');

            component.cancel();

            verify(dialogRefMock.close(false)).called();
        });
    });

    describe('clearAddress()', () => {
        it('should clear the address field', () => {
            init(getDomain('necroskillz.tez'), 'tez');

            expect(component.form.value.address).toBe(TezosAddress);

            component.clearAddress();

            expect(component.form.value.address).toBeNull();
        });
    });

    describe('operationDone({ success: true })', () => {
        it('should close the modal with value of the form', () => {
            init({}, 'necroskillz.tez');
            const data = { label: 'xxx', owner: TezosAddress, address: TezosAddress2, data: {} };

            component.form.setValue(data);

            component.operationDone({ success: true });

            verify(dialogRefMock.close(true)).called();
        });
    });

    describe('operationDone({ success: false })', () => {
        it('should enable the form', () => {
            init(getDomain('play.necroskillz.tez'), 'necroskillz.tez');

            const data = { address: TezosAddress2, owner: TezosAddress3, data: {} };
            component.form.setValue(data);

            component.save();

            expect(component.form.disabled).toBeTrue();

            component.operationDone({ success: false });

            expect(component.form.disabled).toBeFalse();
        });
    });

    function addDataKeyControl(key: string) {
        (component.form.get('data') as UntypedFormGroup).addControl(key, new UntypedFormControl());
    }
});
