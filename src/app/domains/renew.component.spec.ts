import { FormGroupDirective, UntypedFormBuilder } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { getDomain, provider, TestWallet, transloco } from '@td/test';
import { DomainsManager } from '@tezos-domains/manager';
import { TaquitoTezosDomainsClient } from '@tezos-domains/taquito-client';
import { MockBuilder, MockedComponentFixture, MockRender } from 'ng-mocks';
import { Observable, of, ReplaySubject, Subject } from 'rxjs';
import { anyFunction, anything, deepEqual, instance, mock, verify, when } from 'ts-mockito';

import { TranslocoModule } from '@ngneat/transloco';
import { AffiliateService } from '../affiliates/affiliates.service';
import { OperationStatusComponent } from '../shared/operation-status.component';
import { SharedModule } from '../shared/shared.module';
import { TezosDomainsClientService } from '../tezos/integration/tezos-domains-client.service';
import { TezosWallet } from '../tezos/models';
import { SmartContractOperationEvent, TezosService } from '../tezos/tezos.service';
import { RenewComponent } from './renew.component';

describe('RenewComponent', () => {
    let fixture: MockedComponentFixture<RenewComponent, Partial<RenewComponent>>;
    let component: RenewComponent;
    let tezosServiceMock: TezosService;
    let tezosDomainsClientServiceMock: TezosDomainsClientService;
    let tezosDomainsMock: TaquitoTezosDomainsClient;
    let tezosDomainsManagerMock: DomainsManager;
    let dialogRefMock: MatDialogRef<RenewComponent>;
    let operation: Observable<SmartContractOperationEvent>;
    let wallet: ReplaySubject<TezosWallet | null>;
    let affiliateServiceMock: AffiliateService;

    beforeEach(() => {
        dialogRefMock = mock<MatDialogRef<any, any>>(MatDialogRef);
        operation = new Subject();
        tezosServiceMock = mock(TezosService);
        tezosDomainsClientServiceMock = mock(TezosDomainsClientService);
        tezosDomainsMock = mock(TaquitoTezosDomainsClient);
        tezosDomainsManagerMock = mock<DomainsManager>();
        wallet = new ReplaySubject(1);
        affiliateServiceMock = mock(AffiliateService);

        when(tezosDomainsClientServiceMock.current).thenReturn(of(instance(tezosDomainsMock)));
        when(tezosDomainsMock.manager).thenReturn(instance(tezosDomainsManagerMock));
        when(tezosDomainsManagerMock.setChildRecord(anything()));
        when(tezosDomainsManagerMock.updateRecord(anything()));
        when(tezosServiceMock.execute(anyFunction())).thenReturn(operation);
        when(tezosDomainsManagerMock.renew('tez', anything()));

        when(affiliateServiceMock.affiliateId$).thenReturn(of(null));

        when(tezosServiceMock.activeWallet).thenReturn(wallet);
        when(tezosServiceMock.execute(anyFunction())).thenCall(fn => {
            fn(instance(tezosDomainsMock));
            return operation;
        });
        wallet.next(TestWallet);

        return MockBuilder(RenewComponent)
            .provide([provider(dialogRefMock), provider(tezosDomainsClientServiceMock), provider(tezosServiceMock), provider(affiliateServiceMock)])
            .mock(SharedModule)
            .provide(UntypedFormBuilder)
            .keep(OperationStatusComponent)
            .keep(TranslocoModule, { export: true })
            .keep(transloco(), { export: true })
            .mock(FormGroupDirective);
    });

    beforeEach(() => {
        const calculatePrice = jasmine.createSpy();
        calculatePrice.and.callFake(d => d * 2);

        fixture = MockRender(
            RenewComponent,
            {},
            {
                providers: [{ provide: MAT_DIALOG_DATA, useValue: { domain: getDomain('necroskillz.tez'), acquisitionInfo: { calculatePrice } } }],
                detectChanges: true,
            }
        );

        component = fixture.point.componentInstance;
    });

    describe('init', () => {
        it('should create form', () => {
            expect(component.form.get('duration')?.value).toBe(1);
        });

        it('should calculate price', () => {
            expect(component.form.get('price')?.value).toEqual(730 / 1e6);
        });

        it('should update price when duration changes', () => {
            component.form.get('duration')?.setValue(2);

            expect(component.form.get('price')?.value).toEqual(1460 / 1e6);
        });
    });

    describe('renew()', () => {
        it('should disable the form and call renew', () => {
            component.renew();
            fixture.detectChanges();

            verify(tezosDomainsManagerMock.renew('tez', deepEqual({ label: 'necroskillz', duration: 365, affiliate: null }))).called();
            expect(component.operation).toBeDefined();
        });
    });

    describe('cancel()', () => {
        it('should close the dialog', () => {
            component.cancel();

            verify(dialogRefMock.close(false)).called();
        });
    });

    describe('operationDone({ success: true })', () => {
        it('should close the dialog', () => {
            component.operationDone({ success: true });

            verify(dialogRefMock.close(true)).called();
        });
    });

    describe('operationDone({ success: false })', () => {
        it('should enable form', () => {
            component.renew();

            component.operationDone({ success: false });

            expect(component.form.disabled).toBeFalse();
        });
    });
});
