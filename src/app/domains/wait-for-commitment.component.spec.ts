import { MockedComponentFixture, MockBuilder, MockRender } from 'ng-mocks';
import { fakeAsync, tick, discardPeriodicTasks } from '@angular/core/testing';
import { mockNow, resetNow, transloco } from '@td/test';
import dayjs from 'dayjs';

import { WaitForCommitmentComponent } from './wait-for-commitment.component';
import { SharedModule } from '../shared/shared.module';

describe('WaitForCommitmentComponent', () => {
    let fixture: MockedComponentFixture<WaitForCommitmentComponent, Partial<WaitForCommitmentComponent>>;
    let component: WaitForCommitmentComponent;
    let doneSpy: jasmine.Spy;

    beforeEach(() => MockBuilder(WaitForCommitmentComponent, SharedModule).keep(transloco(), { export: true }));

    beforeEach(() => {
        doneSpy = jasmine.createSpy();

        mockNow(dayjs('2020-10-01T20:04:30Z'));
    });

    afterEach(() => {
        resetNow();
    });

    function init(from: dayjs.Dayjs, to: dayjs.Dayjs) {
        fixture = MockRender(WaitForCommitmentComponent, {
            from,
            to,
            done: doneSpy as any,
        });
        component = fixture.point.componentInstance;

        tick();
    }

    it('should display relative progress between from and to', fakeAsync(() => {
        init(dayjs('2020-10-01T20:00:00Z'), dayjs('2020-10-01T20:10:00Z'));

        expect(component.waitTime).toBe('5:30');
        expect(component.waitProgress).toBe(45);

        discardPeriodicTasks();
    }));

    it('should update time every second', fakeAsync(() => {
        init(dayjs('2020-10-01T20:00:00Z'), dayjs('2020-10-01T20:10:00Z'));

        expect(component.waitTime).toBe('5:30');
        expect(component.waitProgress).toBe(45);

        mockNow(dayjs('2020-10-01T20:04:36Z'));
        tick(1000);
        fixture.detectChanges();

        expect(component.waitTime).toBe('5:24');
        expect(component.waitProgress).toBe(46);

        mockNow(dayjs('2020-10-01T20:04:42Z'));
        tick(1000);
        fixture.detectChanges();

        expect(component.waitTime).toBe('5:18');
        expect(component.waitProgress).toBe(47);

        discardPeriodicTasks();
    }));

    it('should emit done and stop counting when it reaches 0', fakeAsync(() => {
        init(dayjs('2020-10-01T20:00:00Z'), dayjs('2020-10-01T20:10:00Z'));

        expect(component.waitTime).toBe('5:30');
        expect(component.waitProgress).toBe(45);

        mockNow(dayjs('2020-10-01T20:10:00Z'));
        tick(1000);
        fixture.detectChanges();

        expect(component.waitTime).toBe(null);
        expect(component.waitProgress).toBe(100);
        expect(doneSpy).toHaveBeenCalled();
    }));

    it('should emit done and stop counting if now is past to', fakeAsync(() => {
        mockNow(dayjs('2020-10-01T20:15:00Z'));
        init(dayjs('2020-10-01T20:00:00Z'), dayjs('2020-10-01T20:10:00Z'));

        tick(1000);
        fixture.detectChanges();

        expect(component.waitTime).toBe(null);
        expect(component.waitProgress).toBe(100);
        expect(doneSpy).toHaveBeenCalled();
    }));
});
