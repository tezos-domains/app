import { fakeAsync } from '@angular/core/testing';
import { TranslocoModule } from '@ngneat/transloco';
import { providers, TestConfig, TestWallet, TezosAddress, transloco } from '@td/test';
import { MockBuilder, MockedComponentFixture, MockRender, ngMocks } from 'ng-mocks';
import { ReplaySubject, Subject } from 'rxjs';
import { anyString, mock, when } from 'ts-mockito';
import { Configuration } from '../configuration';
import { AlertComponent } from '../shared/alert.component';
import { SharedModule } from '../shared/shared.module';
import { TezosWallet } from '../tezos/models';
import { TezosService } from '../tezos/tezos.service';
import { DomainsModule } from './domains.module';
import { ExpiringDomainsStatsService } from './expiring-domains-stats.service';
import { ExpiringDomainsWarningComponent } from './expiring-domains-warning.component';

describe('ExpiringDomainsWarningComponent', () => {
    let fixture: MockedComponentFixture<ExpiringDomainsWarningComponent, Partial<ExpiringDomainsWarningComponent>>;
    let tezosServiceMock: TezosService;
    let expiringDomainsService: ExpiringDomainsStatsService;
    let wallet: ReplaySubject<TezosWallet | null>;
    let stats$: Subject<{ hasExpiringEarly: boolean; hasExpiringLater: boolean }>;

    beforeEach(async () => {
        tezosServiceMock = mock(TezosService);
        expiringDomainsService = mock(ExpiringDomainsStatsService);

        wallet = new ReplaySubject();
        stats$ = new Subject();

        when(tezosServiceMock.activeWallet).thenReturn(wallet);
        when(expiringDomainsService.combinedStatsFor$(anyString())).thenReturn(stats$);

        return MockBuilder(ExpiringDomainsWarningComponent, DomainsModule)
            .keep(TranslocoModule, { export: true })
            .keep(transloco(), { export: true })
            .provide([providers(tezosServiceMock, expiringDomainsService), { provide: Configuration, useValue: TestConfig }])
            .mock(SharedModule);
    });

    beforeEach(() => {
        fixture = MockRender(ExpiringDomainsWarningComponent, {
            address: TezosAddress,
        });
    });

    [
        { state: 'show', expiringEarly: true, expiringLate: false, alertState: 'warning' },
        { state: 'show', expiringEarly: false, expiringLate: true, alertState: 'info' },
        { state: 'show', expiringEarly: true, expiringLate: true, alertState: 'warning' },
        { state: 'NOT show', expiringEarly: false, expiringLate: false },
    ].forEach(({ state, expiringEarly, expiringLate, alertState }) => {
        it(`should ${state} message when early:${expiringEarly} and late:${expiringLate}: `, fakeAsync(() => {
            wallet.next(TestWallet);
            fixture.componentInstance.address = TezosAddress;

            stats$.next({ hasExpiringEarly: expiringEarly, hasExpiringLater: expiringLate });

            fixture.detectChanges();

            const alerts = ngMocks.findAll(fixture, AlertComponent);
            expect(alerts.length).toBe(state === 'show' ? 1 : 0);
            if (alertState) {
                expect(alerts[0].componentInstance.state).toBe(alertState);
            }
        }));
    });
});
