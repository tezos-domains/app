import { fakeAsync } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { TranslocoModule } from '@ngneat/transloco';
import { TestWallet, providers, transloco } from '@td/test';
import { getByDisplayValue, getByTestId, getByText } from '@testing-library/angular';
import BigNumber from 'bignumber.js';
import { MockBuilder, MockRender, MockedComponentFixture } from 'ng-mocks';
import { of } from 'rxjs';
import { anything, capture, instance, mock, verify, when } from 'ts-mockito';
import { GovActionImpl, GovActionService } from '../airdrop/gov-action.service';
import { DayjsDatePipe } from '../i18n/dayjs-date.pipe';
import { SharedModule } from '../shared/shared.module';
import { TruncatePipe } from '../shared/truncate.pipe';
import { DelegationStatusService } from './delegation-status.service';
import { DepositComponent } from './deposit.component';
import { GovPoolModule } from './gov-pool.module';
import { PoolStats } from './models';

describe('DepositComponent', () => {
    let component: DepositComponent;
    let fixture: MockedComponentFixture<DepositComponent, Partial<DepositComponent>>;
    let dialogRefMock: MatDialogRef<DepositComponent>;
    let delegationStatusServiceMock: DelegationStatusService;
    let govActionMock: GovActionService;
    let govActionImpl: GovActionImpl;

    beforeEach(() => {
        dialogRefMock = mock<MatDialogRef<DepositComponent>>(MatDialogRef);

        delegationStatusServiceMock = mock(DelegationStatusService);
        govActionMock = mock(GovActionService);
        govActionImpl = mock(GovActionImpl);

        when(delegationStatusServiceMock.delegationStatus$).thenReturn(of(null));
        when(govActionMock.q).thenReturn(instance(govActionImpl));
        when(govActionImpl.deposit(anything(), anything())).thenReturn(instance(govActionImpl));

        return MockBuilder(DepositComponent, GovPoolModule)
            .mock(SharedModule)
            .mock(DayjsDatePipe, value => value?.toISOString())
            .provide([providers(dialogRefMock, delegationStatusServiceMock, govActionMock)])
            .keep(ReactiveFormsModule)
            .keep(FormsModule)
            .keep(TranslocoModule, { export: true })
            .keep(TruncatePipe)
            .keep(transloco(), { export: true });
    });

    beforeEach(fakeAsync(() => {
        init();
    }));

    it('set maxDeposit', () => {
        expect(component.maxDeposit).toBe(2.335001);
    });

    it('should update willReceive', () => {
        expect(component.willReceive).toEqual(BigNumber(1167500));
    });

    it('should update maxAmount', () => {
        component.form.controls.amount.setValue(1);
        fixture.detectChanges();

        expect(getByDisplayValue(fixture.nativeElement, '1')).toBeDefined();

        getByTestId(fixture.nativeElement, 'max').click();
        fixture.detectChanges();

        expect(getByDisplayValue(fixture.nativeElement, '2.335001')).toBeDefined();
    });

    it('should deposit real max amount', () => {
        getByText(fixture.nativeElement, 'Deposit').click();

        const [amount] = capture(govActionImpl.deposit).last();
        expect(amount.isEqualTo(BigNumber(2.335001 * 1e6))).toBeTrue();

        verify(govActionImpl.deposit(anything(), TestWallet.address)).once();
        verify(govActionImpl.send()).once();
    });

    describe('cancel()', () => {
        it('should close the dialog', fakeAsync(() => {
            component.cancel();

            verify(dialogRefMock.close()).called();
        }));
    });

    function init() {
        fixture = MockRender(
            DepositComponent,
            {},
            {
                providers: [
                    {
                        provide: MAT_DIALOG_DATA,
                        useValue: {
                            poolStats: <PoolStats>{
                                owner: TestWallet.address,
                                poolAverageAPR: BigNumber(0.3),
                                poolCurrentAPR: BigNumber(0.38),
                                poolSize: BigNumber(100 * 1e6),
                                tedBalance: BigNumber(2.335001 * 1e6),
                                votesBalance: BigNumber(1.335 * 1e6),
                                sharesMinted: BigNumber(50 * 1e6),
                            },
                            delegate: {},
                        },
                    },
                ],
                detectChanges: true,
            }
        );

        component = fixture.point.componentInstance;
        fixture.detectChanges();
    }
});
