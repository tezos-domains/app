import BigNumber from 'bignumber.js';
import { Dayjs } from 'dayjs';

export interface PoolStats {
    tedRewarded: BigNumber;
    poolAverageAPR: BigNumber;
    poolCurrentAPR: BigNumber;
    totalVoters: number;
    tedBalance: BigNumber;
    votesBalance: BigNumber;

    poolSize: BigNumber;
    sharesMinted: BigNumber;
    totalSupply: BigNumber;

    depositedTedBalance: BigNumber;

    owner: string | null;
    claims: { address: string; airdrop: boolean; from: Dayjs; amount: BigNumber }[];
}
