import { TestBed, fakeAsync, tick } from '@angular/core/testing';
import { BigMapAbstraction, ContractAbstraction, TezosToolkit, Wallet } from '@taquito/taquito';
import { TestConfig, TestNetwork, TestWallet, providers } from '@td/test';
import { ApolloTestingController, ApolloTestingModule } from 'apollo-angular/testing';
import { of } from 'rxjs';
import { anything, instance, mock, verify, when } from 'ts-mockito';
import { Logger } from '../browser/logger';
import { Configuration } from '../configuration';
import { DelegateDocument } from '../governance-core/governance-graphql.generated';
import { ReverseRecordListDocument } from '../graphql/graphql.generated';
import { TezosToolkitService } from '../tezos/integration/tezos-toolkit.service';
import { TezosService } from '../tezos/tezos.service';
import { ErrorPresenterService } from '../utils/error-presenter.service';
import { DelegationStatusService } from './delegation-status.service';

describe('DelegationStatusService', () => {
    let tezosServiceMock: TezosService;
    let errorPresenterService: ErrorPresenterService;
    let tezosToolkitServiceMock: TezosToolkitService;
    let logger: Logger;
    let apollo: ApolloTestingController;
    let delegatesBigMap: BigMapAbstraction;

    let tezosToolkitMock: TezosToolkit;
    let walletMock: Wallet;

    let votesContractMock: ContractAbstraction<Wallet>;

    beforeEach(() => {
        tezosServiceMock = mock(TezosService);
        tezosToolkitServiceMock = mock(TezosToolkitService);
        errorPresenterService = mock(ErrorPresenterService);
        logger = mock(Logger);
        tezosToolkitMock = mock(TezosToolkit);
        delegatesBigMap = mock(BigMapAbstraction);

        walletMock = mock(Wallet);
        votesContractMock = mock(ContractAbstraction);

        when(tezosToolkitServiceMock.current).thenReturn(of(instance(tezosToolkitMock)));
        when(tezosToolkitMock.wallet).thenReturn(instance(walletMock));
        when(walletMock.at(TestNetwork.tedVotesContract)).thenResolve(instance(votesContractMock));
        when(tezosServiceMock.activeWallet).thenReturn(of(TestWallet));

        TestBed.configureTestingModule({
            providers: [
                ...providers(tezosServiceMock, errorPresenterService, tezosToolkitServiceMock, logger),
                { provide: Configuration, useValue: TestConfig },
            ],
            imports: [ApolloTestingModule.withClients(['gvpool'])],
        });

        apollo = TestBed.inject(ApolloTestingController);
    });

    it('should return null when no wallet connected', fakeAsync(() => {
        when(tezosServiceMock.activeWallet).thenReturn(of(null));

        const service = getService();

        let result = undefined;
        service.delegationStatus$.subscribe(r => (result = r));
        tick();

        verify(errorPresenterService.nodeErrorToast(anything(), anything())).never();
        expect(result).toBeNull();
    }));

    it('should return null when not delegated', fakeAsync(() => {
        when(votesContractMock.storage()).thenResolve({ assets: { delegates: instance(delegatesBigMap) } });
        when(delegatesBigMap.get(anything())).thenResolve(null);

        const service = getService();
        tick();
        let result = undefined;
        service.delegationStatus$.subscribe(r => (result = r));
        tick();

        verify(errorPresenterService.nodeErrorToast(anything(), anything())).never();
        expect(result).toBeNull();
    }));

    it('should trigger refresh logic', fakeAsync(() => {
        when(votesContractMock.storage()).thenResolve({ assets: { delegates: instance(delegatesBigMap) } });
        when(delegatesBigMap.get(anything())).thenResolve(null);

        const service = getService();
        tick();

        const subscriptionSpy = jasmine.createSpy('subscriptionSpy');
        service.delegationStatus$.subscribe(subscriptionSpy);
        tick();

        expect(subscriptionSpy).toHaveBeenCalledTimes(1);

        service.refresh();
        tick();

        expect(subscriptionSpy).toHaveBeenCalledTimes(2);
        verify(errorPresenterService.nodeErrorToast(anything(), anything())).never();
        verify(logger.error(anything())).never();
    }));

    describe('when delegate exists', () => {
        beforeEach(() => {
            when(votesContractMock.storage()).thenResolve({ assets: { delegates: instance(delegatesBigMap) } });
            when(delegatesBigMap.get(TestWallet.address)).thenResolve('tz1zzz');
        });

        it('should handle delegate query error', fakeAsync(() => {
            const service = getService();

            let result: any = undefined;
            service.delegationStatus$.subscribe(r => (result = r));

            tick();
            const delegateOp = apollo.expectOne(DelegateDocument);
            const rrOp = apollo.expectOne(ReverseRecordListDocument);

            delegateOp.networkError(new Error('dead'));
            rrOp.flushData({ reverseRecords: { edges: [{ node: { id: '1', address: 'tz1zzz', owner: 'tz1zzz' } }] } });

            tick();

            verify(logger.error(anything())).once();
            verify(errorPresenterService.nodeErrorToast(anything(), anything())).never();
            expect(result).toEqual({
                address: 'tz1zzz',
                votingPower: 0,
                reverseRecord: { id: '1', address: 'tz1zzz', owner: 'tz1zzz' },
            });
        }));

        it('should handle reverse record query error', fakeAsync(() => {
            const service = getService();

            let result: any = undefined;
            service.delegationStatus$.subscribe(r => (result = r));
            tick();
            const delegateOp = apollo.expectOne(DelegateDocument);
            const rrOp = apollo.expectOne(ReverseRecordListDocument);

            delegateOp.flushData({ delegates: [{ voting_power: 3 }] });
            rrOp.networkError(new Error('dead'));

            tick();

            verify(logger.error(anything())).once();
            verify(errorPresenterService.nodeErrorToast(anything(), anything())).never();
            expect(result).toEqual({
                address: 'tz1zzz',
                votingPower: 3,
                reverseRecord: null,
            });
        }));

        it('should return data', fakeAsync(() => {
            const service = getService();

            let result: any = undefined;
            service.delegationStatus$.subscribe(r => (result = r));
            tick();
            const delegateOp = apollo.expectOne(DelegateDocument);
            const rrOp = apollo.expectOne(ReverseRecordListDocument);

            delegateOp.flushData({ delegates: [{ voting_power: 3 }] });
            rrOp.flushData({ reverseRecords: { edges: [{ node: { id: '1', address: 'tz1zzz', owner: 'tz1zzz' } }] } });

            tick();

            verify(logger.error(anything())).never();
            verify(errorPresenterService.nodeErrorToast(anything(), anything())).never();
            expect(result).toEqual({
                address: 'tz1zzz',
                votingPower: 3,
                reverseRecord: { id: '1', address: 'tz1zzz', owner: 'tz1zzz' },
            });
        }));
    });

    function getService() {
        return TestBed.inject(DelegationStatusService);
    }
});
