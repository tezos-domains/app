import BigNumber from 'bignumber.js';
import dayjs from 'dayjs';
import { calculateDepositedBalance } from './deposited-balance.calculator';

describe('calculateDepositedBalance', () => {
    beforeEach(() => {
        jasmine.clock().install();
        jasmine.clock().mockDate(dayjs('2023-05-26T10:00:01.000Z').toDate());
    });

    it('should return correct result', () => {
        const result = calculateDepositedBalance({
            poolStorage: {
                lastAccruedRewardsAt: dayjs('2023-05-26T10:00:00.000Z'),
                rewardRate: BigNumber(2 * 1e9),
                poolSize: BigNumber(1000),
                votesMinted: BigNumber(500),
            },
            votesBalance: BigNumber(250),
        });

        expect(result.toNumber()).toEqual(501);
    });

    afterEach(() => {
        jasmine.clock().uninstall();
    });
});
