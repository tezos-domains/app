import { PercentPipe } from '@angular/common';
import { TestBed } from '@angular/core/testing';
import BigNumber from 'bignumber.js';
import { AprPipe } from './apr.pipe';

describe('AprPipe', () => {
    let pipe: AprPipe;

    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [AprPipe, PercentPipe],
        });
        pipe = TestBed.inject(AprPipe);
    });

    const TEST_CASES: [BigNumber | null | undefined, string][] = [
        [undefined, ''],
        [null, ''],
        [BigNumber(-1), '> 100%'],
        [BigNumber(0), '> 100%'],
        [BigNumber(0.41), '41%'],
        [BigNumber(0.1), '10%'],
        [BigNumber(1.1), '> 100%'],
        [BigNumber(0.115), '11.5%'],
        [BigNumber(0.11555), '11.56%'],
    ];

    TEST_CASES.forEach(([input, expected]) => {
        it(`should format ${input} to ${expected}`, () => {
            expect(pipe.transform(input)).toBe(expected);
        });
    });
});
