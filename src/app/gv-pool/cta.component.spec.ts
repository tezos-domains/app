// import { fakeAsync, tick } from '@angular/core/testing';
// import { TranslocoModule } from '@ngneat/transloco';
// import { TestConfig, TestWallet, providers, transloco } from '@td/test';
// import BigNumber from 'bignumber.js';
// import { MockBuilder, MockRender, MockedComponentFixture } from 'ng-mocks';
// import { BehaviorSubject, Subject, of } from 'rxjs';
// import { mock, when } from 'ts-mockito';
// import { ClaimInfoService } from '../airdrop/claim-info.service';
// import { AirdropClaim, AirdropClaimState, ClaimStatus } from '../airdrop/models';
// import { Configuration } from '../configuration';
// import { SharedModule } from '../shared/shared.module';
// import { TruncatePipe } from '../shared/truncate.pipe';
// import { TezosWallet } from '../tezos/models';
// import { TezosService } from '../tezos/tezos.service';
// import { PoolCtaComponent } from './cta.component';
// import { GovPoolModule } from './gov-pool.module';
// import { PoolStats } from './models';
// import { PoolStatsService } from './pool-stats.service';

// // TODO - for some reason focusing these tests - they all pass. Remove the focus and they all fail :cry:

// fdescribe('PoolCtaComponent', () => {
//     let component: PoolCtaComponent;
//     let fixture: MockedComponentFixture<PoolCtaComponent, Partial<PoolCtaComponent>>;

//     let poolStatsMock: PoolStatsService;
//     let claimInfoMock: ClaimInfoService;
//     let tezosServiceMock: TezosService;
//     let wallet$: BehaviorSubject<TezosWallet | null>;
//     let claimStatus$: Subject<AirdropClaimState>;

//     beforeEach(() => {
//         poolStatsMock = mock(PoolStatsService);
//         claimInfoMock = mock(ClaimInfoService);
//         tezosServiceMock = mock(TezosService);

//         wallet$ = new BehaviorSubject(null);
//         claimStatus$ = new Subject();

//         when(tezosServiceMock.activeWallet).thenReturn(wallet$);
//         when(poolStatsMock.stats$).thenReturn(of(null));
//         when(claimInfoMock.canClaimTokens(TestWallet.address)).thenReturn(claimStatus$);

//         return MockBuilder(PoolCtaComponent, GovPoolModule)
//             .mock(SharedModule)
//             .provide([...providers(poolStatsMock, claimInfoMock, tezosServiceMock), { provide: Configuration, useValue: TestConfig }])
//             .keep(TranslocoModule, { export: true })
//             .keep(TruncatePipe)
//             .keep(transloco(), { export: true });
//     });

//     beforeEach(fakeAsync(() => {
//         init();
//     }));

//     describe('when not authenticated', () => {
//         it('should be visible ', fakeAsync(() => {
//             let result: any = undefined;
//             const unsub = component.stats$.subscribe(r => (result = r));

//             tick(10);

//             expect(result).toBeNull();
//             expect(component.hidden).toBeFalse();
//             unsub.unsubscribe();
//         }));
//     });

//     describe('when authenticated', () => {
//         beforeEach(() => {
//             wallet$.next(TestWallet);
//         });

//         it('should be visible if no stats', fakeAsync(() => {
//             let result: any = undefined;

//             const unsub = component.stats$.subscribe(r => (result = r));
//             claimStatus$.next({ claim: claim(), status: ClaimStatus.Claimed });
//             tick(10);

//             expect(result).toBeNull();
//             expect(component.hidden).toBeFalse();
//             unsub.unsubscribe();
//         }));

//         it('should be visible if user has no tokens', fakeAsync(() => {
//             const poolStats = <PoolStats>{ tedBalance: BigNumber(0), votesBalance: BigNumber(0) };
//             let result: any = undefined;
//             when(poolStatsMock.stats$).thenReturn(of(poolStats));

//             const unsub = component.stats$.subscribe(r => (result = r));
//             claimStatus$.next({ claim: claim(), status: ClaimStatus.Claimed });
//             tick(10);

//             expect(result).toBe(poolStats);
//             expect(component.hidden).toBeFalse();
//             unsub.unsubscribe();
//         }));

//         it('should be hidden if user has claimable tokens', fakeAsync(() => {
//             const poolStats = <PoolStats>{ tedBalance: BigNumber(0), votesBalance: BigNumber(0) };
//             let result: any = undefined;

//             when(poolStatsMock.stats$).thenReturn(of(poolStats));

//             const unsub = component.stats$.subscribe(r => (result = r));
//             claimStatus$.next({ claim: claim(), status: ClaimStatus.Claimable });
//             tick(10);

//             expect(result).toBe(poolStats);
//             expect(component.hidden).toBeTrue();
//             unsub.unsubscribe();
//         }));

//         it('should be hidden if user has tokens', fakeAsync(() => {
//             const poolStats = <PoolStats>{ tedBalance: BigNumber(1), votesBalance: BigNumber(0) };
//             let result: any = undefined;

//             when(poolStatsMock.stats$).thenReturn(of(poolStats));

//             const unsub = component.stats$.subscribe(r => (result = r));
//             claimStatus$.next({ claim: claim(), status: ClaimStatus.Claimed });
//             tick(10);

//             expect(result).toBe(poolStats);
//             expect(component.hidden).toBeTrue();
//             unsub.unsubscribe();
//         }));
//     });

//     function init() {
//         fixture = MockRender(PoolCtaComponent);
//         component = fixture.point.componentInstance;
//         tick(10);
//         fixture.detectChanges();
//     }

//     function claim(): AirdropClaim {
//         return { amount: BigNumber(10), owner: TestWallet.address, airdrop: true, from: new Date(), meta: <any>{}, proof: [] };
//     }
// });
