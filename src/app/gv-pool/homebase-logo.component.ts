import { Component } from '@angular/core';

@Component({
    selector: 'td-homebase-logo',
    templateUrl: './homebase-logo.component.html',
    styleUrls: ['./homebase-logo.component.scss'],
})
export class HomebaseLogoComponent {}
