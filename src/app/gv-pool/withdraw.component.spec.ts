import { fakeAsync } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { TranslocoModule } from '@ngneat/transloco';
import { TestWallet, providers, transloco } from '@td/test';
import { getByDisplayValue, getByTestId, getByText } from '@testing-library/angular';
import BigNumber from 'bignumber.js';
import { MockBuilder, MockRender, MockedComponentFixture } from 'ng-mocks';
import { anything, capture, instance, mock, verify, when } from 'ts-mockito';
import { GovActionImpl, GovActionService } from '../airdrop/gov-action.service';
import { SharedModule } from '../shared/shared.module';
import { TruncatePipe } from '../shared/truncate.pipe';
import { GovPoolModule } from './gov-pool.module';
import { PoolStats } from './models';
import { WithdrawComponent } from './withdraw.component';

describe('WithdrawComponent', () => {
    let component: WithdrawComponent;
    let fixture: MockedComponentFixture<WithdrawComponent, Partial<WithdrawComponent>>;
    let dialogRefMock: MatDialogRef<WithdrawComponent>;
    let govActionMock: GovActionService;
    let govActionImpl: GovActionImpl;

    beforeEach(() => {
        dialogRefMock = mock<MatDialogRef<WithdrawComponent>>(MatDialogRef);

        govActionMock = mock(GovActionService);
        govActionImpl = mock(GovActionImpl);

        when(govActionMock.q).thenReturn(instance(govActionImpl));
        when(govActionImpl.withdraw(anything(), TestWallet.address)).thenReturn(instance(govActionImpl));

        return MockBuilder(WithdrawComponent, GovPoolModule)
            .mock(SharedModule)
            .provide([providers(dialogRefMock, govActionMock)])
            .keep(ReactiveFormsModule)
            .keep(FormsModule)
            .keep(TranslocoModule, { export: true })
            .keep(TruncatePipe)
            .keep(transloco(), { export: true });
    });

    beforeEach(fakeAsync(() => {
        init();
    }));

    it('set maxWithdraw', () => {
        expect(component.maxWithdraw).toBe(2.335001);
    });

    it('should update maxAmount', () => {
        component.form.controls.amount.setValue(1);
        fixture.detectChanges();

        expect(getByDisplayValue(fixture.nativeElement, '1')).toBeDefined();

        getByTestId(fixture.nativeElement, 'max').click();
        fixture.detectChanges();

        expect(getByDisplayValue(fixture.nativeElement, '2.335001')).toBeDefined();
    });

    it('should withdraw real max amount', () => {
        getByTestId(fixture.nativeElement, 'max').click();
        getByText(fixture.nativeElement, 'Withdraw').click();

        const [amount] = capture(govActionImpl.withdraw).last();
        expect(amount.isEqualTo(BigNumber(1.11 * 1e6))).toBeTrue();

        verify(govActionImpl.withdraw(anything(), TestWallet.address)).once();
        verify(govActionImpl.send()).once();
    });

    it('should convert ted amount to votes before calling transfer', () => {
        component.form.controls.amount.setValue(1);
        fixture.detectChanges();

        getByText(fixture.nativeElement, 'Withdraw').click();

        const [amount] = capture(govActionImpl.withdraw).last();
        expect(amount.isEqualTo(BigNumber(5 * 1e5))).toBeTrue();

        verify(govActionImpl.withdraw(anything(), TestWallet.address)).once();
        verify(govActionImpl.send()).once();
    });

    describe('cancel()', () => {
        it('should close the dialog', fakeAsync(() => {
            component.cancel();

            verify(dialogRefMock.close()).called();
        }));
    });

    function init() {
        fixture = MockRender(
            WithdrawComponent,
            {},
            {
                providers: [
                    {
                        provide: MAT_DIALOG_DATA,
                        useValue: <PoolStats>{
                            owner: TestWallet.address,
                            poolAverageAPR: BigNumber(0.3),
                            poolCurrentAPR: BigNumber(0.38),
                            poolSize: BigNumber(100 * 1e6),
                            tedBalance: BigNumber(2.335001 * 1e6),
                            sharesMinted: BigNumber(50 * 1e6),
                            depositedTedBalance: BigNumber(2.335001 * 1e6),
                            votesBalance: BigNumber(1.11 * 1e6),
                        },
                    },
                ],
                detectChanges: true,
            }
        );

        component = fixture.point.componentInstance;
        fixture.detectChanges();
    }
});
