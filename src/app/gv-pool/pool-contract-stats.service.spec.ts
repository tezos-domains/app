import { TestBed, fakeAsync, tick } from '@angular/core/testing';
import { ContractAbstraction, TezosToolkit, Wallet } from '@taquito/taquito';
import { TestConfig, TestNetwork, providers } from '@td/test';
import BigNumber from 'bignumber.js';
import dayjs from 'dayjs';
import { ReplaySubject, of } from 'rxjs';
import { instance, mock, when } from 'ts-mockito';
import { Configuration } from '../configuration';
import { TezosToolkitService } from '../tezos/integration/tezos-toolkit.service';
import { TezosWallet } from '../tezos/models';
import { PoolContractStatsService, PoolContractStorage } from './pool-contract-stats.service';

describe('PoolContractStatsService', () => {
    let tezosToolkitServiceMock: TezosToolkitService;
    let tezosToolkitMock: TezosToolkit;
    let walletMock: Wallet;
    let service: PoolContractStatsService;

    let contractMock: ContractAbstraction<Wallet>;

    let wallet$: ReplaySubject<TezosWallet | null>;

    beforeEach(() => {
        tezosToolkitServiceMock = mock(TezosToolkitService);
        tezosToolkitMock = mock(TezosToolkit);
        walletMock = mock(Wallet);
        contractMock = mock(ContractAbstraction);

        wallet$ = new ReplaySubject<TezosWallet | null>(1);

        when(tezosToolkitServiceMock.current).thenReturn(of(instance(tezosToolkitMock)));
        when(tezosToolkitMock.wallet).thenReturn(instance(walletMock));
        when(walletMock.at(TestNetwork.tedPoolContract)).thenResolve(instance(contractMock));

        when(contractMock.storage()).thenResolve({
            pool_size: BigNumber(10),
            reward_rate: BigNumber(1),
            votes_minted: BigNumber(100000000),
            last_accrued_rewards_at: '2022-01-01T00:00:00Z',
        });

        TestBed.configureTestingModule({
            providers: [...providers(tezosToolkitServiceMock), { provide: Configuration, useValue: TestConfig }],
        });

        service = TestBed.inject(PoolContractStatsService);
    });

    it('should fetch data from contract', fakeAsync(() => {
        let result: PoolContractStorage | null | undefined = undefined;

        const sub = service.storage$.subscribe(x => (result = x));
        wallet$.next(null);
        tick();

        sub.unsubscribe();

        expect(result!).toEqual({
            poolSize: BigNumber(10),
            rewardRate: BigNumber(1),
            votesMinted: BigNumber(100000000),
            lastAccruedRewardsAt: dayjs('2022-01-01T00:00:00Z'),
        });
    }));
});
