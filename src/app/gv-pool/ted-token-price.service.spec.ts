import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { TestBed, discardPeriodicTasks, fakeAsync, tick } from '@angular/core/testing';
import { TestConfig, provider } from '@td/test';
import BigNumber from 'bignumber.js';
import { capture, mock } from 'ts-mockito';
import { Logger } from '../browser/logger';
import { Configuration } from '../configuration';
import { TEDTokenPriceService } from './ted-token-price.service';

describe('TokenPriceService', () => {
    let httpMock: HttpTestingController;
    let logger: Logger;

    let service: TEDTokenPriceService;

    beforeEach(() => {
        logger = mock(Logger);
        TestBed.configureTestingModule({
            providers: [provider(logger), { provide: Configuration, useValue: TestConfig }],
            imports: [HttpClientTestingModule],
        });

        httpMock = TestBed.inject(HttpTestingController);
    });

    afterEach(() => httpMock.verify());

    it('should return null if tedExchangeUrl not defined', fakeAsync(() => {
        service = TestBed.inject(TEDTokenPriceService);
        tick();

        let result: BigNumber | null | undefined = undefined;
        const sub = service.convert(BigNumber(1)).subscribe(x => (result = x.price));
        tick();
        sub.unsubscribe();

        expect(result).toBeNull();

        // TEDTokenPriceService shouldn't loose it's value if it has no subscribers.
        // because of the `shareReplay` options, the observable never completes and we're left with a :
        // "1 periodic timer(s) still in the queue." - in this case we ignore it.
        discardPeriodicTasks();
    }));

    it('should do the conversion', fakeAsync(() => {
        TestConfig.governance.tedExchangeUrl = 'https://api.gateio.ws/api/v4/spot/tickers?currency_pair=TED_USDT';
        service = TestBed.inject(TEDTokenPriceService);

        tick();

        let result: BigNumber | null | undefined = undefined;
        const sub = service.convert(BigNumber(100 * 1e6)).subscribe(x => (result = x.price));
        tick();

        httpMock.expectOne(`${TestConfig.governance.tedExchangeUrl}`).flush([{ last: 0.1 }]);
        sub.unsubscribe();

        expect(result!.toNumber()).toEqual(10);
        discardPeriodicTasks();
    }));

    it('should handle exception', fakeAsync(() => {
        TestConfig.governance.tedExchangeUrl = 'https://api.gateio.ws/api/v4/spot/tickers?currency_pair=TED_USDT';
        service = TestBed.inject(TEDTokenPriceService);

        tick();

        let result: BigNumber | null | undefined = undefined;
        const sub = service.convert(BigNumber(100 * 1e6)).subscribe(x => (result = x.price));
        tick();

        httpMock.expectOne(`${TestConfig.governance.tedExchangeUrl}`).flush(null, { status: 500, statusText: 'Error' });
        sub.unsubscribe();

        const [logArgs] = capture(logger.error).last();

        expect(logArgs).toContain('[TEDTokenPriceService] Unable to fetch price from GateIO');
        expect(result).toBeNull();
        discardPeriodicTasks();
    }));
});
