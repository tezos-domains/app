import { inject, Injectable } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { StorageMap } from '@ngx-pwa/local-storage';
import dayjs from 'dayjs';
import { first, map, shareReplay } from 'rxjs';
import { AffiliateStoreInfo, StorageSchema } from '../utils/storage';

@Injectable({
    providedIn: 'root',
})
export class AffiliateService {
    private readonly AFFILIATE_PARAM_KEY = 'aid';

    private readonly router = inject(Router);
    private readonly route = inject(ActivatedRoute);
    private readonly storage = inject(StorageMap);

    interceptUrlWithAffiliate(): void {
        this.route.queryParams.subscribe(params => {
            const affiliateId = params[this.AFFILIATE_PARAM_KEY];
            if (affiliateId) {
                this.storeAffiliateId(affiliateId);

                const updatedQueryParams = { ...params };
                delete updatedQueryParams[this.AFFILIATE_PARAM_KEY];

                this.router.navigate([], {
                    relativeTo: this.route,
                    queryParams: updatedQueryParams,
                    replaceUrl: true,
                });
            }
        });
    }

    readonly affiliateId$ = this.storage.watch<AffiliateStoreInfo>(StorageSchema.affiliate.key, StorageSchema.affiliate.schema).pipe(
        map(value => {
            if (!value) {
                return null;
            }

            if (value.expiration && dayjs(value.expiration).isAfter(dayjs())) {
                return value.code;
            }

            return null;
        }),
        shareReplay({ refCount: true, bufferSize: 1 })
    );

    private storeAffiliateId(affiliateId: string): void {
        this.storage
            .set(StorageSchema.affiliate.key, { code: affiliateId, expiration: dayjs().add(1, 'day').toISOString() }, StorageSchema.affiliate.schema)
            .pipe(first())
            .subscribe();
    }
}
