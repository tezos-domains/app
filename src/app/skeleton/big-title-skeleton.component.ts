import { Component } from '@angular/core';

@Component({
    selector: 'td-big-title-skeleton',
    templateUrl: './big-title-skeleton.component.html',
    styles: [
        `
            :host {
                display: block;
            }
        `,
    ],
})
export class BigTitleSkeletonComponent {}
