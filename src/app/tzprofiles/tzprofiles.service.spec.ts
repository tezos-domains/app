import { TestBed, fakeAsync, tick } from '@angular/core/testing';
import { ApolloTestingController, ApolloTestingModule } from 'apollo-angular/testing';

import { TzprofilesService } from './tzprofiles.service';
import { TestWallet, TezosAddress } from '../../test/data';
import { ProfileDocument } from './tzprofiles-graphql.generated';

describe('TzprofilesService', () => {
    let service: TzprofilesService;
    let apollo: ApolloTestingController;
    let spy: jasmine.Spy;

    const response = {
        tzprofiles_by_pk: {
            account: TestWallet.address,
            valid_claims: [
                [
                    'kepler://zCT5htkdz4dGia3VZiJTa2x1nP7hWEyTmrUgZ6RfFodfwADFJXj1/zb38S6ZBrbQqCSpy9Su6WSVdSm1iDPHNV6Lf8RpoVQKTDX8m8',
                    '{"@context":["https://www.w3.org/2018/credentials/v1","https://tzprofiles.com/2021/ethereum-address-control-v1.jsonld"],"id":"urn:uuid:53106a83-0435-4de2-8988-aeea71bce9a7","type":["VerifiableCredential","EthereumAddressControl"],"credentialSubject":{"address":"0x821af3f0a62eba3cef3c973cb7e7d495c34eef7b","sameAs":"tz1bMDsYDyC5e6nuktKhVY23Tr2125LRQBU4"},"issuer":"did:pkh:eth:0x821af3f0a62eba3cef3c973cb7e7d495c34eef7b","issuanceDate":"2021-07-08T12:06:22.610Z","proof":{"@context":"https://demo.spruceid.com/ld/eip712sig-2021/v0.1.jsonld","type":"EthereumEip712Signature2021","proofPurpose":"assertionMethod","proofValue":"0xd4b78637fb91a2d159a62cc89dd0c71acbcd6fa95d29f4acb6f4956d76fdd9f2038ba9da03f8f625f5b1cb129c6d1f807a55f3ed1ffbea96d2e73bc3809f4c6c1c","verificationMethod":"did:pkh:eth:0x821af3f0a62eba3cef3c973cb7e7d495c34eef7b#Recovery2020","created":"2021-07-08T12:06:22.667Z","eip712Domain":{"domain":{"name":"Tezos Profiles Verifiable Credential"},"messageSchema":{"CredentialSubject":[{"name":"address","type":"string"},{"name":"sameAs","type":"string"}],"EIP712Domain":[{"name":"name","type":"string"}],"Proof":[{"name":"@context","type":"string"},{"name":"verificationMethod","type":"string"},{"name":"created","type":"string"},{"name":"proofPurpose","type":"string"},{"name":"type","type":"string"}],"VerifiableCredential":[{"name":"@context","type":"string[]"},{"name":"id","type":"string"},{"name":"type","type":"string[]"},{"name":"issuer","type":"string"},{"name":"issuanceDate","type":"string"},{"name":"credentialSubject","type":"CredentialSubject"},{"name":"proof","type":"Proof"}]},"primaryType":"VerifiableCredential"}}}',
                    'VerifiableCredential',
                ],
                [
                    'kepler://zCT5htkdz4dGia3VZiJTa2x1nP7hWEyTmrUgZ6RfFodfwADFJXj1/zb38S8iQ9L3Mcahack8Lw64xpCXehh2fZW9xyjW4JsyYqjko1',
                    '{"@context":["https://www.w3.org/2018/credentials/v1",{"TwitterVerification":"https://tzprofiles.com/TwitterVerification","sameAs":"http://schema.org/sameAs","TwitterVerificationPublicTweet":{"@context":{"@protected":true,"@version":1.1,"handle":"https://tzprofiles.com/handle","timestamp":{"@id":"https://tzprofiles.com/timestamp","@type":"http://www.w3.org/2001/XMLSchema#dateTime"},"tweetId":"https://tzprofiles.com/tweetId"},"@id":"https://tzprofiles.com/TwitterVerificationPublicTweet"}}],"id":"urn:uuid:22cbb26f-1129-4e92-960b-32d01a9b4a6d","type":["VerifiableCredential","TwitterVerification"],"credentialSubject":{"id":"did:pkh:tz:tz1bMDsYDyC5e6nuktKhVY23Tr2125LRQBU4","sameAs":"https://twitter.com/necros"},"issuer":"did:web:tzprofiles.com","issuanceDate":"2021-07-08T12:06:16.898Z","proof":{"type":"Ed25519Signature2018","proofPurpose":"assertionMethod","verificationMethod":"did:web:tzprofiles.com#controller","created":"2021-07-08T12:06:16.898Z","jws":"eyJhbGciOiJFZERTQSIsImNyaXQiOlsiYjY0Il0sImI2NCI6ZmFsc2V9..B6KHP1gBgf3KGUcmCoeb0AH-7yTowd7Wbu7ZUfkZD-6Si6_MY56N79laT7n5yfyEVyOaIoDOt1oNbUXQSq1kBA"},"evidence":{"type":["TwitterVerificationPublicTweet"],"timestamp":"2021-07-08T12:06:16.898Z","handle":"necros","tweetId":"1413107122913652739"}}',
                    'VerifiableCredential',
                ],
                [
                    'kepler://zCT5htkdz4dGia3VZiJTa2x1nP7hWEyTmrUgZ6RfFodfwADFJXj1/zb38SDfsynpZS9t5JLLvmEkxx2immK3a1Rv9iwWDdxQzgpXyQ',
                    '{"@context":["https://www.w3.org/2018/credentials/v1",{"BasicProfile":"https://tzprofiles.com/BasicProfile","alias":"https://schema.org/name","description":"https://schema.org/description","website":"https://schema.org/url","logo":"https://schema.org/logo"}],"id":"urn:uuid:209423bb-fb62-47b6-8bdc-4129683c31ba","type":["VerifiableCredential","BasicProfile"],"credentialSubject":{"id":"did:pkh:tz:tz1bMDsYDyC5e6nuktKhVY23Tr2125LRQBU4","alias":"Necroskillz","website":"http://www.necronet.org","logo":"https://ucac7c83c4fb3d64769af7b88442.previews.dropboxusercontent.com/p/thumb/ABN2LuY5Gc_iQbsIYw5mUgiIEacl9U6_88WWTJDe8gxyPQFwt3LB9G_0ajQ6ozQbmj7jQn34fQfNGf2E2OxUxtXUZImeI20ueiw0w4m639xO0k3BzjRZRLfj2GVGJsVE5zpYvHAITytVpOqbgikb5YfQrGspnC5PbH_J6xLM6PlDn0yuTecA4fvAK4YqT_TZA7dsJd_FbblWT1HpUIkLsNtsr8AQ-qsZe0fCOQKzUjk_e7R1iolev_5N_2XYBs7XfKgPR8227X5URekPo6LGYPMKVckVofa7a49ocknMzxCdhdy8yGnOaZVib4ZKip2jHbn4Ee5jHb2vl06VHJ6RZhDtCG7mrGcFYQ33JMj7dnUSURWDIPUYIHQ92DfFPcqKAJloDpNAlL8FKlhPbZnDdy2L/p.png?fv_content=true&size_mode=5","description":"Literal god of software engineering"},"issuer":"did:pkh:tz:tz1bMDsYDyC5e6nuktKhVY23Tr2125LRQBU4","issuanceDate":"2021-07-08T12:03:42.361Z","proof":{"@context":{"TezosMethod2021":"https://w3id.org/security#TezosMethod2021","TezosSignature2021":{"@context":{"@protected":true,"@version":1.1,"challenge":"https://w3id.org/security#challenge","created":{"@id":"http://purl.org/dc/terms/created","@type":"http://www.w3.org/2001/XMLSchema#dateTime"},"domain":"https://w3id.org/security#domain","expires":{"@id":"https://w3id.org/security#expiration","@type":"http://www.w3.org/2001/XMLSchema#dateTime"},"id":"@id","nonce":"https://w3id.org/security#nonce","proofPurpose":{"@context":{"@protected":true,"@version":1.1,"assertionMethod":{"@container":"@set","@id":"https://w3id.org/security#assertionMethod","@type":"@id"},"authentication":{"@container":"@set","@id":"https://w3id.org/security#authenticationMethod","@type":"@id"},"id":"@id","type":"@type"},"@id":"https://w3id.org/security#proofPurpose","@type":"@vocab"},"proofValue":"https://w3id.org/security#proofValue","publicKeyJwk":{"@id":"https://w3id.org/security#publicKeyJwk","@type":"@json"},"type":"@type","verificationMethod":{"@id":"https://w3id.org/security#verificationMethod","@type":"@id"}},"@id":"https://w3id.org/security#TezosSignature2021"}},"type":"TezosSignature2021","proofPurpose":"assertionMethod","proofValue":"edsigtYNqjTr5qv56hDpv7MopeFNqLTwLUPbDm4995ZdJP6erdNcDdy4ojeVMqphiPYnW1n82kHMb99XLEJ8FnDAoxN5h9xYtTb","verificationMethod":"did:pkh:tz:tz1bMDsYDyC5e6nuktKhVY23Tr2125LRQBU4#TezosMethod2021","created":"2021-07-08T12:03:42.362Z","publicKeyJwk":{"alg":"EdBlake2b","crv":"Ed25519","kty":"OKP","x":"xfq00rt-LymjhszwuBr2dIC12MTh47cvmi68oXKXq7Q"}}}',
                    'VerifiableCredential',
                ],
            ],
        },
    };

    beforeEach(() => {
        spy = jasmine.createSpy();

        TestBed.configureTestingModule({
            imports: [ApolloTestingModule.withClients(['tzprofiles'])],
        });

        service = TestBed.inject(TzprofilesService);

        apollo = TestBed.inject(ApolloTestingController);
    });

    describe('getProfile', () => {
        it('should parse data from claims', fakeAsync(() => {
            service.getProfile(TezosAddress).subscribe(spy);

            const op = apollo.expectOne(ProfileDocument);

            expect(op.operation.variables.address).toBe(TezosAddress);

            op.flushData(response);
            tick();

            expect(spy).toHaveBeenCalledWith({
                alias: 'Necroskillz',
                description: 'Literal god of software engineering',
                ethAddress: '0x821af3f0a62eba3cef3c973cb7e7d495c34eef7b',
                logo: 'https://ucac7c83c4fb3d64769af7b88442.previews.dropboxusercontent.com/p/thumb/ABN2LuY5Gc_iQbsIYw5mUgiIEacl9U6_88WWTJDe8gxyPQFwt3LB9G_0ajQ6ozQbmj7jQn34fQfNGf2E2OxUxtXUZImeI20ueiw0w4m639xO0k3BzjRZRLfj2GVGJsVE5zpYvHAITytVpOqbgikb5YfQrGspnC5PbH_J6xLM6PlDn0yuTecA4fvAK4YqT_TZA7dsJd_FbblWT1HpUIkLsNtsr8AQ-qsZe0fCOQKzUjk_e7R1iolev_5N_2XYBs7XfKgPR8227X5URekPo6LGYPMKVckVofa7a49ocknMzxCdhdy8yGnOaZVib4ZKip2jHbn4Ee5jHb2vl06VHJ6RZhDtCG7mrGcFYQ33JMj7dnUSURWDIPUYIHQ92DfFPcqKAJloDpNAlL8FKlhPbZnDdy2L/p.png?fv_content=true&size_mode=5',
                website: 'http://www.necronet.org',
                verified: [{ value: 'https://twitter.com/necros', type: 'twitter' }],
                account: TestWallet.address,
            });
        }));

        it('should return null if profile has no valid claims', fakeAsync(() => {
            service.getProfile(TezosAddress).subscribe(spy);

            apollo.expectOne(ProfileDocument).flushData({ tzprofiles_by_pk: { valid_claims: [] } });
            tick();

            expect(spy).toHaveBeenCalledWith(null);
        }));

        it('should return null if profile doesnt exist', fakeAsync(() => {
            service.getProfile(TezosAddress).subscribe(spy);

            apollo.expectOne(ProfileDocument).flushData({ tzprofiles_by_pk: null });
            tick();

            expect(spy).toHaveBeenCalledWith(null);
        }));
    });
});
