import { Router, NavigationStart } from '@angular/router';
import { mock, instance, when } from 'ts-mockito';

import { Subject } from 'rxjs';
import { FilterHistoryStateService } from './filter-history-state.service';

describe('FilterHistoryStateService', () => {
    let service: FilterHistoryStateService;
    let routerMock: Router;
    let events: Subject<NavigationStart>;

    beforeEach(() => {
        routerMock = mock(Router);
        events = new Subject();

        when(routerMock.events).thenReturn(events);

        service = new FilterHistoryStateService(instance(routerMock));
    });

    it('should replace array in state', () => {
        events.next(new NavigationStart(1, '', 'imperative'));

        expect(service.get()).toEqual({});

        service.merge({ a: [1, 2, 3] });

        expect(service.get()).toEqual({ a: [1, 2, 3] });

        service.merge({ a: [3, 4, 5] });

        expect(service.get()).toEqual({ a: [3, 4, 5] });
    });
});
