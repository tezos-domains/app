import { WindowRef } from './window-ref';

describe('WindowRef', () => {
    it('should return window object', () => {
        const windowRef = new WindowRef();

        expect(windowRef.nativeWindow).toBe(window);
    });
});
