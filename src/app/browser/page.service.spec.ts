import { TestBed } from '@angular/core/testing';
import { Title } from '@angular/platform-browser';
import { transloco, provider, TestConfig } from '@td/test';
import { mock, verify } from 'ts-mockito';

import { PageService } from './page.service';
import { Configuration } from '../configuration';

describe('PageService', () => {
    let service: PageService;
    let titleMock: Title;

    beforeEach(() => {
        titleMock = mock(Title);
        TestBed.configureTestingModule({
            imports: [transloco()],
            providers: [PageService, provider(titleMock), { provide: Configuration, useValue: TestConfig }],
        });

        service = TestBed.inject(PageService);
    });

    describe('setDefaultTitle()', () => {
        it('should set title to app name', () => {
            service.setDefaultTitle();

            verify(titleMock.setTitle(TestConfig.appName)).called();
        });
    });

    describe('setTitle()', () => {
        it('should set translated title', () => {
            service.setTitle('my-domains');

            verify(titleMock.setTitle(`My Domains - ${TestConfig.appName}`)).called();
        });

        it('should set translated title with parameters', () => {
            service.setTitle('domain', { name: 'necroskillz.tez' });

            verify(titleMock.setTitle(`necroskillz.tez - ${TestConfig.appName}`)).called();
        });
    });
});
