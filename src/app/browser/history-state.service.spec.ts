import { Router, NavigationStart } from '@angular/router';
import { mock, instance, when } from 'ts-mockito';

import { HistoryStateService } from './history-state.service';
import { Subject } from 'rxjs';

describe('HistoryStateService', () => {
    let service: HistoryStateService;
    let routerMock: Router;
    let events: Subject<NavigationStart>;

    beforeEach(() => {
        routerMock = mock(Router);
        events = new Subject();

        when(routerMock.events).thenReturn(events);

        service = new HistoryStateService(instance(routerMock));
    });

    it('should store state data based on navigation events', () => {
        events.next(new NavigationStart(1, '', 'imperative'));

        expect(service.get()).toEqual({});

        service.merge({ a: 1 });

        expect(service.get()).toEqual({ a: 1 });

        events.next(new NavigationStart(2, '', 'imperative'));

        expect(service.get()).toEqual({});

        service.merge({ a: 3, b: 2 });
        service.merge({ a: 2, c: 2 });

        expect(service.get()).toEqual({ a: 2, b: 2, c: 2 });

        events.next(new NavigationStart(3, '', 'popstate', { navigationId: 1 }));

        expect(service.get()).toEqual({ a: 1 });

        events.next(new NavigationStart(4, '', 'popstate', { navigationId: 2 }));

        expect(service.get()).toEqual({ a: 2, b: 2, c: 2 });

        events.next(new NavigationStart(5, '', 'imperative'));

        expect(service.get()).toEqual({});

        // 1 was replaced by 3
        events.next(new NavigationStart(6, '', 'popstate', { navigationId: 1 }));

        expect(service.get()).toEqual({});

        events.next(new NavigationStart(7, '', 'popstate', { navigationId: 3 }));

        expect(service.get()).toEqual({ a: 1 });
    });
});
