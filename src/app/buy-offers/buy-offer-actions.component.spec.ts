import { getDomain, providers, TestWallet, TestWallet3, transloco } from '@td/test';
import { MockBuilder, MockedComponentFixture, MockRender, ngMocks } from 'ng-mocks';
import { anyFunction, anything, mock, verify, when } from 'ts-mockito';
import { AppService } from '../app-service';
import { BuyOfferRecord } from '../graphql/buy-offer-table-data-source';
import { OfferState } from '../graphql/graphql.generated';
import { OverlayService } from '../overlay.service';
import { SharedModule } from '../shared/shared.module';
import { TezosWallet } from '../tezos/models';

import { BuyOfferActionsComponent } from './buy-offer-actions.component';
import { BuyOffersModule } from './buy-offers.module';
import { IgnoredOffersService } from './ignored-offers.service';

describe('BuyOfferActionsComponent', () => {
    let component: BuyOfferActionsComponent;
    let fixture: MockedComponentFixture<BuyOfferActionsComponent, Partial<BuyOfferActionsComponent>>;

    let overlayServiceMock: OverlayService;
    let ignoredOfferServiceMock: IgnoredOffersService;
    let appServiceMock: AppService;

    beforeEach(() => {
        overlayServiceMock = mock(OverlayService);
        ignoredOfferServiceMock = mock(IgnoredOffersService);
        appServiceMock = mock(AppService);

        return MockBuilder(BuyOfferActionsComponent, BuyOffersModule)
            .provide(providers(overlayServiceMock, ignoredOfferServiceMock, appServiceMock))
            .keep(transloco(), { export: true })
            .mock(SharedModule);
    });

    describe('make offer button', () => {
        it('should appear', () => {
            init({ domain: getDomain('with-operators.tez'), wallet: null });
            const buttons = ngMocks.findAll('button');

            expect(buttons.length).toBe(1);
            expect(component.canMakeOffer).toBeTrue();
        });

        it('should trigger app connect if no wallet is connected', () => {
            init({ domain: getDomain('with-operators.tez'), wallet: null });

            ngMocks.click('button');

            verify(appServiceMock.openConnect(anyFunction())).called();
        });

        it('should trigger place offer popup', () => {
            const domain = getDomain('with-operators.tez');
            init({ domain, wallet: TestWallet });

            ngMocks.click('button');

            verify(overlayServiceMock.openPlaceBuyOffer(domain, anyFunction())).called();
        });
    });

    describe('withdraw offer button', () => {
        it('should appear when offer created by current wallet', () => {
            init({ buyOffer: <BuyOfferRecord>{ buyer: TestWallet.address, state: OfferState.Active }, wallet: TestWallet });
            const buttons = ngMocks.findAll('button');

            expect(buttons.length).toBe(1);
            expect(component.canWithdrawOffer).toBeTrue();
        });

        it('should NOT appear when offer created by different wallet', () => {
            init({ buyOffer: <BuyOfferRecord>{ buyer: TestWallet3.address, state: OfferState.Active }, wallet: TestWallet });
            const buttons = ngMocks.findAll('button');

            expect(buttons.length).toBe(0);
        });

        it('should trigger remove offer popup', () => {
            const buyOffer = <BuyOfferRecord>{ buyer: TestWallet.address, state: OfferState.Active };
            init({ buyOffer, wallet: TestWallet });

            ngMocks.click('button');

            verify(overlayServiceMock.openRemoveBuyOffer(buyOffer, anyFunction())).called();
        });
    });

    describe('accept offer button', () => {
        it('should appear when offer is for current wallet', () => {
            init({ buyOffer: <BuyOfferRecord>{ buyer: TestWallet3.address, seller: TestWallet.address, state: OfferState.Active }, wallet: TestWallet });
            const buttons = ngMocks.findAll('button');

            expect(buttons.length).toBe(1);
            expect(component.canAcceptOffer).toBeTrue();
        });

        it('should NOT appear when offer is for different wallet', () => {
            init({ buyOffer: <BuyOfferRecord>{ buyer: TestWallet3.address, seller: TestWallet3.address, state: OfferState.Active }, wallet: TestWallet });
            const buttons = ngMocks.findAll('button');

            expect(buttons.length).toBe(0);
            expect(component.canAcceptOffer).toBeFalse();
        });

        it('should trigger execute offer popup', () => {
            const buyOffer = <BuyOfferRecord>{ buyer: TestWallet3.address, seller: TestWallet.address, state: OfferState.Active };
            init({ buyOffer, wallet: TestWallet });

            ngMocks.click('button');

            verify(overlayServiceMock.openExecuteBuyOffer(buyOffer, anyFunction())).called();
        });
    });

    describe('with no connected wallet', () => {
        it('should not show any buttons', () => {
            init({ buyOffer: <BuyOfferRecord>{ buyer: TestWallet.address, state: OfferState.Active }, wallet: null });
            const buttons = ngMocks.findAll('button');

            expect(buttons.length).toBe(0);
        });
    });

    describe('ignore offer button', () => {
        beforeEach(() => {
            when(ignoredOfferServiceMock.isIgnored(anything())).thenReturn(false);
        });

        describe('with valid buyOffer', () => {
            let buyOffer: BuyOfferRecord;

            beforeEach(() => {
                buyOffer = <BuyOfferRecord>{ seller: TestWallet.address, state: OfferState.Active };

                init({
                    domain: getDomain('with-operators.tez'),
                    wallet: TestWallet,
                    buyOffer,
                    allowedToIgnoreOffer: true,
                });
            });

            it('should appear', () => {
                const buttons = ngMocks.findAll('button');

                expect(buttons.length).toBe(2);
                expect(component.canIgnoreOffer).toBeTrue();
                expect(component.canAcceptOffer).toBeTrue();
            });

            it('should not appear if `allowedToIgnoreOffer` is false', () => {
                fixture.componentInstance.allowedToIgnoreOffer = false;
                fixture.detectChanges();

                const buttons = ngMocks.findAll('button');

                expect(buttons.length).toBe(1);
                expect(component.canIgnoreOffer).toBeFalse();
                expect(component.canAcceptOffer).toBeTrue();
            });

            it('should ignore offer when clicked', () => {
                const buttons = ngMocks.findAll('button');
                ngMocks.click(buttons[1]);

                verify(ignoredOfferServiceMock.ignore(buyOffer)).called();
            });
        });

        describe('with buyOffer not Active', () => {
            it('should not show', () => {
                init({
                    domain: getDomain('with-operators.tez'),
                    wallet: TestWallet,
                    buyOffer: <BuyOfferRecord>{ seller: TestWallet.address, state: OfferState.OfferExpired },
                    allowedToIgnoreOffer: true,
                });

                const buttons = ngMocks.findAll('button');

                expect(buttons.length).toBe(0);
                expect(component.canIgnoreOffer).toBeFalse();
            });
        });
    });

    function init(options: { domain?: any; buyOffer?: BuyOfferRecord | null | undefined; wallet?: TezosWallet | null; allowedToIgnoreOffer?: boolean }) {
        fixture = MockRender(
            BuyOfferActionsComponent,
            { domain: options.domain, buyOffer: options.buyOffer, wallet: options.wallet, allowedToIgnoreOffer: options.allowedToIgnoreOffer },
            { detectChanges: false }
        );
        fixture.detectChanges();
        component = fixture.point.componentInstance;
    }
});
