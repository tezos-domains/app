import { StorageMap } from '@ngx-pwa/local-storage';
import { TestWallet, TezosAddress } from '@td/test';
import dayjs from 'dayjs';
import { of, ReplaySubject } from 'rxjs';
import { anyString, anything, deepEqual, instance, mock, verify, when } from 'ts-mockito';
import { BuyOfferRecord } from '../graphql/buy-offer-table-data-source';
import { TezosWallet } from '../tezos/models';

import { TezosService } from '../tezos/tezos.service';
import { StorageSchema } from '../utils/storage';
import { IgnoredOffersService, IgnoredOfferValue } from './ignored-offers.service';

describe('IgnoredOffersService', () => {
    let storageMapMock: StorageMap;
    let tezosServiceMock: TezosService;
    let service: IgnoredOffersService;
    let wallet$: ReplaySubject<TezosWallet>;

    beforeEach(() => {
        storageMapMock = mock(StorageMap);
        tezosServiceMock = mock(TezosService);

        wallet$ = new ReplaySubject<TezosWallet>(1);

        when(storageMapMock.set(anyString(), anything(), anything())).thenReturn(of(void 0));
        when(storageMapMock.delete(anyString())).thenReturn(of(void 0));

        when(tezosServiceMock.activeWallet).thenReturn(wallet$);

        service = new IgnoredOffersService(instance(storageMapMock), instance(tezosServiceMock));
    });

    describe('', () => {
        let storageData: any;
        beforeEach(() => {
            storageData = [
                ['buyoffer:1', { expiration: dayjs().add(1, 'day').format() }],
                ['buyoffer:22', { expiration: dayjs().subtract(1, 'day').format() }], // already expired
            ];

            setStorage(new Map(storageData));

            wallet$.next(TestWallet);
        });

        it('should return all ignored offers', () => {
            verify(
                storageMapMock.set(
                    StorageSchema.ignoredBuyOffers(TezosAddress).key,
                    deepEqual([storageData[0]]),
                    StorageSchema.ignoredBuyOffers(TezosAddress).schema
                )
            ).called();

            expect(service.allIgnoredOffers()).toEqual(['buyoffer:1']);
        });

        describe('isIgnored()', () => {
            it('should return true if offer is in storage', () => {
                expect(service.isIgnored(<BuyOfferRecord>{ id: 'buyoffer:1' })).toBeTrue();
            });

            it('should return false if offer is not in storage', () => {
                expect(service.isIgnored(<BuyOfferRecord>{ id: 'buyoffer:2' })).toBeFalse();
            });
        });

        describe('ignore()', () => {
            it('should add ignored offer to storage', () => {
                service.ignore(<BuyOfferRecord>{ id: 'buyoffer:99' });
                expect(service.allIgnoredOffers()).toEqual(['buyoffer:1', 'buyoffer:99']);
                verify(
                    storageMapMock.set(
                        StorageSchema.ignoredBuyOffers(TezosAddress).key,
                        deepEqual([storageData[0], ['buyoffer:99', { expiration: anyString() }]]),
                        StorageSchema.ignoredBuyOffers(TezosAddress).schema
                    )
                ).called();
            });
        });

        describe('remove()', () => {
            it('should remove offer from storage', () => {
                service.ignore(<BuyOfferRecord>{ id: 'buyoffer:99' });
                expect(service.allIgnoredOffers()).toEqual(['buyoffer:1', 'buyoffer:99']);

                service.remove(<BuyOfferRecord>{ id: 'buyoffer:99' });
                expect(service.allIgnoredOffers()).toEqual(['buyoffer:1']);

                verify(
                    storageMapMock.set(
                        StorageSchema.ignoredBuyOffers(TezosAddress).key,
                        deepEqual([storageData[0]]),
                        StorageSchema.ignoredBuyOffers(TezosAddress).schema
                    )
                ).called();
            });
        });
    });

    function setStorage(data: Map<string, IgnoredOfferValue>): void {
        when(storageMapMock.get(StorageSchema.ignoredBuyOffers(TezosAddress).key, StorageSchema.ignoredBuyOffers(TezosAddress).schema)).thenReturn(
            of(Array.from(data))
        );
    }
});
