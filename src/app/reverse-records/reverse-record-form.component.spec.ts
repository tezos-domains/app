import { UntypedFormBuilder, FormGroupDirective } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { TaquitoTezosDomainsClient } from '@tezos-domains/taquito-client';
import { DomainNameValidator } from '@tezos-domains/core';
import { DomainsManager } from '@tezos-domains/manager';
import { TranslocoModule } from '@ngneat/transloco';
import { ReplaySubject, Observable, Subject, of } from 'rxjs';
import { mock, when, anything, deepEqual, verify, instance, anyFunction, anyString } from 'ts-mockito';
import { MockBuilder, MockRender, MockedComponentFixture } from 'ng-mocks';
import { TestWallet, TezosAddress, TezosAddress2, TezosAddress3, provider, transloco, getReverseRecord } from '@td/test';

import { TezosService, SmartContractOperationEvent } from '../tezos/tezos.service';
import { TezosWallet } from '../tezos/models';
import { ReverseRecordDetailQuery } from '../graphql/graphql.generated';
import { ReverseRecordFormComponent } from './reverse-record-form.component';
import { DataSourceFactory } from '../graphql/data-source-factory';
import { DomainTableDataSource } from '../graphql/domain-table-data-source';
import { TezosDomainsClientService } from '../tezos/integration/tezos-domains-client.service';
import { TdAsyncValidatorsFactory } from '../utils/form-validators';
import { ReverseRecordsModule } from './reverse-records.module';

describe('ReverseRecordFormComponent', () => {
    let fixture: MockedComponentFixture<ReverseRecordFormComponent, Partial<ReverseRecordFormComponent>>;
    let component: ReverseRecordFormComponent;
    let dialogRefMock: MatDialogRef<ReverseRecordFormComponent>;
    let tezosServiceMock: TezosService;
    let tezosDomainsMock: TaquitoTezosDomainsClient;
    let tezosDomainsManagerMock: DomainsManager;
    let dataSourceFactoryMock: DataSourceFactory;
    let domainTableDataSourceMock: DomainTableDataSource;
    let tezosDomainsValidatorMock: DomainNameValidator;
    let tezosDomainsClientServiceMock: TezosDomainsClientService;
    let asyncValidatorFactoryMock: TdAsyncValidatorsFactory;
    let wallet: ReplaySubject<TezosWallet | null>;
    let operation: Observable<SmartContractOperationEvent>;

    beforeEach(() => {
        dialogRefMock = mock<MatDialogRef<any, any>>(MatDialogRef);
        tezosServiceMock = mock(TezosService);
        tezosDomainsMock = mock(TaquitoTezosDomainsClient);
        tezosDomainsManagerMock = mock<DomainsManager>();
        dataSourceFactoryMock = mock(DataSourceFactory);
        domainTableDataSourceMock = mock(DomainTableDataSource);
        tezosDomainsValidatorMock = mock<DomainNameValidator>();
        tezosDomainsClientServiceMock = mock(TezosDomainsClientService);
        asyncValidatorFactoryMock = mock(TdAsyncValidatorsFactory);
        wallet = new ReplaySubject(1);
        operation = new Subject();

        when(dataSourceFactoryMock.createDomainsDataSource()).thenReturn(instance(domainTableDataSourceMock));

        when(tezosServiceMock.activeWallet).thenReturn(wallet);

        when(tezosDomainsMock.manager).thenReturn(instance(tezosDomainsManagerMock));
        when(tezosDomainsMock.validator).thenReturn(instance(tezosDomainsValidatorMock));
        when(tezosDomainsClientServiceMock.current).thenReturn(of(instance(tezosDomainsMock)));
        when(tezosDomainsManagerMock.claimReverseRecord(anything()));
        when(tezosDomainsManagerMock.updateReverseRecord(anything()));
        when(tezosServiceMock.execute(anyFunction())).thenCall(fn => {
            fn(instance(tezosDomainsMock));
            return operation;
        });
        when(asyncValidatorFactoryMock.domainWithAddress(anyString())).thenReturn(() => of(null));

        wallet.next(TestWallet);

        return MockBuilder(ReverseRecordFormComponent, ReverseRecordsModule)
            .provide([
                provider(dialogRefMock),
                provider(tezosServiceMock),
                provider(dataSourceFactoryMock),
                provider(tezosDomainsClientServiceMock),
                provider(asyncValidatorFactoryMock),
            ])
            .keep(NoopAnimationsModule, { export: true })
            .keep(TranslocoModule, { export: true })
            .keep(transloco(), { export: true })
            .provide(UntypedFormBuilder)
            .mock(FormGroupDirective);
    });

    function init(reverseRecord: NonNullable<Partial<ReverseRecordDetailQuery['reverseRecord']>>, owner?: string, name?: string) {
        if (reverseRecord.owner) {
            wallet.next({ address: owner || reverseRecord.owner } as any);
        }

        fixture = MockRender(
            ReverseRecordFormComponent,
            {},
            {
                providers: [{ provide: MAT_DIALOG_DATA, useValue: { reverseRecord, name } }],
                detectChanges: true,
            }
        );

        component = fixture.point.componentInstance;
    }

    describe('form', () => {
        it('should create form for new reverse record', () => {
            init({ address: TezosAddress });

            expect(component.form.get('name')?.value).toBeNull();
            expect(component.form.get('owner')?.value).toBe(TestWallet.address);
        });

        it('should create form for existing reverse record', () => {
            const reverseRecord = getReverseRecord(TezosAddress);
            init(reverseRecord);

            expect(component.form.get('name')?.value).toBe('necroskillz.tez');
            expect(component.form.get('owner')?.value).toBe(reverseRecord.owner);
        });

        it('should override name', () => {
            const reverseRecord = getReverseRecord(TezosAddress);
            init(reverseRecord, undefined, 'new.tez');

            expect(component.form.get('name')?.value).toBe('new.tez');
            expect(component.form.get('owner')?.value).toBe(reverseRecord.owner);
            expect(component.overriddenName).toBe('new.tez');
        });
    });

    describe('transferOwnerRequested', () => {
        it('should be true if wallet address is not reverse record address and form has different owner', () => {
            init(getReverseRecord(TezosAddress2));

            expect(component.transferOwnerRequested).toBeFalse();

            component.form.get('owner')!.setValue(TezosAddress);

            expect(component.transferOwnerRequested).toBeTrue();
        });

        it('should be false if wallet address is owner and form owner is invalid', () => {
            init(getReverseRecord(TezosAddress2));

            component.form.get('owner')!.setValue('aaa');

            expect(component.transferOwnerRequested).toBeFalse();
        });

        it('should be false if wallet address is not owner', () => {
            init(getReverseRecord(TezosAddress2), TezosAddress2);

            component.form.get('owner')!.setValue(TezosAddress);

            expect(component.transferOwnerRequested).toBeFalse();
        });
    });

    describe('confirmTransfer()', () => {
        it('should set confirmation when same address as in form is entered', () => {
            init(getReverseRecord(TezosAddress2));
            component.form.get('owner')!.setValue(TezosAddress);
            expect(component.transferOwnerConfirmed).toBeFalse();

            component.confirmTransfer(TezosAddress);

            expect(component.transferOwnerConfirmed).toBeTrue();
        });

        it('should not set confirmation when different address than in form is entered', () => {
            init(getReverseRecord(TezosAddress2));
            component.form.get('owner')!.setValue(TezosAddress);

            component.confirmTransfer(TezosAddress3);

            expect(component.transferOwnerConfirmed).toBeFalse();
        });
    });

    describe('transferOwnerConfirmed', () => {
        it('should be reset when form value changes', () => {
            init(getReverseRecord(TezosAddress2));
            component.form.get('owner')!.setValue(TezosAddress);

            component.confirmTransfer(TezosAddress);

            expect(component.transferOwnerConfirmed).toBeTrue();

            component.form.get('owner')!.setValue(TezosAddress3);

            expect(component.transferOwnerConfirmed).toBeFalse();
        });
    });

    describe('create()', () => {
        beforeEach(() => {
            init(getReverseRecord(TezosAddress));
        });

        it('should send operation with form data', () => {
            const data = { name: 'xxx.tez', owner: TezosAddress };
            component.form.setValue(data);

            component.create();

            verify(
                tezosDomainsManagerMock.claimReverseRecord(
                    deepEqual({
                        ...data,
                    })
                )
            ).called();

            expect(component.operation).toBe(operation);
            expect(component.form.disabled).toBeTrue();
        });

        it('should coalesce name to null', () => {
            const data = { name: '', owner: TezosAddress };
            component.form.setValue(data);

            component.create();

            verify(
                tezosDomainsManagerMock.claimReverseRecord(
                    deepEqual({
                        ...data,
                        name: null,
                    })
                )
            ).called();

            expect(component.operation).toBe(operation);
            expect(component.form.disabled).toBeTrue();
        });
    });

    describe('save()', () => {
        it('should send updateReverseRecord operation with form data if user is the owner', () => {
            init(getReverseRecord(TezosAddress));

            const data = { name: 'xxx.tez', owner: TezosAddress };
            component.form.setValue(data);

            component.save();

            verify(
                tezosDomainsManagerMock.updateReverseRecord(
                    deepEqual({
                        ...data,
                        name: 'xxx.tez',
                        address: TezosAddress,
                    })
                )
            ).called();

            expect(component.operation).toBe(operation);
            expect(component.form.disabled).toBeTrue();
        });

        it('should coalesce name to null for update', () => {
            init(getReverseRecord(TezosAddress));

            const data = { name: '', owner: TezosAddress };
            component.form.setValue(data);

            component.save();

            verify(
                tezosDomainsManagerMock.updateReverseRecord(
                    deepEqual({
                        ...data,
                        address: TezosAddress,
                        name: null,
                    })
                )
            ).called();

            expect(component.operation).toBe(operation);
            expect(component.form.disabled).toBeTrue();
        });

        it('should send claimReverseRecord operation with form data if user is NOT the owner (= user connected with reverse record address)', () => {
            init(getReverseRecord(TezosAddress2), TezosAddress2);

            const data = { name: 'necroskillz.tez', owner: TezosAddress3 };
            component.form.setValue(data);

            component.save();

            verify(
                tezosDomainsManagerMock.claimReverseRecord(
                    deepEqual({
                        ...data,
                        name: 'necroskillz.tez',
                    })
                )
            ).called();

            expect(component.operation).toBe(operation);
            expect(component.form.disabled).toBeTrue();
        });
    });

    describe('cancel()', () => {
        it('should close the modal', () => {
            init(getReverseRecord(TezosAddress));

            component.cancel();

            verify(dialogRefMock.close(false)).called();
        });
    });

    describe('clearName()', () => {
        it('should clear the name field', () => {
            init(getReverseRecord(TezosAddress));

            expect(component.form.value.name).toBe('necroskillz.tez');

            component.clearName();

            expect(component.form.value.name).toBeNull();
        });
    });

    describe('operationDone({ success: true })', () => {
        it('should close the modal with value of the form', () => {
            init(getReverseRecord(TezosAddress));
            const data = { name: 'xxx.tez', owner: TezosAddress };

            component.form.setValue(data);

            component.operationDone({ success: true });

            verify(dialogRefMock.close(true)).called();
        });
    });

    describe('operationDone({ success: false })', () => {
        it('should enable the form', () => {
            init(getReverseRecord(TezosAddress));

            const data = { name: 'xxx.tez', owner: TezosAddress };
            component.form.setValue(data);

            component.save();

            expect(component.form.disabled).toBeTrue();

            component.operationDone({ success: false });

            expect(component.form.disabled).toBeFalse();
        });
    });
});
