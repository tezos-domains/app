import { TestBed, fakeAsync, tick } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { MatDialogRef } from '@angular/material/dialog';
import { TranslocoModule } from '@ngneat/transloco';
import { Subject, ReplaySubject } from 'rxjs';
import { GraphQLError } from 'graphql';
import { mock, when, anything, verify, deepEqual, instance } from 'ts-mockito';
import { MockBuilder, MockRender, MockedComponentFixture, DefaultRenderComponent } from 'ng-mocks';
import { ApolloTestingModule, ApolloTestingController } from 'apollo-angular/testing';
import { TestWallet, provider, transloco, TezosAddress, getReverseRecord, TezosAddress2 } from '@td/test';

import { SharedModule } from '../shared/shared.module';
import { TezosService } from '../tezos/tezos.service';
import { TezosWallet } from '../tezos/models';
import { AppService } from '../app-service';
import { ErrorModule } from '../error/error.module';
import { PageService } from '../browser/page.service';
import { ReverseRecordDetailComponent } from './reverse-record-detail.component';
import { ReverseRecordService } from '../domains/reverse-record.service';
import { ReverseRecordsModule } from './reverse-records.module';
import { ReverseRecordDetailDocument, ReverseRecordDetailGQL } from '../graphql/graphql.generated';
import { APOLLO_OPTIONS } from 'apollo-angular';
import { DataSourceFactory } from '../graphql/data-source-factory';
import { OverlayService } from '../overlay.service';

describe('ReverseRecordDetailComponent', () => {
    let fixture: MockedComponentFixture<ReverseRecordDetailComponent, DefaultRenderComponent<ReverseRecordDetailComponent>>;
    let component: ReverseRecordDetailComponent;
    let activateRouteMock: ActivatedRoute;
    let tezosServiceMock: TezosService;
    let reverseRecordServiceMock: ReverseRecordService;
    let appServiceMock: AppService;
    let overlayServiceMock: OverlayService;
    let pageServiceMock: PageService;
    let modalMock: MatDialogRef<any>;
    let routeParamsMock: Subject<{ [key: string]: string }>;
    let wallet: ReplaySubject<TezosWallet | null>;
    let dialogClose: Subject<boolean>;
    let apollo: ApolloTestingController;
    let offlineToOnline: Subject<boolean>;

    beforeEach(() => {
        activateRouteMock = mock(ActivatedRoute);
        tezosServiceMock = mock(TezosService);
        reverseRecordServiceMock = mock(ReverseRecordService);
        modalMock = mock(MatDialogRef);
        appServiceMock = mock(AppService);
        overlayServiceMock = mock(OverlayService);
        pageServiceMock = mock(PageService);
        routeParamsMock = new Subject();
        wallet = new ReplaySubject(1);
        dialogClose = new Subject();
        offlineToOnline = new Subject();

        when(activateRouteMock.params).thenReturn(routeParamsMock);
        when(overlayServiceMock.openReverseRecordEdit(anything())).thenReturn(instance(modalMock));
        when(modalMock.afterClosed()).thenReturn(dialogClose);
        when(appServiceMock.goingOnlineFromOffline).thenReturn(offlineToOnline);
        when(tezosServiceMock.activeWallet).thenReturn(wallet);

        wallet.next(TestWallet);

        return MockBuilder(ReverseRecordDetailComponent, ReverseRecordsModule)
            .keep(ApolloTestingModule, { export: true })
            .keep(TranslocoModule, { export: true })
            .keep(DataSourceFactory)
            .keep(ReverseRecordDetailGQL)
            .keep(transloco(), { export: true })
            .provide([
                provider(activateRouteMock),
                provider(tezosServiceMock),
                provider(reverseRecordServiceMock),
                provider(appServiceMock),
                provider(overlayServiceMock),
                provider(pageServiceMock),
                { provide: APOLLO_OPTIONS, useValue: null },
            ])
            .mock(ErrorModule)
            .mock(SharedModule);
    });

    beforeEach(() => {
        fixture = MockRender(ReverseRecordDetailComponent);

        apollo = TestBed.inject(ApolloTestingController);

        component = fixture.point.componentInstance;
    });

    afterEach(() => {
        apollo.verify();
    });

    it('should load reverse record data', fakeAsync(() => {
        navigate(TezosAddress);

        const op = apollo.expectOne(ReverseRecordDetailDocument);

        expect(op.operation.variables).toEqual({ address: TezosAddress });

        op.flush({
            data: {
                reverseRecord: getReverseRecord(TezosAddress),
            },
        });
        tick();

        expect(component.reverseRecord).toEqual(getReverseRecord(TezosAddress));
        expect(component.errorMessage).toBeNull();
        expect(component.wallet?.address).toBe(TezosAddress);
    }));

    it('should set error message if api returns error', fakeAsync(() => {
        navigate(TezosAddress);

        apollo.expectOne(ReverseRecordDetailDocument).graphqlErrors([new GraphQLError('gql error'), new GraphQLError('gql error2')]);
        tick();

        expect(component.reverseRecord).toBeNull();
        expect(component.errorMessage).not.toBeNull();
        expect(component.errorTitleKey).toBe('general.error');
    }));

    it('should load reverse record data as url changes', fakeAsync(() => {
        navigate(TezosAddress);

        apollo.expectOne(ReverseRecordDetailDocument).flush({
            data: {
                reverseRecord: getReverseRecord(TezosAddress),
            },
        });
        tick();

        expect(component.reverseRecord).toEqual(getReverseRecord(TezosAddress));
        expect(component.errorMessage).toBeNull();
        verify(pageServiceMock.setTitle('reverse-record', deepEqual({ address: TezosAddress }))).called();

        navigate('aaa');

        apollo.expectNone(ReverseRecordDetailDocument);

        expect(component.reverseRecord).toBeNull();
        expect(component.errorMessage).not.toBeNull();
        expect(component.errorTitleKey).toBe('general.title-invalid-address');
        verify(pageServiceMock.setTitle('reverse-record', deepEqual({ address: 'aaa' }))).called();

        navigate(TezosAddress2);

        const op = apollo.expectOne(ReverseRecordDetailDocument);
        expect(op.operation.variables).toEqual({ address: TezosAddress2 });
        op.flush({
            data: {
                reverseRecord: getReverseRecord(TezosAddress2),
            },
        });
        tick();

        expect(component.reverseRecord).toEqual(getReverseRecord(TezosAddress2));
        expect(component.errorMessage).toBeNull();
        verify(pageServiceMock.setTitle('reverse-record', deepEqual({ address: TezosAddress2 }))).called();
    }));

    it('setup unclaimed reverse record connected address', fakeAsync(() => {
        navigate(TezosAddress);

        apollo.expectOne(ReverseRecordDetailDocument).flush({
            data: {
                reverseRecord: null,
            },
        });
        tick();

        expect(component.reverseRecord).toEqual({ address: TezosAddress } as any);
        expect(component.errorMessage).toBeNull();
    }));

    it('show error non existent reverse record', fakeAsync(() => {
        navigate(TezosAddress2);

        apollo.expectOne(ReverseRecordDetailDocument).flush({
            data: {
                reverseRecord: null,
            },
        });
        tick();

        expect(component.errorMessage).not.toBeNull();
    }));

    it('should stop loading domains when destroyed', () => {
        fixture.destroy();

        navigate('necroskillz.tez');

        apollo.expectNone(ReverseRecordDetailDocument);

        expect(component.reverseRecord).toBeUndefined();
    });

    describe('operations', () => {
        function init() {
            navigate(TezosAddress);

            apollo.expectOne(ReverseRecordDetailDocument).flush({
                data: {
                    reverseRecord: getReverseRecord(TezosAddress),
                },
            });
            tick();
        }

        describe('edit()', () => {
            it('should show dialog', fakeAsync(() => {
                init();
                component.edit();

                verify(overlayServiceMock.openReverseRecordEdit(deepEqual(getReverseRecord(TezosAddress)))).called();
            }));

            it('should reload data after its saved', fakeAsync(() => {
                init();
                component.edit();

                dialogClose.next(true);

                verifyDataReloaded();
            }));

            it('should not reload data after its canceled', fakeAsync(() => {
                init();
                component.edit();

                dialogClose.next(false);

                verifyDataNotReloaded();
            }));
        });

        function verifyDataReloaded() {
            apollo.expectOne(ReverseRecordDetailDocument).flush({
                data: {
                    reverseRecord: getReverseRecord(TezosAddress2),
                },
            });
            tick();

            expect(component.reverseRecord).toEqual(getReverseRecord(TezosAddress2));
        }

        function verifyDataNotReloaded() {
            apollo.expectNone(ReverseRecordDetailDocument);

            expect(component.reverseRecord).toEqual(getReverseRecord(TezosAddress));
        }
    });

    function navigate(address: string) {
        when(activateRouteMock.snapshot).thenReturn({ params: { address } } as any);
        routeParamsMock.next({ address });
    }
});
