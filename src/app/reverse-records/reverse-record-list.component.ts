import { Component } from '@angular/core';

@Component({
    selector: 'td-reverse-record-list',
    templateUrl: './reverse-record-list.component.html',
    styleUrls: ['./reverse-record-list.component.scss'],
})
export class ReverseRecordListComponent {}
