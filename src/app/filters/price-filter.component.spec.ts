import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TranslocoModule } from '@ngneat/transloco';
import { transloco } from '@td/test';
import { MockBuilder, MockedComponentFixture, MockRender, ngMocks } from 'ng-mocks';
import { SharedModule } from 'src/app/shared/shared.module';
import { TezComponent } from 'src/app/shared/tez.component';
import { TruncatePipe } from 'src/app/shared/truncate.pipe';
import { EventsModule } from '../events/events.module';
import { PriceFilterComponent } from './price-filter.component';

describe('PriceFilterComponent', () => {
    let component: PriceFilterComponent;
    let fixture: MockedComponentFixture<PriceFilterComponent, Partial<PriceFilterComponent>>;

    beforeEach(() => {
        return MockBuilder(PriceFilterComponent, EventsModule)
            .mock(SharedModule)
            .keep(FormsModule)
            .keep(ReactiveFormsModule)
            .keep(TranslocoModule, { export: true })
            .keep(TruncatePipe)
            .keep(transloco(), { export: true });
    });

    describe('when price is provided', () => {
        beforeEach(() => {
            init({ initialPrice: { min: 3, max: 34 } });
            fixture.detectChanges();
        });

        it('should use it in the form', () => {
            const prices = ngMocks.findAll(fixture, 'input');
            expect(prices[0].nativeElement.value).toBe('3');
            expect(prices[1].nativeElement.value).toBe('34');
        });

        it('should display entered price range', () => {
            const tezs = ngMocks.findInstances(TezComponent);
            expect(tezs.length).toBe(2);

            expect(tezs[0].value).toBe(3);
            expect(tezs[1].value).toBe(34);
        });
    });

    it('should emit entered price', () => {
        init();

        const spy = jasmine.createSpy('priceFilterEmitter');
        component.priceFilterChange.subscribe(spy);

        ngMocks.change('input', 2);
        ngMocks.find('form').triggerEventHandler('submit', {});

        expect(spy).toHaveBeenCalledWith({ min: 2, max: 0 });
    });

    it('should set price display as `lowerBound`', () => {
        init();

        ngMocks.change('input', 2);
        ngMocks.find('form').triggerEventHandler('submit', {});

        component.menuClosed();

        expect(component.priceDisplay).toBe('lowerBound');
    });

    function init(data?: Partial<PriceFilterComponent>) {
        fixture = MockRender(PriceFilterComponent, data ?? {});
        component = fixture.point.componentInstance;
    }
});
