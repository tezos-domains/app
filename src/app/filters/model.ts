import { Dayjs } from 'dayjs';

export interface PriceFilter {
    min?: number;
    max?: number;
}

export interface DateFilter {
    from: Dayjs;
    to?: Dayjs | null;
}
