import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TranslocoModule } from '@ngneat/transloco';
import { transloco } from '@td/test';
import { DomainNameValidationResult, DomainNameValidator, TezosDomainsValidator } from '@tezos-domains/core';
import { TaquitoTezosDomainsClient } from '@tezos-domains/taquito-client';
import { MockBuilder, MockedComponentFixture, MockRender, ngMocks } from 'ng-mocks';
import { SharedModule } from 'src/app/shared/shared.module';
import { TruncatePipe } from 'src/app/shared/truncate.pipe';
import { anyString, instance, mock, when } from 'ts-mockito';
import { EventsModule } from '../events/events.module';
import { DomainFilterComponent } from './domain-filter.component';

describe('DomainFilterComponent', () => {
    let component: DomainFilterComponent;
    let fixture: MockedComponentFixture<DomainFilterComponent, Partial<DomainFilterComponent>>;
    let tezosDomainsMock: TaquitoTezosDomainsClient;

    beforeEach(() => {
        tezosDomainsMock = mock(TaquitoTezosDomainsClient);

        const validator = mock<DomainNameValidator>();
        when(validator.isValidWithKnownTld(anyString())).thenReturn(DomainNameValidationResult.VALID);
        when(tezosDomainsMock.validator).thenReturn(instance(validator));

        return MockBuilder(DomainFilterComponent, EventsModule)
            .mock(SharedModule)
            .keep(FormsModule)
            .keep(ReactiveFormsModule)
            .keep(TranslocoModule, { export: true })
            .keep(TruncatePipe)
            .keep(transloco(), { export: true });
    });

    describe('when domain is provided', () => {
        beforeEach(() => {
            init({ initialDomain: 'test-domain.tez' });
            fixture.detectChanges();
        });

        it('should use it in the form', () => {
            const domainInput = ngMocks.find(fixture, 'input');
            expect(domainInput.nativeElement.value).toBe('test-domain.tez');
        });

        it('should display entered domain', () => {
            const text = ngMocks.formatText(ngMocks.find('button'));
            expect(text).toContain('test-domain.tez');
        });
    });

    it('should emit entered domain', () => {
        init();

        const spy = jasmine.createSpy('domainChangeEmitter');
        component.domainChange.subscribe(spy);

        ngMocks.change('input', 'test.tez');

        ngMocks.find('form').triggerEventHandler('submit', {});

        expect(spy).toHaveBeenCalledWith('test.tez');
    });

    function init(data?: Partial<DomainFilterComponent>) {
        fixture = MockRender(DomainFilterComponent, { ...(data ?? {}), tzDomainsClient: instance(tezosDomainsMock) });
        component = fixture.point.componentInstance;
    }
});
