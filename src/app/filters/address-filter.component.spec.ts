import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TranslocoModule } from '@ngneat/transloco';
import { TezosAddress, TezosAddress2, transloco } from '@td/test';
import { MockBuilder, MockedComponentFixture, MockRender, ngMocks } from 'ng-mocks';
import { SharedModule } from 'src/app/shared/shared.module';
import { TruncatePipe } from 'src/app/shared/truncate.pipe';
import { AddressFilterComponent } from './address-filter.component';
import { FiltersModule } from './filters.module';

describe('AddressFilterComponent', () => {
    let component: AddressFilterComponent;
    let fixture: MockedComponentFixture<AddressFilterComponent, Partial<AddressFilterComponent>>;

    beforeEach(() => {
        return MockBuilder(AddressFilterComponent, FiltersModule)
            .mock(SharedModule)
            .keep(FormsModule)
            .keep(ReactiveFormsModule)
            .keep(TranslocoModule, { export: true })
            .keep(TruncatePipe)
            .keep(transloco(), { export: true })
            .provide([]);
    });

    describe('when address is provided', () => {
        beforeEach(() => {
            init({ initialAddress: TezosAddress });
            fixture.detectChanges();
        });

        it('should use it in the form', () => {
            const addrInput = ngMocks.find(fixture, 'input');
            expect(addrInput.nativeElement.value).toBe(TezosAddress);
        });

        it('should display truncated address', () => {
            const text = ngMocks.formatText(ngMocks.find('button'));
            expect(text).toContain('tz1...E8Qt');
        });
    });

    it('should emit entered address', () => {
        init();

        const spy = jasmine.createSpy('addressChangeEmitter');
        component.addressChange.subscribe(spy);

        ngMocks.change('input', TezosAddress2);

        ngMocks.find('form').triggerEventHandler('submit', {});

        expect(spy).toHaveBeenCalledWith(TezosAddress2);
    });

    function init(data?: Partial<AddressFilterComponent>) {
        fixture = MockRender(AddressFilterComponent, data ?? {});
        component = fixture.point.componentInstance;
    }
});
