import { Provider } from '@angular/core';
import { instance, mock } from 'ts-mockito';
import dayjs from 'dayjs';
import { DateAdapter } from '@angular/material/core';

import { LocaleService } from '../app/i18n/locale.service';

beforeEach(() => {
    const adapterMock = mock<DateAdapter<dayjs.Dayjs>>(DateAdapter);
    new LocaleService(instance(adapterMock)).setup();

    expect().nothing(); // we are using verify, suppress warning about no expectations
});

export function provider(mock: any, type?: any): Provider {
    type = type || mock.clazz.mocker.clazz;
    return { provide: type, useFactory: () => instance(mock) };
}

export function providers(...mocks: any[]): Provider[] {
    return mocks.map(mock => {
        const type = mock.clazz.mocker.clazz;
        return { provide: type, useFactory: () => instance(mock) };
    });
}

const originalDateNow = Date.now;
let dateSpy: jasmine.Spy | null = null;

export function mockNow(date: dayjs.Dayjs) {
    if (dateSpy) {
        Date.now = dateSpy;
        dateSpy.and.returnValue(date.unix() * 1000);
    } else {
        dateSpy = spyOn(Date, 'now').and.returnValue(date.unix() * 1000);
    }
}

export function resetNow() {
    Date.now = originalDateNow;
}
