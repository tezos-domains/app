export class TestPromise<T = void> extends Promise<T> {
    private resolveFn: (data?: T) => void;
    private rejectFn: (err?: any) => void;

    constructor() {
        let resolveFn: (data?: T | PromiseLike<T>) => void;
        let rejectFn: (err?: any) => void;
        super((resolve, reject) => {
            resolveFn = resolve;
            rejectFn = reject;
        });

        this.resolveFn = resolveFn!;
        this.rejectFn = rejectFn!;
    }

    resolve(data?: T) {
        this.resolveFn(data);
    }

    reject(err?: any) {
        this.rejectFn(err);
    }
}
