import { TranslocoConfig, TranslocoTestingModule } from '@ngneat/transloco';
import en from '../i18n/en.json';
import cs from '../i18n/cs.json';

const languages = [
    { id: 'en', label: 'English', locale: 'en' },
    { id: 'cs', label: 'Čeština', locale: 'cs' },
];

export function transloco(config: Partial<TranslocoConfig> = {}): any {
    return TranslocoTestingModule.forRoot({
        langs: { en, cs },
        preloadLangs: true,
        translocoConfig: {
            prodMode: true,
            missingHandler: { logMissingKey: false },
            defaultLang: 'en',
            availableLangs: languages,
            ...config,
        },
    });
}
