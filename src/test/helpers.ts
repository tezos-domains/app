import { DomainListRecord } from '../app/graphql/domain-table-data-source';
import { DomainsListQuery } from '../app/graphql/graphql.generated';

export function prepareGQLDomainListResponse(domains: DomainListRecord[], hasNextPage = false): DomainsListQuery['domains'] {
    const edges = domains.map(d => ({
        node: d,
        cursor: `${d.name}_cursor`,
    }));

    return {
        edges: edges,
        pageInfo: {
            startCursor: edges.length ? edges[0].cursor : null,
            endCursor: edges.length ? edges[edges.length - 1].cursor : null,
            hasNextPage,
            hasPreviousPage: false, // unused in the app
        },
    };
}
