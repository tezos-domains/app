import { getLevel, StandardRecordMetadataKey } from '@tezos-domains/core';
import BigNumber from 'bignumber.js';
import dayjs from 'dayjs';
import { BehaviorSubject } from 'rxjs';
import { Configuration } from '../app/configuration';
import { DomainListRecord } from '../app/graphql/domain-table-data-source';
import { DomainDetailQuery, ReverseRecordDetailQuery } from '../app/graphql/graphql.generated';
import { TezosNetwork, TezosWallet } from '../app/tezos/models';

export const TezosAddress = 'tz1esQ4Zp2jWp9gsZhdqBgNBQKrrdHokE8Qt';
export const TezosAddress2 = 'tz1WCd2jm4uSt4vntk4vSuUWoZQGhLcDuR9q';
export const TezosAddress3 = 'tz1ajVP6JT5exSZAJbhReKnThp9dKrQxNeCx';
export const TezosAddress4 = 'tz1Lxmk7eZiomcdJ6JPaeiLDkg4GJehVpSFV';

export const TestWallet: TezosWallet = {
    address: TezosAddress,
    balance: new BehaviorSubject(10),
    isBalanceLow: new BehaviorSubject(false),
    tedBalance$: new BehaviorSubject(BigNumber(0)),
    votesBalance$: new BehaviorSubject(BigNumber(0)),
    bidderBalances: new BehaviorSubject([]),
};

export const TestWallet3: TezosWallet = {
    address: TezosAddress3,
    balance: new BehaviorSubject(10),
    isBalanceLow: new BehaviorSubject(false),
    tedBalance$: new BehaviorSubject(BigNumber(0)),
    votesBalance$: new BehaviorSubject(BigNumber(0)),
    bidderBalances: new BehaviorSubject([]),
};

export const LowTestWallet: TezosWallet = {
    address: TezosAddress,
    balance: new BehaviorSubject(0.1),
    isBalanceLow: new BehaviorSubject(true),
    tedBalance$: new BehaviorSubject(BigNumber(0)),
    votesBalance$: new BehaviorSubject(BigNumber(0)),
    bidderBalances: new BehaviorSubject([]),
};

export const TestNetwork: TezosNetwork = {
    name: 'testnet',
    rpcUrls: [{ name: 'Giganode', url: 'https://testnet-tezos.giganode.io', enabled: true }],
    rpcUrl: '',
    statsUrl: 'https://testnet.tzstats.com',
    apiUrl: 'https//test.api.tezos.domains',
    blockIndexedTimeoutMs: 20000,
    confirmations: 1,
    chainId: 'NetXjD3HPJJjmcd',
    youvesOracleContract: 'KT1LWDzd6mFhjjnb65a1PjHDNZtFKBieTQKH',
    tzprofilesApiUrl: 'https://indexer.tzprofiles.com/v1/graphql',
    directBrokerContract: 'KT1UaPcfphum7ShyndWuC5uKSGACw3HUpjs5',
    buyOfferBrokerContract: 'KT1WYMxgZxkkF328pGiUb2FvybBeTksEzLwV',
    tokenContract: 'KT1Ch6PstAQG32uNfQJUSL2bf2WvimvY5umk',
    oracleRegistrarContract: 'KT1X9se9JdF8Z5JpZfHLTaTW878umtQ1nZde',
    vestingContract: 'KT1Cvdh4XSLonAd3Bsqgrrma8aF62vVzaaFS',
    rewardsApiUrl: '',
    tedTokenContract: 'KT1VHyQPyaqvRuyv2cAmQi1zRNrgqn99dLwp',
    tedVotesContract: '',
    tedPoolContract: 'KT19ewvnH3S4HFcroLALnGdvRAePxh51M3VY',
};

export const MainNetwork: TezosNetwork = {
    name: 'mainnet',
    rpcUrls: [{ name: 'Giganode', url: 'https://mainnet-tezos.giganode.io', enabled: true }],
    rpcUrl: '',
    statsUrl: 'https://tzstats.com',
    apiUrl: 'https//api.tezos.domains',
    blockIndexedTimeoutMs: 20000,
    confirmations: 5,
    chainId: 'NetXdQprcVkpaWU',
    youvesOracleContract: 'KT1AdbYiPYb5hDuEuVrfxmFehtnBCXv4Np7r',
    tzprofilesApiUrl: 'https://indexer.tzprofiles.com/v1/graphql',
    buyOfferBrokerContract: 'KT1WYMxgZxkkF328pGiUb2FvybBeTksEzLwV',
    directBrokerContract: 'KT1UaPcfphum7ShyndWuC5uKSGACw3HUpjs5',
    tokenContract: 'KT1Ch6PstAQG32uNfQJUSL2bf2WvimvY5umk',
    oracleRegistrarContract: 'KT1X9se9JdF8Z5JpZfHLTaTW878umtQ1nZde',
    vestingContract: 'KT1Cvdh4XSLonAd3Bsqgrrma8aF62vVzaaFS',
    rewardsApiUrl: '',
    tedTokenContract: 'KT1VHyQPyaqvRuyv2cAmQi1zRNrgqn99dLwp',
    tedVotesContract: '',
    tedPoolContract: 'KT19ewvnH3S4HFcroLALnGdvRAePxh51M3VY',
};

export const TestConfig: Configuration = {
    network: TestNetwork,
    appName: 'app',
    hardwareWallet: {
        derivationPath: `44'/1729'/0'/0'`,
    },
    safeBalanceThreshold: 400000,
    landingPageUrl: 'https://tezos.domains',
    mailingApiUrl: 'https://mail.tezos.domains',
    cookieDomain: 'tezos.domains',
    decentralizedWebsiteUrl: 'https://{domain}.{tld}.page',
    tzprofilesAppUrl: 'https://tzprofiles.com',
    dnsVerifyApiUrl: 'https://dns-verify-api',
    minEventsFilterDate: new Date(2021, 4, 1),
    buyOfferFee: 10000,
    domainIsExpiringThreshold: {
        time: 90,
        units: 'days',
    },
    expiringDomainThreshold: {
        time: 90,
        units: 'days',
    },
    expiringDomainEarlyWarningThreshold: {
        time: 120,
        units: 'days',
    },
    maxWebsiteDomainNameLength: 63,
    allowImpersonation: false,
    airdrop: {
        delegates: [],
        start: new Date(2022, 10, 1),
        end: new Date(2099, 10, 1),
    },
    maxBulkCreate: 40,
    storageUrl: 'https://storage.tezos.domains',
    governance: { tedExchangeUrl: '', plentyUrl: '', homebaseUrl: 'https://google.com', showAverageApr: true },
};

const ReverseRecordData: NonNullable<ReverseRecordDetailQuery['reverseRecord']>[] = [
    {
        id: 'ID:tz1esQ4Zp2jWp9gsZhdqBgNBQKrrdHokE8Qt',
        address: TezosAddress,
        domain: {
            id: 'ID:necroskillz.tez',
            name: 'necroskillz.tez',
            data: [
                {
                    id: 'DataItem:1',
                    key: StandardRecordMetadataKey.TTL,
                    rawValue: '3639',
                },
            ],
        },
        owner: TezosAddress,
    },
    {
        id: 'ID:tz1WCd2jm4uSt4vntk4vSuUWoZQGhLcDuR9q',
        address: 'tz1WCd2jm4uSt4vntk4vSuUWoZQGhLcDuR9q',
        domain: {
            id: 'ID:martin.tez',
            name: 'martin.tez',
            data: [],
        },
        owner: 'tz1ajVP6JT5exSZAJbhReKnThp9dKrQxNeCx',
    },
];

export const TestReverseRecord = ReverseRecordData[0];
export const TestReverseRecord2 = ReverseRecordData[1];

const DomainData: NonNullable<DomainDetailQuery['domain']>[] = [
    {
        id: 'ID:tez',
        name: 'tez',
        owner: TezosAddress,
        expires: null,
        address: null,
        parentOwner: null,
        tokenId: null,
        subdomains: {
            totalCount: 10,
        },
        data: [],
        operators: [],
    },
    {
        id: 'ID:necroskillz.tez',
        address: TezosAddress,
        name: 'necroskillz.tez',
        owner: TezosAddress,
        parentOwner: 'tz1ajVP6JT5exSZAJbhReKnThp9dKrQxNeCx',
        expires: dayjs('2021-12-24'),
        subdomains: {
            totalCount: 2,
        },
        tokenId: null,
        data: [
            {
                id: 'DataItem:2',
                key: StandardRecordMetadataKey.TTL,
                rawValue: '3639',
            },
        ],
        reverseRecord: TestReverseRecord,
        operators: [],
    },
    {
        id: 'ID:martin.tez',
        address: 'tz1ajVP6JT5exSZAJbhReKnThp9dKrQxNeCx',
        name: 'martin.tez',
        owner: TezosAddress,
        parentOwner: null,
        expires: dayjs('2021-11-03'),
        subdomains: {
            totalCount: 1,
        },
        tokenId: null,
        data: [],
        reverseRecord: ReverseRecordData[1],
        operators: [],
    },
    {
        id: 'ID:happytezos.tez',
        address: 'tz1WCd2jm4uSt4vntk4vSuUWoZQGhLcDuR9q',
        name: 'happytezos.tez',
        owner: 'tz1WCd2jm4uSt4vntk4vSuUWoZQGhLcDuR9q',
        parentOwner: null,
        expires: dayjs('2022-10-20'),
        subdomains: {
            totalCount: 0,
        },
        tokenId: null,
        data: [],
        operators: [],
    },
    {
        id: 'ID:random.tez',
        address: 'tz3VEZ4k6a4Wx42iyev6i2aVAptTRLEAivNN',
        name: 'random.tez',
        owner: TezosAddress,
        parentOwner: null,
        expires: dayjs('2021-01-01'),
        subdomains: {
            totalCount: 0,
        },
        tokenId: null,
        data: [],
        operators: [],
    },
    {
        id: 'ID:test.necroskillz.tez',
        address: 'tz2esQ4Zp2jWp9gsZhdqBgNBQKrrdHokE8Qt',
        name: 'test.necroskillz.tez',
        owner: TezosAddress,
        parentOwner: null,
        expires: null,
        subdomains: {
            totalCount: 0,
        },
        tokenId: null,
        data: [],
        operators: [],
    },
    {
        id: 'ID:play.necroskillz.tez',
        name: 'play.necroskillz.tez',
        owner: 'tz1ajVP6JT5exSZAJbhReKnThp9dKrQxNeCx',
        parentOwner: null,
        expires: null,
        address: null,
        subdomains: {
            totalCount: 0,
        },
        tokenId: null,
        data: [],
        operators: [],
    },
    {
        id: 'ID:ble.happytezos.tez',
        name: 'ble.happytezos.tez',
        owner: 'tz1ajVP6JT5exSZAJbhReKnThp9dKrQxNeCx',
        parentOwner: null,
        expires: null,
        address: null,
        subdomains: {
            totalCount: 0,
        },
        tokenId: null,
        data: [],
        operators: [],
    },
    {
        id: 'ID:mike.tez',
        address: TezosAddress,
        name: 'mike.tez',
        owner: TezosAddress,
        parentOwner: null,
        expires: dayjs('2021-12-24'),
        subdomains: {
            totalCount: 0,
        },
        tokenId: null,
        data: [],
        operators: [],
    },
    {
        id: 'ID:expired.tez',
        address: TezosAddress,
        name: 'expired.tez',
        owner: TezosAddress,
        parentOwner: null,
        expires: dayjs('2019-12-24'),
        subdomains: {
            totalCount: 0,
        },
        tokenId: null,
        data: [],
        operators: [],
    },
    {
        id: 'ID:expires-soon.tez',
        address: TezosAddress,
        name: 'expires-soon.tez',
        owner: TezosAddress,
        parentOwner: null,
        expires: dayjs().add(1, 'month'),
        subdomains: {
            totalCount: 0,
        },
        tokenId: null,
        data: [],
        operators: [],
    },
    {
        id: 'ID:never-expires.tez',
        address: TezosAddress,
        name: 'never-expires.tez',
        owner: TezosAddress,
        parentOwner: null,
        expires: dayjs().add(10, 'years'),
        subdomains: {
            totalCount: 0,
        },
        tokenId: null,
        data: [],
        operators: [{ address: TezosAddress3, id: '' }],
    },
    {
        id: 'ID:with-operators.tez',
        address: TezosAddress,
        name: 'with-operators.tez',
        owner: TezosAddress,
        parentOwner: null,
        expires: null,
        subdomains: {
            totalCount: 0,
        },
        tokenId: null,
        data: [],
        operators: [
            { address: TezosAddress3, id: '' },
            { address: TezosAddress2, id: '' },
        ],
    },
];

export function getDomain(name: string): NonNullable<DomainDetailQuery['domain']> {
    const domain = DomainData.find(d => d.name === name);

    if (!domain) {
        throw new Error(`Domain ${name} not found in test data`);
    }

    return domain;
}

export function getDomains(...names: string[]): DomainListRecord[] {
    return names
        .map(n => getDomain(n))
        .map(d => ({
            id: d.id,
            name: d.name,
            owner: d.owner,
            level: getLevel(d.name),
            tokenId: d.tokenId,
            address: d.address,
            expires: d.expires,
            parentOwner: d.parentOwner,
            operators: d.operators,
            data: [],
        }));
}

export function getReverseRecord(address: string) {
    const reverseRecord = ReverseRecordData.find(d => d.address === address);

    if (!reverseRecord) {
        throw new Error(`Reverse record ${address} not found in test data`);
    }

    return reverseRecord;
}

export function getDomainByIndex(index: number) {
    return getDomains(DomainData[index].name)[0];
}
