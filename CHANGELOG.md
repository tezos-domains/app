# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## [1.51.0](https://gitlab.com/tezos-domains/app/compare/v1.50.1...v1.51.0) (2025-03-05)


### Features

* add affiliate buy&renew support ([393914f](https://gitlab.com/tezos-domains/app/commit/393914f5785883eaf8f887aa5e154f9052b81749))

### [1.50.1](https://gitlab.com/tezos-domains/app/compare/v1.50.0...v1.50.1) (2025-01-09)


### Bug Fixes

* change xtz to usd price source ([7fd0a16](https://gitlab.com/tezos-domains/app/commit/7fd0a16529433878a8bd516c82a681a127888670))

## [1.50.0](https://gitlab.com/tezos-domains/app/compare/v1.49.5...v1.50.0) (2024-12-22)


### Features

* upgrade taquito to 20.1.1 ([93343c2](https://gitlab.com/tezos-domains/app/commit/93343c2f198caacaeec7a92a20e42fdb085b3f51))

### [1.49.5](https://gitlab.com/tezos-domains/app/compare/v1.49.4...v1.49.5) (2024-12-21)

### [1.49.4](https://gitlab.com/tezos-domains/app/compare/v1.49.3...v1.49.4) (2024-12-08)

### [1.49.3](https://gitlab.com/tezos-domains/app/compare/v1.49.2...v1.49.3) (2024-11-14)

### [1.49.2](https://gitlab.com/tezos-domains/app/compare/v1.49.1...v1.49.2) (2024-10-06)

### [1.49.1](https://gitlab.com/tezos-domains/app/compare/v1.49.0...v1.49.1) (2024-10-02)

## [1.49.0](https://gitlab.com/tezos-domains/app/compare/v1.48.1...v1.49.0) (2024-09-11)


### Features

* show reverse record name in event details when available ([cbfa14b](https://gitlab.com/tezos-domains/app/commit/cbfa14b98c83996ded216929f9c3e338ae37bc2f))

### [1.48.1](https://gitlab.com/tezos-domains/app/compare/v1.48.0...v1.48.1) (2024-06-17)


### Bug Fixes

* login modal double opening ([271b6cc](https://gitlab.com/tezos-domains/app/commit/271b6cc0adb3da6bb00e077cb3b303528632b8cb))

## [1.48.0](https://gitlab.com/tezos-domains/app/compare/v1.47.1...v1.48.0) (2024-06-16)


### Features

* add eth address metadata support ([fcfe4dc](https://gitlab.com/tezos-domains/app/commit/fcfe4dc1e8dc0000ebae65a24b6a69a0eddc31ef))
* update taquito and beacon ([12d24c9](https://gitlab.com/tezos-domains/app/commit/12d24c9befb5f10b5c5af79f122452b3fc1b6b5f))

### [1.47.1](https://gitlab.com/tezos-domains/app/compare/v1.47.0...v1.47.1) (2024-05-25)


### Bug Fixes

* improve governance api unavailable message visibility ([a746c4e](https://gitlab.com/tezos-domains/app/commit/a746c4e4370b99778d4cabdbf32f5b95053a38ce))

## [1.47.0](https://gitlab.com/tezos-domains/app/compare/v1.46.0...v1.47.0) (2024-05-25)


### Features

* add bulk transfer ([8a71040](https://gitlab.com/tezos-domains/app/commit/8a710405a42ada0d1ebf44898f6024a185db3880))

## [1.46.0](https://gitlab.com/tezos-domains/app/compare/v1.45.0...v1.46.0) (2024-04-22)


### Features

* improve filters for expired domains ([5143b80](https://gitlab.com/tezos-domains/app/commit/5143b80089a3666a00c3572af8e5ea33898d9223))

## [1.45.0](https://gitlab.com/tezos-domains/app/compare/v1.44.1...v1.45.0) (2024-03-02)


### Features

* upgrade to taquito 19.0.1 ([4693db3](https://gitlab.com/tezos-domains/app/commit/4693db365986771973d6896cc9efaeea2bc6cc7f))


### Bug Fixes

* expired domains scrolling ([c8f5c67](https://gitlab.com/tezos-domains/app/commit/c8f5c673c30bf847d341a26029a55315b14be7f1))

### [1.44.1](https://gitlab.com/tezos-domains/app/compare/v1.44.0...v1.44.1) (2024-01-13)


### Bug Fixes

* remove "your listing" badge and show register button ([29b24b8](https://gitlab.com/tezos-domains/app/commit/29b24b8ff0329e159bd8184d76de9f65b5a184f8))

## [1.44.0](https://gitlab.com/tezos-domains/app/compare/v1.43.0...v1.44.0) (2023-12-30)


### Features

* add favourites ([ce80afd](https://gitlab.com/tezos-domains/app/commit/ce80afd13947c2a832009fbd4d726649463b0d17))

## [1.43.0](https://gitlab.com/tezos-domains/app/compare/v1.42.1...v1.43.0) (2023-12-08)


### Features

* show all transactions on reward history page ([1515cbf](https://gitlab.com/tezos-domains/app/commit/1515cbf69d43ac5e53598f15e3fbfe6d70fb3536))

### [1.42.1](https://gitlab.com/tezos-domains/app/compare/v1.42.0...v1.42.1) (2023-11-13)


### Bug Fixes

* update discord link ([5b000fc](https://gitlab.com/tezos-domains/app/commit/5b000fc14ef4af8f00198bd82c6debff255a0519))

## [1.42.0](https://gitlab.com/tezos-domains/app/compare/v1.41.2...v1.42.0) (2023-11-11)


### Features

* delegate detail screens ([9f5a3dd](https://gitlab.com/tezos-domains/app/commit/9f5a3dd012e612454418817c0359e91e03a5b4d4))

### [1.41.2](https://gitlab.com/tezos-domains/app/compare/v1.41.1...v1.41.2) (2023-11-01)


### Bug Fixes

* show correct percentage for tranches ([752c76a](https://gitlab.com/tezos-domains/app/commit/752c76a08d69a2ea0fa7d831206bff5105c4ac74))

### [1.41.1](https://gitlab.com/tezos-domains/app/compare/v1.41.0...v1.41.1) (2023-10-31)

## [1.41.0](https://gitlab.com/tezos-domains/app/compare/v1.40.3...v1.41.0) (2023-10-24)


### Features

* show listing status on domain details ([0543fb0](https://gitlab.com/tezos-domains/app/commit/0543fb00c31047f330a21c39ecc426b6fbce0781))

### [1.40.3](https://gitlab.com/tezos-domains/app/compare/v1.40.2...v1.40.3) (2023-10-13)

### [1.40.2](https://gitlab.com/tezos-domains/app/compare/v1.40.1...v1.40.2) (2023-10-12)

### [1.40.1](https://gitlab.com/tezos-domains/app/compare/v1.40.0...v1.40.1) (2023-10-11)


### Bug Fixes

* update translation for ted tokens in governance pool ([a6d5a96](https://gitlab.com/tezos-domains/app/commit/a6d5a96349dba038cdf005c9485e8aa873b443ae))

## [1.40.0](https://gitlab.com/tezos-domains/app/compare/v1.39.6...v1.40.0) (2023-10-11)


### Features

* move vesting power widget to governance page ([19fc69e](https://gitlab.com/tezos-domains/app/commit/19fc69e8428a5b26bf2c23d40ba721fc6c217939))

### [1.39.6](https://gitlab.com/tezos-domains/app/compare/v1.39.5...v1.39.6) (2023-10-11)


### Bug Fixes

* learn more link,rr record limit, scroll ([04de78f](https://gitlab.com/tezos-domains/app/commit/04de78f5ffe6a128cc5fbfc0be4ade443eeb9a7b))

### [1.39.5](https://gitlab.com/tezos-domains/app/compare/v1.39.4...v1.39.5) (2023-10-10)


### Bug Fixes

* update plenty url and banner ([9dd7b12](https://gitlab.com/tezos-domains/app/commit/9dd7b12d7b0bbed35f13d044aff55744831e4fee))

### [1.39.4](https://gitlab.com/tezos-domains/app/compare/v1.39.3...v1.39.4) (2023-10-10)

### [1.39.3](https://gitlab.com/tezos-domains/app/compare/v1.39.2...v1.39.3) (2023-10-10)


### Bug Fixes

* storage limit for claim ([3692f70](https://gitlab.com/tezos-domains/app/commit/3692f706d43fdd253bec7079317887dcf43fe98e))

### [1.39.2](https://gitlab.com/tezos-domains/app/compare/v1.39.0...v1.39.2) (2023-10-10)


### Bug Fixes

* update storage limit ([9d45c7c](https://gitlab.com/tezos-domains/app/commit/9d45c7c37747c4294ced337b416d450f0b72147e))

### [1.39.1](https://gitlab.com/tezos-domains/app/compare/v1.39.0...v1.39.1) (2023-10-10)


### Bug Fixes

* update storage limit ([9d45c7c](https://gitlab.com/tezos-domains/app/commit/9d45c7c37747c4294ced337b416d450f0b72147e))

## [1.39.0](https://gitlab.com/tezos-domains/app/compare/v1.38.3...v1.39.0) (2023-10-10)


### Features

* add voting power view, use price from gate.io and other small changes ([4954714](https://gitlab.com/tezos-domains/app/commit/4954714f3475bcfa475de7ecc856740d5e82087a))

### [1.38.3](https://gitlab.com/tezos-domains/app/compare/v1.38.2...v1.38.3) (2023-10-06)


### Bug Fixes

* various airdrop fixes ([41d4529](https://gitlab.com/tezos-domains/app/commit/41d4529247cd22382809d52ce7a89f32c1afa99f))

### [1.38.2](https://gitlab.com/tezos-domains/app/compare/v1.38.1...v1.38.2) (2023-10-05)

### [1.38.1](https://gitlab.com/tezos-domains/app/compare/v1.38.0...v1.38.1) (2023-10-04)

## [1.38.0](https://gitlab.com/tezos-domains/app/compare/v1.37.0...v1.38.0) (2023-10-03)


### Features

* add airdrop and governance features ([7114151](https://gitlab.com/tezos-domains/app/commit/7114151b5df3b9cda51f157434edb4ce716b8ce4))
* add airdrop translations ([117e26e](https://gitlab.com/tezos-domains/app/commit/117e26eb15c279d59051f2dc498c5315c37f6cdd))

## [1.37.0](https://gitlab.com/tezos-domains/app/compare/v1.36.0...v1.37.0) (2023-09-29)


### Features

* change banner message ([e5cb1ad](https://gitlab.com/tezos-domains/app/commit/e5cb1ad4d1970d76c3caa8f8198697ec1976a08b))

## [1.36.0](https://gitlab.com/tezos-domains/app/compare/v1.35.0...v1.36.0) (2023-09-25)


### Features

* graphical banner ([5ca2ec8](https://gitlab.com/tezos-domains/app/commit/5ca2ec897ecca383015860d8e5b3ae85de476c1f))

## [1.35.0](https://gitlab.com/tezos-domains/app/compare/v1.34.0...v1.35.0) (2023-09-21)


### Features

* add global announcement ([180a1ee](https://gitlab.com/tezos-domains/app/commit/180a1eeaa247a1fd0e231e5b4630b720256332e4))

## [1.34.0](https://gitlab.com/tezos-domains/app/compare/v1.33.0...v1.34.0) (2023-09-12)


### Features

* improve lcp performance ([83139d7](https://gitlab.com/tezos-domains/app/commit/83139d74f9a99b97f8775c430e14ec3d633a0b8b))
* update to beacon 4.0.10 ([2948c90](https://gitlab.com/tezos-domains/app/commit/2948c90cb961a5c359a7ad228916ee518159f045))

## [1.33.0](https://gitlab.com/tezos-domains/app/compare/v1.32.1...v1.33.0) (2023-06-26)


### Features

* upgrade taquito and beacon-sdk ([d8e235c](https://gitlab.com/tezos-domains/app/commit/d8e235c9f9667fe802778610f55471c8b02da4f7))

### [1.32.1](https://gitlab.com/tezos-domains/app/compare/v1.32.0...v1.32.1) (2023-06-12)


### Bug Fixes

* auction period for domains with old expired auction settlement that also have expired ([cd6f788](https://gitlab.com/tezos-domains/app/commit/cd6f7882c724d7be6e95bb5a8765433a6cd90009))

## [1.32.0](https://gitlab.com/tezos-domains/app/compare/v1.31.0...v1.32.0) (2023-05-16)


### Features

* add translation for multi-registration button and remove not needed translations ([e7c80e9](https://gitlab.com/tezos-domains/app/commit/e7c80e90b5c4b0983b8c85d59a4226d085eda9c1))
* change the flip switch color to primary blue in dark mode ([669e64b](https://gitlab.com/tezos-domains/app/commit/669e64be0ea37a3714a9186742500673cb5c79b1))


### Bug Fixes

* buy offer removed translation ([c9e3a61](https://gitlab.com/tezos-domains/app/commit/c9e3a61593daf4f5088f28990845ce2974f620c5))
* correctly show auction screen if in auction period ([0f88f27](https://gitlab.com/tezos-domains/app/commit/0f88f274a371f97146de7f6815b369f3481dbecf))
* prevent long domain name overflow in the status bar ([94fa961](https://gitlab.com/tezos-domains/app/commit/94fa961a0501d669d41d2cc92ad0f3f8ac271c5e))

## [1.31.0](https://gitlab.com/tezos-domains/app/compare/v1.30.0...v1.31.0) (2023-02-09)


### Features

* bulk registration ([ed159f8](https://gitlab.com/tezos-domains/app/commit/ed159f83da8502748b6121ba2cb155160e88b540))
* include buy offer events in activity feed ([7230ce8](https://gitlab.com/tezos-domains/app/commit/7230ce85af84edcb7d372936bf765025656b6e24))
* update to taquito 15.1 and latest beacon ([999b823](https://gitlab.com/tezos-domains/app/commit/999b8239ce3dc21b5616f483facfed531a4a0fde))

## [1.30.0](https://gitlab.com/tezos-domains/app/compare/v1.29.1...v1.30.0) (2023-01-24)


### Features

* add ability to impersonate an account for non-prod env ([27333b8](https://gitlab.com/tezos-domains/app/commit/27333b8edeeb1bee6f1dafe2511bb5bc769ec8e3))
* add ability to sort by Price on listing pages ([828c599](https://gitlab.com/tezos-domains/app/commit/828c599173d355cfaeb770f626bb33c624b4ca02))
* add limanet support and remove kahtmandunet ([c054b3d](https://gitlab.com/tezos-domains/app/commit/c054b3d726776cb6ba54a669035ccee81b9c3449))
* add warning when buy offer expires later than the domain expires ([6162eb7](https://gitlab.com/tezos-domains/app/commit/6162eb7c4e3a580a713f173f982bb2065bb8bb35))
* bulk cancel secondary market offers ([b4ae307](https://gitlab.com/tezos-domains/app/commit/b4ae307fcde4825e5ebdd89248a0a915882c09ba))
* bulk withdraw buy offers ([bd91a32](https://gitlab.com/tezos-domains/app/commit/bd91a320b0052792f973a63534b160258dd55636))


### Bug Fixes

* purchase domain from secondary market flow improvement ([e015c64](https://gitlab.com/tezos-domains/app/commit/e015c6424b773b85001c626579517cace51557ab))
* small layout fixes ([bf70060](https://gitlab.com/tezos-domains/app/commit/bf70060ae82f7a427c407d6017636aefd1894850))
* styling fix for activity page ([a09bbe4](https://gitlab.com/tezos-domains/app/commit/a09bbe4477cd80da08129a8ac09d82caea2bf101))

### [1.29.1](https://gitlab.com/tezos-domains/app/compare/v1.29.0...v1.29.1) (2022-12-12)


### Bug Fixes

* extend conditions when to show buy-offers ([f3864b5](https://gitlab.com/tezos-domains/app/commit/f3864b5c54a5343f29010379ded201a2a81e6339))

## [1.29.0](https://gitlab.com/tezos-domains/app/compare/v1.28.1...v1.29.0) (2022-12-02)


### Features

* update to latest taquito and beacon-sdk ([ee21793](https://gitlab.com/tezos-domains/app/commit/ee21793d357b12d5a8773d0cc3c77296d7a4ca15))
* use beacon native UI elements for connecting to user wallet ([6ff14a3](https://gitlab.com/tezos-domains/app/commit/6ff14a3bf75304574bb57ba0f86204de7ca5fb4d))

### [1.28.1](https://gitlab.com/tezos-domains/app/compare/v1.28.0...v1.28.1) (2022-10-19)


### Bug Fixes

* add buyOfferBrokerContract for ghostnet ([ea9e35b](https://gitlab.com/tezos-domains/app/commit/ea9e35bfdcf17a819f02db0515dbb15cec3e2288))
* show domain name on small screen ([0ff4efd](https://gitlab.com/tezos-domains/app/commit/0ff4efddbd2ca4a4e910311281e716f3c1270bed))

## [1.28.0](https://gitlab.com/tezos-domains/app/compare/v1.27.1...v1.28.0) (2022-10-18)


### Features

* buy offer ([a11b356](https://gitlab.com/tezos-domains/app/commit/a11b356129169e0c5bcfdd6477d2ee58077a2d20))

### [1.27.1](https://gitlab.com/tezos-domains/app/compare/v1.27.0...v1.27.1) (2022-10-17)


### Bug Fixes

* update link for publishing websites ([1f43a16](https://gitlab.com/tezos-domains/app/commit/1f43a16091744c42b7aee6d7b01316db99649be6))

## [1.27.0](https://gitlab.com/tezos-domains/app/compare/v1.26.0...v1.27.0) (2022-10-12)


### Features

* drop jakarta support ([207f46a](https://gitlab.com/tezos-domains/app/commit/207f46a0c14c164627f301573170fee1350a10c5))
* upgrade to angular 14 ([503a2da](https://gitlab.com/tezos-domains/app/commit/503a2da33512c304bea47a35a510a647338e6b0b))


### Bug Fixes

* remove distributed website section for dns bridged domains ([1d5abc8](https://gitlab.com/tezos-domains/app/commit/1d5abc8b4075081f842d17a7ca9d06fe5cc68157))

## [1.26.0](https://gitlab.com/tezos-domains/app/compare/v1.25.0...v1.26.0) (2022-09-07)


### Features

* remove ithaca, add kathmandu ([43954ea](https://gitlab.com/tezos-domains/app/commit/43954ea5039cc80ed3606a1ace4edbe98f1f4373))


### Bug Fixes

* chinese translation has an issue for buy domain button ([748f6ef](https://gitlab.com/tezos-domains/app/commit/748f6ef7fd1fc284b32761d583fca280907feb76))

## [1.25.0](https://gitlab.com/tezos-domains/app/compare/v1.24.2...v1.25.0) (2022-08-23)


### Features

* add support for ghostnet ([3f16ed1](https://gitlab.com/tezos-domains/app/commit/3f16ed1f8edc57840928e030159d24300563abc9))
* add support for Naan & Autonomy wallets ([ac5527d](https://gitlab.com/tezos-domains/app/commit/ac5527dab179b80832e41ebb0b5e28be0e5f0eb7))
* add underline for tooltips ([da32e86](https://gitlab.com/tezos-domains/app/commit/da32e8627e46aaa6326dd52c8a1d45eb959fcb77))
* upgrade to angular 13 ([55e6373](https://gitlab.com/tezos-domains/app/commit/55e6373ddca68a22a1180d293da1fdbd8383b8d2))


### Bug Fixes

* show distributed website warning when domain name too long ([98e17b2](https://gitlab.com/tezos-domains/app/commit/98e17b260cb7a217be48c9b829d205efedc303e6))

### [1.24.2](https://gitlab.com/tezos-domains/app/compare/v1.24.1...v1.24.2) (2022-06-29)


### Bug Fixes

* prevent re-rendering when loading more data ([35fe5e3](https://gitlab.com/tezos-domains/app/commit/35fe5e3a90d9c28bdfb34e5592b39d131c46fa0a))
* remove space when user doesn't have expiring domains ([99b7ef1](https://gitlab.com/tezos-domains/app/commit/99b7ef1d37fa29e95a02c8e5a66e0a617d37b5e9))
* update landing page urls ([31030ab](https://gitlab.com/tezos-domains/app/commit/31030ab8c2a662d0b95174c77b0f7486b91d33c2))

### [1.24.1](https://gitlab.com/tezos-domains/app/compare/v1.24.0...v1.24.1) (2022-06-23)


### Bug Fixes

* dark theme styling and wording on help tooltip ([490b05d](https://gitlab.com/tezos-domains/app/commit/490b05d82c2f78564b5ed3fe02ef82cc0b8be5b6))
* update "learn more" link ([65e4e84](https://gitlab.com/tezos-domains/app/commit/65e4e84b3bc8cecce5f1c363c5d6e94afd9a9f42))

## [1.24.0](https://gitlab.com/tezos-domains/app/compare/v1.23.1...v1.24.0) (2022-06-23)


### Features

* add dark-mode switch ([338c3d7](https://gitlab.com/tezos-domains/app/commit/338c3d7609ddcabe2b19d5a1dbd610c9743e4a1b))
* add jakarta support ([e4cf592](https://gitlab.com/tezos-domains/app/commit/e4cf5925d460683d4b7a98fc3dcc4643183acbd2))
* add support to claim a real world domain ([13ea48c](https://gitlab.com/tezos-domains/app/commit/13ea48cbffa60eebe9b4cbf8fcaee49694c2f0db))
* check user preferred theme and add tooltip ([90a2067](https://gitlab.com/tezos-domains/app/commit/90a20678a0849fff63e704aa0df6a345821ca70f))
* ensure dark-mode css gets invalidated upon release ([105a6bd](https://gitlab.com/tezos-domains/app/commit/105a6bd018da26d746165311e42d4ef33fa8930f))


### Bug Fixes

* allow multi renew on mobile ([2783d40](https://gitlab.com/tezos-domains/app/commit/2783d40c373b52d706816c1eae523dd9053c98c0))
* incorrect translation text ([47f3923](https://gitlab.com/tezos-domains/app/commit/47f3923846a8c2d6d2f2e80ad7321a5d79c32e94))
* update to latest client packages ([54d467f](https://gitlab.com/tezos-domains/app/commit/54d467f08fdf4523bd6eec5af610477a1bf82bf7))

### [1.23.1](https://gitlab.com/tezos-domains/app/compare/v1.23.0...v1.23.1) (2022-06-02)


### Bug Fixes

* remove not needed caching ([ff213a2](https://gitlab.com/tezos-domains/app/commit/ff213a20026edae64b96343965a34dbf5ea00f91))

## [1.23.0](https://gitlab.com/tezos-domains/app/compare/v1.22.1...v1.23.0) (2022-05-25)


### Features

* show recently expired domains ([deeff4a](https://gitlab.com/tezos-domains/app/commit/deeff4aa5b9654858542b4eaee837b263a299e55))


### Bug Fixes

* remove no longer needed text ([13ab991](https://gitlab.com/tezos-domains/app/commit/13ab991575f264f5a2d71d002da970da86f81c1f))

### [1.22.1](https://gitlab.com/tezos-domains/app/compare/v1.22.0...v1.22.1) (2022-05-16)


### Bug Fixes

* show auction info for domains where an unclaimed auction has expired ([397bc4c](https://gitlab.com/tezos-domains/app/commit/397bc4cf2a7cc5d7d2db07aafa8416ed1aa0cbea))

## [1.22.0](https://gitlab.com/tezos-domains/app/compare/v1.21.1...v1.22.0) (2022-05-12)


### Features

* add toggle for address removal to transfer screen ([cfc97cb](https://gitlab.com/tezos-domains/app/commit/cfc97cb1c962999c5aeeb981f81c64bebb01dffe))


### Bug Fixes

* load domain details even if expired ([faf1f38](https://gitlab.com/tezos-domains/app/commit/faf1f385030f88ea5dbf011d450b49f0d11fe43b))
* permissions button is visible for domains without token id ([aa0e7cf](https://gitlab.com/tezos-domains/app/commit/aa0e7cfdd22bc7e9b7c4ff97501a57824f8834dc))
* up-for-renewal list selection issue after domains were mass-renewed ([d1e8bb8](https://gitlab.com/tezos-domains/app/commit/d1e8bb82ca511c88cc8e56675ad10b77cff4b4d1))

### [1.21.1](https://gitlab.com/tezos-domains/app/compare/v1.21.0...v1.21.1) (2022-04-13)


### Bug Fixes

* renewal flow for not connected user ([b0e00bb](https://gitlab.com/tezos-domains/app/commit/b0e00bb8fcd67807cd84c86d95c8fd3eeec77322))

## [1.21.0](https://gitlab.com/tezos-domains/app/compare/v1.20.3...v1.21.0) (2022-04-09)


### Features

* update to latest taquito, airgap/beacon-sdk, tezos-domains ([fec55d5](https://gitlab.com/tezos-domains/app/commit/fec55d5b5be77fdc8d5902fd36347fb5c83388dd))

### [1.20.3](https://gitlab.com/tezos-domains/app/compare/v1.20.2...v1.20.3) (2022-04-04)


### Bug Fixes

* dropdown selection behavior (fixes [#192](https://gitlab.com/tezos-domains/app/issues/192)) ([6a8b975](https://gitlab.com/tezos-domains/app/commit/6a8b97539c56f5b4d45f628bff70595af143f735))
* show only offers that have an end date when ordering for offers ending soon ([218c701](https://gitlab.com/tezos-domains/app/commit/218c701748448cfd9d0472ac72271e9f6bd2a53d))

### [1.20.2](https://gitlab.com/tezos-domains/app/compare/v1.20.1...v1.20.2) (2022-03-31)


### Bug Fixes

* domains owned by address not visible if user not connected ([a3c9cca](https://gitlab.com/tezos-domains/app/commit/a3c9cca5c670ca966f5ce67cceb47daa2d250a70))

### [1.20.1](https://gitlab.com/tezos-domains/app/compare/v1.20.0...v1.20.1) (2022-03-31)


### Bug Fixes

* remove unwanted text ([b652c2a](https://gitlab.com/tezos-domains/app/commit/b652c2aa95b7abcee1aabe76958ec5b418357701))
* tr translation update ([2cb4053](https://gitlab.com/tezos-domains/app/commit/2cb40535e5ccfb97e1d9ddba9f5fc64c0ac1666c))

## [1.20.0](https://gitlab.com/tezos-domains/app/compare/v1.19.0...v1.20.0) (2022-03-31)


### Features

* add notification into subscription the registration flow wizard ([29abdca](https://gitlab.com/tezos-domains/app/commit/29abdca4a59fede9d8d6b137936d315a072ba849))

## [1.19.0](https://gitlab.com/tezos-domains/app/compare/v1.18.0...v1.19.0) (2022-03-23)


### Features

* improve multi domain renewal UX ([b32a4d5](https://gitlab.com/tezos-domains/app/commit/b32a4d57d6f3f3618ab5c0f92cdb1b9b7af71c08))

## [1.18.0](https://gitlab.com/tezos-domains/app/compare/v1.17.0...v1.18.0) (2022-03-23)


### Features

* enhanced activity page ([04b16de](https://gitlab.com/tezos-domains/app/commit/04b16de752e5a401934f18a3f32a05a3f68295bc))
* ithacanet support and upgrade to taquito 11.2.0 ([97d6cb8](https://gitlab.com/tezos-domains/app/commit/97d6cb8e8251e3b56c35f444e4da55cb8392fda0))

## [1.17.0](https://gitlab.com/tezos-domains/app/compare/v1.16.0...v1.17.0) (2022-02-24)


### Features

* add korean translation ([6483a88](https://gitlab.com/tezos-domains/app/commit/6483a88e1ee6211b8efd50b0c54698485835469f))

## [1.16.0](https://gitlab.com/tezos-domains/app/compare/v1.15.1...v1.16.0) (2022-02-15)


### Features

* add indonesian translation ([6462da4](https://gitlab.com/tezos-domains/app/commit/6462da44970d31eb1be5572bee400fd07d92a24b))
* load preferred language if it was set on the website ([02888c8](https://gitlab.com/tezos-domains/app/commit/02888c83ac186d2ed6f6afda2443a0f26e83e114))

### [1.15.1](https://gitlab.com/tezos-domains/app/compare/v1.15.0...v1.15.1) (2022-02-08)


### Bug Fixes

* translation issues ([4243209](https://gitlab.com/tezos-domains/app/commit/4243209f3c18fd82cf3f02cd122c854426269d92))

## [1.15.0](https://gitlab.com/tezos-domains/app/compare/v1.14.1...v1.15.0) (2022-02-07)


### Features

* add tr translation ([3caa9bc](https://gitlab.com/tezos-domains/app/commit/3caa9bc61bbfa8f3bae4a1a08f9ed2cec3e24b29))

### [1.14.1](https://gitlab.com/tezos-domains/app/compare/v1.14.0...v1.14.1) (2022-01-25)

## [1.14.0](https://gitlab.com/tezos-domains/app/compare/v1.13.2...v1.14.0) (2022-01-25)


### Features

* allow multi domain renewal ([a49e387](https://gitlab.com/tezos-domains/app/commit/a49e38751dff9077807fdf840fef496f00091e34))
* allow user to add/remove operators ([133e630](https://gitlab.com/tezos-domains/app/commit/133e6301be94fd502dea9456bc8d4da503a97c49))
* cleanup Granada configs ([1eb4458](https://gitlab.com/tezos-domains/app/commit/1eb44580668a86edfb575b12ccefdaffeb2c415c))
* operators should be able to modify domains ([6ea1d86](https://gitlab.com/tezos-domains/app/commit/6ea1d861303aebf9a53fcba733c06973029ff192))
* remove "Report Issue" link and replace with a link to Discord ([5e30bda](https://gitlab.com/tezos-domains/app/commit/5e30bdae1d88d240a7737135f10469b994827725))
* update to new harbinger contract which supports views ([25d169f](https://gitlab.com/tezos-domains/app/commit/25d169f7e44549a725d026a0be3c97c874922fc7))


### Bug Fixes

* detail screen button misaligned on mobile ([e3aa298](https://gitlab.com/tezos-domains/app/commit/e3aa298dd12412342ab067c354856666ca77d8f5))
* fix operator removal request ([2f0c377](https://gitlab.com/tezos-domains/app/commit/2f0c37779abb82b06a43c456cf3e1d4cf34fffad))
* Test environments have unset RPC when started with clean local storage ([3a8fbfe](https://gitlab.com/tezos-domains/app/commit/3a8fbfe9af30fbd6603e1c91b63fe5ac3b639c8e))

### [1.13.2](https://gitlab.com/tezos-domains/app/compare/v1.13.1...v1.13.2) (2021-12-09)


### Bug Fixes

* translation fixes ([1804e4b](https://gitlab.com/tezos-domains/app/commit/1804e4b688d0acc064e385d3e5eceee77b251624))

### [1.13.1](https://gitlab.com/tezos-domains/app/compare/v1.13.0...v1.13.1) (2021-11-02)


### Bug Fixes

* correctly display event text for domains granted forever ([172d013](https://gitlab.com/tezos-domains/app/commit/172d013941214e412564ae4679bc3fcc9103043b))
* ignore case when searching for a domain name ([1f8c3ae](https://gitlab.com/tezos-domains/app/commit/1f8c3ae5804f1d3814eb1a36ef4fa0b70fac5218))

## [1.13.0](https://gitlab.com/tezos-domains/app/compare/v1.12.0...v1.13.0) (2021-10-01)


### Features

* add back giganode ([fa62bc3](https://gitlab.com/tezos-domains/app/commit/fa62bc350b14aa9bb2a15f01a044e186013bd77f))


### Bug Fixes

* fix offers infinite scrolling ([042103d](https://gitlab.com/tezos-domains/app/commit/042103d649b5e20f3fca0bb1d8a07c927e3a6488))

## [1.12.0](https://gitlab.com/tezos-domains/app/compare/v1.11.1...v1.12.0) (2021-09-29)


### Features

* add fr, jp translations ([c9b6229](https://gitlab.com/tezos-domains/app/commit/c9b6229b03331a37eee27997e6886b5da4e9c611))
* add new node urls (tezos domains node, ateza node) ([78481a0](https://gitlab.com/tezos-domains/app/commit/78481a0e5719956253c3b9ac1451b965de86af78))

### [1.11.1](https://gitlab.com/tezos-domains/app/compare/v1.11.0...v1.11.1) (2021-09-28)


### Bug Fixes

* fix an issue for chinese that prevented display of register page when not connected ([289813a](https://gitlab.com/tezos-domains/app/commit/289813a0e06948609e4cab4f4af49cc60e42ae9b))
* fix an issue that prevents creating offer without expiration ([7d5e983](https://gitlab.com/tezos-domains/app/commit/7d5e9839b98e1f53b59730553e76a16001e0158b))

## [1.11.0](https://gitlab.com/tezos-domains/app/compare/v1.10.0...v1.11.0) (2021-09-28)


### Features

* add buy dialog ([0ad4af0](https://gitlab.com/tezos-domains/app/commit/0ad4af0db9323b5612f47f5a3df62fb9519f048e)), closes [#150](https://gitlab.com/tezos-domains/app/issues/150)
* add buy domain button to domain detail ([a6d0bba](https://gitlab.com/tezos-domains/app/commit/a6d0bba475da2cea2c8f007251b03c24f1315b45))
* add listings page & home section ([c09d0d2](https://gitlab.com/tezos-domains/app/commit/c09d0d23dfa6dbc662a8851bb424f5561927186f)), closes [#152](https://gitlab.com/tezos-domains/app/issues/152)
* add my listings to dashboard ([ee34f02](https://gitlab.com/tezos-domains/app/commit/ee34f02c6f1ac39ff6c4440469264c32258e5c78)), closes [#147](https://gitlab.com/tezos-domains/app/issues/147)
* add offer events ([6406fea](https://gitlab.com/tezos-domains/app/commit/6406fea24c5bd13348b719df081728c609d32dc3))
* add pt translation ([7c3b2fc](https://gitlab.com/tezos-domains/app/commit/7c3b2fca018e130348ff4358d33f5cb55de912e7))
* add search results page ([265fb1c](https://gitlab.com/tezos-domains/app/commit/265fb1c83d09a7a3b92c37ab49c53cd6530e935b))
* add sell/remove offer dialog, add sell button to domain detail ([5849872](https://gitlab.com/tezos-domains/app/commit/58498728ec6c022bd87ef298fe709955b59c545b)), closes [#149](https://gitlab.com/tezos-domains/app/issues/149)
* add social share buttons after placing offer ([9d7f8c9](https://gitlab.com/tezos-domains/app/commit/9d7f8c9b3812cd80c8885b933bc1fa23f7230d70))
* add zh, pt, de, es, it, ru translations ([2790e21](https://gitlab.com/tezos-domains/app/commit/2790e214b2aadac11127912d0a8ad7322e0bdba1))
* poc marketplace integration ([20a096a](https://gitlab.com/tezos-domains/app/commit/20a096a61d9ba1418488a10088c35cc001393aff))


### Bug Fixes

* correclty display error message when subscribing to notification fails ([a42245e](https://gitlab.com/tezos-domains/app/commit/a42245e802ef80c747ab79555374087ce237ebea)), closes [#157](https://gitlab.com/tezos-domains/app/issues/157)

## [1.10.0](https://gitlab.com/tezos-domains/app/compare/v1.9.0...v1.10.0) (2021-09-01)


### Features

* add copy pairing request button to airgap connector ([a173157](https://gitlab.com/tezos-domains/app/commit/a173157314fcf8bbe2bcaf6b2c2013b7a9089811)), closes [#145](https://gitlab.com/tezos-domains/app/issues/145)
* disallow transfering of ownership to KT1 addresses ([36eea21](https://gitlab.com/tezos-domains/app/commit/36eea212e1cd2f4d28dd7d35da54b80c04a98cd8))


### Bug Fixes

* add hack to correctly display temple on firefox ([c7fcc78](https://gitlab.com/tezos-domains/app/commit/c7fcc782b8d6aae2d08edf7aef682c653f3f9456))

## [1.9.0](https://gitlab.com/tezos-domains/app/compare/v1.8.0...v1.9.0) (2021-08-06)


### Features

* add umami wallet ([45545b8](https://gitlab.com/tezos-domains/app/commit/45545b8659bd41f1275203823ef881ca645c2a5f))

## [1.8.0](https://gitlab.com/tezos-domains/app/compare/v1.7.1...v1.8.0) (2021-07-14)


### Features

* integrate tzprofiles icon ([ed54280](https://gitlab.com/tezos-domains/app/commit/ed54280095845389099f08b5255ab78c9a690caf))

### Bug Fixes

* switch to webhid for Linux/Mac Chrome as a workaround for ledger not connecting with U2F ([b5085a3](https://gitlab.com/tezos-domains/app/commit/b5085a309d1bb0e2b8c9da13ec9ad7faaa5e2edf))

### [1.7.1](https://gitlab.com/tezos-domains/app/compare/v1.7.0...v1.7.1) (2021-07-06)


### Bug Fixes

* correctly enable/disable fields on website form ([8f52983](https://gitlab.com/tezos-domains/app/commit/8f529835d77606f55b2a546e43b1ea31189cf5aa))

## [1.7.0](https://gitlab.com/tezos-domains/app/compare/v1.6.0...v1.7.0) (2021-07-02)


### Features

* add decentralized web section ([75a3705](https://gitlab.com/tezos-domains/app/commit/75a3705fb59b1164971c4753f7fc72797e549706))
* add link to websites guide ([add0694](https://gitlab.com/tezos-domains/app/commit/add0694c9cc8a4792682399eff26ee3d0a4a474e))
* add twitter validation and redirect url ([f3049e7](https://gitlab.com/tezos-domains/app/commit/f3049e7e9426d9885dbc6e5ee229c88f8af63abd))
* allow http content url ([52dd382](https://gitlab.com/tezos-domains/app/commit/52dd382ffd7115e7e33a8e94d10813434da22a0f))
* automatically add metadata row when autocomplete value is selected ([6904858](https://gitlab.com/tezos-domains/app/commit/6904858611af9964b7f738aada6ce86248171b85))

## [1.6.0](https://gitlab.com/tezos-domains/app/compare/v1.5.1...v1.6.0) (2021-06-01)


### Features

* add content url editor ([0ea50c7](https://gitlab.com/tezos-domains/app/commit/0ea50c7916be02f05272612d81cf0663904c82e0))
* display domain transfer event ([48fb869](https://gitlab.com/tezos-domains/app/commit/48fb8692bed990508ef9a420d9fd7fcdb3e6f97b))

### [1.5.1](https://gitlab.com/tezos-domains/app/compare/v1.5.0...v1.5.1) (2021-05-20)


### Bug Fixes

* disallow html injection ([e952477](https://gitlab.com/tezos-domains/app/commit/e9524772b885715e0169259020662de6018fefb3))
* handle case when commitment is not available on the node yet (retry, display error after) ([eac360e](https://gitlab.com/tezos-domains/app/commit/eac360ed211561a1ba205ed9309229c62cf43ed4)), closes [#137](https://gitlab.com/tezos-domains/app/issues/137)

## [1.5.0](https://gitlab.com/tezos-domains/app/compare/v1.4.1...v1.5.0) (2021-05-18)


### Features

* separate transfer ownership of a domain into a new dialogue ([e121609](https://gitlab.com/tezos-domains/app/commit/e121609d9fe7adea933384044e72893dd0f5634c))
* transfer ownership as NFT when possible, allow to transfer to a domain ([a724101](https://gitlab.com/tezos-domains/app/commit/a72410109055d802a29d3cd89b0fcf4811a1d191))

### [1.4.1](https://gitlab.com/tezos-domains/app/compare/v1.4.0...v1.4.1) (2021-05-13)


### Bug Fixes

* convert storage_limit to string before passing to beacon ([8fae7aa](https://gitlab.com/tezos-domains/app/commit/8fae7aaf1da910ac6e0e768d161d743807a4fb03))

## [1.4.0](https://gitlab.com/tezos-domains/app/compare/v1.3.0...v1.4.0) (2021-05-12)


### Features

* allow editing custom metadata fields as JSON in addition to HEX ([1d6bac9](https://gitlab.com/tezos-domains/app/commit/1d6bac9c44f8a0f97d3594d526809f99dd551b93))
* pass storage limit to wallets ([c2923a9](https://gitlab.com/tezos-domains/app/commit/c2923a91e5d5517f544d0d8ce106b1bef6c85aef))


### Bug Fixes

* display error for backtracked operations ([7a5e61c](https://gitlab.com/tezos-domains/app/commit/7a5e61c24eff4120a3fcc1166da8eb342bc8b879))

## [1.3.0](https://gitlab.com/tezos-domains/app/compare/v1.2.0...v1.3.0) (2021-05-10)


### Features

* order popular auctions by bid sum ([3c35410](https://gitlab.com/tezos-domains/app/commit/3c354102e355d29d26e160f8144f9563ad4bd2da))


### Bug Fixes

* actually show indexer outdated message ([98a20c2](https://gitlab.com/tezos-domains/app/commit/98a20c2d0af2d9aa78872c4a03ac0d0be6c68e39))
* poll for correct block on indexer after an operation ([d9fa1b0](https://gitlab.com/tezos-domains/app/commit/d9fa1b090e5e0608a127e6e543a42c821497737a))

## [1.2.0](https://gitlab.com/tezos-domains/app/compare/v1.1.0...v1.2.0) (2021-04-27)


### Features

* display domains that point to wallet address in my domains ([a9b3980](https://gitlab.com/tezos-domains/app/commit/a9b39806e12f0ec50541748d53b6eb44827fb091))
* update to taquito 9 and beacon 2.2.5 (improves p2p connectivity) ([be5540d](https://gitlab.com/tezos-domains/app/commit/be5540d90071936636dc1779c46743986d330395))


### Bug Fixes

* correct text on subscription dialog ([976dd0b](https://gitlab.com/tezos-domains/app/commit/976dd0b2d31d3bf47b9834a0cac1f7a72d51d9a0)), closes [#132](https://gitlab.com/tezos-domains/app/issues/132)
* properly remove connect status cookie when logging out ([8515227](https://gitlab.com/tezos-domains/app/commit/8515227febeec534d91f6ca8b4ac95fc0af360ba))

## [1.1.0](https://gitlab.com/tezos-domains/app/compare/v1.0.1...v1.1.0) (2021-04-21)


### Features

* make giganode default ([053e68d](https://gitlab.com/tezos-domains/app/commit/053e68d2d3144bfcda9c87d15178219388ba2961))

### [1.0.1](https://gitlab.com/tezos-domains/app/compare/v1.0.0...v1.0.1) (2021-04-21)


### Bug Fixes

* display correct section titles on dashboard vs other account ([e51dbc8](https://gitlab.com/tezos-domains/app/commit/e51dbc8b891b2fb21b7b1fc4e2f06d9de754f3db))
* set unique title on popular auction list ([57cc2ab](https://gitlab.com/tezos-domains/app/commit/57cc2ab1f48f3d6bfa3e00f743e658a6c7a83f72))

## [1.0.0](https://gitlab.com/tezos-domains/app/compare/v0.9.1...v1.0.0) (2021-04-20)


### Features

* **top-nav:** remove beta badge from mainnet ([24d9233](https://gitlab.com/tezos-domains/app/commit/24d923338c1c2f2d8c641f633eb9fd3c4ecf939d))
* add dev audit page ([596cacf](https://gitlab.com/tezos-domains/app/commit/596cacff40527fc46f59ea26793741ab8a1d1317))
* add loading indicator when balance is not loaded yet ([fa4e98a](https://gitlab.com/tezos-domains/app/commit/fa4e98a908e9d50681d71836bf77bdd936671e32)), closes [#126](https://gitlab.com/tezos-domains/app/issues/126)
* add outdated data warning ([b0b01da](https://gitlab.com/tezos-domains/app/commit/b0b01da22423dc50745d6538d74cefac1c1c2ba2))
* **connect:** add warning before connecting on mobile ([11a7ba8](https://gitlab.com/tezos-domains/app/commit/11a7ba841b30774400fc37151d118d376817e3fa)), closes [#129](https://gitlab.com/tezos-domains/app/issues/129)
* calculate domain state from tld config and API data. saves 2 big map gets ([bf561bb](https://gitlab.com/tezos-domains/app/commit/bf561bbbf14a230fa19ea520b107e02512462bf4))
* display background refresh errors ([a5233b3](https://gitlab.com/tezos-domains/app/commit/a5233b3f9a875fad7587c1b6b9c39b46181825f8))
* remove activity from not connected account page ([c0ffc6a](https://gitlab.com/tezos-domains/app/commit/c0ffc6a0bba8d4a7980d6f185b1605e730198c97))
* show claim text instead of go to auction when state is CanBeSettled on auction table ([ed85f29](https://gitlab.com/tezos-domains/app/commit/ed85f295ab651d3a734dc4c181628199a414ab7a))
* show special cta when domain is taken after search ([57f594c](https://gitlab.com/tezos-domains/app/commit/57f594cff3b627750b0d7a4c3ea64d8cf99116f7)), closes [#125](https://gitlab.com/tezos-domains/app/issues/125)
* use more compact style when value is copied to clipboard ([cebd965](https://gitlab.com/tezos-domains/app/commit/cebd96580b2e4970832db2bddcb81984620e7308))


### Bug Fixes

* **connect:** remove extra trailing slash in galleon url ([d2072ed](https://gitlab.com/tezos-domains/app/commit/d2072ed4d722d06b2b20733566b5a6610de58ce5)), closes [#130](https://gitlab.com/tezos-domains/app/issues/130)
* remove hardcoded GA id ([37866dd](https://gitlab.com/tezos-domains/app/commit/37866dda574386eb7779b12196270e348c81f551))

### [0.9.1](https://gitlab.com/tezos-domains/app/compare/v0.9.0...v0.9.1) (2021-04-12)


### Features

* add more info when domain is unobtainable ([ccc1f7f](https://gitlab.com/tezos-domains/app/commit/ccc1f7f269f8ddcc5e2f19ca1ee3ead73ef417cd))


### Bug Fixes

* enable ga tracking ([d22a563](https://gitlab.com/tezos-domains/app/commit/d22a56326ab8d5429a1331a8f744734ae15f6ab1))
* hide notification bell when notifications are disabled ([f33cf96](https://gitlab.com/tezos-domains/app/commit/f33cf96cfb53b2e240cefdd350d3191aa0135932))

## [0.9.0](https://gitlab.com/tezos-domains/app/compare/v0.8.1...v0.9.0) (2021-04-09)


### Features

* add dashboard cta ([005cace](https://gitlab.com/tezos-domains/app/commit/005caced768d221dd91b2c3f195db09aae383023))
* add more rpc url options ([432a18b](https://gitlab.com/tezos-domains/app/commit/432a18b534110e80ac8c2643f4460746dd147ef3))
* dashboard update ([af089e8](https://gitlab.com/tezos-domains/app/commit/af089e88b781ef1b743aeae9bf8eea114fa78a37)), closes [#115](https://gitlab.com/tezos-domains/app/issues/115)
* new topnav + account dialog ([bdbe8b0](https://gitlab.com/tezos-domains/app/commit/bdbe8b08f32e8ffd42de8249c27f711f723fd2f6)), closes [#116](https://gitlab.com/tezos-domains/app/issues/116)
* remove everything wallet related from dashboard ([157bce0](https://gitlab.com/tezos-domains/app/commit/157bce0a08241e19e1d55be316a64e85a0788ec8)), closes [#114](https://gitlab.com/tezos-domains/app/issues/114)
* remove notifications from settings, rename Hardware wallet to Ledger ([0cf8f44](https://gitlab.com/tezos-domains/app/commit/0cf8f44e8e85eb9d4a45cacd851526e7e5b0047d)), closes [#117](https://gitlab.com/tezos-domains/app/issues/117)
* save connect status cookie ([9dceea8](https://gitlab.com/tezos-domains/app/commit/9dceea812314e53ebe6d8a9cf2c272566cee0e87)), closes [#118](https://gitlab.com/tezos-domains/app/issues/118)
* update taquito & client to use local pack ([8baf4b2](https://gitlab.com/tezos-domains/app/commit/8baf4b2e74b7b31ce720eb8868aaf88b59fa9756))


### Bug Fixes

* go to dashboard (/launch) from confirm email ([21ea473](https://gitlab.com/tezos-domains/app/commit/21ea4730fa528ced42f98d623bd37612d33fd5d3)), closes [#121](https://gitlab.com/tezos-domains/app/issues/121)
* handle event case when reverse record name is empty ([1ae46fe](https://gitlab.com/tezos-domains/app/commit/1ae46fe489c8c011c78b9a0862c167bcb8b692ce))
* show temple wallet even if not installed ([caad76f](https://gitlab.com/tezos-domains/app/commit/caad76fcca3cd48b2dc42893915e9911dde10cd1))

### [0.8.1](https://gitlab.com/tezos-domains/app/compare/v0.8.0...v0.8.1) (2021-03-16)


### Features

* **auctions:** make bid amount imput bigger ([1223cdd](https://gitlab.com/tezos-domains/app/commit/1223cddb938ef7d04ae13a6643557d1b5437912d))
* track search ([db26ef1](https://gitlab.com/tezos-domains/app/commit/db26ef187105a9fb48ff03bd0e35133d370c6e8d))
* **activity:** add grant event ([64aa960](https://gitlab.com/tezos-domains/app/commit/64aa960a8aaaedac98dc7d1b773258b73033cf07))
* **auctions:** add auction list ([3a10024](https://gitlab.com/tezos-domains/app/commit/3a10024ef6156147355dcd04cca7953e10dce301))
* **auctions:** show info about extending auction end ([21beb55](https://gitlab.com/tezos-domains/app/commit/21beb5569186632634959a56449a298a692e47fc))
* add GTM ([e3aea22](https://gitlab.com/tezos-domains/app/commit/e3aea227a966932955b80f9f78b52cd73887d177))
* add more metadata fields to data editor ([574e966](https://gitlab.com/tezos-domains/app/commit/574e9667eeeef3b5b5e36eda9c00b3d163e3f7fc)), closes [#105](https://gitlab.com/tezos-domains/app/issues/105)
* allow to set reverse record in batch when claiming a domain from an auction ([b5ad7a6](https://gitlab.com/tezos-domains/app/commit/b5ad7a6e55e78cfd64c1dfb8daeb5c97ba21d1d9)), closes [#103](https://gitlab.com/tezos-domains/app/issues/103)
* display error when request to tezos node fails ([c2b16f3](https://gitlab.com/tezos-domains/app/commit/c2b16f3f95927cd19e71958ad377a7d0870f20bd)), closes [#106](https://gitlab.com/tezos-domains/app/issues/106)
* do not show commit event on account page ([bad951e](https://gitlab.com/tezos-domains/app/commit/bad951e7422c376221717ae74a769ac02a0e9934)), closes [#100](https://gitlab.com/tezos-domains/app/issues/100)
* parse tezos error id, streamline low balance error code ([e595d8e](https://gitlab.com/tezos-domains/app/commit/e595d8ed70b1abd2df2006c2add63ca4f8e9da94))
* refresh auction page data when it ends ([89ec837](https://gitlab.com/tezos-domains/app/commit/89ec8371875ec3cf015bc227f45d5df08f0c2cef))
* streamline settings design ([cb4a724](https://gitlab.com/tezos-domains/app/commit/cb4a7245d6d0c7d1f838313fdd06e84711f70f70)), closes [#102](https://gitlab.com/tezos-domains/app/issues/102) [#99](https://gitlab.com/tezos-domains/app/issues/99)
* **auctions:** add countdown on auction detail page ([3e6dbba](https://gitlab.com/tezos-domains/app/commit/3e6dbba96ec81d8d0f847f5e1baac25dbd439efa)), closes [#98](https://gitlab.com/tezos-domains/app/issues/98)
* rename thanos to temple in texts, update connect CTA text ([91e3a39](https://gitlab.com/tezos-domains/app/commit/91e3a39f89ee14a2c418029d081380eb50c5e691))
* **buy:** use new nonce when old commitment expires ([474e21b](https://gitlab.com/tezos-domains/app/commit/474e21b7585f32098cf1379e0955ee5003392778))


### Bug Fixes

* don't error on reloading activity page with custom network settings ([47ac712](https://gitlab.com/tezos-domains/app/commit/47ac7129c8a210a814e391d8fd408ae16b6ba3a2))
* handle case when domain for reverse record is null on account page and reverse record page ([92ae89c](https://gitlab.com/tezos-domains/app/commit/92ae89c8fe028fff49ace5b0f48efcd910662e40))
* improve beacon connect/disconnect stability ([45b1f6f](https://gitlab.com/tezos-domains/app/commit/45b1f6f153cbf5b85de39ed357701a563a8da3f6))
* prevent KT1 address with an entrypoint to be entered as owner ([060a166](https://gitlab.com/tezos-domains/app/commit/060a16670600474a6b0e55b83669cd470ed03a59))
* update beacon to fix temple/kukai in connect dialog ([a120d27](https://gitlab.com/tezos-domains/app/commit/a120d270a8a999b99c9511fe0a147d42cb1a4382))
* **buy:** display price scaled for a year in domain title ([101e819](https://gitlab.com/tezos-domains/app/commit/101e819a5db820f16e4f091bf3ee5f18d5ab4813))
* remove reverse record from storage when api returns null ([a0770b5](https://gitlab.com/tezos-domains/app/commit/a0770b55d9665a61439e3e086d2aa5983bc02348)), closes [#104](https://gitlab.com/tezos-domains/app/issues/104)

## [0.8.0](https://gitlab.com/tezos-domains/app/compare/v0.7.1...v0.8.0) (2021-03-01)


### Features

* update to new edo contracts ([95c3416](https://gitlab.com/tezos-domains/app/commit/95c34162aa6ceb04c32fc6991ed2e7ec11e2157c))

### [0.7.1](https://gitlab.com/tezos-domains/app/compare/v0.7.0...v0.7.1) (2021-02-26)


### Features

* enable notifications on edonet env ([22f294b](https://gitlab.com/tezos-domains/app/commit/22f294bcf6304bb4eda564d7a06e0d216e366624))

## [0.7.0](https://gitlab.com/tezos-domains/app/compare/v0.6.1...v0.7.0) (2021-02-25)


### Features

* add auction lost event, read outbid from bid ([fcd58c1](https://gitlab.com/tezos-domains/app/commit/fcd58c179e34e4c44e48f04941df088743c4b3c9))
* add custom message for indexer timeout ([4f3d98e](https://gitlab.com/tezos-domains/app/commit/4f3d98ec4c52950a0a1c192e888b87f2486d57a7))
* display price for a domain before buy flow ([eefc289](https://gitlab.com/tezos-domains/app/commit/eefc289aabb89247d44ee411a1d262fe72cbcad6))
* display Winner badge on ended auction highest bid ([d6702e1](https://gitlab.com/tezos-domains/app/commit/d6702e15b2fcffbc30743f3156d5a99601457dcd)), closes [#97](https://gitlab.com/tezos-domains/app/issues/97)
* remove reverse record step from buy, send claim RR in batch with buy ([885cda8](https://gitlab.com/tezos-domains/app/commit/885cda8d69438487f706967395a3e6c859dc0859))
* show human readable errors, fix beacon issues, handle backtracked operations ([8e25084](https://gitlab.com/tezos-domains/app/commit/8e25084b72f92e71d9736b3f64137914408f7e82)), closes [#95](https://gitlab.com/tezos-domains/app/issues/95) [#52](https://gitlab.com/tezos-domains/app/issues/52)
* switch to edonet ([19bd0d0](https://gitlab.com/tezos-domains/app/commit/19bd0d024486302166813a6517c8f799084294e7))
* **account:** add withdraw dialog ([49ea28f](https://gitlab.com/tezos-domains/app/commit/49ea28f4a084e73e32946d366493ee2500773edb)), closes [#73](https://gitlab.com/tezos-domains/app/issues/73)
* **account:** hide auction funds when balance is 0 ([aa96cdc](https://gitlab.com/tezos-domains/app/commit/aa96cdc190d171db03e9a70ab85d461d2a4476e4))
* **account:** refresh bidder balances when account page is open ([e3a65ee](https://gitlab.com/tezos-domains/app/commit/e3a65ee07d9fd064115e18572acd5fbf8c5a7bea))
* **activity:** add user activity ([18a483e](https://gitlab.com/tezos-domains/app/commit/18a483eace0fa63a009eb7bb1fecbd40aad81aa0)), closes [#76](https://gitlab.com/tezos-domains/app/issues/76)
* **auction:** display notification dialog after placing a bid ([874eefb](https://gitlab.com/tezos-domains/app/commit/874eefbbcd3935a3723e68dba89a205b39e7ac17))
* **auctions:** add history page (list/detail) ([cf750bf](https://gitlab.com/tezos-domains/app/commit/cf750bf1f62ae30e398221bf215c4d1eef8e7cd3))
* **auctions:** add my auctions page ([b3e07d9](https://gitlab.com/tezos-domains/app/commit/b3e07d9f4bc5c7061db273efb58946c85ceb19f3))
* **auctions:** display summary of auctions in address view and home page ([45309eb](https://gitlab.com/tezos-domains/app/commit/45309eb11746f7d42805bd1ea8cc9bf78d7a97e5))
* **auctions:** hide auctions on my account if none are found ([66b2d38](https://gitlab.com/tezos-domains/app/commit/66b2d38bae6244d6c90c00e5a44fcb833038d85e))
* **auctions:** show popular auctions with high number of bids ([bacf56e](https://gitlab.com/tezos-domains/app/commit/bacf56e306042db05402d3ad3097b79bc91780c9))
* **auctions:** use overlays for bid and settle ([2f8f1ab](https://gitlab.com/tezos-domains/app/commit/2f8f1ab087809299e1a0ca0a48f691194b90d474))
* **buy:** remove wait step (merged into commit step) ([74fa540](https://gitlab.com/tezos-domains/app/commit/74fa5402681bfdb5fecad9988f134bd815df60f0))
* **buy:** use generated nonce ([456c1e5](https://gitlab.com/tezos-domains/app/commit/456c1e59786ffe1ee97d973be8727f110f53c5e1))
* **data-editor:** display human keys in data editor dropdown ([1a1e222](https://gitlab.com/tezos-domains/app/commit/1a1e2227bea5706fcd7935728a498854794cada5)), closes [#70](https://gitlab.com/tezos-domains/app/issues/70)
* **data-editor:** update gravatar hash editor with faq link and preview ([3e9a40a](https://gitlab.com/tezos-domains/app/commit/3e9a40aebb014310fd7da167094f91c19414249f))
* **domain:** display avatar on detail page ([7acbfff](https://gitlab.com/tezos-domains/app/commit/7acbfff9c2c5fc2c2516be7d4757d15f93b4107f))
* **home:** add title ([ab0e338](https://gitlab.com/tezos-domains/app/commit/ab0e338e318b3d58ead640498ae9a9c1d9b82e85)), closes [#67](https://gitlab.com/tezos-domains/app/issues/67)
* **notifications:** add calendar options ([8ca6c91](https://gitlab.com/tezos-domains/app/commit/8ca6c91c9f2929fbf7fa9ef54383062807a8496a)), closes [#96](https://gitlab.com/tezos-domains/app/issues/96)
* **search:** replace invalid TLD with default tld ([b8f55b3](https://gitlab.com/tezos-domains/app/commit/b8f55b3e559b53e77eefdca0d995dd61944eccd2)), closes [#92](https://gitlab.com/tezos-domains/app/issues/92)
* **search:** use the same logic for search bar and register cta ([de8df9f](https://gitlab.com/tezos-domains/app/commit/de8df9f5593ee6d940bb21b8d0af12f6e8f2ef30)), closes [#91](https://gitlab.com/tezos-domains/app/issues/91)
* add skeleton loader on home, account, history auction pages ([6f66404](https://gitlab.com/tezos-domains/app/commit/6f664046cb351e72f6ea4ea0cd14f73f8eb9d077)), closes [#71](https://gitlab.com/tezos-domains/app/issues/71)
* added route for navigating to specified domain name with some postprocessing ([6c4f866](https://gitlab.com/tezos-domains/app/commit/6c4f8662853577cbe0d28d034efaf3524c42abee))
* display gravatars ([b68a3b8](https://gitlab.com/tezos-domains/app/commit/b68a3b82b64cf56cacd7e86bbfca14dd84b3182f)), closes [#58](https://gitlab.com/tezos-domains/app/issues/58)
* redirect to home after logout ([9a3c4ee](https://gitlab.com/tezos-domains/app/commit/9a3c4ee64379c026aa9e3d64c56abb9a0ccbbf7a))
* show approx USD value for bid/buy/renew price ([e83fabf](https://gitlab.com/tezos-domains/app/commit/e83fabf978ebfbaf14d33c735f4825dc492e5a05)), closes [#78](https://gitlab.com/tezos-domains/app/issues/78)
* update beacon, add galleon connector, only disconnect on reconnectRequired ([421303a](https://gitlab.com/tezos-domains/app/commit/421303ae17a1c1db3d8f7d7d58555e4bcbfff26a)), closes [#86](https://gitlab.com/tezos-domains/app/issues/86)
* update to beacon v2, taquito 7.1 - show list of wallets intead of Beacon API ([9800a45](https://gitlab.com/tezos-domains/app/commit/9800a45381ecbb444e33adf1adac192374795b69))
* update to delphinet contracts #2 ([4bafa97](https://gitlab.com/tezos-domains/app/commit/4bafa97c90e4a9184a01e6b8f466869eda314191)), closes [#88](https://gitlab.com/tezos-domains/app/issues/88) [#89](https://gitlab.com/tezos-domains/app/issues/89)
* update to tzip16 client ([253a063](https://gitlab.com/tezos-domains/app/commit/253a063e2e05ff7ab82e4b2b99fd1811f8173cb0))
* use events for recent activity ([ec67b1c](https://gitlab.com/tezos-domains/app/commit/ec67b1cc3bea4323756a0d21ae678ffc3253a973))
* **home:** show register cta only if user has no running auctions and no domains ([48a6103](https://gitlab.com/tezos-domains/app/commit/48a6103a2337575481406a3ffe0c13afeb4ee59f))
* **menu:** update link to faq to point to the landing page ([febd20a](https://gitlab.com/tezos-domains/app/commit/febd20a99f37fa9fe55e8606e4cdff5d45e3a1d5))
* **notifications:** add email confirm and unsubscribe pages ([a660ab7](https://gitlab.com/tezos-domains/app/commit/a660ab7ec1fae339fc971c16a28651fdff4e6e05))
* **notifications:** add subscribe functionality ([3e01211](https://gitlab.com/tezos-domains/app/commit/3e012111764334c46f6eea30419abd4645a78ffe))
* **settings:** display reverse record with address (+ avatar) ([04647d7](https://gitlab.com/tezos-domains/app/commit/04647d743d7d63498403d2d08f298f12dd00b24a))
* **settings:** improve rpc url and derivation path add custom options overlays ([ca5e69d](https://gitlab.com/tezos-domains/app/commit/ca5e69db0fa8cb464aad752f2464781fb4dde648))


### Bug Fixes

* correctly reload data when switching between account views ([0a58832](https://gitlab.com/tezos-domains/app/commit/0a588329f2114865007b68f01cfb374108c6b8ce))
* **data_editor:** update keys in autocomplete when a row is removed ([1103f36](https://gitlab.com/tezos-domains/app/commit/1103f3657101b0a526fe84343862ac6afb00ace1))

### [0.6.1](https://gitlab.com/tezos-domains/app/compare/v0.6.0...v0.6.1) (2020-11-28)

## [0.6.0](https://gitlab.com/tezos-domains/app/compare/v0.5.0...v0.6.0) (2020-11-28)


### Features

* display all domains sorted by level in short domains tables ([ab1da70](https://gitlab.com/tezos-domains/app/commit/ab1da70922484615a254c238b913eb41c9b0a676))
* **auctions:** add list of bids ([e25191e](https://gitlab.com/tezos-domains/app/commit/e25191eb4575aeeee19aeec7e6bd56b026dfe1cc))


### Bug Fixes

* **domain-detail:** correctly display subdomains for all users ([d3ef273](https://gitlab.com/tezos-domains/app/commit/d3ef273290426e31180101454c18c82a07f675e3))
* **domain-detail:** dont load and use acquisitionInfo for non 2nd level domains ([89eb224](https://gitlab.com/tezos-domains/app/commit/89eb224327d7eb14b48df92adffdad4c9d4946db))

## [0.5.0](https://gitlab.com/tezos-domains/app/compare/v0.4.0...v0.5.0) (2020-11-25)


### Features

* add buttons to set current address or clear an address input (edit domain/rr, buy) ([779b786](https://gitlab.com/tezos-domains/app/commit/779b786e6662c835303e19320a12ff447ee7272b)), closes [#51](https://gitlab.com/tezos-domains/app/issues/51)
* add openid address (JSON metadata value) ([f9e1459](https://gitlab.com/tezos-domains/app/commit/f9e1459cccb64b238aa0a1bad9e33beb26b69ccf))
* **auctions:** add basic auction UI for bidding and settle ([8e60a5e](https://gitlab.com/tezos-domains/app/commit/8e60a5eef48d41f8085c253f270ae0708935f7c7))
* **auctions:** add bidder balance features (display, withdraw) ([e1b9c5c](https://gitlab.com/tezos-domains/app/commit/e1b9c5c64e3fe0cd192804175a26d88e93a473df))
* **auctions:** refresh auction state on auction page periodically ([2b3cd02](https://gitlab.com/tezos-domains/app/commit/2b3cd0289878c44cd606bd350e16bb348b52db84))
* **register:** move address field to buy flow - register step ([7facdb4](https://gitlab.com/tezos-domains/app/commit/7facdb48611f7139aa9b5fe529a57ff7b4846477))


### Bug Fixes

* use umd version of punycode, because upstream cant use es6. fixes dev wallet name resolution ([a05ae61](https://gitlab.com/tezos-domains/app/commit/a05ae61c1eceff260ae3db3e904d7b658b2602cf))

## [0.4.0](https://gitlab.com/tezos-domains/app/compare/v0.3.0...v0.4.0) (2020-10-30)


### Features

* **app:** direct Thanos integration removed in favor of Beacon starting with Delphi ([9696bb2](https://gitlab.com/tezos-domains/app/commit/9696bb2d163941a6e51aa0eff653483844232c78))


### Bug Fixes

* **domain-detail:** make subdomain section visible to users that can add subdomains even if there are none ([21c3598](https://gitlab.com/tezos-domains/app/commit/21c3598c36f5dcf5089e1ad1b9a27d2db0dd343c))

## [0.3.0](https://gitlab.com/tezos-domains/app/compare/v0.2.0...v0.3.0) (2020-10-26)


### Features

* support delphinet, switch staging env to delphi ([baaf9ee](https://gitlab.com/tezos-domains/app/commit/baaf9ee40ae618a2478cac8a4e094397e06668b7))
* **address:** add address detail page ([65489fc](https://gitlab.com/tezos-domains/app/commit/65489fc6c3e4908a509efc6be75b86f866119adb))
* add links to setup reverse record from home and menu ([1acfd20](https://gitlab.com/tezos-domains/app/commit/1acfd20c8aa2f600fabc74577199d66b3f64ddf4))
* **app:** mark required fields ([7b259d1](https://gitlab.com/tezos-domains/app/commit/7b259d1130c978549ae0054e9fdd84b805f10fd9))
* **app:** wait for indexer in each operation ([05a9b9c](https://gitlab.com/tezos-domains/app/commit/05a9b9ccf6e220882ff6ef91b7d317d87b3d50e3))
* **data_editor:** display better names for known keys ([57ba724](https://gitlab.com/tezos-domains/app/commit/57ba724ce7bf0158852462d81d7d3d6ff2dc73a4))
* **domain:** add data viewer and editor ([949d4f7](https://gitlab.com/tezos-domains/app/commit/949d4f72ea7fd063f5aa55f598a0fd8d2842fab1))
* **domain-detail:** apply new UX ([c758eab](https://gitlab.com/tezos-domains/app/commit/c758eab9c5405a5fa22504968c83e36239e50f4d))
* **domains:** display full hierarchy in domain table if ancestors are not owned ([927d7b4](https://gitlab.com/tezos-domains/app/commit/927d7b41b4e48ad25b841465db4d8235c2f2e009))
* **reverse_records:** display not owned but same address reverse record in the list ([5c7e8e9](https://gitlab.com/tezos-domains/app/commit/5c7e8e9952ea1010f29e417bfcd6dadb6487fcc5))
* add clear address and clear name buttons to domain and reverse record forms ([d005630](https://gitlab.com/tezos-domains/app/commit/d005630d83081d0f6bb9b4d5f9bdef46f62aaf6b))
* **home:** refresh recent activity and stats periodically ([f826048](https://gitlab.com/tezos-domains/app/commit/f826048fb6b4132c92eee96064b3c281105755bb))
* **reverse_records:** add reverse record list and detail pages ([3ef6ffc](https://gitlab.com/tezos-domains/app/commit/3ef6ffcccc4cd8a5fbf0665d73ee29da72683c93))
* **reverse_records:** link reverse records from domain page and reuse edit dialog ([abb994d](https://gitlab.com/tezos-domains/app/commit/abb994d966b38dd6a825bc253a7310e265d34ec9))


### Bug Fixes

* language ([091e3fa](https://gitlab.com/tezos-domains/app/commit/091e3fadebec47f67599842b56d0370a2a759944))

## [0.2.0](https://gitlab.com/tezos-domains/app/compare/v0.1.1...v0.2.0) (2020-09-16)


### Features

* **app:** display balance and reject operations if it's too low ([1421747](https://gitlab.com/tezos-domains/app/commit/1421747807215bda5e5f65deb78909c2a09abcf7))
* **app:** integrate offline recovery to base data source ([d77c341](https://gitlab.com/tezos-domains/app/commit/d77c34185cc50871e099ecaae604ff7cb0d7aac8))
* **domain-list:** display reverse record in domain list title ([c9f99cf](https://gitlab.com/tezos-domains/app/commit/c9f99cf83efb4a3036dd4c66b4b75b073e31c7e6))
* **edit:** add stronger confirmation when transferring ownership ([c32e327](https://gitlab.com/tezos-domains/app/commit/c32e3275ed2fe7b96eee62f1b887d242c23630ef))
* **home:** add infinite scroll for recent activity ([dfdc97c](https://gitlab.com/tezos-domains/app/commit/dfdc97c386bb1d9dc446bcce499f6985663012ac))
* **home:** display 10 items instead of 5 in recent activity ([1979baa](https://gitlab.com/tezos-domains/app/commit/1979baa9da5ff06dd6a978d81e1b6a0583a6da9d))
* **home:** validate and check availability of entered domain on register cta ([b66f077](https://gitlab.com/tezos-domains/app/commit/b66f0776a2d5cab3b8f786ac6bb87ccc9f2ee273))


### Bug Fixes

* display correct validation message for tezos address ([51b4af3](https://gitlab.com/tezos-domains/app/commit/51b4af3782c84e0bde07df6bd8f11514270621cf))
* **app:** correctly trim names that are too long ([bfe1128](https://gitlab.com/tezos-domains/app/commit/bfe1128af5497a79961e9b7b54aec1980f3e1ca5))
* **home:** correctly display renew text in recent activity ([d19ee07](https://gitlab.com/tezos-domains/app/commit/d19ee07b1030025fc27ff485829f973abdead8b0))

### [0.1.1](https://gitlab.com/tezos-domains/app/compare/v0.1.0...v0.1.1) (2020-09-09)


### Bug Fixes

* convert string to BigNumber for prices ([8eaa25f](https://gitlab.com/tezos-domains/app/commit/8eaa25fb347127a779a83109fc2311b1524a7c70))

## 0.1.0 (2020-09-09)


### Features

* **app:** add derivation path editor ([1e04573](https://gitlab.com/tezos-domains/app/commit/1e04573ad2f4a480ed1299e11c84fa41352541eb))
* **app:** add duplicate subdomain validation ([0c3faa6](https://gitlab.com/tezos-domains/app/commit/0c3faa66526e951a473bafe8a74a5d0668f6abc5))
* **app:** add home page register domain cta ([aa2eb4e](https://gitlab.com/tezos-domains/app/commit/aa2eb4e0cfc524a46fa7ecffd6f49b359e207594))
* **app:** add homepage ([32191c3](https://gitlab.com/tezos-domains/app/commit/32191c3ef522490928cc5bfa4791b3e48c5ea0f8))
* **app:** add i18n support ([#9](https://gitlab.com/tezos-domains/app/issues/9)) ([f90fc9f](https://gitlab.com/tezos-domains/app/commit/f90fc9fdf275e0f24b4cb51affb6fa94cb81b645))
* **app:** add label length validation ([637a8f5](https://gitlab.com/tezos-domains/app/commit/637a8f5222b8d57e109b1f40d4493d2efc6824dd))
* **app:** add last purchase feed to homepage ([c85f1aa](https://gitlab.com/tezos-domains/app/commit/c85f1aad95749d8e43be20297b9f6a608cb8f560))
* **app:** add loading skeleton on domain detail ([133dbbc](https://gitlab.com/tezos-domains/app/commit/133dbbc65f89698eec424b31249fa9a88ffee5d8))
* **app:** add network indicator ([2f37ce0](https://gitlab.com/tezos-domains/app/commit/2f37ce014b779fe5138f7397556f3a8f912c8d9d))
* **app:** add new settings menu ([0c3b144](https://gitlab.com/tezos-domains/app/commit/0c3b144d35ae6dd17ebd913d04a7ce5616dc5693))
* **app:** add paging/sorting/filtering to domain lists ([#28](https://gitlab.com/tezos-domains/app/issues/28)) ([0694a68](https://gitlab.com/tezos-domains/app/commit/0694a68c559e6589c89090ed5d610b38106f3dcc))
* **app:** add price loading message ([515d06d](https://gitlab.com/tezos-domains/app/commit/515d06de0e11daba87a726793f18412555fa2cc9))
* **app:** add searchbar and copy to clipboard, update favicon ([8a4bd59](https://gitlab.com/tezos-domains/app/commit/8a4bd59222b2642680957e2e42345c7bab93660e))
* **app:** add testing wallet ([7cf4719](https://gitlab.com/tezos-domains/app/commit/7cf4719fd4ffb46f141b5ab626b163321c2757d4))
* **app:** add toastr messages ([e918567](https://gitlab.com/tezos-domains/app/commit/e918567ff1625017a145c9aafae3c194c82babd8))
* **app:** add view in explorer to menu ([531db46](https://gitlab.com/tezos-domains/app/commit/531db4699cf8ed09dc044658215dba2a033f9522))
* **app:** add warning when renewing domain that is not owned by current wallet ([#36](https://gitlab.com/tezos-domains/app/issues/36)) ([b0ce8fd](https://gitlab.com/tezos-domains/app/commit/b0ce8fdbaa86d999fc735de37a3ad874740c268f))
* **app:** app update notification + alert styles ([#8](https://gitlab.com/tezos-domains/app/issues/8)) ([f331e1e](https://gitlab.com/tezos-domains/app/commit/f331e1e930ddd9ea12f2a171e0d21c9ac1c80d65))
* **app:** display tooltip with formatted date on relative date ([dc42df4](https://gitlab.com/tezos-domains/app/commit/dc42df43385a8a57b3cf239f6dfc007eec5501ca))
* **app:** force reconnect if underlaying settings are changed ([2e003aa](https://gitlab.com/tezos-domains/app/commit/2e003aa01fb5df697f9b08ed949e29fef37a692a))
* **app:** hide subdomains when there are 0 ([e0105b4](https://gitlab.com/tezos-domains/app/commit/e0105b49dee4e72bdb1b37dafdc4704587a1ae1e))
* **app:** implement domain table filtering and update gql query ([a9a9eb7](https://gitlab.com/tezos-domains/app/commit/a9a9eb738db3f78b53ad190332657eb51fac5b36))
* **app:** improve search bar ([a299f41](https://gitlab.com/tezos-domains/app/commit/a299f4136dcb01eb21b9f863902e913ab02fa350))
* **app:** improve search bar even more + test button ([a5da13d](https://gitlab.com/tezos-domains/app/commit/a5da13d357861b17a191ef09dd4f37500f9bacc2))
* **app:** improve search bar more ([0be12be](https://gitlab.com/tezos-domains/app/commit/0be12be61064ea3e419229c5fedbabe215a71caf))
* **app:** inline buy form, add set adrress and reverse record steps ([4e12f41](https://gitlab.com/tezos-domains/app/commit/4e12f41920b2e112124c471893497afd99fa243c))
* **app:** integrate ledger signer ([672a66a](https://gitlab.com/tezos-domains/app/commit/672a66a44d205b1c433c8ee829b5cbd76e7dadf8))
* **app:** integrate thanos wallet ([9243402](https://gitlab.com/tezos-domains/app/commit/9243402778d5797a46fbb7fdb5062d7ccdd92024))
* **app:** new connect experience ([41f93d7](https://gitlab.com/tezos-domains/app/commit/41f93d7be001ea142b16166f1b2414ee4553dfe3))
* **app:** offline notification ([a8dc440](https://gitlab.com/tezos-domains/app/commit/a8dc4404b831c074df9efabe164c34671eff3900))
* **app:** reduce redo timeout ([ece909b](https://gitlab.com/tezos-domains/app/commit/ece909bb42a83272ff13fcf3dd17155e3311ccbe))
* **app:** refresh last purchases periodically ([360222b](https://gitlab.com/tezos-domains/app/commit/360222b36aefe7c7c0a21fe37273b2fb2d47f67a))
* **app:** remove indent and arrows from domain table when filter is active ([91045b0](https://gitlab.com/tezos-domains/app/commit/91045b05757dc6b5cc4f26ebb02f38e906e0e3bf))
* **app:** responsive styles, gql schema update, ios friendly searchbar ([c721c39](https://gitlab.com/tezos-domains/app/commit/c721c390842306d5679a443df8f9c22e37a92124))
* **app:** restore filter/sort on back navigation, add loading ([2c32192](https://gitlab.com/tezos-domains/app/commit/2c321920bd9bc0a3676be731274333e70b2da554))
* **app:** switch to new contracts ([2e5c866](https://gitlab.com/tezos-domains/app/commit/2e5c866e643daa55af7d7dcc2b54694310c3d29f))
* **app:** update to August Contracts ([6ae786b](https://gitlab.com/tezos-domains/app/commit/6ae786b139eed87bf0cf51f13f28dbdd66e4f5a5))
* add app config json + gulp tasks to generate it, refactor tezos infrastructure ([97f33bd](https://gitlab.com/tezos-domains/app/commit/97f33bda32b7e96cb6d2f47153d77c9d4d512f2b)), closes [#5](https://gitlab.com/tezos-domains/app/issues/5)
* add identicon ([b2c564d](https://gitlab.com/tezos-domains/app/commit/b2c564d29b59e68473195cea3a8e871d4da26933))
* add subdomain ([b8a322f](https://gitlab.com/tezos-domains/app/commit/b8a322f348692dc4d5cd39ca2dfc414a1a3453bf)), closes [#12](https://gitlab.com/tezos-domains/app/issues/12)
* add warning if changing address will remove reverse record ([c782f03](https://gitlab.com/tezos-domains/app/commit/c782f034422f38a12c8cd9cce423dbb13c003624))
* buy 2ld ([6b7c035](https://gitlab.com/tezos-domains/app/commit/6b7c035628645a40b750e9a93ab9f68a288b6bf0))
* change page title based on route ([#24](https://gitlab.com/tezos-domains/app/issues/24)) ([aeb035c](https://gitlab.com/tezos-domains/app/commit/aeb035c5952f6728b58236f54a3eb9fd5060e328))
* edit domain ([c5bbc10](https://gitlab.com/tezos-domains/app/commit/c5bbc10fa164204db66757f90d1c9862298da2e6)), closes [#13](https://gitlab.com/tezos-domains/app/issues/13)
* invalid input errors + 404 ([aaaf713](https://gitlab.com/tezos-domains/app/commit/aaaf713bf888e8e53aedf9b3e83eafa725e78afa))
* proxy contract address resolve, get real domain data ([164783a](https://gitlab.com/tezos-domains/app/commit/164783a739a9623344b327a37181fab74ecfb90f))
* renew ([bdc9159](https://gitlab.com/tezos-domains/app/commit/bdc91591d7cab76bffe13c0cb9ceb5abf55b1ec4))
* restore scroll position on history navigations ([34d1bbf](https://gitlab.com/tezos-domains/app/commit/34d1bbfdb736e7e4949094496c652902622467bd))
* show more reverse record on domain detail ([4e7017a](https://gitlab.com/tezos-domains/app/commit/4e7017a8d5f632de276ff71080efd2dbbdc81615))
* **app:** use graphql api ([49e01e4](https://gitlab.com/tezos-domains/app/commit/49e01e403837c40338a4c17ff922ef681e25c845))
* **app:** use stepper for registering ([2b990a7](https://gitlab.com/tezos-domains/app/commit/2b990a7c4000ea3ae5f62f11eee7604957abc14b))
* set/display reverse record ([330feef](https://gitlab.com/tezos-domains/app/commit/330feef9dee67781522f323914fc6ede987a4eed))
* show app version in the ui ([fcc0a65](https://gitlab.com/tezos-domains/app/commit/fcc0a658005a5f2edcebc8a7b15b78b654499071))
* **style:** improve card shadow ([ab5f916](https://gitlab.com/tezos-domains/app/commit/ab5f91622630961dd9851f29828477e8295a3489))
* **style:** use monospace font for addresses, remove address link from details address/reverseaddress ([e1f728c](https://gitlab.com/tezos-domains/app/commit/e1f728cd94ae986cff31b049024b6d9ae1b4ebb4))


### Bug Fixes

* bypass service worker in connection check ([ff30894](https://gitlab.com/tezos-domains/app/commit/ff30894f1401d069642ca34ee3abb089b2086a90))
* calculate price base on per year price, add validation for max duration ([e738b19](https://gitlab.com/tezos-domains/app/commit/e738b1997c3f058392af7dc63031c701e816d559))
* clear resolver cache when changing reverse record ([72ca463](https://gitlab.com/tezos-domains/app/commit/72ca4630e4027621dce8b28ef750f9fc0cca1813))
* correctly display error for invalid domain on detail page ([2f206de](https://gitlab.com/tezos-domains/app/commit/2f206de0f6ff4f2daa2b5400637fae8e7914fe6b))
* display time in ios ([49133fd](https://gitlab.com/tezos-domains/app/commit/49133fd53d25e4a5ac5e3d27f567510cd4ad8c24))
* do not flash unconnected content ([572d66a](https://gitlab.com/tezos-domains/app/commit/572d66a8b81329216eda9802f28411dbd2326422))
* fix unit test compilation ([a3afeaf](https://gitlab.com/tezos-domains/app/commit/a3afeaf7d332370b88e0cd79adb31f48b42fbe68))
* move config to root so assets caching is not applied ([e0f7477](https://gitlab.com/tezos-domains/app/commit/e0f747766f23f09c6d96131d50f2273bdbfd610d))
* only allow adding subdomains for owners ([1a74150](https://gitlab.com/tezos-domains/app/commit/1a741506100bff3a2a7a9bf955d222f73f3473e1))
* properly focus searchbar in safari after clicking mag glass button ([5278f3d](https://gitlab.com/tezos-domains/app/commit/5278f3dea25bdc8f058e6bfc4ad6fe413e0980a7))
* remove i18n flatten from code ([31cc769](https://gitlab.com/tezos-domains/app/commit/31cc769aa87acf0de80081a375ecc7291e98699a))
* rollback of bigmap fix, its not necesary ([639fe41](https://gitlab.com/tezos-domains/app/commit/639fe418b128b2576e3e98d3c3adc77e19708385))
* show domain as unowned when expired ([#35](https://gitlab.com/tezos-domains/app/issues/35)) ([e342984](https://gitlab.com/tezos-domains/app/commit/e34298481f643580e9d8a96084b451c605f07561))
* unable to submit form with empty address after it was touched ([586e8aa](https://gitlab.com/tezos-domains/app/commit/586e8aa9c43e9e1cd29f63b6bd356f91b7a6cae1))
* unable to view domain list when not logged in ([668b261](https://gitlab.com/tezos-domains/app/commit/668b26103394fdbda5c9fbccad2086c8c8dd331a))
* update client packages to fix parallel resolve issue ([d15e56f](https://gitlab.com/tezos-domains/app/commit/d15e56f578430f7001a630626c1751e60597aeba))
* update tsconfig ([d1eaceb](https://gitlab.com/tezos-domains/app/commit/d1eaceb726c3a1915de581c9c8e535fd1a9f4f5b))
* use seperate apollo client for stats ([01c059a](https://gitlab.com/tezos-domains/app/commit/01c059a5e0713baf083551f838dc1fe2d456e34b))
* **app:** hide domain table results when loading initially or switching filter ([31cf459](https://gitlab.com/tezos-domains/app/commit/31cf459bd8ed2e9c8af1a828543e4d5414053e0e))
* **app:** wait for wallet when restoring connector from storage ([#31](https://gitlab.com/tezos-domains/app/issues/31)) ([2225015](https://gitlab.com/tezos-domains/app/commit/222501551472e34bc05381c9e56377a0b7a4eb48))
* **custom_rpc:** display error message when url is invalid ([75fcc91](https://gitlab.com/tezos-domains/app/commit/75fcc91be7f6ebc59b8225ec25b1f9920dd97604))
