module.exports = {
    ignorePatterns: ['**/coverage', '**/dist', '**/node_modules', '**/*.config.ts', '**/*.config.js', '.eslintrc.js', 'index-html.transform.js'],
    plugins: ['@typescript-eslint', '@angular-eslint', 'jasmine', 'deprecation'],
    overrides: [
        {
            files: ['*.ts'],
            extends: [
                'plugin:@typescript-eslint/recommended',
                'plugin:@typescript-eslint/recommended-requiring-type-checking',
                'plugin:@angular-eslint/recommended',
            ],
            parser: '@typescript-eslint/parser',
            parserOptions: {
                ecmaVersion: 2020,
                tsconfigRootDir: __dirname,
                project: ['./tsconfig.eslint.json'],
                sourceType: 'module',
            },
            rules: {
                'deprecation/deprecation': 'warn',
                '@angular-eslint/directive-selector': ['error', { type: 'attribute', prefix: 'td', style: 'camelCase' }],
                '@angular-eslint/component-selector': ['error', { type: 'element', prefix: 'td', style: 'kebab-case' }],
                '@typescript-eslint/no-non-null-assertion': 'off',
                'max-len': 'off',
                '@typescript-eslint/explicit-function-return-type': 'off',
                '@typescript-eslint/no-explicit-any': 'off',
                '@typescript-eslint/no-use-before-define': ['error', 'nofunc'],
                '@typescript-eslint/no-unused-vars': 'off', // We already have noUnusedLocals
                '@typescript-eslint/explicit-module-boundary-types': 'off',
                '@typescript-eslint/no-unsafe-return': 'off',
                '@typescript-eslint/unbound-method': 'off',
                '@typescript-eslint/no-unsafe-assignment': 'off',
                '@typescript-eslint/no-unsafe-argument': 'off',
                '@typescript-eslint/no-unsafe-call': 'off',
                '@typescript-eslint/no-unsafe-member-access': 'off',
                '@typescript-eslint/restrict-template-expressions': 'off',
                '@typescript-eslint/no-floating-promises': 'off', // jasmine expect
                '@typescript-eslint/no-unnecessary-type-assertion': 'off', // jasmine
                '@typescript-eslint/no-misused-promises': 'off', // jasmine
                '@typescript-eslint/restrict-plus-operands': 'off',
                'jasmine/expect-matcher': 2,
                'jasmine/no-focused-tests': 2,
                'jasmine/no-disabled-tests': 1,
                'jasmine/no-suite-dupes': 1,
                'jasmine/no-spec-dupes': [1, 'branch'],
                'jasmine/no-pending-tests': 1,
                'jasmine/new-line-between-declarations': 1,
                'jasmine/prefer-jasmine-matcher': 1,
                quotes: [
                    'error',
                    'single',
                    {
                        allowTemplateLiterals: true,
                    },
                ],
            },
        },
        {
            files: ['*.component.html'],
            extends: ['plugin:@angular-eslint/recommended'],
            rules: {
                'max-len': 'off',
            },
        },
    ],
    env: {
        browser: true,
        jasmine: true,
    },
};
