module.exports = {
    rootTranslationsPath: 'i18n/',
    langs: ['en', 'cs', 'zh', 'pt', 'de', 'es', 'it', 'ru', 'ja', 'fr', 'tr', 'id', 'ko'],
    keysManager: {
        unflat: true,
        defaultValue: 'MISSING_TRANSLATION',
        sort: true,
        output: 'i18n',
    },
};