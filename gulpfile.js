const child_process = require('child_process');
const fs = require('fs-extra');
const readdir = require('recursive-readdir-sync');
const path = require('path');
const log = require('fancy-log');

const files = readdir('gulp');
let sourceChanged = false;
let basepath = __dirname;

for (const file of files) {
    if (path.extname(file) !== '.ts') {
        continue;
    }
    const srcFileChanged = fs.statSync(file).mtime;
    const destPath = path.relative(path.join(basepath, 'gulp'), file);
    const destFile = path.join(basepath, 'gulp-compiled', `${path.join(path.dirname(destPath), path.basename(destPath, '.ts'))}.js`);
    const destFileChanged = fs.existsSync(destFile) ? fs.statSync(destFile).mtime : new Date(2000, 1, 1);

    if (srcFileChanged > destFileChanged) {
        sourceChanged = true;
        break;
    }
}

if (sourceChanged) {
    log.info('Recompiling gulp tasks...');
    const ts = path.join(__dirname, 'node_modules/.bin/tsc');
    child_process.execSync(`${ts} --project gulp/tsconfig.json`, { stdio: 'inherit' });
}

require('./gulp-compiled/gulpfile.js');
